<?php
require_once 'DB.php';
require_once 'lib.php';

$db = new DB();

//harvest tenants

//add an id column in each table
@mysql_query("alter table test.tenants add tenantid int(11)");

$sql="select distinct(tenantcode) tenantcode, tenantname from test.tenants where tenantname !=''";
$res=mysql_query($sql);
while($row=mysql_fetch_object($res)){
	
	$row->tenantname = str_replace("'", "\\'", $row->tenantname);
	//extract first & last names
	$names=explode(" ", $row->tenantname);
	if(empty($names[1]))
		$names[1]="";
	
	if(empty($names[2]))
		$names[2]="";
	
	//insert into new table
	$query = "insert into em_tenants(code,firstname,middlename,lastname,nationalityid) values('$row->tenantcode','$names[0]','$names[1]','$names[2]',93)";
	if(mysql_query($query)){
		$id=mysql_insert_id();
		echo "Tenants: Added ".$row->tenantname."\n";
		//update test.tenants id with this id
		mysql_query("update test.tenants t set t.tenantid='$id' where tenantname='$row->tenantname'");
		
		//create a journal account
		$ql="insert into fn_generaljournalaccounts(refid,code,name,acctypeid,main) values('$id','$row->tenantcode','$row->tenantname A/C',32,6)";
		mysql_query($ql);
		
		$accountid=mysql_insert_id();
		$daccountid=5;
		$class="B";
		$remarks="Opening Balances";		
		$jvno=$row->tenantcode;
		$transactdate=date("Y-m-d");
		
	}
	else{
		echo "Tenants: Could not Add ".$row->tenantname."\n";
		logging($row->tenantcode." ".mysql_error());
	}
}

//harvest landlords

//add an id column in each table
@mysql_query("alter table test.landlordandplots add landlordid int(11)");

$sql="select distinct(landlordno) landlordno, landlord, landlordtel from test.landlordandplots where landlord !=''";
$res=mysql_query($sql);
while($row=mysql_fetch_object($res)){
	//extract first & last names
	$names=explode(" ", $row->landlord);
	if(empty($names[1]))
		$names[1]="";
	if(empty($names[2]))
		$names[2]="";

	//insert into new table
	$query = "insert into em_landlords(llcode,firstname,middlename,lastname,tel) values('$row->landlordno','$names[0]','$names[1]','$names[2]','$row->landlordtel')";
	if(mysql_query($query)){
		$id=mysql_insert_id();
		echo "Landlords: Added ".$row->landlord."\n";
		//update test.tenants id with this id
		mysql_query("update test.landlordandplots t set t.landlordid='$id' where landlord='$row->landlord'");
				
		//create journal accounts
		$ql="insert into fn_generaljournalaccounts(refid,code,name,acctypeid,main) values('$id','$row->landlordno','$row->landlord A/C',33,7)";
		mysql_query($ql);
		
	}
	else{
		echo "Landlords: Could not Add ".$row->landlord."\n";
		logging($row->landlord." ".mysql_error());
	}
}

//harvest plots

//add an id column in each table
@mysql_query("alter table test.landlordandplots add plotid int(11)");
mysql_query("set foreign_key_checks=0");

$sql="select distinct(lp.plotno) plotno, lp.plotname,lp.landlordid,lp.lrno,lp.estate,lp.location, lp.letarea, lp.commission from test.landlordandplots lp where lp.plotname !='' ";
$res=mysql_query($sql);
while($row=mysql_fetch_object($res)){
	
		//insert into new table
	$query = "insert into em_plots(code,name,landlordid,lrno,estate,road,location,commission) values('$row->plotno','$row->plotname','$row->landlordid','$row->lrno','$row->estate','$row->road','$row->location','$row->commission')";
	if(mysql_query($query)){
		$id=mysql_insert_id();
		echo "Plots: Added ".$row->plotname."\n";
		//update test.tenants id with this id
		mysql_query("update test.landlordandplots t set t.plotid='$id' where plotno='$row->plotno'");
	}
	else{
		echo "Plots: Could not Add ".$row->plotname."\n";
		logging($row->plotname." ".mysql_error());
	}
}

//harvest houses

//add an id column in each table
@mysql_query("alter table test.tenants add houseid int(11)");
mysql_query("set foreign_key_checks=0");

$sql="select distinct(t.housecode) housecode, t.houseno hseno,t.bedrooms,t.rent, l.plotid from test.tenants t, test.landlordandplots l where t.houseno !='' and t.plotno=l.plotno";
$res=mysql_query($sql);
while($row=mysql_fetch_object($res)){

	//insert into new table
	$query = "insert into em_houses(hsecode,hseno,bedrms,amount,plotid) values('$row->housecode','$row->hseno','$row->bedrooms','$row->rent','$row->plotid')";
	if(mysql_query($query)){
		$id=mysql_insert_id();
		echo "Houses: Added ".$row->hseno."\n";
		//update test.tenants id with this id
		mysql_query("update test.tenants t set t.houseid='$id' where houseno='$row->hseno'");
	}
	else{
		echo "Houses: Could not Add ".$row->hseno."\n";
		logging($row->hseno." ".mysql_error());
	}
}

//harvest tenant houses

mysql_query("update test.tenants set houseid=''");
mysql_query("update test.tenants t, em_houses h set t.houseid=h.id where t.housecode=h.hsecode");

$sql="select t.tenantid,t.dateoccupied, t.tenantname,t.rentduedate,t.leasestarts,t.leasemonths, t.houseid from test.tenants t where t.tenantid !='' and t.tenantid!=0 and t.houseid!='' and t.houseid!=0 and t.tenantname!='' order by t.houseid";
$res=mysql_query($sql);
while($row=mysql_fetch_object($res)){
  echo $row->houseid." = ";
	//insert into new table
	$query = "insert into em_housetenants(houseid,tenantid,occupiedon,rentduedate,leasestarts,renewevery) values('$row->houseid','$row->tenantid','$row->dateoccupied','$row->rentduedate','$row->leasestarts','$row->leasemonths')";
	if(mysql_query($query)){
		//$id=mysql_insert_id();
		echo "Tenant Houses: Added ".$row->tenantname."\n";
		//update test.tenants id with this id
		//mysql_query("update test.tenants t set t.houseid='$id'");
	}
	else{
		echo "Tenant Houses: Could not Add ".$row->tenantname."\n";
		logging($row->tenantname." ".mysql_error());
	}
}

//make journal entries for op balances - harvest tenant balances
@mysql_query("alter table test.tenantbalances add tenantid int(11), add houseid int(11), add tenantcode varchar(32);");
mysql_query("update test.tenantbalances b, test.tenants t set b.tenantid=t.tenantid, b.houseid=t.houseid, b.tenantcode=t.tenantcode where b.tenant=t.tenantname");
mysql_query("set foreign_key_checks=0");

$obj->month=10;
$obj->year=2013;
$obj->paidby='';
$obj->documentno='';
$obj->chequeno='';
$obj->id=0;
$obj->paidon=date("Y-m-d");
$q="select * from test.tenantbalances where total!=0";
$s=mysql_query($q);
while($r=mysql_fetch_object($s)){
		echo "Balances for ".$r->tenant."\n";
	if($r->elecdeposit>0){
		
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=5;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->elecdeposit;
		$obj->remarks="Opening Balance";
		
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->rdeposit>0){
		
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=2;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->rdeposit;
		$obj->remarks="Opening Balance";
		
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->water>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=10;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->water;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->electricity>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=4;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->electricity;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->latepayment>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=6;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->latepayment;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->lettingfee>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=14;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->lettingfee;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->waterdeposit>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=15;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->waterdeposit;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->agencyfees>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=8;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->agencyfees;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->rent>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=1;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->rent;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->servicecharge>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=8;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->servicecharge;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->garbage>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=7;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->garbage;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->security>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=11;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->security;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->auctionfees>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=12;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->auctionfees;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	
	if($r->bankcharges>0){
	
		$obj->tenantid=$r->tenantid;
		$obj->paymenttermid=13;
		$obj->houseid=$r->houseid;
		$obj->paymentmodeid=NULL;
		$obj->bankid=NULL;
		$obj->amount=$r->bankcharges;
		$obj->remarks="Opening Balance";
	
		$sql="insert into em_tenantpayments(id,tenantid,houseid,documentno,paymenttermid,paymentmodeid,bankid,chequeno,amount,paidon,month,year,paidby,remarks)
		values('$obj->id','$obj->tenantid','$obj->houseid','$obj->documentno','$obj->paymenttermid','$obj->paymentmodeid','$obj->bankid','$obj->chequeno','$obj->amount','$obj->paidon','$obj->month','$obj->year','$obj->paidby','$obj->remarks')";
		mysql_query($sql);
	}
	//$memo=" House no ".$r->houseno;
	
	$acc=mysql_fetch_object(mysql_query("select * from fn_generaljournalaccounts where refid='$r->tenantid' and acctypeid='32'"));
	$accountid=$acc->id;
	
	$daccountid=5;
	
	if($r->total>0){
		$l="insert into fn_generaljournals(accountid,daccountid,remarks,memo,transactdate,debit,credit,jvno,class)
		values('$accountid','$daccountid','Opening Balance','Opening Balance','Now()','$r->total','0','$r->tenantcode','B')";
		if(mysql_query($l)){
			$did=mysql_insert_id();
			$l="insert into fn_generaljournals(accountid,daccountid,remarks,memo,transactdate,debit,credit,jvno,class,did)
			values('$daccountid','$accountid','Opening Balance','Opening Balance','Now()','0','$r->total','$r->tenantcode','B','$did')";
			if(mysql_query($l)){
				$did2=mysql_insert_id();
				mysql_query("update fn_generaljournals set did='$did2' where id='$did'");
			}
		}
	}
}
?>