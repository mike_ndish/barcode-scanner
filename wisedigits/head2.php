<?php
require_once("../../../DB.php");
require_once("../../pm/notificationrecipients/Notificationrecipients_class.php");

$db = new DB();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- 	vital -->
<script src="../../../js/jquery-1.9.1.js"></script>
<script src="../../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="../../../js/jquery-1.7.1.js" type="text/javascript"></script>
<script src="../../../js/ui/jquery-ui.js"></script>
<script type="text/javascript" src="../../../js/jquery.js"></script>
<script type="text/javascript" src="../../../js/jquery.dataTables.js"></script> 
<script src="../../../js/lib/datatables/DT_bootstrap.js"></script>
<!--<script src="../../../js/tablesorter/js/jquery.tablesorter.min.js"></script>-->
<script type="text/javascript" src="../../../js/main.js"></script>


<!-- vitalEnd -->
<script>
$('#action').click(function() {
    history.go(0);
});
</script>


<script src="../../../js/ui/jquery-ui.js"></script>
<script src="../../../js/functions.js"></script>
<script type="text/javascript" src="../../../js/bpops.js"></script>
<!-- <script type="text/javascript" language="javascript" src="../../../js/jquery-ui-timepicker-addon.js"></script> -->
<script type="text/javascript" src="../../../js/bootstrap.min.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="../../../js/jquery-ui-timepicker-addon.css" /> -->
<script src="../../../js/fullcalendar/fullcalendar.min.js"></script>
<!-- <script src="../../../js/sparkline/jquery.sparkline.min.js"></script> -->
<script type="text/javascript" src="../../../js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../../../js/bootstrap-datetimepicker.min.js"></script>

	

<script language="javascript" src="../../../j-modal/js/jqmodal.js" type="text/javascript" ></script>
<script language="javascript" src="../../../j-modal/js/jqDnR.js" type="text/javascript"></script>
<script language="javascript" src="../../../js/tabIndex.js" type="text/javascript"></script> 
<script language="javascript" type="text/javascript" src="../../../js/jquery.ifixpng.js"></script>
<script language="javascript" type="text/javascript" src="../../../lkimage/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript" src="../../../js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="../../../js/cal.js"></script>
<script type="text/javascript" src="../../../js/shortcut.js"></script>
<script type="text/javascript" src="../../../js/womon.js"></script>
<link rel="stylesheet" type="text/css" href="../../../dmodal/style.css" />
<link rel="stylesheet" type="text/css" href="../../../dmodal/subModal.css" />
<script type="text/javascript" src="../../../dmodal/common.js"></script>
<script type="text/javascript" src="../../../dmodal/subModal.js"></script>  
	
	<script src="../../../js/jquery.bgiframe-2.1.2.js"></script>

	<script src="../../../js/ui/jquery.ui.core.js"></script>
	<script src="../../../js/ui/jquery.ui.widget.js"></script>
	<script src="../../../js/ui/jquery.ui.mouse.js"></script>
	<script src="../../../js/ui/jquery.ui.button.js"></script>
	<script src="../../../js/ui/jquery.ui.draggable.js"></script>
	<script src="../../../js/ui/jquery.ui.position.js"></script>
	<script src="../../../js/ui/jquery.ui.resizable.js"></script>
	<script src="../../../js/ui/jquery.ui.dialog.js"></script>
	<script src="../../../js/ui/jquery.effects.core.js"></script>
	<script src="../../../js/ui/jquery.ui.tabs.js"></script>
	<script type="text/javascript" language="javascript" src="../../../media/ZeroClipboard/ZeroClipboard.js"></script>
	<script type="text/javascript" language="javascript" src="../../../media/js/TableTools.js"></script>
<!-- validation -->
<script src="../../../js/tobechanged.js"></script>
<link rel="stylesheet" href="../../../js/validationengine/css/validationEngine.jquery.css">

<script src="../../../js/validationengine/js/jquery.validationEngine.js"></script>
<script src="../../../js/validationengine/js/languages/jquery.validationEngine-en.js"></script>
<script src="../../../js/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
<script src="../../../js/jquery-validation-1.11.1/localization/messages_ja.js"></script>

<script>
   $(function() { formValidation(); });
</script>
<!-- validationEnd -->
	
	
<!-- the css -->

<link rel="stylesheet" href="../../../css/bootstrap/css/bootstrap.css">
<link href="../../../css/fa/css/font-awesome.css" rel="stylesheet">
<link href="../../../css/main.css" rel="stylesheet">
<link href="../../../fs-css/elements.css" media="all" type="text/css" rel="stylesheet" /> 
<link href="../../../fs-css/html-elements.css" media="all" type="text/css" rel="stylesheet" /> 
<link href="../../../css/bootstrap.min.css" rel="stylesheet">  

<!-- thecssEnd -->

	

	<script>
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-form" ).dialog({
			autoOpen: false,
			modal: true,

		});

		$( "#nyef" )
			.button()
			.click(function() {
				$( "#dialog-form" ).dialog( "open" );
			});
	});
	</script>

<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>

<script>
$(document).ready(function(){
$('*[data-ajaxload]').bind('hover',function(){
  var e=$(this);
  e.unbind('hover');
  $.get(e.data('ajaxload'),function(d){
      e.popover({content: d}).popover('show');
  });
});
});
</script>
	<script>
		jQuery(document).ready(function () {
			$('input.date_input').datepicker({
					changeMonth: true,
					changeYear: true,
					yearRange:  '1930:2100',
					dateFormat: 'yy-mm-dd'
				});
				$('input.dob_input').datepicker({
					changeMonth: true,
					changeYear: true,
					yearRange:  '1900:1994',
					dateFormat: 'yy-mm-dd'
				});
			});
	</script>

<script language="javascript" type="text/javascript">
function checkDate(field){
	var allowBlank = true; var minYear = 1902; var maxYear = 2099; 
	var errorMsg = ""; 
// regular expression to match required date format 
	//re = /^(\d{4})\/(\d{1,2})\/(\d{1,2})$/; 
	re = /^(\d{4})-(\d{1,2})-(\d{1,2})/;
	if(field.value != ''){
		if(regs = field.value.match(re)) { 
			if(regs[3] < 1 || regs[3] > 31) { 
				errorMsg = "Invalid value for day: " + regs[3];
			} 
			else if(regs[2] < 1 || regs[2] > 12) { 
				errorMsg = "Invalid value for month: " + regs[2]; 
			} else if(regs[1] < minYear || regs[1] > maxYear) { 
				errorMsg = "Invalid value for year: " + regs[1] + " - must be between " + minYear + " and " + maxYear; 
			} 
		 } 
		 else { 
		 	errorMsg = "Invalid date format: " + field.value; 
		} 
	} 
	else if(!allowBlank) { 
		errorMsg = "Empty date not allowed!"; 
	} 
	if(errorMsg != "") {
		 alert(errorMsg); field.focus(); 
		 return false; 
		} 
	return true; 
}
</script>
<script>



</script>
<script type="text/javascript">
<!--
function showmenu(id){
var s = document.getElementById(id).style;
s.visibility='visible'; 
}
//-->
function timeOut(id){
setTimeout('hideShow("'+id+'")',3000)
}
function hideShow(id){
var s = document.getElementById(id).style;
s.visibility=s.visibility=='hidden'?'visible':'hidden'; 
} 
</script>
<script language="javascript" type="text/javascript">
var newwindow;
function poptastic(url,h,w)
{
	var ht=h;
	var wd=w;
	newwindow=window.open(url,'name','height='+ht+',width='+wd+',scrollbars=yes,left=250,top=80');
	if (window.focus) {newwindow.focus()}
}

function placeCursorOnPageLoad()
{
	if(document.stores)
		showUser();
	document.cashsales.itemname.focus();
		
}
</script>
<!-- TemplateBegin<img src="../edit.png" alt="edit" title="edit" />able name="doctitle" -->
<title>WiseDigits</title>
<!-- TemplateEnd<img src="../edit.png" alt="edit" title="edit" />able -->
<script language="javascript" type="text/javascript">
$(document).ready(function(){
var t = $('#ex4 div.jqmdMSG');
$('#ex4').jqm({
		trigger: 'a.addpop',
		ajax: '@href', /* Extract ajax URL from the 'href' attribute of triggering element */
		target: t,
		modal: true, /* FORCE FOCUS */
		onHide: function(h) { 
			t.html('Please Wait...');  // Clear Content HTML on Hide.
			h.o.remove(); // remove overlay
			h.w.fadeOut(888); // hide window
		},
		overlay: 0}).jqmHide();
$('a.addpop').bind('click',function(){
		var x = $('a.addpop').attr('rel');
		var y = []; 
		y = x.split(',');
		var jWidth = y[0]+'px';
		var jHeight = y[1]+'px';
		$('.jqmdBC').css({'width':jWidth,'height':jHeight});
		return false;
});
$('#ex4').jqDrag('.jqDrag').jqResize('.jqResize');	

	   if($.browser.msie)
		$('input')
		  .focus(function(){$(this).addClass('iefocus');})
		  .blur(function(){$(this).removeClass('iefocus');})
 });
</script>	
<script language="javascript" type="text/javascript">
$(document).ready(function(){
	$('#formbox').hide();
	$("#uselog").click(function(){
		$(this).next("#formbox").animate({opacity:"show", top: "50"},"slow");
	});
	$("a#slick-hide").click(function(){
		$('#formbox').hide('fast');
		return false;
	});

});
</script>
<!-- datatables -->
<script type="text/javascript">
/* Table initialisation */
$(document).ready(function() {
    $('#example').dataTable( {
        "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
    } );
} );
$.extend( true, $.fn.dataTable.defaults, {
    "sDom": "<'row'<'col-6'f><'col-6'l>r>t<'row'<'col-6'i><'col-6'p>>",
    "sPaginationType": "bootstrap",
    "oLanguage": {
        "sLengthMenu": "Show _MENU_ Rows",
                "sSearch": ""
    }
} );

</script>



<!-- TemplateBegin<img src="../edit.png" alt="edit" title="edit" />able name="head" -->
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	TableToolsInit.sSwfPath = "../../../media/swf/ZeroClipboard.swf";
	$('#example').dataTable( {
		"sScrollY": 500,
		"bJQueryUI": true,
		"iDisplayLength": 200,
		"sPaginationType": "full_numbers"
	} );
} );
</script> 
<!-- TemplateEnd<img src="../edit.png" alt="edit" title="edit" />able -->
<style media="all" type="text/css">
#navamenu
{
visibility:hidden;
}
</style>

</head>
<?php
if (get_magic_quotes_gpc()){
 $_GET = array_map('stripslashes', $_GET);
 $_POST = array_map('stripslashes', $_POST);
 $_COOKIE = array_map('stripslashes', $_COOKIE);
}
?>
<body>
        <!-- #helpModal -->        
        <div id="helpModal" class="modal fade">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Modal title</h4>
	      </div>
	      <div class="modal-body">

	      </div>
	      <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->.popover-examples a
	</div><!-- /.modal -->        
        <!-- /#helpModal -->
<div class='navbar'>

      <div class='navbar-inner nav-collapse' style="height: auto;">

    <div class="topnav">

        <div class="btn-toolbar">
            <div class="btn-group">
                <a data-placement="bottom" data-original-title="Back" data-toggle="tooltip" class="btn btn-success btn-sm" href="./">
                    <i class="fa fa-resize-horizontal"></i>
                </a>
            </div>
            <?php
    $notificationrecipients = new Notificationrecipients();
    $fields="count(*) msg";
    $where=" where status='unread' and employeeid in(select employeeid from auth_users where id='".$_SESSION['userid']."')";
    $join="";
    $having="";
    $groupby="";
    $orderby="";
    $notificationrecipients->retrieve($fields,$join,$where,$having,$groupby,$orderby);
    $notificationrecipients=$notificationrecipients->fetchObject;
    $where="";
    ?>
            <div class="btn-group">
                <a data-placement="bottom" href="../../pm/notificationrecipients/notificationrecipients.php" data-original-title="E-mail" data-toggle="popover" class="btn btn-default btn-sm">
                    <i class="fa fa-envelope"></i>
                    <span class="label label-warning"><?php echo $notificationrecipients->msg; ?></span>
                </a>
                
                 <a data-placement="bottom" id="popover" rel="" data-original-title="Messages" 
                 data-ajaxload="../../pm/notificationrecipients/notificationrecipients.php" href="#" data-toggle="popover" class="btn btn-default btn-sm">
                    <i class="fa fa-messages"></i>
                    <span class="label label-danger">10</span>
                </a>
            </div>
            <div class="btn-group">
                <a data-placement="bottom" data-original-title="User Account" onclick="showPopWin('../../auth/users/addusers_proc.php?id=<?php echo $_SESSION['userid'];?>',600,430);"data-toggle="#" data-target="#" class="btn btn-default btn-sm">
                    <i class="fa fa-cog"></i>
                </a>
                <a data-toggle="modal" data-original-title="Help" data-placement="bottom" class="btn btn-default btn-sm" href="#helpModal">
                    <i class="fa fa-question"></i>
                </a>
            </div>
            <div class="btn-group">
                <a href="../../auth/users/logout.php" data-toggle="tooltip" data-original-title="Logout" data-placement="bottom" class="btn btn-metis-1 btn-sm">
		  <i class="fa fa-power-off"></i>
                </a>
            </div>
        </div>


    </div>
<!-- /.topnav -->
        <!-- .nav -->
        <ul class="nav navbar-nav">
            <li><a href="/estate">WiseDigits ERP</a></li>
<!--            <li><a href="table.html">Tables</a></li>
            <li><a href="file.html">File Manager</a></li>
            <li class='dropdown '>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Form Elements <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="form-general.html">General</a></li>
                    <li><a href="form-validation.html">Validation</a></li>
                    <li><a href="form-wysiwyg.html">WYSIWYG</a></li>
                    <li><a href="form-wizard.html">Wizard &amp; File Upload</a></li>
                </ul>
            </li>-->
        </ul>
        <!-- /.nav -->
 
    </div>
      </div>
      

   <div class="clear"></div> 
 <div class="span4">

 <div id="left">
   <div class="media user-media">
    <a class="user-link" href="">
        <img class="media-object img-thumbnail user-img" alt="User Picture" src="../../../img/user.gif">
        <span class="label label-danger user-label">16</span>
    </a>

    <div class="media-body">
    <?php
    $query="select auth_users.id, concat(concat(hrm_employees.firstname,' ',hrm_employees.middlename),' ',hrm_employees.lastname) as employeeid, auth_users.username, auth_users.password, auth_levels.name as levelid, auth_users.status, auth_users.lastlogin, auth_users.createdby, auth_users.createdon, auth_users.lasteditedby, auth_users.lasteditedon from auth_users left join hrm_employees on auth_users.employeeid=hrm_employees.id  left join auth_levels on auth_users.levelid=auth_levels.id where auth_users.id='".$_SESSION['userid']."'";
    $row=mysql_fetch_object(mysql_query($query));
    ?>
        <h5 class="media-heading"><?php echo $row->username; ?></h5>
        <ul class="list-unstyled user-info">
            <li><a href=""><?php echo $row->levelid; ?></a></li>
            <li>Last Access : <br>
                <small><i class="fa fa-calendar"><?php echo $row->lastlogin; ?></i></small>
            </li>
        </ul>
    </div>
</div>
</div>
	<!--Sidebar content-->
  
	
     <ul class="nav nav-tabs nav-stacked">
	 	<li data-toggle="collapse" class="panel">
  <a href="#" data-toggle="collapse" class="accordion-toggle" data-target="#userMenu"> My Account 
	<span class="pull-right">
	<i class="fa fa-angle-left"></i>
	</span>
  </a>
  <ul style="list-style: none;" class="" id="userMenu">  
    <li><a href="../../auth/users/addusers_proc.php?id=<?php echo $_SESSION['userid'];?>"><i class="icon-cog"></i>Settings</a></li>
    <?php
    $notificationrecipients = new Notificationrecipients();
    $fields="count(*) msg";
    $where=" where status='unread' and employeeid in(select employeeid from auth_users where id='".$_SESSION['userid']."')";
    $join="";
    $having="";
    $groupby="";
    $orderby="";
    $notificationrecipients->retrieve($fields,$join,$where,$having,$groupby,$orderby);
    $notificationrecipients=$notificationrecipients->fetchObject;
    $where="";
    ?>
    <li><a href="../../pm/notificationrecipients/notificationrecipients.php"><i class="icon-envelope"></i>Messages <span class="badge badge-info"><?php echo $notificationrecipients->msg; ?></span></a></li>
    <li><a href="../../auth/users/logout.php"><i class="icon-off"></i>Leave Application</a></li>
    <li><a href="#"><i class="icon-off"></i>Clock In</a></li>
    <li><a href="#"><i class="icon-off"></i>Off Days</a></li>
    <li><a href="../../auth/users/logout.php"><i class="icon-off"></i>Logout</a></li>
  </ul>
</li>
<!--<li><a href="">MODULES</a></li>
		<?php
		$query="select * from sys_modules where description!='' and id in(select distinct auth_roles.moduleid from auth_roles left join auth_rules on auth_roles.id=auth_rules.roleid where auth_rules.levelid=(select levelid from auth_users where id='".$_SESSION['userid']."')) ";
		$res=mysql_query($query);
		while($row=mysql_fetch_object($res))//{
		?>
		<li><a href="../../<?php echo $row->url; ?>"> <?php echo $row->description; ?></a></li>-->
		<?php
		//}
		?>
	  </ul> 
     
</div>
 
 
 
 </div><!-- sidebarEnd -->
 <div class="clear"></div>
  <div class="container-fluid">
   <div class="row-fluid">
  <div class="span9">
 
 