
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript" type="text/javascript" src="../../../js/jquery-1.3.2.min.js"></script>
<script src="../../../js/jquery-1.9.1.js"></script>
<script src="../../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="../../../js/jquery-1.7.1.js" type="text/javascript"></script>
<script src="../../../js/ui/jquery-ui.js"></script>
<script src="../../../js/functions.js"></script>

<script type="text/javascript" src="../../../js/bootstrap.min.js"></script>


<script type="text/javascript" language="javascript" src="../../../js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" type="text/css" href="../../../js/jquery-ui-timepicker-addon.css" />

<script type="text/javascript" src="../../../js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="../../../js/bootstrap-datetimepicker.min.js"></script>


<script language="javascript" src="../../../j-modal/js/jqmodal.js" type="text/javascript" ></script>
<script language="javascript" src="../../../j-modal/js/jqDnR.js" type="text/javascript"></script>
<script language="javascript" src="../../../js/tabIndex.js" type="text/javascript"></script> 
<script language="javascript" type="text/javascript" src="../../../js/jquery.ifixpng.js"></script>
<script language="javascript" type="text/javascript" src="../../../lkimage/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript" src="../../../js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="../../../js/cal.js"></script>
<script type="text/javascript" src="../../../js/shortcut.js"></script>
<script type="text/javascript" src="../../../js/womon.js"></script>
<link rel="stylesheet" type="text/css" href="../../../dmodal/style.css" />
<link rel="stylesheet" type="text/css" href="../../../dmodal/subModal.css" />
<script type="text/javascript" src="../../../dmodal/common.js"></script>
<script type="text/javascript" src="../../../dmodal/subModal.js"></script>  
	
	<script src="../../../js/jquery.bgiframe-2.1.2.js"></script>

	<script src="../../../js/ui/jquery.ui.core.js"></script>
	<script src="../../../js/ui/jquery.ui.widget.js"></script>
	<script src="../../../js/ui/jquery.ui.mouse.js"></script>
	<script src="../../../js/ui/jquery.ui.button.js"></script>
	<script src="../../../js/ui/jquery.ui.draggable.js"></script>
	<script src="../../../js/ui/jquery.ui.position.js"></script>
	<script src="../../../js/ui/jquery.ui.resizable.js"></script>
	<script src="../../../js/ui/jquery.ui.dialog.js"></script>
	<script src="../../../js/ui/jquery.effects.core.js"></script>
	<script src="../../../js/ui/jquery.ui.tabs.js"></script>
	<script>
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-form" ).dialog({
			autoOpen: false,
			modal: true,

		});

		$( "#nyef" )
			.button()
			.click(function() {
				$( "#dialog-form" ).dialog( "open" );
			});
	});
	</script>

<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>

		<script>
		jQuery(document).ready(function () {
			$('input.date_input').datepicker({
					changeMonth: true,
					changeYear: true,
					yearRange:  '1930:2100',
					dateFormat: 'yy-mm-dd'
				});
			});
	</script>

	<!-- validation -->
<script src="../../../js/tobechanged.js"></script>
<link rel="stylesheet" href="../../../js/validationengine/css/validationEngine.jquery.css">

<script src="../../../js/validationengine/js/jquery.validationEngine.js"></script>
<script src="../../../js/validationengine/js/languages/jquery.validationEngine-en.js"></script>
<script src="../../../js/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
<script src="../../../js/jquery-validation-1.11.1/localization/messages_ja.js"></script>

<script>
   $(function() { formValidation(); });
</script>
<!-- validationEnd -->
<script language="javascript" type="text/javascript">
function checkDate(field){
	var allowBlank = true; var minYear = 1902; var maxYear = 2099; 
	var errorMsg = ""; 
// regular expression to match required date format 
	//re = /^(\d{4})\/(\d{1,2})\/(\d{1,2})$/; 
	re = /^(\d{4})-(\d{1,2})-(\d{1,2})/;
	if(field.value != ''){
		if(regs = field.value.match(re)) { 
			if(regs[3] < 1 || regs[3] > 31) { 
				errorMsg = "Invalid value for day: " + regs[3];
			} 
			else if(regs[2] < 1 || regs[2] > 12) { 
				errorMsg = "Invalid value for month: " + regs[2]; 
			} else if(regs[1] < minYear || regs[1] > maxYear) { 
				errorMsg = "Invalid value for year: " + regs[1] + " - must be between " + minYear + " and " + maxYear; 
			} 
		 } 
		 else { 
		 	errorMsg = "Invalid date format: " + field.value; 
		} 
	} 
	else if(!allowBlank) { 
		errorMsg = "Empty date not allowed!"; 
	} 
	if(errorMsg != "") {
		 alert(errorMsg); field.focus(); 
		 return false; 
		} 
	return true; 
}
</script>
<script type="text/javascript">
<!--
function showmenu(id){
var s = document.getElementById(id).style;
s.visibility='visible'; 
}
//-->
function timeOut(id){
setTimeout('hideShow("'+id+'")',3000)
}
function hideShow(id){
var s = document.getElementById(id).style;
s.visibility=s.visibility=='hidden'?'visible':'hidden'; 
} 
</script>
<script language="javascript" type="text/javascript">
var newwindow;
function poptastic(url,h,w)
{
	var ht=h;
	var wd=w;
	newwindow=window.open(url,'name','height='+ht+',width='+wd+',scrollbars=yes,left=250,top=80');
	if (window.focus) {newwindow.focus()}
}

function placeCursorOnPageLoad()
{
	if(document.stores)
		showUser();
	document.cashsales.itemname.focus();
		
}
</script>
<!-- TemplateBegin<img src="../edit.png" alt="edit" title="edit" />able name="doctitle" -->
<title>WiseDigits</title>
<!-- TemplateEnd<img src="../edit.png" alt="edit" title="edit" />able -->
<script language="javascript" type="text/javascript">
$(document).ready(function(){
var t = $('#ex4 div.jqmdMSG');
$('#ex4').jqm({
		trigger: 'a.addpop',
		ajax: '@href', /* Extract ajax URL from the 'href' attribute of triggering element */
		target: t,
		modal: true, /* FORCE FOCUS */
		onHide: function(h) { 
			t.html('Please Wait...');  // Clear Content HTML on Hide.
			h.o.remove(); // remove overlay
			h.w.fadeOut(888); // hide window
		},
		overlay: 0}).jqmHide();
$('a.addpop').bind('click',function(){
		var x = $('a.addpop').attr('rel');
		var y = []; 
		y = x.split(',');
		var jWidth = y[0]+'px';
		var jHeight = y[1]+'px';
		$('.jqmdBC').css({'width':jWidth,'height':jHeight});
		return false;
});
$('#ex4').jqDrag('.jqDrag').jqResize('.jqResize');	

	   if($.browser.msie)
		$('input')
		  .focus(function(){$(this).addClass('iefocus');})
		  .blur(function(){$(this).removeClass('iefocus');})
 });
</script>	
<script language="javascript" type="text/javascript">
$(document).ready(function(){
	$('#formbox').hide();
	$("#uselog").click(function(){
		$(this).next("#formbox").animate({opacity:"show", top: "50"},"slow");
	});
	$("a#slick-hide").click(function(){
		$('#formbox').hide('fast');
		return false;
	});

});
</script>
<!--<script type="text/javascript">
    $(function () {
	$("#popover").popover({
	    html : 'true', 
	    placement : 'bottom'
	});
    });
</script>-->

<link href="../../../css/bootstrap.css" rel="stylesheet">
<link href="../../../css/bootstrap.min.css" rel="stylesheet">
	<link href="../../../css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="../../../css/datepicker.css" rel="stylesheet">
	<link href="../../../css/fa/css/font-awesome.css" rel="stylesheet">
<!-- 	<link rel="stylesheet" href="../../../css/fa/datatables/demo_page.css">	 -->
	<link href="../../../lkimage/jquery.autocomplete.css" media="all" type="text/css" rel="stylesheet" />
<!-- 	<link rel="stylesheet" href="../../../css/fa/datatables/css/DT_bootstrap.css"> -->
	<link rel="stylesheet" href="../../../css/lib/datepicker/css/datepicker.css">
	<link href="../../../css/main.css" rel="stylesheet">
	<link href="../../../fs-css/elements.css" media="all" type="text/css" rel="stylesheet" />
	<link href="../../../fs-css/html-elements.css" media="all" type="text/css" rel="stylesheet" />
	<style type="text/css" title="currentStyle">
@import "../../../css/demo_page.css";
@import "../../../css/demo_table_jui.css";
@import "../../../css/demo_table.css";
@import "../../../css/jquery-ui-1.8.4.custom.css";
@import "../../../media/css/TableTools.css";
</style>
<script type="text/javascript" language="javascript" src="../../../js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="../../../media/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript" language="javascript" src="../../../media/js/TableTools.js"></script>


<!-- TemplateBegin<img src="../edit.png" alt="edit" title="edit" />able name="head" -->
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	TableToolsInit.sSwfPath = "../../../media/swf/ZeroClipboard.swf";
	$('#example').dataTable( {
		"sScrollY": 500,
		"iDisplayLength":20,
		"bJQueryUI": true,
		"sPaginationType": "full_numbers"
	} );
} );
</script> 
<!-- TemplateEnd<img src="../edit.png" alt="edit" title="edit" />able -->
<style media="all" type="text/css">
body{background-image:none !important;}
</style>

</head>