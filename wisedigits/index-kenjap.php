<?
session_start();
include "lib.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WiseDigits</title>
<link href="fs-css/layout.css" media="all" type="text/css" rel="stylesheet" />
<link href="fs-css/html-elements.css" media="all" type="text/css" rel="stylesheet" />
<link href="fs-css/elements.css" media="all" type="text/css" rel="stylesheet" />
<style media="all" type="text/css">
div.page-title{
	background:none;
	color:#1A1A1A;
	font-size:18px; font-weight:normal;
	padding:3px 10px 1px;
}
div#content{ width:800px;}
div#content-flex{ border:none; background:none; height:350px;}
</style>

<style >
u{
color:#0000FF;
font-style:bold;
}
</style>
<script type="text/javascript" src="js/shortcut.js"></script>
<script type="text/javascript">
function init() {
	shortcut.add("s", function() {
		location.replace("modules/sales");
	});
	shortcut.add("p", function() {
		location.replace("modules/purchases");
	});
	shortcut.add("r", function() {
		location.replace("modules/reports");
	});
	shortcut.add("i", function() {
		location.replace("modules/inventory");
	});
	shortcut.add("g", function() {
		location.replace("modules/generaljournal");
	});
	shortcut.add("a", function() {
		location.replace("modules/administration");
	});
	shortcut.add("c", function() {
		location.replace("modules/reconciliation");
	});
	shortcut.add("e", function() {
		location.replace("modules/equitystatus");
	});
	shortcut.add("n", function() {
		location.replace("modules/expenses & incomes");
	});
	shortcut.add("t", function() {
		location.replace("modules/systemtools");
	});
	shortcut.add("y", function() {
		location.replace("modules/payroll");
	});
}
window.onload=init;

</script>
</head>
<body>
<div id="console"><div id="header">
<div id="header-inner">
<div id="logo-title">
<div id="logo"></div>
</div>
<div id="company"><div class="company-title"><img src="images/wise_roads.png"></div></div><div id="header-right">
</div>
<div id="header-right">
<div id="header-blocks">
<div id="userlog"></div>
</div>
</div>
<div class="clearb"></div>
</div>
</div>
<div id="console-inner">

<div id="main">
<div id="main-inner">

<div id="content">
<div id="content-inner">
<div id="content-header">
<div class="page-title">
Module Console</div>
<div class="clearb"></div>
</div>
 <div id="content-flex"> 
<ul class="modules">
<li><a href="modules/pos/items/">Inventory</a></li>
<li><a href="modules/pos/sales/">Sales</a></li>
<li><a href="modules/motor/locations/">System Tools</a></li>
<li><a href="modules/fn/generaljournals/">Finance</a></li>
<li><a href="reports/"><u>R</u>eports</a></li>
<li><a href="modules/auth/users/"><u>A</u>dministration    </a></li>
<li><a href="modules/help/index.php">Help</a></li>
</ul>
  <div id="cur-user">
  <img align="left" name="" src="fsimage/default-user.jpg"   />
    <div class="info">
<span class="cur">Currently Logged in as:</span>
<span class="name"><?php echo $_SESSION['username']; ?> 
<?
			if($_SESSION['level']=="1")
            	$level="Administrator";
            elseif($_SESSION['level']=="2")
            	$level="Level 2";
            elseif($_SESSION['level']=="3")
            	$level="Level 3";
            if($_SESSION['level']=="4")
            	$level="Level 4";
        if(!empty($_SESSION['userid']))
		{
        	?>
            &nbsp;[<?php echo $level; ?>]&nbsp;</span>
           	 <div class="links">
             <a href="modules/administration/logout.php">Log Out</a></div>
             <?            
        }
        ?></span></div>
  </div>
  <div class="clearb"></div>
  </div><form name="ind">
  </form>
<div class="clearb"></div>
</div>
</div>
</div>
</div>
<div id="footer">
<div id="footer-inner">
<div id="footer-message">
<p align="center">Copyright &copy; 2013 WiseDigits. All Rights Reserved.</p>
</div>
</div>
<div class="clearb"></div>
</div>
<div class="clearb"></div>
</div>
</div>
</body>
</html>
