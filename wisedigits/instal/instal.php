<?php
require_once("../lib.php");

$obj=(object)$_POST;
if($obj->action=="Save Configuration")
{
	if(empty($obj->hostname))
	{
		$error="must provide host name";
	}
	elseif(empty($obj->dbname))
	{
		$error="must provide database name";
	}
	elseif(empty($obj->username))
	{
		$error="must provide username";
	}
	elseif(empty($obj->password))
	{
		$error="must provide password";
	}
	elseif(empty($obj->cpassword))
	{
		$error="must confirm password";
	}
	elseif($obj->password!=$obj->cpassword)
	{
		$error="password confirmed incorrectly";
	}
	elseif(empty($obj->companyname))
	{
		$error="must provide company name";
	}
	elseif(empty($obj->companytype))
	{
		$error="must provide companytype";
	}
	elseif(empty($obj->address))
	{
		$error="must provide address";
	}
	elseif(empty($obj->town))
	{
		$error="must provide company town";
	}
	elseif(empty($obj->desc))
	{
		$error="must provide company description";
	}
	elseif(empty($obj->logo))
	{
		$error="must provide company logo";
	}
	elseif(empty($obj->pinno))
	{
		$error="must provide company PIN No";
	}
	elseif(empty($obj->vatno))
	{
		$error="must provide company VAT No";
	}
	else
	{
		$filename="../config.php";
		$handle=fopen($filename,"w");
		$content='define("HOST","$obj->hostname");\n';
		$content.='define("DBNAME","$obj->dbname");\n';
		$content.='define("DBUSER","$obj->username");\n';
		$content.='define("DBPASS","$obj->password");\n';
		$content.='define("PORT","");\n';
		$content.='define("TITLE","$obj->title");\n';
		$content.='define("COMPANY_NAME","$obj->companyname");\n';
		$content.='define("COMPANY_TYPE","$obj->companytype");\n';
		$content.='define("COMPANY_ADDR","$obj->address");\n';
		$content.='define("COMPANY_TOWN","$obj->town");\n';
		$content.='define("COMPANY_DESC","$obj->desc.");\n';
		$content.='define("COMPANY_LOGO","$obj->logo");\n';
		$content.='define("COMPANY_PINNO","$obj->pinno");\n';
		$content.='define("COMPANY_VATNO","$obj->vatno");\n';
		fwrite($handle,$content);
		fclose($handle);
		
		$filename="../instaldone.txt";
		$handle=fopen($handle,"w");
		$content="Installed;";
		if(fwrite($handle,$content))
		{
			unlink("instal/instal.php");
			unlink("instal/instal.html");
			redirect("populate.php");
		}
	}
}
include"instal.html";
?>