<title>WiseDigits: Assets </title>
<?php 
include "../../../head.php";

?>
<script type="text/javascript">
$().ready(function() {
 $("#suppliername").autocomplete("../../../modules/server/server/search.php?main=proc&module=suppliers&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#suppliername").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("suppliername").value=data[0];
     document.getElementById("supplierid").value=data[1];
     document.getElementById("supplierid").value=data[1];
     document.getElementById("contact").value=data[7];
     document.getElementById("physicaladdress").value=data[8];
     document.getElementById("tel").value=data[9];
     document.getElementById("cellphone").value=data[12];
     document.getElementById("email").value=data[11];
   }
 });
});
</script>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 </script>

<div class='main'>
<form class="forms" id="theform" action="addassets_proc.php" name="assets" method="POST" enctype="multipart/form-data">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
			<tr>
				<td><label>Supplier:</label></td>
				<td><input type='text' size='20' name='suppliername' id='suppliername' value='<?php echo $obj->suppliername; ?>'>
					<input type="hidden" name='supplierid' id='supplierid' value='<?php echo $obj->supplierid; ?>'></td>
				<td><label>:</label></td>
				<td><input type='text' name='supplierid' id='supplierid' size='20'  value='<?php echo $obj->supplierid; ?>'/></td>			<tr>
				<td><label>Contact:</label></td>
				<td><input type='text' name='contact' id='contact' size='20' readonly value='<?php echo $obj->contact; ?>'/></td>				<td><label>Physical Address:</label></td>
				<td><textarea name='physicaladdress' id='physicaladdress' readonly><?php echo $obj->physicaladdress; ?></textarea></td>
			<tr>
				<td><label>Phone No.:</label></td>
				<td><input type='text' name='tel' id='tel' size='20' readonly value='<?php echo $obj->tel; ?>'/></td>				<td><label>Cell-Phone:</label></td>
				<td><input type='text' name='cellphone' id='cellphone' size='20' readonly value='<?php echo $obj->cellphone; ?>'/></td>			<tr>
				<td><label>E-mail:</label></td>
				<td><input type='text' name='email' id='email' size='20' readonly value='<?php echo $obj->email; ?>'/></td>			</td>
			</tr>
		</table>
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
	<tr>
		<th align="right">Asset Name  </th>
		<th align="right">Photo  </th>
		<th align="right">Asset Category  </th>
		<th align="right">Gross Value  </th>
		<th align="right">Salvage Value  </th>
		<th align="right">Delivery Note No  </th>
		<th align="right">Remarks  </th>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<td><input type="text" name="name" id="name" value="<?php echo $obj->name; ?>"><font color='red'>*</font></td>
		<td><input type="file" name="photo" id="photo" value="<?php echo $obj->photo; ?>"></td>
		<td><select name="categoryid" >
<option value="">Select...</option>
<?php
	$categorys=new Categorys();
	$where="  ";
	$fields="assets_categorys.id, assets_categorys.name, assets_categorys.timemethod, assets_categorys.noofdepr, assets_categorys.endingdate, assets_categorys.periodlength, assets_categorys.computationmethod, assets_categorys.degressivefactor, assets_categorys.firstentry, assets_categorys.ipaddress, assets_categorys.createdby, assets_categorys.createdon, assets_categorys.lasteditedby, assets_categorys.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$categorys->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($categorys->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->categoryid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
<font color='red'>*</font>		<td><input type="text" name="value" id="value" size="20" value="<?php echo $obj->value; ?>"></td>
		<td><input type="text" name="salvagevalue" id="salvagevalue" size="20" value="<?php echo $obj->salvagevalue; ?>"></td>
		<td><input type="text" name="deliveryno" id="deliveryno" value="<?php echo $obj->deliveryno; ?>"></td>
		<td><textarea name="remarks" id="remarks"><?php echo $obj->remarks; ?></textarea></td>
	<td><input type="submit" name="action2" value="Add"/></td>
	</tr>
	</table>
		<table align='center'>
			<tr>
			<td>
		Purchase Date:<input type="date" name="purchasedon" id="purchasedon"  class="date_input" size="12" readonly  value="<?php echo $obj->purchasedon; ?>">
		Invoice No:<input type="text" name="documentno" id="documentno"  size="20"  value="<?php echo $obj->documentno; ?>">
		LPO No:<input type="text" name="lpono" id="lpono"  size="20"  value="<?php echo $obj->lpono; ?>">
		Memo:<textarea name="memo" ><?php echo $obj->memo; ?></textarea>
			</td>
			</tr>
		</table>
<table style="clear:both" class="tgrid display" id="tbl" cellpadding="0" align="center" width="98%" cellspacing="0">
	<thead>
	<tr style="font-size:18px; vertical-align:text-top; ">
		<th align="left" >#</th>
		<th align="left">Asset Name  </th>
		<th align="left">Photo  </th>
		<th align="left">Asset Category  </th>
		<th align="left">Gross Value  </th>
		<th align="left">Salvage Value  </th>
		<th align="left">Delivery Note No  </th>
		<th align="left">Remarks  </th>
		<th><input type="hidden" name="iterator" value="<?php echo $obj->iterator; ?>"/></th>
		<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php
	if($_SESSION['shpassets']){
		$shpassets=$_SESSION['shpassets'];
		$i=0;
		$j=$obj->iterator;
		$total=0;
		while($j>0){
		?>
		<tr style="font-size:12px; vertical-align:text-top; ">
			<td><?php echo ($i+1); ?></td>
			<td><?php echo $shpassets[$i]['name']; ?> </td>
			<td><?php echo $shpassets[$i]['photo']; ?> </td>
			<td><?php echo $shpassets[$i]['categoryname']; ?> </td>
			<td><?php echo $shpassets[$i]['value']; ?> </td>
			<td><?php echo $shpassets[$i]['salvagevalue']; ?> </td>
			<td><?php echo $shpassets[$i]['deliveryno']; ?> </td>
			<td><?php echo $shpassets[$i]['remarks']; ?> </td>
			<td><?php echo $shpassets[$i]['total']; ?> </td>
			<td><a href="edit.php?i=<?php echo $i; ?>&action=edit&edit=<?php echo $obj->edit; ?>">Edit</a></td>
			<td><a href="edit.php?i=<?php echo $i; ?>&action=del&edit=<?php echo $obj->edit; ?>">Del</a></td>
		</tr>
		<?php
		$i++;
		$j--;
		}
	}
	?>
	</tbody>
</table>
<table align="center" width="98%">
	<?php if(empty($obj->retrieve)){?>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
	<?php }else{?>
	<tr>
		<td colspan="2" align="center"><input type="button" name="action" id="action" value="Print" onclick="Clickheretoprint();"/></td>
	</tr>
	<?php }?>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
if(!empty($error)){
	showError($error);
}
?>