<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Assets_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../assets/categorys/Categorys_class.php");
require_once("../../assets/fleets/Fleets_class.php");
require_once("../../proc/suppliers/Suppliers_class.php");
require_once("../../fn/generaljournalaccounts/Generaljournalaccounts_class.php");
require_once("../../fn/generaljournalaccounts/Generaljournalaccounts_class.php");
require_once("../../fn/generaljournals/Generaljournals_class.php");
require_once("../../sys/transactions/Transactions_class.php");
//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="7612";//Edit
}
else{
	$auth->roleid="7610";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
if(empty($obj->action)){
	$obj->purchasedon=date('Y-m-d');

}
	
if($obj->action=="Save"){
	$assets=new Assets();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$shpassets=$_SESSION['shpassets'];
	$error=$assets->validates($obj);
	if(!empty($error)){
		$error=$error;
	}
	elseif(empty($shpassets)){
		$error="No items in the sale list!";
	}
	else{
		$assets=$assets->setObject($obj);
		if($assets->add($assets,$shpassets)){

			//adding general journal account(s)
			$name=$obj->name;
			$obj->name=$name." Fixed Asset ";
			$generaljournalaccounts = new Generaljournalaccounts();
			$obj->refid=$assets->id;
			$obj->acctypeid=7;
			$generaljournalaccounts->setObject($obj);
			$generaljournalaccounts->add($generaljournalaccounts);

			$error=SUCCESS;
			redirect("addassets_proc.php?id=".$assets->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$assets=new Assets();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$assets->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$assets=$assets->setObject($obj);
		$shpassets=$_SESSION['shpassets'];
		if($assets->edit($assets,$shpassets)){

			//updating corresponding general journal account
			$name=$obj->name;
			$obj->name=$name." Fixed Asset ";
			$generaljournalaccounts = new Generaljournalaccounts();
			$obj->refid=$assets->id;
			$obj->acctypeid=7;
			$generaljournalaccounts->setObject($obj);
			$upwhere=" refid='$assets->id' and acctypeid='7' ";
			$generaljournalaccounts->edit($generaljournalaccounts,$upwhere);


			//Make a journal entry

			//retrieve account to debit
			$generaljournalaccounts = new Generaljournalaccounts();
			$fields="*";
			$where=" where refid='$obj->supplierid' and acctypeid='30'";
			$join="";
			$having="";
			$groupby="";
			$orderby="";
			$generaljournalaccounts->retrieve($fields, $join, $where, $having, $groupby, $orderby);
			$generaljournalaccounts=$generaljournalaccounts->fetchObject;

			//retrieve account to credit
			$generaljournalaccounts2 = new Generaljournalaccounts();
			$fields="*";
			$where=" where refid='1' and acctypeid='26'";
			$join="";
			$having="";
			$groupby="";
			$orderby="";
			$generaljournalaccounts2->retrieve($fields, $join, $where, $having, $groupby, $orderby);
			$generaljournalaccounts2=$generaljournalaccounts2->fetchObject;
			$error=UPDATESUCCESS;
			redirect("addassets_proc.php?id=".$assets->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if($obj->action2=="Add"){

	$_SESSION['obj']=$obj;
	if(empty($obj->iterator))
		$it=0;
	else
		$it=$obj->iterator;
	$shpassets=$_SESSION['shpassets'];

	$categorys = new Categorys();
	$fields=" * ";
	$join="";
	$groupby="";
	$having="";
	$where=" where id='$obj->categoryid'";
	$categorys->retrieve($fields, $join, $where, $having, $groupby, $orderby);
	$categorys=$categorys->fetchObject;
	$shpassets[$it]=array('name'=>"$obj->name", 'photo'=>"$obj->photo", 'categoryid'=>"$obj->categoryid", 'categoryname'=>"$categorys->name", 'value'=>"$obj->value", 'salvagevalue'=>"$obj->salvagevalue", 'deliveryno'=>"$obj->deliveryno", 'remarks'=>"$obj->remarks");

 	$it++;
		$obj->iterator=$it;
 	$_SESSION['shpassets']=$shpassets;

	$obj->name="";
 	$obj->photo="";
 	$obj->categoryname="";
 	$obj->categoryid="";
 	$obj->value="";
 	$obj->salvagevalue="";
 	$obj->deliveryno="";
 	$obj->remarks="";
 }

if(empty($obj->action)){

	$categorys= new Categorys();
	$fields="assets_categorys.id, assets_categorys.name, assets_categorys.timemethod, assets_categorys.noofdepr, assets_categorys.endingdate, assets_categorys.periodlength, assets_categorys.computationmethod, assets_categorys.degressivefactor, assets_categorys.firstentry, assets_categorys.ipaddress, assets_categorys.createdby, assets_categorys.createdon, assets_categorys.lasteditedby, assets_categorys.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$categorys->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$suppliers= new Suppliers();
	$fields="proc_suppliers.id, proc_suppliers.code, proc_suppliers.name, proc_suppliers.suppliercategoryid, proc_suppliers.regionid, proc_suppliers.subregionid, proc_suppliers.contact, proc_suppliers.physicaladdress, proc_suppliers.tel, proc_suppliers.fax, proc_suppliers.email, proc_suppliers.cellphone, proc_suppliers.status, proc_suppliers.createdby, proc_suppliers.createdon, proc_suppliers.lasteditedby, proc_suppliers.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$suppliers->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$assets=new Assets();
	$where=" where id=$id ";
	$fields="assets_assets.id, assets_assets.name, assets_assets.photo, assets_assets.documentno, assets_assets.categoryid, assets_assets.value, assets_assets.salvagevalue, assets_assets.purchasedon, assets_assets.supplierid, assets_assets.lpono, assets_assets.deliveryno, assets_assets.remarks, assets_assets.memo, assets_assets.ipaddress, assets_assets.createdby, assets_assets.createdon, assets_assets.lasteditedby, assets_assets.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$assets->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$assets->fetchObject;

	//for autocompletes
	$suppliers = new Suppliers();
	$fields=" * ";
	$where=" where id='$obj->supplierid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$suppliers->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$suppliers->fetchObject;

	$obj->suppliername=$auto->name;
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Assets ";
include "addassets.php";
?>