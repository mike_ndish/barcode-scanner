<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Assets_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
//Redirect to horizontal layout
//redirect("addassets_proc.php?retrieve=".$_GET['retrieve']);

$page_title="Assets";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="7611";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$assets=new Assets();
if(!empty($delid)){
	$assets->id=$delid;
	$assets->delete($assets);
	redirect("assets.php");
}
//Authorization.
$auth->roleid="7610";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <a href='addassets_proc.php'>New Assets</a></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Asset Name </th>
			<th>Photo </th>
			<th>Invoice No </th>
			<th>Asset Category </th>
			<th>Gross Value </th>
			<th>Salvage Value </th>
			<th>Purchase Date </th>
			<th>Supplier </th>
			<th>LPO No </th>
			<th>Delivery Note No </th>
			<th>Remarks </th>
			<th>Memo </th>
<?php
//Authorization.
$auth->roleid="7612";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="7613";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="assets_assets.id, assets_assets.name, assets_assets.photo, assets_assets.documentno, assets_categorys.name as categoryid, assets_assets.value, assets_assets.salvagevalue, assets_assets.purchasedon, proc_suppliers.name as supplierid, assets_assets.lpono, assets_assets.deliveryno, assets_assets.remarks, assets_assets.memo, assets_assets.ipaddress, assets_assets.createdby, assets_assets.createdon, assets_assets.lasteditedby, assets_assets.lasteditedon";
		$join=" left join assets_categorys on assets_assets.categoryid=assets_categorys.id  left join proc_suppliers on assets_assets.supplierid=proc_suppliers.id ";
		$having="";
		$groupby="";
		$orderby="";
		$assets->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$assets->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->name; ?></td>
			<td><?php echo $row->photo; ?></td>
			<td><?php echo $row->documentno; ?></td>
			<td><?php echo $row->categoryid; ?></td>
			<td><?php echo formatNumber($row->value); ?></td>
			<td><?php echo formatNumber($row->salvagevalue); ?></td>
			<td><?php echo formatDate($row->purchasedon); ?></td>
			<td><?php echo $row->supplierid; ?></td>
			<td><?php echo $row->lpono; ?></td>
			<td><?php echo $row->deliveryno; ?></td>
			<td><?php echo $row->remarks; ?></td>
			<td><?php echo $row->memo; ?></td>
<?php
//Authorization.
$auth->roleid="7612";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="addassets_proc.php?id=<?php echo $row->id; ?>">View</a></td>
<?php
}
//Authorization.
$auth->roleid="7613";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='assets.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
