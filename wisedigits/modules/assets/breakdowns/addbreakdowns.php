<title>WiseDigits: Breakdowns </title>
<?php 
include "../../../headerpop.php";

?>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 </script>

<div class='main'>
<form class="forms" id="theform" action="addbreakdowns_proc.php" name="breakdowns" method="POST" enctype="multipart/form-data">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
	<tr>
		<td colspan="2"><input type="hidden" name="id" id="id" value="<?php echo $obj->id; ?>"></td>
	</tr>
	<tr>
		<td align="right">Asset : </td>
		<td><input type="text" name="assetid" id="assetid" value="<?php echo $obj->assetid; ?>"><font color='red'>*</font></td>
	</tr>
	<tr>
		<td align="right">Breakdown Description : </td>
		<td><textarea name="description"><?php echo $obj->description; ?></textarea><font color='red'>*</font></td>
	</tr>
	<tr>
		<td align="right">Break Down Date : </td>
		<td><input type="text" name="brokedownon" id="brokedownon" class="date_input" size="12" readonly  value="<?php echo $obj->brokedownon; ?>"><font color='red'>*</font></td>
	</tr>
	<tr>
		<td align="right">Date Of Reactivation : </td>
		<td><input type="text" name="reactivatedon" id="reactivatedon" class="date_input" size="12" readonly  value="<?php echo $obj->reactivatedon; ?>"></td>
	</tr>
	<tr>
		<td align="right">Cost : </td>
		<td><input type="text" name="cost" id="cost" size="8"  value="<?php echo $obj->cost; ?>"></td>
	</tr>
	<tr>
		<td align="right">Ref. # : </td>
		<td><input type="text" name="refno" id="refno" value="<?php echo $obj->refno; ?>"></td>
	</tr>
	<tr>
		<td align="right">Remarks : </td>
		<td><textarea name="remarks"><?php echo $obj->remarks; ?></textarea></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
if(!empty($error)){
	showError($error);
}
?>