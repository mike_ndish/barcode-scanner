<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Fleetcolors_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Fleetcolors";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="7627";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$fleetcolors=new Fleetcolors();
if(!empty($delid)){
	$fleetcolors->id=$delid;
	$fleetcolors->delete($fleetcolors);
	redirect("fleetcolors.php");
}
//Authorization.
$auth->roleid="7626";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <input onclick="showPopWin('addfleetcolors_proc.php',600,430);" value="Add Fleetcolors " type="button"/></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Fuel Color </th>
			<th>Remarks </th>
<?php
//Authorization.
$auth->roleid="7628";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="7629";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="assets_fleetcolors.id, assets_fleetcolors.name, assets_fleetcolors.remarks, assets_fleetcolors.ipaddress, assets_fleetcolors.createdby, assets_fleetcolors.createdon, assets_fleetcolors.lasteditedby, assets_fleetcolors.lasteditedon";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$fleetcolors->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$fleetcolors->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->name; ?></td>
			<td><?php echo $row->remarks; ?></td>
<?php
//Authorization.
$auth->roleid="7628";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addfleetcolors_proc.php?id=<?php echo $row->id; ?>',600,430);">View</a></td>
<?php
}
//Authorization.
$auth->roleid="7629";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='fleetcolors.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
