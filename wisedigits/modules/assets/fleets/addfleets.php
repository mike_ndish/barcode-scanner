<title>WiseDigits: Fleets </title>
<?php 
include "../../../headerpop.php";

?>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 </script>

<div class='main'>
<form class="forms" id="theform" action="addfleets_proc.php" name="fleets" method="POST" enctype="multipart/form-data">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
	<tr>
		<td colspan="2"><input type="hidden" name="id" id="id" value="<?php echo $obj->id; ?>"></td>
	</tr>
	<tr>
		<td align="right">Fleet : </td>
		<td><input type="text" name="assetid" id="assetid" value="<?php echo $obj->assetid; ?>"><font color='red'>*</font></td>
	</tr>
	<tr>
		<td align="right">Model : </td>
			<td><select name="fleetmodelid">
<option value="">Select...</option>
<?php
	$fleetmodels=new Fleetmodels();
	$where="  ";
	$fields="assets_fleetmodels.id, assets_fleetmodels.name, assets_fleetmodels.fleetmakeid, assets_fleetmodels.remarks, assets_fleetmodels.ipaddress, assets_fleetmodels.createdby, assets_fleetmodels.createdon, assets_fleetmodels.lasteditedby, assets_fleetmodels.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$fleetmodels->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($fleetmodels->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->fleetmodelid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">Year : </td>
		<td><input type="text" name="year" id="year" value="<?php echo $obj->year; ?>"></td>
	</tr>
	<tr>
		<td align="right">Color : </td>
		<td><input type="text" name="fleetcolorid" id="fleetcolorid" value="<?php echo $obj->fleetcolorid; ?>"></td>
	</tr>
	<tr>
		<td align="right">Vehicle Identification Number : </td>
		<td><input type="text" name="vin" id="vin" value="<?php echo $obj->vin; ?>"></td>
	</tr>
	<tr>
		<td align="right">Vehicle Type : </td>
			<td><select name="fleettypeid">
<option value="">Select...</option>
<?php
	$fleettypes=new Fleettypes();
	$where="  ";
	$fields="assets_fleettypes.id, assets_fleettypes.name, assets_fleettypes.remarks, assets_fleettypes.ipaddress, assets_fleettypes.createdby, assets_fleettypes.createdon, assets_fleettypes.lasteditedby, assets_fleettypes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$fleettypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($fleettypes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->fleettypeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">Plate No : </td>
		<td><input type="text" name="plateno" id="plateno" value="<?php echo $obj->plateno; ?>"></td>
	</tr>
	<tr>
		<td align="right">Engine : </td>
		<td><input type="text" name="engine" id="engine" value="<?php echo $obj->engine; ?>"></td>
	</tr>
	<tr>
		<td align="right">Fuel Type : </td>
			<td><select name="fleetfueltypeid">
<option value="">Select...</option>
<?php
	$fleetfueltypes=new Fleetfueltypes();
	$where="  ";
	$fields="assets_fleetfueltypes.id, assets_fleetfueltypes.name, assets_fleetfueltypes.remarks, assets_fleetfueltypes.ipaddress, assets_fleetfueltypes.createdby, assets_fleetfueltypes.createdon, assets_fleetfueltypes.lasteditedby, assets_fleetfueltypes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$fleetfueltypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($fleetfueltypes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->fleetfueltypeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">Odometer Type : </td>
			<td><select name="fleetodometertypeid">
<option value="">Select...</option>
<?php
	$fleetodometertypes=new Fleetodometertypes();
	$where="  ";
	$fields="assets_fleetodometertypes.id, assets_fleetodometertypes.name, assets_fleetodometertypes.remarks, assets_fleetodometertypes.ipaddress, assets_fleetodometertypes.createdby, assets_fleetodometertypes.createdon, assets_fleetodometertypes.lasteditedby, assets_fleetodometertypes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$fleetodometertypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($fleetodometertypes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->fleetodometertypeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">Service Mileage : </td>
		<td><input type="text" name="mileage" id="mileage" size="8"  value="<?php echo $obj->mileage; ?>"></td>
	</tr>
	<tr>
		<td align="right">Last Service Mileage : </td>
		<td><input type="text" name="lastservicemileage" id="lastservicemileage" size="8"  value="<?php echo $obj->lastservicemileage; ?>"></td>
	</tr>
	<tr>
		<td align="right">Allocated To : </td>
			<td><select name="employeeid">
<option value="">Select...</option>
<?php
	$employees=new Employees();
	$where="  ";
	$fields="hrm_employees.id, hrm_employees.pfnum, hrm_employees.firstname, hrm_employees.middlename, hrm_employees.lastname, hrm_employees.gender, hrm_employees.bloodgroup, hrm_employees.rhd, hrm_employees.supervisorid, hrm_employees.startdate, hrm_employees.enddate, hrm_employees.dob, hrm_employees.idno, hrm_employees.passportno, hrm_employees.phoneno, hrm_employees.email, hrm_employees.officemail, hrm_employees.physicaladdress, hrm_employees.nationalityid, hrm_employees.countyid, hrm_employees.constituencyid, hrm_employees.location, hrm_employees.town, hrm_employees.marital, hrm_employees.spouse, hrm_employees.spouseidno, hrm_employees.spousetel, hrm_employees.spouseemail, hrm_employees.nssfno, hrm_employees.nhifno, hrm_employees.pinno, hrm_employees.helbno, hrm_employees.bankid, hrm_employees.bankbrancheid, hrm_employees.bankacc, hrm_employees.clearingcode, hrm_employees.ref, hrm_employees.basic, hrm_employees.assignmentid, hrm_employees.gradeid, hrm_employees.statusid, hrm_employees.image, hrm_employees.createdby, hrm_employees.createdon, hrm_employees.lasteditedby, hrm_employees.lasteditedon, hrm_employees.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$employees->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($employees->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->employeeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">HR Department : </td>
			<td><select name="departmentid">
<option value="">Select...</option>
<?php
	$departments=new Departments();
	$where="  ";
	$fields="hrm_departments.id, hrm_departments.name, hrm_departments.code, hrm_departments.leavemembers, hrm_departments.description, hrm_departments.createdby, hrm_departments.createdon, hrm_departments.lasteditedby, hrm_departments.lasteditedon, hrm_departments.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$departments->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($departments->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->departmentid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
if(!empty($error)){
	showError($error);
}
?>