<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Fleets_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../assets/fleetmodels/Fleetmodels_class.php");
require_once("../../assets/fleetfueltypes/Fleetfueltypes_class.php");
require_once("../../assets/fleettypes/Fleettypes_class.php");
require_once("../../assets/fleetodometertypes/Fleetodometertypes_class.php");
require_once("../../hrm/employees/Employees_class.php");
require_once("../../hrm/departments/Departments_class.php");
//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="7648";//Edit
}
else{
	$auth->roleid="7646";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
	
if($obj->action=="Save"){
	$fleets=new Fleets();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$error=$fleets->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$fleets=$fleets->setObject($obj);
		if($fleets->add($fleets)){
			$error=SUCCESS;
			redirect("addfleets_proc.php?id=".$fleets->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$fleets=new Fleets();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$fleets->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$fleets=$fleets->setObject($obj);
		if($fleets->edit($fleets)){
			$error=UPDATESUCCESS;
			redirect("addfleets_proc.php?id=".$fleets->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if(empty($obj->action)){

	$fleetmodels= new Fleetmodels();
	$fields="assets_fleetmodels.id, assets_fleetmodels.name, assets_fleetmodels.fleetmakeid, assets_fleetmodels.remarks, assets_fleetmodels.ipaddress, assets_fleetmodels.createdby, assets_fleetmodels.createdon, assets_fleetmodels.lasteditedby, assets_fleetmodels.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$fleetmodels->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$fleetfueltypes= new Fleetfueltypes();
	$fields="assets_fleetfueltypes.id, assets_fleetfueltypes.name, assets_fleetfueltypes.remarks, assets_fleetfueltypes.ipaddress, assets_fleetfueltypes.createdby, assets_fleetfueltypes.createdon, assets_fleetfueltypes.lasteditedby, assets_fleetfueltypes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$fleetfueltypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$fleettypes= new Fleettypes();
	$fields="assets_fleettypes.id, assets_fleettypes.name, assets_fleettypes.remarks, assets_fleettypes.ipaddress, assets_fleettypes.createdby, assets_fleettypes.createdon, assets_fleettypes.lasteditedby, assets_fleettypes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$fleettypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$fleetodometertypes= new Fleetodometertypes();
	$fields="assets_fleetodometertypes.id, assets_fleetodometertypes.name, assets_fleetodometertypes.remarks, assets_fleetodometertypes.ipaddress, assets_fleetodometertypes.createdby, assets_fleetodometertypes.createdon, assets_fleetodometertypes.lasteditedby, assets_fleetodometertypes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$fleetodometertypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$employees= new Employees();
	$fields="hrm_employees.id, hrm_employees.pfnum, hrm_employees.firstname, hrm_employees.middlename, hrm_employees.lastname, hrm_employees.gender, hrm_employees.bloodgroup, hrm_employees.rhd, hrm_employees.supervisorid, hrm_employees.startdate, hrm_employees.enddate, hrm_employees.dob, hrm_employees.idno, hrm_employees.passportno, hrm_employees.phoneno, hrm_employees.email, hrm_employees.officemail, hrm_employees.physicaladdress, hrm_employees.nationalityid, hrm_employees.countyid, hrm_employees.constituencyid, hrm_employees.location, hrm_employees.town, hrm_employees.marital, hrm_employees.spouse, hrm_employees.spouseidno, hrm_employees.spousetel, hrm_employees.spouseemail, hrm_employees.nssfno, hrm_employees.nhifno, hrm_employees.pinno, hrm_employees.helbno, hrm_employees.bankid, hrm_employees.bankbrancheid, hrm_employees.bankacc, hrm_employees.clearingcode, hrm_employees.ref, hrm_employees.basic, hrm_employees.assignmentid, hrm_employees.gradeid, hrm_employees.statusid, hrm_employees.image, hrm_employees.createdby, hrm_employees.createdon, hrm_employees.lasteditedby, hrm_employees.lasteditedon, hrm_employees.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$employees->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$departments= new Departments();
	$fields="hrm_departments.id, hrm_departments.name, hrm_departments.code, hrm_departments.leavemembers, hrm_departments.description, hrm_departments.createdby, hrm_departments.createdon, hrm_departments.lasteditedby, hrm_departments.lasteditedon, hrm_departments.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$departments->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$fleets=new Fleets();
	$where=" where id=$id ";
	$fields="assets_fleets.id, assets_fleets.assetid, assets_fleets.fleetmodelid, assets_fleets.year, assets_fleets.fleetcolorid, assets_fleets.vin, assets_fleets.fleettypeid, assets_fleets.plateno, assets_fleets.engine, assets_fleets.fleetfueltypeid, assets_fleets.fleetodometertypeid, assets_fleets.mileage, assets_fleets.lastservicemileage, assets_fleets.employeeid, assets_fleets.departmentid, assets_fleets.ipaddress, assets_fleets.createdby, assets_fleets.createdon, assets_fleets.lasteditedby, assets_fleets.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$fleets->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$fleets->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Fleets ";
include "addfleets.php";
?>