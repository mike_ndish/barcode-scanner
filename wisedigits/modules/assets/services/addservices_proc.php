<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Services_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="7709";//Edit
}
else{
	$auth->roleid="7707";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
	
if($obj->action=="Save"){
	$services=new Services();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$error=$services->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$services=$services->setObject($obj);
		if($services->add($services)){
			$error=SUCCESS;
			redirect("addservices_proc.php?id=".$services->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$services=new Services();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$services->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$services=$services->setObject($obj);
		if($services->edit($services)){
			$error=UPDATESUCCESS;
			redirect("addservices_proc.php?id=".$services->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if(empty($obj->action)){
}

if(!empty($id)){
	$services=new Services();
	$where=" where id=$id ";
	$fields="assets_services.id, assets_services.assetid, assets_services.servicescheduleid, assets_services.supplierid, assets_services.documentno, assets_services.servicedon, assets_services.servicetype, assets_services.description, assets_services.recommendations, assets_services.remarks, assets_services.ipaddress, assets_services.createdby, assets_services.createdon, assets_services.lasteditedby, assets_services.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$services->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$services->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Services ";
include "addservices.php";
?>