<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Serviceschedules_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="7713";//Edit
}
else{
	$auth->roleid="7711";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
	
if($obj->action=="Save"){
	$serviceschedules=new Serviceschedules();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$error=$serviceschedules->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$serviceschedules=$serviceschedules->setObject($obj);
		if($serviceschedules->add($serviceschedules)){
			$error=SUCCESS;
			redirect("addserviceschedules_proc.php?id=".$serviceschedules->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$serviceschedules=new Serviceschedules();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$serviceschedules->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$serviceschedules=$serviceschedules->setObject($obj);
		if($serviceschedules->edit($serviceschedules)){
			$error=UPDATESUCCESS;
			redirect("addserviceschedules_proc.php?id=".$serviceschedules->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if(empty($obj->action)){
}

if(!empty($id)){
	$serviceschedules=new Serviceschedules();
	$where=" where id=$id ";
	$fields="assets_serviceschedules.id, assets_serviceschedules.assetid, assets_serviceschedules.servicedate, assets_serviceschedules.servicetype, assets_serviceschedules.description, assets_serviceschedules.recommendations, assets_serviceschedules.remarks, assets_serviceschedules.ipaddress, assets_serviceschedules.createdby, assets_serviceschedules.createdon, assets_serviceschedules.lasteditedby, assets_serviceschedules.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$serviceschedules->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$serviceschedules->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Serviceschedules ";
include "addserviceschedules.php";
?>