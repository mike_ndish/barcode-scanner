<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Projecttypes_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Projecttypes";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="7587";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$projecttypes=new Projecttypes();
if(!empty($delid)){
	$projecttypes->id=$delid;
	$projecttypes->delete($projecttypes);
	redirect("projecttypes.php");
}
//Authorization.
$auth->roleid="7586";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div class="container">
<hr>
<a class="btn btn-info" onclick="showPopWin('addstatuss_proc.php',600,430);">Add Projecttypes </a>
<?php }?>
<hr>
<table style="clear:both;"  class="table table-codensed table-stripped" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Project Type </th>
			<th>Remarks </th>
<?php
//Authorization.
$auth->roleid="7588";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="7589";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="con_projecttypes.id, con_projecttypes.name, con_projecttypes.remarks, con_projecttypes.ipaddress, con_projecttypes.createdby, con_projecttypes.createdon, con_projecttypes.lasteditedby, con_projecttypes.lasteditedon";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$projecttypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$projecttypes->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->name; ?></td>
			<td><?php echo $row->remarks; ?></td>
<?php
//Authorization.
$auth->roleid="7588";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addprojecttypes_proc.php?id=<?php echo $row->id; ?>',600,430);">View</a></td>
<?php
}
//Authorization.
$auth->roleid="7589";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='projecttypes.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
