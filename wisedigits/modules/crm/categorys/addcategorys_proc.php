<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Categorys_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="4786";//Edit
}
else{
	$auth->roleid="4786";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
	
	
if($obj->action=="Save"){
	$categorys=new Categorys();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$error=$categorys->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$categorys=$categorys->setObject($obj);
		if($categorys->add($categorys)){
			$error=SUCCESS;
			redirect("addcategorys_proc.php?id=".$categorys->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$categorys=new Categorys();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$categorys->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$categorys=$categorys->setObject($obj);
		if($categorys->edit($categorys)){
			$error=UPDATESUCCESS;
			redirect("addcategorys_proc.php?id=".$categorys->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if(empty($obj->action)){
}

if(!empty($id)){
	$categorys=new Categorys();
	$where=" where id=$id ";
	$fields="crm_categorys.id, crm_categorys.name, crm_categorys.remarks, crm_categorys.createdby, crm_categorys.createdon, crm_categorys.lasteditedby, crm_categorys.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$categorys->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$categorys->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Categorys ";
include "addcategorys.php";
?>