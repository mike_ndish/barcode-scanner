<?php 
require_once("CustomerpaymentsDBO.php");
class Customerpayments
{				
	var $id;			
	var $customerid;			
	var $documentno;			
	var $paidon;			
	var $amount;			
	var $paymentmodeid;			
	var $bankid;			
	var $chequeno;			
	var $ipaddress;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $customerpaymentsDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		if(empty($obj->customerid))
			$obj->customerid='NULL';
		$this->customerid=$obj->customerid;
		$this->documentno=str_replace("'","\'",$obj->documentno);
		$this->paidon=str_replace("'","\'",$obj->paidon);
		$this->amount=str_replace("'","\'",$obj->amount);
		if(empty($obj->paymentmodeid))
			$obj->paymentmodeid='NULL';
		$this->paymentmodeid=$obj->paymentmodeid;
		if(empty($obj->bankid))
			$obj->bankid='NULL';
		$this->bankid=$obj->bankid;
		$this->chequeno=str_replace("'","\'",$obj->chequeno);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get customerid
	function getCustomerid(){
		return $this->customerid;
	}
	//set customerid
	function setCustomerid($customerid){
		$this->customerid=$customerid;
	}

	//get documentno
	function getDocumentno(){
		return $this->documentno;
	}
	//set documentno
	function setDocumentno($documentno){
		$this->documentno=$documentno;
	}

	//get paidon
	function getPaidon(){
		return $this->paidon;
	}
	//set paidon
	function setPaidon($paidon){
		$this->paidon=$paidon;
	}

	//get amount
	function getAmount(){
		return $this->amount;
	}
	//set amount
	function setAmount($amount){
		$this->amount=$amount;
	}

	//get paymentmodeid
	function getPaymentmodeid(){
		return $this->paymentmodeid;
	}
	//set paymentmodeid
	function setPaymentmodeid($paymentmodeid){
		$this->paymentmodeid=$paymentmodeid;
	}

	//get bankid
	function getBankid(){
		return $this->bankid;
	}
	//set bankid
	function setBankid($bankid){
		$this->bankid=$bankid;
	}

	//get chequeno
	function getChequeno(){
		return $this->chequeno;
	}
	//set chequeno
	function setChequeno($chequeno){
		$this->chequeno=$chequeno;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj){
		$customerpaymentsDBO = new CustomerpaymentsDBO();
		if($customerpaymentsDBO->persist($obj)){
		
			$transaction = new Transactions();
			$fields="*";
			$where=" where lower(replace(name,' ',''))='customerremittance'";
			$join="";
			$having="";
			$groupby="";
			$orderby="";
			$transaction->retrieve($fields, $join, $where, $having, $groupby, $orderby);
			$transaction=$transaction->fetchObject;
			
			$shpgeneraljournals=array();
			$it=0;
			
			$obj->transactdate = $obj->paidon;
			
			$generaljournalaccounts = new Generaljournalaccounts();
			$fields="*";
			$where=" where refid='$obj->customerid' and acctypeid='29'";
			$join="";
			$having="";
			$groupby="";
			$orderby="";
			$generaljournalaccounts->retrieve($fields, $join, $where, $having, $groupby, $orderby);
			$generaljournalaccounts=$generaljournalaccounts->fetchObject;
				
			$paymentmodes = new Paymentmodes();
			$fields=" * ";
			$having="";
			$groupby="";
			$orderby="";
			$where=" where id='$obj->paymentmodeid'";
			$join=" ";
			$paymentmodes->retrieve($fields, $join, $where, $having, $groupby, $orderby);
			$paymentmodes = $paymentmodes->fetchObject;
					
			if(!empty($obj->imprestaccountid) and !is_null($obj->imprestaccountid))
			  $obj->bankid=$obj->imprestaccountid;
			  
			if(empty($obj->bankid) or is_null($obj->bankid) or $obj->bankid=="NULL"){
				$obj->bankid=1;
			}
			
					//retrieve account to debit
			$generaljournalaccounts2 = new Generaljournalaccounts();
			$fields="*";
			$where=" where refid='$obj->bankid' and acctypeid='$paymentmodes->acctypeid'";
			$join="";
			$having="";
			$groupby="";
			$orderby="";
			$generaljournalaccounts2->retrieve($fields, $join, $where, $having, $groupby, $orderby);echo $generaljournalaccounts2->sql;
			$generaljournalaccounts2=$generaljournalaccounts2->fetchObject;

			$generaljournal = new Generaljournals();
			$ob->tid=$customerpaymentsDBO->id;
			$ob->documentno="$obj->documentno";
			$ob->remarks="Payment ";
			$ob->memo=$obj->remarks;
			$ob->accountid=$generaljournalaccounts2->id;
			$ob->transactionid=$transaction->id;
			$ob->mode=$obj->paymentmodeid;
			$ob->class="B";
			$ob->debit=$obj->amount;
			$ob->credit=0;
			$generaljournal->setObject($ob);
			//$generaljournal->add($generaljournal);
			
			$shpgeneraljournals[$it]=array('accountid'=>"$generaljournal->accountid", 'documentno'=>"$generaljournal->documentno", 'class'=>"B", 'accountname'=>"$generaljournalaccounts->name", 'memo'=>"$generaljournal->memo", 'remarks'=>"$generaljournal->remarks", 'debit'=>"$generaljournal->debit", 'credit'=>"$generaljournal->credit", 'total'=>"$generaljournal->total",'transactdate'=>"$obj->paidon",'transactionid'=>"$generaljournal->transactionid");
			
			$it++;
			
					//make credit entry
			$generaljournal = new Generaljournals();
			$ob->tid=$customerpaymentsDBO->id;
			$ob->documentno="$obj->documentno";
			$ob->remarks="Payment ";
			$ob->memo=$obj->remarks;
			$ob->accountid=$generaljournalaccounts->id;
			$ob->transactionid=$transaction->id;
			$ob->mode=$obj->paymentmodeid;
			$ob->class="B";
			$ob->debit=0;
			$ob->credit=$obj->amount;
			$generaljournal->setObject($ob);
			//$generaljournal->add($generaljournal);
			
			$shpgeneraljournals[$it]=array('accountid'=>"$generaljournal->accountid", 'documentno'=>"$generaljournal->documentno", 'class'=>"B", 'accountname'=>"$generaljournalaccounts->name", 'memo'=>"$generaljournal->memo", 'remarks'=>"$generaljournal->remarks", 'debit'=>"$generaljournal->debit", 'credit'=>"$generaljournal->credit", 'total'=>"$generaljournal->total",'transactdate'=>"$obj->paidon",'transactionid'=>"$generaljournal->transactionid");
						
			$gn = new Generaljournals();
			$gn->add($obj, $shpgeneraljournals);	
			
			//$this->id=$customerpaymentsDBO->id;
			$this->sql=$customerpaymentsDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$customerpaymentsDBO = new CustomerpaymentsDBO();
		if($customerpaymentsDBO->update($obj,$where)){
			$this->sql=$customerpaymentsDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$customerpaymentsDBO = new CustomerpaymentsDBO();
		if($customerpaymentsDBO->delete($obj,$where=""))		
			$this->sql=$customerpaymentsDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$customerpaymentsDBO = new CustomerpaymentsDBO();
		$this->table=$customerpaymentsDBO->table;
		$customerpaymentsDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$customerpaymentsDBO->sql;
		$this->result=$customerpaymentsDBO->result;
		$this->fetchObject=$customerpaymentsDBO->fetchObject;
		$this->affectedRows=$customerpaymentsDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->customerid)){
			$error="Customer should be provided";
		}else if(empty($obj->paymentmodeid)){
			$error="Payment Mode should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
		if(empty($obj->customerid)){
			$error="Customer should be provided";
		}else if(empty($obj->paymentmodeid)){
			$error="Payment Mode should be provided";
		}
		if(!empty($error))
			return $error;
		else
			return null;
	
	}
}				
?>
