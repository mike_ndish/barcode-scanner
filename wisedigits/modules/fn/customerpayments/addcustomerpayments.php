<title>WiseDigits: Customerpayments </title>
<?php 
include "../../../head.php";

?>

  <script type="text/javascript">
function Clickheretoprint()
{ 
	var msg;
	msg="Do You Want To Print?";
	var ans=confirm(msg);
	if(ans)
	{
		poptastic('print.php?customerid=<?php echo $obj->customerid; ?>&projectid=<?php echo $obj->projectid; ?>&doc=<?php echo $obj->documentno; ?>',700,1020);
	}
}
 </script>


<script type="text/javascript">
$().ready(function() {
 $("#customername").autocomplete("../../../modules/server/server/search.php?main=crm&module=customers&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#customername").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("customername").value=data[0];
     document.getElementById("customerid").value=data[1];
     document.getElementById("contact").value=data[7];
     document.getElementById("physicaladdress").value=data[8];
     document.getElementById("tel").value=data[9];
     document.getElementById("cellphone").value=data[12];
     document.getElementById("email").value=data[11];
   }
 });
});
</script>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 
 function checkSelected(){
  alert($(this).is('checked'));
 }
 </script>

 
<div class='main'>
<form class="forms" id="theform" action="addcustomerpayments_proc.php" name="customerpayments" method="POST" enctype="multipart/form-data">
	<table width="100%" class="table" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample" border='1'>
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
			<tr>
				<td><label>Customer:</label></td>
				<td><input type='text' size='20' name='customername' id='customername' value='<?php echo $obj->customername; ?>'>
					<input type="hidden" name='customerid' id='customerid' value='<?php echo $obj->customerid; ?>'></td>
				<td><label>Contact:</label></td>
				<td><input type='text' name='contact' id='contact' size='0' readonly value='<?php echo $obj->contact; ?>'/></td>			<tr>
			</tr>
			<tr>
				<td><label>Physical Address:</label></td>
				<td><textarea name='physicaladdress' id='physicaladdress' readonly><?php echo $obj->physicaladdress; ?></textarea></td>
				<td><label>Phone No.:</label></td>
				<td><input type='text' name='tel' id='tel' size='8' readonly value='<?php echo $obj->tel; ?>'/></td>			<tr>
			</tr>
			<tr>
				<td><label>Cell-Phone:</label></td>
				<td><input type='text' name='cellphone' id='cellphone' size='8' readonly value='<?php echo $obj->cellphone; ?>'/></td>				<td><label>E-mail:</label></td>
				<td><input type='text' name='email' id='email' size='0' readonly value='<?php echo $obj->email; ?>'/></td>			</td>
			</tr>
			
			<tr>
				<td colspan="4" align="center"><input type="submit" class="btn" name="action2" value="Retrieve"/></td>
			</tr>
		</table>
	<table style="clear:both" class="tgrid display" id="tbl" cellpadding="0" align="center" width="98%" cellspacing="0">
	<thead>
	<tr style="font-size:18px; vertical-align:text-top; ">
		<th align="left" >#</th>
		<th>&nbsp;</th>
		<th>Document No</th>
		<th>Remarks</th>
		<th>Debit</th>
		<th>Credit</th>
		<th>Balance</th>
		</tr>
	</thead>
	<tbody>
	<?php
	if($obj->action2=="Retrieve"){
	  $generaljournals = new Generaljournals();
	  $fields=" fn_generaljournals.remarks, fn_generaljournals.documentno, fn_generaljournals.memo, fn_generaljournals.reconstatus, fn_generaljournals.debit, fn_generaljournals.credit, sys_transactions.name transactionid ";
	  $join=" left join fn_generaljournalaccounts on fn_generaljournalaccounts.id=fn_generaljournals.accountid left join sys_transactions on sys_transactions.id=fn_generaljournals.transactionid ";
	  $where=" where fn_generaljournalaccounts.refid='$obj->customerid' and fn_generaljournalaccounts.acctypeid=29 ";
	  $having="";
	  $orderby="";
	  $groupby="";
	  $generaljournals->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	  $crtotal=0;
	  $drtotal=0;
	  $balance=0;
	  $i=0;
	  
	  while($row=mysql_fetch_object($generaljournals->result)){$i++;
	  $crtotal+=$row->credit;
	  $drtotal+=$row->debit;
	  $color="";
	  
	  if($row->reconstatus==$row->debit){
	    $color="#FF6666";
	  }
	  else if($row->reconstatus!=$row->debit and $row->reconstatus>0){
	    $color="#339966";
	  }
	  else{
	    $color="";
	  }
	  
	  $balance=$row->debit-floatval($row->reconstatus);
	  ?>
	  <tr style="font-size:12px; vertical-align:text-top; color:<?php echo $color; ?>;">
		  <td><?php echo ($i); ?></td>
		  <td><input type="checkbox" onclick="checkSelected();" name="<?php echo $row->documentno; ?>" <?php if($row->reconstatus==$row->debit){echo "disabled";}?> />
		  <td><?php echo $row->documentno; ?> </td>
		  <td><?php echo $row->remarks; ?></td>
		  <td align="right"><?php echo formatNumber($row->debit);?></td>
		  <td align="right"><?php echo formatNumber($row->credit);?></td>
		  <?php if($row->reconstatus!=$row->debit){ ?>
		  <td align="right"><input type="text" style="padding: 0px 0px; line-height: 2px;" size="4" name="amnt<?php echo $row->documentno; ?>" value="<?php echo $balance;?>"/></td>
		  <?php }else{?>
		  <td>&nbsp;</td>
		  <?php }?>
	  </tr>
	  <?php
	  //$i++;  
	  
	 } 
	}
	?>
	</tbody>
	
	<tfoot>
		<tr style="font-size:18px; vertical-align:text-top; ">
		<th align="left" ></th>
		<th>&nbsp;</th>
		<th>Balance</th>
		<?php if($drtotal<$crtotal){?>
		<th>&nbsp;</th>
		<th align="right"><?php echo formatNumber($crtotal-$drtotal);?></th>
		<?php }else{?>
		<th align="right"><?php echo formatNumber($drtotal-$crtotal);?></th>
		<th>&nbsp;</th>
		<?php }?>
		</tr>
	</tfoot>
</table>
<form class="forms" id="theform" action="addcustomerpayments_proc.php" name="customerpayments" method="POST" enctype="multipart/form-data">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
	<tr>
		<td colspan="2"><div id="ttselected"></div><input type="hidden" name="id" id="id" value="<?php echo $obj->id; ?>"></td>
	</tr>
	<tr>
		<td align="right">Voucher No : </td>
		<td><input type="text" readonly name="documentno" id="documentno" value="<?php echo $obj->documentno; ?>"></td>
	</tr>
	<tr>
		<td align="right">Payment Date : </td>
		<td><input type="text" name="paidon" id="paidon" class="date_input" size="12" readonly  value="<?php echo $obj->paidon; ?>"></td>
	</tr>
	<tr>
		<td align="right">Amount : </td>
		<td><input type="text" name="amount" id="amount" size="8"  value="<?php echo $obj->amount; ?>"></td>
	</tr>
	<div id="paymentmodediv" style="float:left;">
	<tr>
		<td align="right">Payment Mode : </td>
			<td><select name="paymentmodeid">
<option value="">Select...</option>
<?php
	$paymentmodes=new Paymentmodes();
	$where="  ";
	$fields="sys_paymentmodes.id, sys_paymentmodes.name, sys_paymentmodes.remarks";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$paymentmodes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($paymentmodes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->paymentmodeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	</div>
	<div id="bankdiv">
	<tr>
	
		<td align="right">Bank : </td>
			<td><select name="bankid">
<option value="">Select...</option>
<?php
	$banks=new Banks();
	$where="  ";
	$fields="fn_banks.id, fn_banks.name, fn_banks.bankacc, fn_banks.bankbranch, fn_banks.remarks, fn_banks.createdby, fn_banks.createdon, fn_banks.lasteditedby, fn_banks.lasteditedon, fn_banks.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$banks->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($banks->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->bankid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	
	</tr>
	</div>
	<tr>
		<td align="right">Cheque No : </td>
		<td><input type="text" name="chequeno" id="chequeno" value="<?php echo $obj->chequeno; ?>"></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
include "../../../foot.php";

if(!empty($error)){
	showError($error);
}

if($saved=="Yes"){
	?>	
    <script language="javascript1.1" type="text/javascript">Clickheretoprint();</script>
    <?
    redirect("addcustomerpayments_proc.php?id=".$customerpayments->id."&error=".$error);
  }
?>