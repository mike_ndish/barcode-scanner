<?php 
require_once("ExpensecategorysDBO.php");
class Expensecategorys
{				
	var $id;			
	var $name;			
	var $remarks;			
	var $expensecategorysDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->name=str_replace("'","\'",$obj->name);
		$this->remarks=str_replace("'","\'",$obj->remarks);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get name
	function getName(){
		return $this->name;
	}
	//set name
	function setName($name){
		$this->name=$name;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	function add($obj){
		$expensecategorysDBO = new ExpensecategorysDBO();
		if($expensecategorysDBO->persist($obj)){
			$this->id=$expensecategorysDBO->id;
			$this->sql=$expensecategorysDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$expensecategorysDBO = new ExpensecategorysDBO();
		if($expensecategorysDBO->update($obj,$where)){
			$this->sql=$expensecategorysDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$expensecategorysDBO = new ExpensecategorysDBO();
		if($expensecategorysDBO->delete($obj,$where=""))		
			$this->sql=$expensecategorysDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$expensecategorysDBO = new ExpensecategorysDBO();
		$this->table=$expensecategorysDBO->table;
		$expensecategorysDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$expensecategorysDBO->sql;
		$this->result=$expensecategorysDBO->result;
		$this->fetchObject=$expensecategorysDBO->fetchObject;
		$this->affectedRows=$expensecategorysDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->name)){
			$error="Expense Category should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
