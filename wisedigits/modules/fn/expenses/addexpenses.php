<title>WiseDigits: Expenses </title>
<?php 
include "../../../headerpop.php";

?>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 </script>

<div class='main'>
<form class="forms" id="theform" action="addexpenses_proc.php" name="expenses" method="POST" enctype="multipart/form-data">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
	<tr>
		<td colspan="2"><input type="hidden" name="id" id="id" value="<?php echo $obj->id; ?>"></td>
	</tr>
	<tr>
		<td align="right">Name : </td>
		<td><input type="text" name="name" id="name" size="45"  value="<?php echo $obj->name; ?>"></td>
	</tr>
	<tr>
		<td align="right">Code : </td>
		<td><input type="text" name="code" id="code" value="<?php echo $obj->code; ?>"></td>
	</tr>
	<tr>
		<td align="right">Expense Type : </td>
			<td><select name="expensetypeid">
<option value="">Select...</option>
<?php
	$expensetypes=new Expensetypes();
	$where="  ";
	$fields="fn_expensetypes.id, fn_expensetypes.name, fn_expensetypes.remarks, fn_expensetypes.ipaddress, fn_expensetypes.createdby, fn_expensetypes.createdon, fn_expensetypes.lasteditedby, fn_expensetypes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$expensetypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($expensetypes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->expensetypeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">Expense Category : </td>
			<td><select name="expensecategoryid">
<option value="">Select...</option>
<?php
	$expensecategorys=new Expensecategorys();
	$where="  ";
	$fields="fn_expensecategorys.id, fn_expensecategorys.name, fn_expensecategorys.remarks, fn_expensecategorys.ipaddress, fn_expensecategorys.createdby, fn_expensecategorys.createdon, fn_expensecategorys.lasteditedby, fn_expensecategorys.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$expensecategorys->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($expensecategorys->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->expensecategoryid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">Description : </td>
		<td><textarea name="description"><?php echo $obj->description; ?></textarea></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
if(!empty($error)){
	showError($error);
}
?>