<?php 
require_once("ImprestaccountsDBO.php");
class Imprestaccounts
{				
	var $id;			
	var $name;			
	var $employeeid;			
	var $remarks;			
	var $ipaddress;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $imprestaccountsDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->name=str_replace("'","\'",$obj->name);
		if(empty($obj->employeeid))
			$obj->employeeid='NULL';
		$this->employeeid=$obj->employeeid;
		$this->remarks=str_replace("'","\'",$obj->remarks);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get name
	function getName(){
		return $this->name;
	}
	//set name
	function setName($name){
		$this->name=$name;
	}

	//get employeeid
	function getEmployeeid(){
		return $this->employeeid;
	}
	//set employeeid
	function setEmployeeid($employeeid){
		$this->employeeid=$employeeid;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj){
		$imprestaccountsDBO = new ImprestaccountsDBO();
		if($imprestaccountsDBO->persist($obj)){
			$this->id=$imprestaccountsDBO->id;
			$this->sql=$imprestaccountsDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$imprestaccountsDBO = new ImprestaccountsDBO();
		if($imprestaccountsDBO->update($obj,$where)){
			$this->sql=$imprestaccountsDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$imprestaccountsDBO = new ImprestaccountsDBO();
		if($imprestaccountsDBO->delete($obj,$where=""))		
			$this->sql=$imprestaccountsDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$imprestaccountsDBO = new ImprestaccountsDBO();
		$this->table=$imprestaccountsDBO->table;
		$imprestaccountsDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$imprestaccountsDBO->sql;
		$this->result=$imprestaccountsDBO->result;
		$this->fetchObject=$imprestaccountsDBO->fetchObject;
		$this->affectedRows=$imprestaccountsDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->name)){
			$error="Account Name should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
