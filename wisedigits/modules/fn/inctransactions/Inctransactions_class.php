<?php 
require_once("InctransactionsDBO.php");
class Inctransactions
{				
	var $id;			
	var $incomeid;			
	var $amount;			
	var $paymentmodeid;			
	var $bank;			
	var $chequeno;			
	var $incomedate;			
	var $remarks;			
	var $memo;			
	var $drawer;			
	var $jvno;			
	var $documentno;			
	var $ipaddress;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $inctransactionsDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		if(empty($obj->incomeid))
			$obj->incomeid='NULL';
		$this->incomeid=$obj->incomeid;
		$this->amount=str_replace("'","\'",$obj->amount);
		if(empty($obj->paymentmodeid))
			$obj->paymentmodeid='NULL';
		$this->paymentmodeid=$obj->paymentmodeid;
		$this->bank=str_replace("'","\'",$obj->bank);
		$this->chequeno=str_replace("'","\'",$obj->chequeno);
		$this->incomedate=str_replace("'","\'",$obj->incomedate);
		$this->remarks=str_replace("'","\'",$obj->remarks);
		$this->memo=str_replace("'","\'",$obj->memo);
		$this->drawer=str_replace("'","\'",$obj->drawer);
		$this->jvno=str_replace("'","\'",$obj->jvno);
		$this->documentno=str_replace("'","\'",$obj->documentno);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get incomeid
	function getIncomeid(){
		return $this->incomeid;
	}
	//set incomeid
	function setIncomeid($incomeid){
		$this->incomeid=$incomeid;
	}

	//get amount
	function getAmount(){
		return $this->amount;
	}
	//set amount
	function setAmount($amount){
		$this->amount=$amount;
	}

	//get paymentmodeid
	function getPaymentmodeid(){
		return $this->paymentmodeid;
	}
	//set paymentmodeid
	function setPaymentmodeid($paymentmodeid){
		$this->paymentmodeid=$paymentmodeid;
	}

	//get bank
	function getBank(){
		return $this->bank;
	}
	//set bank
	function setBank($bank){
		$this->bank=$bank;
	}

	//get chequeno
	function getChequeno(){
		return $this->chequeno;
	}
	//set chequeno
	function setChequeno($chequeno){
		$this->chequeno=$chequeno;
	}

	//get incomedate
	function getIncomedate(){
		return $this->incomedate;
	}
	//set incomedate
	function setIncomedate($incomedate){
		$this->incomedate=$incomedate;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	//get memo
	function getMemo(){
		return $this->memo;
	}
	//set memo
	function setMemo($memo){
		$this->memo=$memo;
	}

	//get drawer
	function getDrawer(){
		return $this->drawer;
	}
	//set drawer
	function setDrawer($drawer){
		$this->drawer=$drawer;
	}

	//get jvno
	function getJvno(){
		return $this->jvno;
	}
	//set jvno
	function setJvno($jvno){
		$this->jvno=$jvno;
	}

	//get documentno
	function getDocumentno(){
		return $this->documentno;
	}
	//set documentno
	function setDocumentno($documentno){
		$this->documentno=$documentno;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj){
		$inctransactionsDBO = new InctransactionsDBO();
		if($inctransactionsDBO->persist($obj)){
			$this->id=$inctransactionsDBO->id;
			$this->sql=$inctransactionsDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$inctransactionsDBO = new InctransactionsDBO();
		if($inctransactionsDBO->update($obj,$where)){
			$this->sql=$inctransactionsDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$inctransactionsDBO = new InctransactionsDBO();
		if($inctransactionsDBO->delete($obj,$where=""))		
			$this->sql=$inctransactionsDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$inctransactionsDBO = new InctransactionsDBO();
		$this->table=$inctransactionsDBO->table;
		$inctransactionsDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$inctransactionsDBO->sql;
		$this->result=$inctransactionsDBO->result;
		$this->fetchObject=$inctransactionsDBO->fetchObject;
		$this->affectedRows=$inctransactionsDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->paymentmodeid)){
			$error="Mode Of Payment should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
		if(empty($obj->paymentmodeid)){
			$error="Mode Of Payment should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}
}				
?>
