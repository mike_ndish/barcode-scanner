<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Inctransactions_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../fn/incomes/Incomes_class.php");
require_once("../../sys/paymentmodes/Paymentmodes_class.php");
//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="769";//Edit
}
else{
	$auth->roleid="767";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
	
if($obj->action=="Save"){
	$inctransactions=new Inctransactions();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$error=$inctransactions->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$inctransactions=$inctransactions->setObject($obj);
		if($inctransactions->add($inctransactions)){
			$error=SUCCESS;
			redirect("addinctransactions_proc.php?id=".$inctransactions->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$inctransactions=new Inctransactions();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$inctransactions->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$inctransactions=$inctransactions->setObject($obj);
		if($inctransactions->edit($inctransactions)){
			$error=UPDATESUCCESS;
			redirect("addinctransactions_proc.php?id=".$inctransactions->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if(empty($obj->action)){

	$incomes= new Incomes();
	$fields="fn_incomes.id, fn_incomes.name, fn_incomes.code, fn_incomes.remarks, fn_incomes.ipaddress, fn_incomes.createdby, fn_incomes.createdon, fn_incomes.lasteditedby, fn_incomes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$incomes->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$paymentmodes= new Paymentmodes();
	$fields="sys_paymentmodes.id, sys_paymentmodes.name, sys_paymentmodes.remarks";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$paymentmodes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$inctransactions=new Inctransactions();
	$where=" where id=$id ";
	$fields="fn_inctransactions.id, fn_inctransactions.incomeid, fn_inctransactions.amount, fn_inctransactions.paymentmodeid, fn_inctransactions.bank, fn_inctransactions.chequeno, fn_inctransactions.incomedate, fn_inctransactions.remarks, fn_inctransactions.memo, fn_inctransactions.drawer, fn_inctransactions.jvno, fn_inctransactions.documentno, fn_inctransactions.ipaddress, fn_inctransactions.createdby, fn_inctransactions.createdon, fn_inctransactions.lasteditedby, fn_inctransactions.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$inctransactions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$inctransactions->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Inctransactions ";
include "addinctransactions.php";
?>