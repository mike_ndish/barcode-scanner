<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Inctransactions_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Inctransactions";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="768";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$inctransactions=new Inctransactions();
if(!empty($delid)){
	$inctransactions->id=$delid;
	$inctransactions->delete($inctransactions);
	redirect("inctransactions.php");
}
//Authorization.
$auth->roleid="767";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <input onclick="showPopWin('addinctransactions_proc.php',600,430);" value="Add Inctransactions " type="button"/></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Income </th>
			<th>Amount </th>
			<th>Mode Of Payment </th>
			<th>Bank </th>
			<th>Cheque No. </th>
			<th>Income Date </th>
			<th>Remarks </th>
			<th>Memo </th>
			<th>Drawer </th>
			<th>JV No. </th>
			<th>Document No. </th>
<?php
//Authorization.
$auth->roleid="769";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="770";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="fn_inctransactions.id, fn_incomes.name as incomeid, fn_inctransactions.amount, sys_paymentmodes.name as paymentmodeid, fn_inctransactions.bank, fn_inctransactions.chequeno, fn_inctransactions.incomedate, fn_inctransactions.remarks, fn_inctransactions.memo, fn_inctransactions.drawer, fn_inctransactions.jvno, fn_inctransactions.documentno, fn_inctransactions.ipaddress, fn_inctransactions.createdby, fn_inctransactions.createdon, fn_inctransactions.lasteditedby, fn_inctransactions.lasteditedon";
		$join=" left join fn_incomes on fn_inctransactions.incomeid=fn_incomes.id  left join sys_paymentmodes on fn_inctransactions.paymentmodeid=sys_paymentmodes.id ";
		$having="";
		$groupby="";
		$orderby="";
		$inctransactions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$inctransactions->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->incomeid; ?></td>
			<td><?php echo formatNumber($row->amount); ?></td>
			<td><?php echo $row->paymentmodeid; ?></td>
			<td><?php echo $row->bank; ?></td>
			<td><?php echo $row->chequeno; ?></td>
			<td><?php echo formatDate($row->incomedate); ?></td>
			<td><?php echo $row->remarks; ?></td>
			<td><?php echo $row->memo; ?></td>
			<td><?php echo $row->drawer; ?></td>
			<td><?php echo $row->jvno; ?></td>
			<td><?php echo $row->documentno; ?></td>
<?php
//Authorization.
$auth->roleid="769";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addinctransactions_proc.php?id=<?php echo $row->id; ?>',600,430);">View</a></td>
<?php
}
//Authorization.
$auth->roleid="770";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='inctransactions.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
