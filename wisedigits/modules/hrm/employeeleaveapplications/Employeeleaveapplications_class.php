<?php 
require_once("EmployeeleaveapplicationsDBO.php");
class Employeeleaveapplications
{				
	var $id;			
	var $employeeid;			
	var $leaveid;			
	var $startdate;			
	var $duration;			
	var $appliedon;			
	var $status;			
	var $remarks;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $employeeleaveapplicationsDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->employeeid=str_replace("'","\'",$obj->employeeid);
		$this->leaveid=str_replace("'","\'",$obj->leaveid);
		$this->startdate=str_replace("'","\'",$obj->startdate);
		$this->duration=str_replace("'","\'",$obj->duration);
		$this->appliedon=str_replace("'","\'",$obj->appliedon);
		$this->status=str_replace("'","\'",$obj->status);
		$this->remarks=str_replace("'","\'",$obj->remarks);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get employeeid
	function getEmployeeid(){
		return $this->employeeid;
	}
	//set employeeid
	function setEmployeeid($employeeid){
		$this->employeeid=$employeeid;
	}

	//get leaveid
	function getLeaveid(){
		return $this->leaveid;
	}
	//set leaveid
	function setLeaveid($leaveid){
		$this->leaveid=$leaveid;
	}

	//get startdate
	function getStartdate(){
		return $this->startdate;
	}
	//set startdate
	function setStartdate($startdate){
		$this->startdate=$startdate;
	}

	//get duration
	function getDuration(){
		return $this->duration;
	}
	//set duration
	function setDuration($duration){
		$this->duration=$duration;
	}

	//get appliedon
	function getAppliedon(){
		return $this->appliedon;
	}
	//set appliedon
	function setAppliedon($appliedon){
		$this->appliedon=$appliedon;
	}

	//get status
	function getStatus(){
		return $this->status;
	}
	//set status
	function setStatus($status){
		$this->status=$status;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj){
		$employeeleaveapplicationsDBO = new EmployeeleaveapplicationsDBO();
		if($employeeleaveapplicationsDBO->persist($obj)){
			$this->id=$employeeleaveapplicationsDBO->id;
			$this->sql=$employeeleaveapplicationsDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$employeeleaveapplicationsDBO = new EmployeeleaveapplicationsDBO();
		if($employeeleaveapplicationsDBO->update($obj,$where)){
			$this->sql=$employeeleaveapplicationsDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$employeeleaveapplicationsDBO = new EmployeeleaveapplicationsDBO();
		if($employeeleaveapplicationsDBO->delete($obj,$where=""))		
			$this->sql=$employeeleaveapplicationsDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$employeeleaveapplicationsDBO = new EmployeeleaveapplicationsDBO();
		$this->table=$employeeleaveapplicationsDBO->table;
		$employeeleaveapplicationsDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$employeeleaveapplicationsDBO->sql;
		$this->result=$employeeleaveapplicationsDBO->result;
		$this->fetchObject=$employeeleaveapplicationsDBO->fetchObject;
		$this->affectedRows=$employeeleaveapplicationsDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->employeeid)){
			$error="Employee should be provided";
		}
		else if(empty($obj->leaveid)){
			$error="Type of Leave should be provided";
		}
		else if(empty($obj->startdate)){
			$error="Start Date should be provided";
		}
		else if(empty($obj->duration)){
			$error="Duration (Working Days) should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
