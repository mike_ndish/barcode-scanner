<?php 
require_once("EmployeeleavesDBO.php");
class Employeeleaves
{				
	var $id;			
	var $employeeid;
	var $leaveid;
	var $startdate;			
	var $duration;	
	var $remarks;
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $employeeleavesDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->employeeid=str_replace("'","\'",$obj->employeeid);
		$this->leaveid=str_replace("'","\'",$obj->leaveid);
		$this->startdate=str_replace("'","\'",$obj->startdate);
		$this->duration=str_replace("'","\'",$obj->duration);
		$this->remarks=str_replace("'","\'",$obj->remarks);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get employeeleaveapplicationid
	function getEmployeeleaveapplicationid(){
		return $this->employeeleaveapplicationid;
	}
	//set employeeleaveapplicationid
	function setEmployeeleaveapplicationid($employeeleaveapplicationid){
		$this->employeeleaveapplicationid=$employeeleaveapplicationid;
	}

	//get startdate
	function getStartdate(){
		return $this->startdate;
	}
	//set startdate
	function setStartdate($startdate){
		$this->startdate=$startdate;
	}

	//get duration
	function getDuration(){
		return $this->duration;
	}
	//set duration
	function setDuration($duration){
		$this->duration=$duration;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj){
		$employeeleavesDBO = new EmployeeleavesDBO();
		if($employeeleavesDBO->persist($obj)){
			$this->id=$employeeleavesDBO->id;
			$this->sql=$employeeleavesDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$employeeleavesDBO = new EmployeeleavesDBO();
		if($employeeleavesDBO->update($obj,$where)){
			$this->sql=$employeeleavesDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$employeeleavesDBO = new EmployeeleavesDBO();
		if($employeeleavesDBO->delete($obj,$where=""))		
			$this->sql=$employeeleavesDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$employeeleavesDBO = new EmployeeleavesDBO();
		$this->table=$employeeleavesDBO->table;
		$employeeleavesDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$employeeleavesDBO->sql;
		$this->result=$employeeleavesDBO->result;
		$this->fetchObject=$employeeleavesDBO->fetchObject;
		$this->affectedRows=$employeeleavesDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->employeeid)){
			$error="Employee should be provided";
		}
		else if(empty($obj->duration)) {
		
		$error="Duration Should Be Provided";
		
		}
		else if(empty($obj->leaveid)) {
		
		$error="Leave Type should be provided";
		
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
