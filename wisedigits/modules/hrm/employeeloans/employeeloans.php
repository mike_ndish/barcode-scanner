<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Employeeloans_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
// 	$loanid=$_GET['loanid'];
// 	$where=" where loanid='$loanid'";

$page_title="Employeeloans";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="1136";//View
$auth->levelid=$_SESSION['level'];

$ob = (object)$_GET;

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$employeeloans=new Employeeloans();
if(!empty($delid)){
	$employeeloans->id=$delid;
	$employeeloans->delete($employeeloans);
	redirect("employeeloans.php");
}
//Authorization.
$auth->roleid="1135";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div class="container">
<hr>
<a class="btn btn-info" onclick="showPopWin('addemployeeloans_proc.php?loanid=<?php echo $loanid; ?>',600,430);">Add Employeeloans</a>
<?php }?>
<hr>
<table style="clear:both;" class="table table-bordered table-condensed table-hover table-striped" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Loan </th>
			<th>Employee </th>
			<th>Principal </th>
			<th>Method </th>
			<th>Initial Value </th>
			<th>Payable </th>
			<th>Duration </th>
			<th> </th>
			<th>Interest </th>
			<th>Month </th>
			<th>Year </th>
<?php
//Authorization.
$auth->roleid="1137";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="1138";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<!--<th>&nbsp;</th>-->
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="hrm_employeeloans.id, hrm_loans.name as loanid, concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname)) as employeeid, hrm_employeeloans.principal, hrm_employeeloans.method, hrm_employeeloans.initialvalue, hrm_employeeloans.payable, hrm_employeeloans.duration, hrm_employeeloans.interesttype, hrm_employeeloans.interest, hrm_employeeloans.month, hrm_employeeloans.year, hrm_employeeloans.createdby, hrm_employeeloans.createdon, hrm_employeeloans.lasteditedby, hrm_employeeloans.lasteditedon, hrm_employeeloans.ipaddress";
		$join=" left join hrm_loans on hrm_employeeloans.loanid=hrm_loans.id  left join hrm_employees on hrm_employeeloans.employeeid=hrm_employees.id ";
		$having="";
		$groupby="";
		$orderby="";
		$where="";
		if(!empty($ob->loanid))
		  $where=" where hrm_employeeloans.loanid='$ob->loanid'";
		$employeeloans->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$employeeloans->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->loanid; ?></td>
			<td><?php echo $row->employeeid; ?></td>
			<td><?php echo formatNumber($row->principal); ?></td>
			<td><?php echo $row->method; ?></td>
			<td><?php echo formatNumber($row->initialvalue); ?></td>
			<td><?php echo formatNumber($row->payable); ?></td>
			<td><?php echo formatNumber($row->duration); ?></td>
			<td><?php echo $row->interesttype; ?></td>
			<td><?php echo formatNumber($row->interest); ?></td>
			<td><?php echo $row->month; ?></td>
			<td><?php echo $row->year; ?></td>
<?php
//Authorization.
$auth->roleid="1137";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addemployeeloans_proc.php?id=<?php echo $row->id; ?>',600,430);">View</a></td>
<?php
}
//Authorization.
$auth->roleid="1138";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<!--<td><a href='employeeloans.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>-->
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<hr>
</div>
<?php
include"../../../foot.php";
?>
