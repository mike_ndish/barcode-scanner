<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Employeepaiddeductions_class.php");
require_once("../../auth/rules/Rules_class.php");
require_once("../deductions/Deductions_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Employeepaiddeductions";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="1145";//View
$auth->levelid=$_SESSION['level'];

$obj = (object)$_POST;
$ob = (object)$_GET;

if(!empty($ob->deductionid)){
  $obj->deductionid=$ob->deductionid;
}

if(empty($obj->action)){
  $obj->month=date("m");
  $obj->year=date("Y");
}

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$employeepaiddeductions=new Employeepaiddeductions();
if(!empty($delid)){
	$employeepaiddeductions->id=$delid;
	$employeepaiddeductions->delete($employeepaiddeductions);
	redirect("employeepaiddeductions.php");
}

$deductions = new Deductions();
$fields="*";
$where=" where id='$obj->deductionid'";
$join="";
$orderby="";
$groupby="";
$having="";
$deductions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
$deductions = $deductions->fetchObject;

//Authorization.
$auth->roleid="1144";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

	//TableToolsInit.sSwfPath = "../../../media/swf/ZeroClipboard.swf";
	TableTools.DEFAULTS.aButtons = [ "copy", "csv", "xls","pdf" ];
 	$('#tbl').dataTable( {
		"sDom": 'T<"H"lfr>t<"F"ip>',
		"oTableTools": {
			"sSwfPath": "../../../media/swf/copy_cvs_xls_pdf.swf"
		},
 		"bJQueryUI": true,
		"sScrollY": 500,
		"iDisplayLength":20,
		"sPaginationType": "full_numbers"
	} );
} );
</script> 
<!-- <div style="float:left;" class="buttons"> <input onclick="showPopWin('addemployeepaiddeductions_proc.php',600,430);" value="Add Employeepaiddeductions " type="button"/></div> -->
<?php }?>
<form action="" method="post">
<table>
<tr>
<td><h2><?php echo $deductions->name; ?></h2></td>
</tr>
  <tr>
    <td>Month:<input type="hidden" name="deductionid" value="<?php echo $obj->deductionid; ?>"/> 
    <select name="month" id="month" class="selectbox">
        <option value="">Select...</option>
        <option value="1" <?php if($obj->month==1){echo"selected";}?>>January</option>
        <option value="2" <?php if($obj->month==2){echo"selected";}?>>February</option>
        <option value="3" <?php if($obj->month==3){echo"selected";}?>>March</option>
        <option value="4" <?php if($obj->month==4){echo"selected";}?>>April</option>
        <option value="5" <?php if($obj->month==5){echo"selected";}?>>May</option>
        <option value="6" <?php if($obj->month==6){echo"selected";}?>>June</option>
        <option value="7" <?php if($obj->month==7){echo"selected";}?>>July</option>
        <option value="8" <?php if($obj->month==8){echo"selected";}?>>August</option>
        <option value="9" <?php if($obj->month==9){echo"selected";}?>>September</option>
        <option value="10" <?php if($obj->month==10){echo"selected";}?>>October</option>
        <option value="11" <?php if($obj->month==11){echo"selected";}?>>November</option>
        <option value="12" <?php if($obj->month==12){echo"selected";}?>>December</option>
      </select>
    Year:
    <select name="year" id="year" class="selectbox">
          <option value="">Select...</option>
          <?php
	  $i=date("Y")-10;
	  while($i<date("Y")+10)
	  {
		?>
		  <option value="<?php echo $i; ?>" <?php if($obj->year==$i){echo"selected";}?>><?php echo $i; ?></option>
		  <?
	    $i++;
	  }
	  ?>
        </select>&nbsp;<input type="submit" name="action" value="Filter"/></td>   
  </tr>
</table>
</form>
<table style="clear:both;"  class="tgrid display" id="tbl" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>PF NUM </th>
			<th>Employee </th>
			<th>IDNO </th>
			<th>PIN No </th>
			<th>NSSF No </th>
			<th>NHIF No </th>
			<th>Amount </th>
<?php
//Authorization.
$auth->roleid="1146";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="4172";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="hrm_employeepaiddeductions.id, hrm_employeepaiddeductions.employeepaymentid, hrm_deductions.name as deductionid, hrm_loans.name as loanid, hrm_employees.name as employeeid, hrm_employeepaiddeductions.amount, hrm_employeepaiddeductions.month, hrm_employeepaiddeductions.year, hrm_employeepaiddeductions.paidon, hrm_employeepaiddeductions.createdby, hrm_employeepaiddeductions.createdon, hrm_employeepaiddeductions.lasteditedby, hrm_employeepaiddeductions.lasteditedon, hrm_employeepaiddeductions.ipaddress";
		$join=" left join hrm_deductions on hrm_employeepaiddeductions.deductionid=hrm_deductions.id  left join hrm_loans on hrm_employeepaiddeductions.loanid=hrm_loans.id  left join hrm_employees on hrm_employeepaiddeductions.employeeid=hrm_employees.id ";
		$having="";
		$groupby="";
		$orderby="";
		$where=" where hrm_employeepaiddeductions.deductionid='$obj->deductionid'";
		$employeepaiddeductions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$employeepaiddeductions->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->pfnum; ?></td>
			<td><?php echo $row->employeeid; ?></td>
			<td><?php echo $row->idno; ?></td>
			<td><?php echo $row->pinno; ?></td>
			<td><?php echo $row->nssfno; ?></td>
			<td><?php echo $row->nhifno; ?></td>
			<td><?php echo formatNumber($row->amount); ?></td>
<?php
//Authorization.
$auth->roleid="1146";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addemployeepaiddeductions_proc.php?id=<?php echo $row->id; ?>',600,430);">View</a></td>
<?php
}
//Authorization.
$auth->roleid="4172";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='employeepaiddeductions.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
