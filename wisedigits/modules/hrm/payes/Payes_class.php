<?php 
require_once("PayesDBO.php");
class Payes
{				
	var $id;			
	var $low;			
	var $high;			
	var $percent;			
	var $payesDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->low=str_replace("'","\'",$obj->low);
		$this->high=str_replace("'","\'",$obj->high);
		$this->percent=str_replace("'","\'",$obj->percent);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get low
	function getLow(){
		return $this->low;
	}
	//set low
	function setLow($low){
		$this->low=$low;
	}

	//get high
	function getHigh(){
		return $this->high;
	}
	//set high
	function setHigh($high){
		$this->high=$high;
	}

	//get percent
	function getPercent(){
		return $this->percent;
	}
	//set percent
	function setPercent($percent){
		$this->percent=$percent;
	}

	function add($obj){
		$payesDBO = new PayesDBO();
		if($payesDBO->persist($obj)){
			$this->id=$payesDBO->id;
			$this->sql=$payesDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$payesDBO = new PayesDBO();
		if($payesDBO->update($obj,$where)){
			$this->sql=$payesDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$payesDBO = new PayesDBO();
		if($payesDBO->delete($obj,$where=""))		
			$this->sql=$payesDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$payesDBO = new PayesDBO();
		$this->table=$payesDBO->table;
		$payesDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$payesDBO->sql;
		$this->result=$payesDBO->result;
		$this->fetchObject=$payesDBO->fetchObject;
		$this->affectedRows=$payesDBO->affectedRows;
	}	

	function getPAYE($taxable,$id){
		$paye=0;
		$payes = new Payes();
		$fields="*";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$where="";
		$payes->retrieve($fields, $join, $where, $having, $groupby, $orderby);
		while($py=mysql_fetch_object($payes->result)){
			//if taxable is greater than low and less than high
			if($taxable>=$py->low and $taxable>=$py->high and $py->high>0 ){
				$paye+=($py->high-$py->low)*$py->percent;
				//break;
			}
			elseif($taxable>=$py->low and $taxable<$py->high and $py->high>0 ){
				$paye+=($taxable-$py->low)*$py->percent;
				//break;
			}
			elseif($taxable>=$py->low and $taxable>$py->high and $py->high==0 ){
				$paye+=($taxable-$py->low)*$py->percent;
				//break;
			}
			
		}
		$reliefs = new Reliefs();
		$fields="*";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$where="";
		$reliefs->retrieve($fields, $join, $where, $having, $groupby, $orderby);
		$reliefs = $reliefs->fetchObject;
		$paye=$paye-$reliefs->amount;
		
		$employeereliefs = new Employeereliefs();
		$fields="sum(amount) amount";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$where=" where employeeid='$id'";
		$employeereliefs->retrieve($fields, $join, $where, $having, $groupby, $orderby);
		$employeereliefs = $employeereliefs->fetchObject;
		$paye=$paye-$employeereliefs->amount;
		
		if($paye<0)
			$paye=0;
		return $paye;
	}
	
	function validate($obj){
	
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
