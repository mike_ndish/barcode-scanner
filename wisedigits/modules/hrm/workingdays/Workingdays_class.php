<?php 
require_once("WorkingdaysDBO.php");
class Workingdays
{				
	var $id;			
	var $employeeid;			
	var $days;			
	var $daysbf;			
	var $othours;			
	var $month;			
	var $year;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $workingdaysDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->employeeid=str_replace("'","\'",$obj->employeeid);
		$this->days=str_replace("'","\'",$obj->days);
		$this->daysbf=str_replace("'","\'",$obj->daysbf);
		$this->othours=str_replace("'","\'",$obj->othours);
		$this->month=str_replace("'","\'",$obj->month);
		$this->year=str_replace("'","\'",$obj->year);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get employeeid
	function getEmployeeid(){
		return $this->employeeid;
	}
	//set employeeid
	function setEmployeeid($employeeid){
		$this->employeeid=$employeeid;
	}

	//get days
	function getDays(){
		return $this->days;
	}
	//set days
	function setDays($days){
		$this->days=$days;
	}

	//get daysbf
	function getDaysbf(){
		return $this->daysbf;
	}
	//set daysbf
	function setDaysbf($daysbf){
		$this->daysbf=$daysbf;
	}

	//get othours
	function getOthours(){
		return $this->othours;
	}
	//set othours
	function setOthours($othours){
		$this->othours=$othours;
	}

	//get month
	function getMonth(){
		return $this->month;
	}
	//set month
	function setMonth($month){
		$this->month=$month;
	}

	//get year
	function getYear(){
		return $this->year;
	}
	//set year
	function setYear($year){
		$this->year=$year;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj){
		$workingdaysDBO = new WorkingdaysDBO();
		if($workingdaysDBO->persist($obj)){
			$this->id=$workingdaysDBO->id;
			$this->sql=$workingdaysDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$workingdaysDBO = new WorkingdaysDBO();
		if($workingdaysDBO->update($obj,$where)){
			$this->sql=$workingdaysDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$workingdaysDBO = new WorkingdaysDBO();
		if($workingdaysDBO->delete($obj,$where=""))		
			$this->sql=$workingdaysDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$workingdaysDBO = new WorkingdaysDBO();
		$this->table=$workingdaysDBO->table;
		$workingdaysDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$workingdaysDBO->sql;
		$this->result=$workingdaysDBO->result;
		$this->fetchObject=$workingdaysDBO->fetchObject;
		$this->affectedRows=$workingdaysDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->daysbf)){
			$error="Days B/F should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
