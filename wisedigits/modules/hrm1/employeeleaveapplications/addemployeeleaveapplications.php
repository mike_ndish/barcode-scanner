<title>WiseDigits: Employeeleaveapplications </title>
<?php 
include "../../../head.php";

?>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 </script>

<div class='main'>
<form class="forms" id="theform" action="addemployeeleaveapplications_proc.php" name="employeeleaveapplications" method="POST" enctype="multipart/form-data">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
	<tr>
		<td colspan="2"><input type="hidden" name="id" id="id" value="<?php echo $obj->id; ?>"></td>
	</tr>
	<tr>
		<td align="right">Employee : </td>
			<td>
			<?php 
			$employees=new Employees();
			$where=" where auth_users.id='".$_SESSION['userid']."'  ";
			$fields="hrm_employees.id, hrm_employees.pfnum, concat(concat(hrm_employees.firstname,' ',hrm_employees.middlename),' ',hrm_employees.lastname) name, hrm_employees.gender, hrm_employees.dob, hrm_employees.idno, hrm_employees.passportno, hrm_employees.phoneno, hrm_employees.email, hrm_employees.physicaladdress, hrm_employees.nationalityid, hrm_employees.countyid, hrm_employees.marital, hrm_employees.spouse, hrm_employees.nssfno, hrm_employees.nhifno, hrm_employees.pinno, hrm_employees.helbno, hrm_employees.bankacc, hrm_employees.clearingcode, hrm_employees.ref, hrm_employees.basic, hrm_employees.assignmentid, hrm_employees.gradeid, hrm_employees.statusid, hrm_employees.image, hrm_employees.createdby, hrm_employees.createdon, hrm_employees.lasteditedby, hrm_employees.lasteditedon";
			$join=" left join auth_users on auth_users.employeeid=hrm_employees.id ";
			$having="";
			$groupby="";
			$orderby="";
			$employees->retrieve($fields,$join,$where,$having,$groupby,$orderby);echo mysql_error();
			$employees=$employees->fetchObject;
			?>
			<input type="hidden" name="employeeid" value="<?php echo $employees->id; ?>"/>
			<font color='red' ><strong><?php echo $employees->pfnum; ?>&nbsp;<?php echo initialCap($employees->name); ?></strong></font>
		</td>
	</tr>
	<tr>
		<td align="right">Type Of Leave : </td>
			<td><select name="leaveid" class="selectbox">
<option value="">Select...</option>
<?php
	$leaves=new Leaves();
	$where="  ";
	$fields="hrm_leaves.id, hrm_leaves.name, hrm_leaves.days, hrm_leaves.remarks, hrm_leaves.createdby, hrm_leaves.createdon, hrm_leaves.lasteditedby, hrm_leaves.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$leaves->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($leaves->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->leaveid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select><font color='red'>*</font>
		</td>
	</tr>
	<tr>
		<td align="right">Start Date : </td>
		<td><input type="text" name="startdate" id="startdate" class="date_input" size="12" readonly  value="<?php echo $obj->startdate; ?>"><font color='red'>*</font></td>
	</tr>
	<tr>
		<td align="right">Duration (Working Days) : </td>
		<td><input type="text" name="duration" id="duration" value="<?php echo $obj->duration; ?>"><font color='red'>*</font></td>
	</tr>
	<tr>
		<td align="right">Date Applied : </td>
		<td><input type="text" name="appliedon" id="appliedon" class="date_input" size="12" readonly  value="<?php echo $obj->appliedon; ?>"/>
		<input type="hidden" name='status' value="<?php echo $obj->status; ?>"/></td>
	</tr>
	
	<tr>
		<td align="right">Remarks : </td>
		<td><textarea name="remarks"><?php echo $obj->remarks; ?></textarea></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
if(!empty($error)){
	showError($error);
}
?>