<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Employeeleaveapplications_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../hrm/employees/Employees_class.php");
require_once("../../hrm/leaves/Leaves_class.php");
//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="4231";//Edit
}
else{
	$auth->roleid="4229";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
	
	
if($obj->action=="Save"){
	$employeeleaveapplications=new Employeeleaveapplications();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$error=$employeeleaveapplications->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$employeeleaveapplications=$employeeleaveapplications->setObject($obj);
		if($employeeleaveapplications->add($employeeleaveapplications)){
			$error=SUCCESS;
			redirect("addemployeeleaveapplications_proc.php?id=".$employeeleaveapplications->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$employeeleaveapplications=new Employeeleaveapplications();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$employeeleaveapplications->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$employeeleaveapplications=$employeeleaveapplications->setObject($obj);
		if($employeeleaveapplications->edit($employeeleaveapplications)){
			$error=UPDATESUCCESS;
			redirect("addemployeeleaveapplications_proc.php?id=".$employeeleaveapplications->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if(empty($obj->action)){

	$employees= new Employees();
	$fields="hrm_employees.id, hrm_employees.pfnum, hrm_employees.firstname, hrm_employees.middlename, hrm_employees.lastname, hrm_employees.gender, hrm_employees.dob, hrm_employees.idno, hrm_employees.passportno, hrm_employees.phoneno, hrm_employees.email, hrm_employees.physicaladdress, hrm_employees.nationalityid, hrm_employees.countyid, hrm_employees.marital, hrm_employees.spouse, hrm_employees.nssfno, hrm_employees.nhifno, hrm_employees.pinno, hrm_employees.helbno, hrm_employees.bankid, hrm_employees.bankbrancheid, hrm_employees.bankacc, hrm_employees.clearingcode, hrm_employees.ref, hrm_employees.basic, hrm_employees.assignmentid, hrm_employees.gradeid, hrm_employees.statusid, hrm_employees.image, hrm_employees.createdby, hrm_employees.createdon, hrm_employees.lasteditedby, hrm_employees.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$employees->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$leaves= new Leaves();
	$fields="hrm_leaves.id, hrm_leaves.name, hrm_leaves.days, hrm_leaves.remarks, hrm_leaves.createdby, hrm_leaves.createdon, hrm_leaves.lasteditedby, hrm_leaves.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$leaves->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$employeeleaveapplications=new Employeeleaveapplications();
	$where=" where id=$id ";
	$fields="hrm_employeeleaveapplications.id, hrm_employeeleaveapplications.employeeid, hrm_employeeleaveapplications.leaveid, hrm_employeeleaveapplications.startdate, hrm_employeeleaveapplications.duration, hrm_employeeleaveapplications.appliedon, hrm_employeeleaveapplications.status, hrm_employeeleaveapplications.remarks, hrm_employeeleaveapplications.createdby, hrm_employeeleaveapplications.createdon, hrm_employeeleaveapplications.lasteditedby, hrm_employeeleaveapplications.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$employeeleaveapplications->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$employeeleaveapplications->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
		$obj->appliedon=date("Y-m-d");
		$obj->status="pending";
	}
	else{
		$obj=$_SESSION['obj'];
	}
	$obj->status="pending";
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Employeeleaveapplications ";
include "addemployeeleaveapplications.php";
?>