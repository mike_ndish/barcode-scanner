<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Employeeleaves_class.php");
require_once("../../auth/rules/Rules_class.php");
require_once("../../hrm/leaves/Leaves_class.php");
// require_once("../../hrm/employeeleavedays/Employeeleavedays_class.php");

if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="4239";//Edit
}
else{
	$auth->roleid="4237";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}




$id=$_GET['id'];


$error=$_GET['error'];
	
	
if($obj->action=="Save"){
	$employeeleaves=new Employeeleaves();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$error=$employeeleaves->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$employeeleaves=$employeeleaves->setObject($obj);
		if($employeeleaves->add($employeeleaves)){
			$error=SUCCESS;
			redirect("addemployeeleaves_proc.php?id=".$employeeleaves->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$employeeleaves=new Employeeleaves();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$employeeleaves->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$employeeleaves=$employeeleaves->setObject($obj);
		if($employeeleaves->edit($employeeleaves)){
			$error=UPDATESUCCESS;
			redirect("addemployeeleaves_proc.php?id=".$employeeleaves->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}


if(!empty($id)){

      echo "here";
	$employeeleaves=new Employeeleaves();
	$where=" where id=$id ";
	$fields="hrm_employeeleaves.id, hrm_employeeleaves.employeeleaveapplicationid, hrm_employeeleaves.startdate, hrm_employeeleaves.duration, hrm_employeeleaves.createdby, hrm_employeeleaves.createdon, hrm_employeeleaves.lasteditedby, hrm_employeeleaves.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$employeeleaves->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$employeeleaves->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Employeeleaves ";
include "addemployeeleaves.php";
?>