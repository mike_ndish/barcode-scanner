<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Employeepaiddeductions_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Employeepaiddeductions";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="1145";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$employeepaiddeductions=new Employeepaiddeductions();
if(!empty($delid)){
	$employeepaiddeductions->id=$delid;
	$employeepaiddeductions->delete($employeepaiddeductions);
	redirect("employeepaiddeductions.php");
}
//Authorization.
$auth->roleid="1144";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <input onclick="showPopWin('addemployeepaiddeductions_proc.php',600,430);" value="Add Employeepaiddeductions " type="button"/></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Payment </th>
			<th>Deduction </th>
			<th>Loan </th>
			<th>Employee </th>
			<th>Amount </th>
			<th>Month </th>
			<th>Year </th>
			<th>Payment Date </th>
<?php
//Authorization.
$auth->roleid="1146";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="4172";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="hrm_employeepaiddeductions.id, hrm_employeepaiddeductions.employeepaymentid, hrm_deductions.name as deductionid, hrm_loans.name as loanid, hrm_employees.name as employeeid, hrm_employeepaiddeductions.amount, hrm_employeepaiddeductions.month, hrm_employeepaiddeductions.year, hrm_employeepaiddeductions.paidon, hrm_employeepaiddeductions.createdby, hrm_employeepaiddeductions.createdon, hrm_employeepaiddeductions.lasteditedby, hrm_employeepaiddeductions.lasteditedon, hrm_employeepaiddeductions.ipaddress";
		$join=" left join hrm_deductions on hrm_employeepaiddeductions.deductionid=hrm_deductions.id  left join hrm_loans on hrm_employeepaiddeductions.loanid=hrm_loans.id  left join hrm_employees on hrm_employeepaiddeductions.employeeid=hrm_employees.id ";
		$having="";
		$groupby="";
		$orderby="";
		$employeepaiddeductions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$employeepaiddeductions->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->employeepaymentid; ?></td>
			<td><?php echo $row->deductionid; ?></td>
			<td><?php echo $row->loanid; ?></td>
			<td><?php echo $row->employeeid; ?></td>
			<td><?php echo formatNumber($row->amount); ?></td>
			<td><?php echo $row->month; ?></td>
			<td><?php echo $row->year; ?></td>
			<td><?php echo formatDate($row->paidon); ?></td>
<?php
//Authorization.
$auth->roleid="1146";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addemployeepaiddeductions_proc.php?id=<?php echo $row->id; ?>',600,430);">View</a></td>
<?php
}
//Authorization.
$auth->roleid="4172";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='employeepaiddeductions.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
