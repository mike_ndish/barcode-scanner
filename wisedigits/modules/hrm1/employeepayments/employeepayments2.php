
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 TableTools.DEFAULTS.aButtons = [ "copy", "csv", "xls","pdf" ];
 	$('#tbl').dataTable( {
		"sDom": 'T<"H"lfr>t<"F"ip>',
		"oTableTools": {
			"sSwfPath": "../../../media/swf/copy_cvs_xls_pdf.swf"
		},
		"sDom": 'T<"H"lfr>t<"F"ip>',
		"sScrollY": 500,
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"iDisplayLength": 1000,
		"sScrollX": "100%",
		"aaSorting":[[3,"asc"]],
		"sScrollXInner": "<?php echo $inner; ?>%",
		"bScrollCollapse": true,
		"aoColumnDefs": [
		                 { "bSortable": false, "aTargets": [ 1 ] }
		               ],
		"fnDrawCallback": function ( oSettings ) {
			/* Need to redo the counters if filtered or sorted */
			if ( oSettings.bSorted || oSettings.bFiltered ) {
				for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ) {
					this.fnUpdate( i+1, oSettings.aiDisplay[i], 0, false, false );
				}
			}
		},
		"fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
			$('th:eq(0)', nRow).html("");
			$('th:eq(1)', nRow).html("TOTAL");
			var total=0;
			for(var i=0; i<aaData.length; i++){
			total=0;
			  for(var j=9; j<aaData[i].length; j++){
			    if(aaData[i][j]=='')
			      aaData[i][j]=0;
				total+=parseFloat(aaData[i][j]);
				
			  }
			  $('th:eq('+j+')', nRow).html(total);
			}
		}
	} );
} );

function selectAll(str)
{
	if(str.checked)
	{//check all checkboxes under it
		
		<?php
		$employees = new Employees();
		$fields="*";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$where=" where hrm_employees.statusid=1 and hrm_employees.id not in(select employeeid from hrm_employeepayments where month='$obj->month' and year='$obj->year')";
		$employees->retrieve($fields, $join, $where, $having, $groupby, $orderby);
		
		while($rw=mysql_fetch_object($employees->result))
		{			
		?>
		if(document.getElementById("<?php echo $rw->id; ?>")){
			//alert("Success <?php echo $rw->id; ?>");
			document.getElementById("<?php echo $rw->id; ?>").checked=true;
		}
		<?php		
		}
		?>
	}
	else
	{
		//uncheck all checkboxes under it
		<?php
		$employees = new Employees();
		$fields="*";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$where=" where hrm_employees.statusid=1 and hrm_employees.id not in(select employeeid from hrm_employeepayments where month='$obj->month' and year='$obj->year')";
		$employees->retrieve($fields, $join, $where, $having, $groupby, $orderby);
		
		while($rw=mysql_fetch_object($employees->result))
		{
		?>
		document.getElementById("<?php echo $rw->id; ?>").checked=false;
		<?php
		}
		?>
	}
}

</script>
		<form action="employeepayments_proc.php" method="post">
<div style="float:center;">
<hr>
<table align="center" id="tasktable">
  <tr>
    <td><div align="right"></div>
        <strong>Year:</strong>
        <select name="year" class="input-small">
          <option value="">Select...</option>
          <?php
  $i=date("Y")-10;
  while($i<date("Y")+10)
  {
  	?>
          <option value="<?php echo $i; ?>" <?php if($obj->year==$i){echo"selected";}?>><?php echo $i; ?></option>
          <?
    $i++;
  }
  ?>
        </select>
      &nbsp;&nbsp; <strong>Month</strong>:
      <select name="month" class="input-small">
        <option value="">Select...</option>
        <option value="1" <?php if($obj->month==1){echo"selected";}?>>January</option>
        <option value="2" <?php if($obj->month==2){echo"selected";}?>>February</option>
        <option value="3" <?php if($obj->month==3){echo"selected";}?>>March</option>
        <option value="4" <?php if($obj->month==4){echo"selected";}?>>April</option>
        <option value="5" <?php if($obj->month==5){echo"selected";}?>>May</option>
        <option value="6" <?php if($obj->month==6){echo"selected";}?>>June</option>
        <option value="7" <?php if($obj->month==7){echo"selected";}?>>July</option>
        <option value="8" <?php if($obj->month==8){echo"selected";}?>>August</option>
        <option value="9" <?php if($obj->month==9){echo"selected";}?>>September</option>
        <option value="10" <?php if($obj->month==10){echo"selected";}?>>October</option>
        <option value="11" <?php if($obj->month==11){echo"selected";}?>>November</option>
        <option value="12" <?php if($obj->month==12){echo"selected";}?>>December</option>
      </select>
      &nbsp;
      <strong>Bank:</strong>
    <select name="bankid" class="input-small">
    <option value="">Select...</option>
    <?php
    $banks=new Banks();
    $i=0;
    $fields="hrm_banks.id, hrm_banks.code, hrm_banks.name, hrm_banks.remarks, hrm_banks.createdby, hrm_banks.createdon, hrm_banks.lasteditedby, hrm_banks.lasteditedon";
    $join="";
    $having="";
    $groupby="";
    $orderby="";
    $banks->retrieve($fields,$join,$where,$having,$groupby,$orderby);
    $res=$banks->result;
    while($row=mysql_fetch_object($res))
    {
    ?>
    <option value="<?php echo $row->id; ?>" <?php if($row->id==$obj->bankid){echo"selected";}?>><?php echo $row->name; ?></option>
    <?
    }
    ?>
    
    
    </select>   
    &nbsp;
    
    Department:
    <select name="departmentid" class="input-small">
  <option value="">All</option>
  <?php
  $departments=new Departments();
  $i=0;
  $fields="hrm_departments.id, hrm_departments.name, hrm_departments.code, hrm_departments.leavemembers, hrm_departments.description, hrm_departments.createdby, hrm_departments.createdon, hrm_departments.lasteditedby, hrm_departments.lasteditedon";
  $join="";
  $having="";
  $groupby="";
  $orderby="";
  $departments->retrieve($fields,$join,$where,$having,$groupby,$orderby);
  $res=$departments->result;
	while($row=mysql_fetch_object($res))
	{
	?>
	<option value="<?php echo $row->id; ?>" <?php if($obj->departmentid==$row->id){echo"selected";}?>><?php echo $row->name; ?></option>
	<?
	}
  ?>
  </select>
  &nbsp;
  <input type="checkbox" name="allowances" value="1" <?php if($obj->allowances==1){echo "checked";}?> /> Allowances&nbsp;
  <input type="checkbox" name="deductions" value="1" <?php if($obj->deductions==1){echo "checked";}?>/> Deductions
  <?php if(empty($paydate)){?>
      <input type="submit" name="action" id="action" class="btn btn-primary btn-sm" value="Load" />
      <?php }?>
      </td>
  </tr>
  
</table>

</div>
<hr>
<table style="clear:both;"  class="table table-responsive table-stripped" id="tbl" id="example" >
	<thead>
		<tr>
			<th>#</th>
			<th><input type="checkbox" onclick="selectAll(this);"/>All</th>
			<th>Employee </th>
			<th>Position </th>
			<th>Bank (if Paid Via Bank) </th>
			<th>Bank Branch </th>
			<th>Bank Account </th>
			<th>Clearing Code </th>
			<th>Reference </th>
			<th>Basic </th>
			<?php 
			if($obj->allowances==1){
			$allowances=new Allowances();
			$fields="hrm_allowances.id, hrm_allowances.name, hrm_allowances.amount, hrm_allowances.percentaxable, hrm_allowancetypes.name as allowancetypeid, hrm_allowances.overall, hrm_allowances.frommonth, hrm_allowances.fromyear, hrm_allowances.tomonth, hrm_allowances.toyear, hrm_allowances.status, hrm_allowances.createdby, hrm_allowances.createdon, hrm_allowances.lasteditedby, hrm_allowances.lasteditedon";
			$join=" left join hrm_allowancetypes on hrm_allowances.allowancetypeid=hrm_allowancetypes.id ";
			$having="";
			$groupby="";
			$orderby="";
			$where=" where  hrm_allowances.status='active'";
			$allowances->retrieve($fields,$join,$where,$having,$groupby,$orderby);
			$res=$allowances->result;
			while($row=mysql_fetch_object($res)){
			?>
				<th><?php echo initialCap($row->name); ?></th>
			<?php 
			}
			}
			?>
			<th>Total Allowances</th>
			<th>Gross Pay</th>
			<?php 
			if($obj->deductions==1){
			$deductions=new Deductions();
			$fields="hrm_deductions.id, hrm_deductions.name, hrm_deductiontypes.name as deductiontypeid, hrm_deductions.frommonth, hrm_deductions.fromyear, hrm_deductions.tomonth, hrm_deductions.toyear, hrm_deductions.amount, hrm_deductions.overall, hrm_deductions.status, hrm_deductions.createdby, hrm_deductions.createdon, hrm_deductions.lasteditedby, hrm_deductions.lasteditedon";
			$join=" left join hrm_deductiontypes on hrm_deductions.deductiontypeid=hrm_deductiontypes.id ";
			$having="";
			$groupby="";
			$orderby="";
			$where=" where  hrm_deductions.status='active'";
			$deductions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
			$res=$deductions->result;
			while($row=mysql_fetch_object($res)){
			
			
			if($row->id==4){
			  $loans = new Loans();
			  $fields="*";
			  $join="";
			  $having="";
			  $groupby="";
			  $orderby="";
			  $where=" ";
			  $loans->retrieve($fields,$join,$where,$having,$groupby,$orderby);
			  $ress=$loans->result;
			  while($roww=mysql_fetch_object($ress)){
				  ?>
						  <th><?php echo $roww->name; ?></th>						
						  <th><?php echo $roww->name; ?> Interest</th>
					  <?php
					  }
			 }
			 elseif($row->id==5){
			  continue;
			 }
			 else{
			?>
				<th><?php echo $row->name; ?></th>
			<?php
			}
			}
			
			
					
					$surchages = new Surchages();
					$fields="*";
					$join="";
					$having="";
					$groupby="";
					$orderby="";
					$where=" ";
					$surchages->retrieve($fields,$join,$where,$having,$groupby,$orderby);
					$res=$surchages->result;
					while($row=mysql_fetch_object($res)){
						?>
							<th><?php echo $row->name; ?></th>		
						<?php
						}
			}
			
			?>
			<th>Total Deductions</th>			
			<th>Net Pay</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="hrm_employees.id, hrm_employees.pfnum, concat(concat(hrm_employees.firstname,' ',hrm_employees.middlename),' ',hrm_employees.lastname) employeeid, hrm_employees.gender, hrm_employees.supervisorid, hrm_employees.startdate, hrm_employees.enddate, hrm_employees.dob, hrm_employees.idno, hrm_employees.passportno, hrm_employees.phoneno, hrm_employees.email, hrm_employees.officemail, hrm_employees.physicaladdress, hrm_nationalitys.name as nationalityid, hrm_countys.name as countyid, hrm_employees.marital, hrm_employees.spouse, hrm_employees.spouseidno, hrm_employees.spousetel, hrm_employees.spouseemail, hrm_employees.nssfno, hrm_employees.nhifno, hrm_employees.pinno, hrm_employees.helbno, hrm_employeebanks.name as bankid, hrm_bankbranches.name as bankbrancheid, hrm_employees.bankacc, hrm_employees.clearingcode, hrm_employees.ref, hrm_employees.basic, hrm_assignments.name as assignmentid, hrm_grades.name as gradeid, hrm_employeestatuss.name as statusid, hrm_employees.image, hrm_employees.createdby, hrm_employees.createdon, hrm_employees.lasteditedby, hrm_employees.lasteditedon";
		$join=" left join hrm_nationalitys on hrm_employees.nationalityid=hrm_nationalitys.id  left join hrm_countys on hrm_employees.countyid=hrm_countys.id  left join hrm_employeebanks on hrm_employees.employeebankid=hrm_employeebanks.id  left join hrm_bankbranches on hrm_employees.bankbrancheid=hrm_bankbranches.id  left join hrm_assignments on hrm_employees.assignmentid=hrm_assignments.id  left join hrm_grades on hrm_employees.gradeid=hrm_grades.id  left join hrm_employeestatuss on hrm_employees.statusid=hrm_employeestatuss.id ";
		$having="";
		$groupby="";
		$orderby="";
		$where=" where hrm_employees.statusid=1 and hrm_employees.id not in(select employeeid from hrm_employeepayments where month='$obj->month' and year='$obj->year')";
		$employees->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$employees->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><input name="<?php echo $row->id; ?>" type="checkbox" id="<?php echo $row->id; ?>"  value="<?php echo $row->id; ?>"  /></td>
			<td><?php echo $row->employeeid; ?></td>
			<td><?php echo $row->assignmentid; ?></td>
			<td><?php echo $row->employeebankid; ?></td>
			<td><?php echo $row->bankbrancheid; ?></td>
			<td><?php echo $row->bankacc; ?></td>
			<td><?php echo $row->clearingcode; ?></td>
			<td><?php echo $row->ref; ?></td>
			<td align="right"><?php echo $row->basic; ?></td>
			<?php 
			$totalallowances = 0;
			$taxable=$row->basic;
			$allowances=new Allowances();
			$fields="hrm_allowances.id, hrm_allowances.name, hrm_allowances.amount, hrm_allowances.percentaxable, hrm_allowancetypes.name as allowancetypeid, hrm_allowancetypes.repeatafter, hrm_allowances.overall, hrm_allowances.frommonth, hrm_allowances.fromyear, hrm_allowances.tomonth, hrm_allowances.toyear, hrm_allowances.status, hrm_allowances.createdby, hrm_allowances.createdon, hrm_allowances.lasteditedby, hrm_allowances.lasteditedon";
			$join=" left join hrm_allowancetypes on hrm_allowances.allowancetypeid=hrm_allowancetypes.id ";
			$having="";
			$groupby="";
			$orderby="";
			//to ensure that the allowance is active
			$where=" where  hrm_allowances.status='active'";
			$allowances->retrieve($fields,$join,$where,$having,$groupby,$orderby);
			while($rw=mysql_fetch_object($allowances->result)){
			$allowance=0;
			$now=getDates($obj->year, $obj->month, 01);
				//check allowances that affect all
				if($rw->overall=="All"){
					//check if the to date is reached
					$fromdate=getDates($rw->fromyear, $rw->frommonth, 01);
					$todate=getDates($rw->toyear, $rw->tomonth, 01);					
					if($now>=$fromdate and $now<=$todate){
							//check frequency qualifier
							$employeepaidallowances=new Employeepaidallowances();
							$fields="hrm_employeepaidallowances.id, hrm_employeepayments.id employeepaymentid, hrm_employeepaidallowances.allowanceid, hrm_employeepaidallowances.employeeid, hrm_employeepaidallowances.amount, hrm_employeepaidallowances.month, hrm_employeepaidallowances.year, hrm_employeepaidallowances.createdby, hrm_employeepaidallowances.createdon, hrm_employeepaidallowances.lasteditedby, hrm_employeepaidallowances.lasteditedon";
							$join=" left join hrm_employeepayments on hrm_employeepaidallowances.employeepaymentid=hrm_employeepayments.id ";
							$where=" where hrm_employeepaidallowances.employeeid=$row->id and hrm_employeepaidallowances.allowanceid=$rw->id ";
							$having="";
							$groupby="";
							$orderby=" order by hrm_employeepaidallowances.id desc";
							$employeepaidallowances->retrieve($fields,$join,$where,$having,$groupby,$orderby);
							$employeepaidallowances->fetchObject;
							$next=getDates($employeepaidallowances->year, $employeepaidallowances->month+$row->repeatafter, 01);
							if($next<=$now){
								$allowance=$row->amount;
								$taxable+=($row->amount*$rw->percentaxable);
							}
							else{
								$allowance=0;
							}
					}
					else{
						$allowance=0;
					}
				}
				//check employee specific allowances
				else{
					$employeeallowances=new Employeeallowances();
					$fields="hrm_employeeallowances.id, hrm_allowances.name as allowanceid, concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname)) as employeeid, hrm_allowancetypes.name as allowancetypeid, hrm_employeeallowances.amount, hrm_employeeallowances.frommonth, hrm_employeeallowances.fromyear, hrm_employeeallowances.tomonth, hrm_employeeallowances.toyear, hrm_employeeallowances.remarks, hrm_employeeallowances.createdby, hrm_employeeallowances.createdon, hrm_employeeallowances.lasteditedby, hrm_employeeallowances.lasteditedon";
					$join=" left join hrm_allowances on hrm_employeeallowances.allowanceid=hrm_allowances.id  left join hrm_employees on hrm_employeeallowances.employeeid=hrm_employees.id  left join hrm_allowancetypes on hrm_employeeallowances.allowancetypeid=hrm_allowancetypes.id ";
					$having="";
					$groupby="";
					$orderby="";
					//checks if allowance is still active
					$where=" where hrm_employeeallowances.employeeid=$row->id and hrm_employeeallowances.allowanceid=$rw->id ";
					$employeeallowances->retrieve($fields,$join,$where,$having,$groupby,$orderby);
					$employeeallowances = $employeeallowances->fetchObject;
					$next=getDates($employeeallowances->year, $employeeallowances->month+$row->repeatafter, 01);
					if($next<=$now){
						$allowance=$employeeallowances->amount;
						$taxable+=($employeeallowances->amount*$rw->percentaxable);
					}
					else{
						$allowance=0;
					}
				}
				
			$totalallowances+=$allowance;
			if($obj->allowances==1){
			?>
				<td align="right"><?php echo $allowance;  ?></td>
			<?php 
			}
			}
			$grosspay=$row->basic+$totalallowances;
			?>
			<td align="right"><?php echo $totalallowances; ?></td>
			<td align="right"><?php echo $grosspay; ?></td>
			<?php 
			$totaldeductions = 0;
			$deductions=new Deductions();
			$fields="hrm_deductions.id, hrm_deductions.name, hrm_deductions.amount, hrm_deductiontypes.name as deductiontypeid, hrm_deductiontypes.repeatafter, hrm_deductions.overall, hrm_deductions.frommonth, hrm_deductions.fromyear, hrm_deductions.tomonth, hrm_deductions.toyear, hrm_deductions.status, hrm_deductions.createdby, hrm_deductions.createdon, hrm_deductions.lasteditedby, hrm_deductions.lasteditedon";
			$join=" left join hrm_deductiontypes on hrm_deductions.deductiontypeid=hrm_deductiontypes.id ";
			$having="";
			$groupby="";
			$orderby="";
			//to ensure that the deduction is active
			$where=" where  hrm_deductions.status='active'";
			$deductions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
			while($rw=mysql_fetch_object($deductions->result)){
			$deduction=0;
			$now=getDates($obj->year, $obj->month, 01);
				//check deductions that affect all
				if($rw->id==1){
					//get PAYE
					$payes = new Payes();
					//get NSSF
					$nssfs = new Nssfs();
					$taxable=$taxable-$nssfs->getNSSF($grosspay);
					$deduction=$payes->getPAYE($taxable,$row->id);
				}
				elseif ($rw->id==2){
					//get NHIF
					$nhifs = new Nhifs();
					$deduction=$nhifs->getNHIF($grosspay);
				}
				elseif ($rw->id==3){
					//get NSSF
					$nssfs = new Nssfs();
					$deduction=$nssfs->getNSSF($grosspay);
				}
				elseif($rw->overall=="All"){
					//check if the to date is reached
					$fromdate=getDates($rw->fromyear, $rw->frommonth, 01);
					$todate=getDates($rw->toyear, $rw->tomonth, 01);					
					if($now>=$fromdate and $now<=$todate){
							//check frequency qualifier
							$employeepaiddeductions=new Employeepaiddeductions();
							$fields="hrm_employeepaiddeductions.id, hrm_employeepayments.id employeepaymentid, hrm_employeepaiddeductions.deductionid, hrm_employeepaiddeductions.employeeid, hrm_employeepaiddeductions.amount, hrm_employeepaiddeductions.month, hrm_employeepaiddeductions.year, hrm_employeepaiddeductions.createdby, hrm_employeepaiddeductions.createdon, hrm_employeepaiddeductions.lasteditedby, hrm_employeepaiddeductions.lasteditedon";
							$join=" left join hrm_employeepayments on hrm_employeepaiddeductions.employeepaymentid=hrm_employeepayments.id ";
							$where=" where hrm_employeepaiddeductions.employeeid=$row->id and hrm_employeepaiddeductions.deductionid=$rw->id ";
							$having="";
							$groupby="";
							$orderby=" order by hrm_employeepaiddeductions.id desc";
							$employeepaiddeductions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
							$employeepaiddeductions->fetchObject;
							$next=getDates($employeepaiddeductions->year, $employeepaiddeductions->month+$row->repeatafter, 01);
							if($next<=$now){
								$deduction=$row->amount;
							}
							else{
								$deduction=0;
							}
					}
					else{
						$deduction=0;
					}
				}
				//check employee specific deductions
				else{
					$employeedeductions=new Employeedeductions();
					$fields="hrm_employeedeductions.id, hrm_deductions.name as deductionid, concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname)) as employeeid, hrm_deductiontypes.name as deductiontypeid, hrm_employeedeductions.amount, hrm_employeedeductions.frommonth, hrm_employeedeductions.fromyear, hrm_employeedeductions.tomonth, hrm_employeedeductions.toyear, hrm_employeedeductions.remarks, hrm_employeedeductions.createdby, hrm_employeedeductions.createdon, hrm_employeedeductions.lasteditedby, hrm_employeedeductions.lasteditedon";
					$join=" left join hrm_deductions on hrm_employeedeductions.deductionid=hrm_deductions.id  left join hrm_employees on hrm_employeedeductions.employeeid=hrm_employees.id  left join hrm_deductiontypes on hrm_employeedeductions.deductiontypeid=hrm_deductiontypes.id ";
					$having="";
					$groupby="";
					$orderby="";
					//checks if deduction is still active
					$where=" where hrm_employeedeductions.employeeid=$row->id and hrm_employeedeductions.deductionid=$rw->id ";
					$employeedeductions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
					$employeedeductions = $employeedeductions->fetchObject;
					$next=getDates($employeedeductions->year, $employeedeductions->month+$row->repeatafter, 01);
					if($next<=$now){
						$deduction=$employeedeductions->amount;
					}
					else{
						$deduction=0;
					}
				}
				
				if($rw->id==4){
				  $loans = new Loans();
				  $fields="*";
				  $join="";
				  $having="";
				  $groupby="";
				  $orderby="";
				  $where="";
				  $loans->retrieve($fields, $join, $where, $having, $groupby, $orderby);
				  while($wr=mysql_fetch_object($loans->result)){
					  $employeeloans = new Employeeloans();
					  $fields="*";
					  $join="";
					  $having="";
					  $groupby="";
					  $orderby="";
					  $where=" where employeeid='$row->id' and loanid='$wr->id' and principal>0";
					  $employeeloans->retrieve($fields, $join, $where, $having, $groupby, $orderby);
					  if($employeeloans->affectedRows>0){
						  while($rww=mysql_fetch_object($employeeloans->result)){					
							  $deduction=$rww->payable;
							  $totaldeductions+=$deduction;
							  if($obj->deductions==1){
								  ?>
								  <td align="right"><?php echo $deduction;  ?></td>
								  <?php 
								  }
								  if(strtolower($rww->interesttype)=="amount"){
								    $deduction=$rww->interest;
								  }
								  else{
								    if($rww->method=="straight-line")
									    $deduction=$rww->interest*$rww->initialvalue*$rww->duration/100;
								    elseif($rww->method=="reducing balance")
									    $deduction=$rww->interest*$rww->principal*$rww->duration/100;
								    }
								  
								  $totaldeductions+=$deduction;
								  if($obj->deductions==1){
								  ?>
								  <td align="right"><?php echo $deduction;  ?></td>
								  <?php 
								  }
						  }
					  }
					  else{
						  if($obj->deductions==1){
						  ?>
						  <td align="right"><?php echo 0;  ?></td>
						  <td align="right"><?php echo 0;  ?></td>
						  <?php 
						  }
					  }
				  }
				 }
				 elseif($rw->id==5){
				  continue;
				 }
				 else{
				
				  $totaldeductions+=$deduction;
				  if($obj->deductions==1){
				  ?>
					  <td align="right"><?php echo $deduction;  ?></td>
				  <?php 
				  }
				 }
			}
						
			$surchages = new Surchages();
			$fields="*";
			$join="";
			$having="";
			$groupby="";
			$orderby="";
			$where=" where status='Active'";
			$surchages->retrieve($fields, $join, $where, $having, $groupby, $orderby);
			while($wr=mysql_fetch_object($surchages->result)){
				$employeesurchages = new Employeesurchages();
				$fields="*";
				$join="";
				$having="";
				$groupby="";
				$orderby="";
				$where=" where employeeid='$row->id' and surchageid='$wr->id' ";
				$employeesurchages->retrieve($fields, $join, $where, $having, $groupby, $orderby);
				if($employeesurchages->affectedRows>0){
					while($rw=mysql_fetch_object($employeesurchages->result)){
						$deduction=$rw->amount;
						$totaldeductions+=$deduction;
						if($obj->deductions==1){
						?>
						<td align="right"><?php echo $deduction;  ?></td>
						<?php 
						}
					}
				}
				else{
					if($obj->deductions==1){
				?>
					<td align="right"><?php echo 0;  ?></td>
					<?php 
					}
				}
			}
			$netpay=$grosspay-$totaldeductions;
			?>
			<td align="right"><?php echo $totaldeductions; ?></td>
			<td align="right"><?php echo $netpay;?></td>
		</tr>
	<?php 
	}
	?>
	</tbody>
	<tfoot>
	<tr>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp; </th>
			<th>&nbsp; </th>
			<th>&nbsp; </th>
			<th>&nbsp; </th>
			<th>&nbsp; </th>
			<th>&nbsp; </th>
			<th>&nbsp; </th>
			<th>&nbsp; </th>
			<?php 
			if($obj->allowances==1){
			$allowances=new Allowances();
			$fields="hrm_allowances.id, hrm_allowances.name, hrm_allowances.amount, hrm_allowances.percentaxable, hrm_allowancetypes.name as allowancetypeid, hrm_allowances.overall, hrm_allowances.frommonth, hrm_allowances.fromyear, hrm_allowances.tomonth, hrm_allowances.toyear, hrm_allowances.status, hrm_allowances.createdby, hrm_allowances.createdon, hrm_allowances.lasteditedby, hrm_allowances.lasteditedon";
			$join=" left join hrm_allowancetypes on hrm_allowances.allowancetypeid=hrm_allowancetypes.id ";
			$having="";
			$groupby="";
			$orderby="";
			$where=" where  hrm_allowances.status='active'";
			$allowances->retrieve($fields,$join,$where,$having,$groupby,$orderby);
			$res=$allowances->result;
			while($row=mysql_fetch_object($res)){
			?>
				<th><?php echo initialCap($row->name); ?></th>
			<?php 
			}
			}
			?>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<?php 
			if($obj->deductions==1){
			$deductions=new Deductions();
			$fields="hrm_deductions.id, hrm_deductions.name, hrm_deductiontypes.name as deductiontypeid, hrm_deductions.frommonth, hrm_deductions.fromyear, hrm_deductions.tomonth, hrm_deductions.toyear, hrm_deductions.amount, hrm_deductions.overall, hrm_deductions.status, hrm_deductions.createdby, hrm_deductions.createdon, hrm_deductions.lasteditedby, hrm_deductions.lasteditedon";
			$join=" left join hrm_deductiontypes on hrm_deductions.deductiontypeid=hrm_deductiontypes.id ";
			$having="";
			$groupby="";
			$orderby="";
			$where=" where  hrm_deductions.status='active'";
			$deductions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
			$res=$deductions->result;
			while($row=mysql_fetch_object($res)){
			
			
			if($row->id==4){
			  $loans = new Loans();
			  $fields="*";
			  $join="";
			  $having="";
			  $groupby="";
			  $orderby="";
			  $where=" ";
			  $loans->retrieve($fields,$join,$where,$having,$groupby,$orderby);
			  $ress=$loans->result;
			  while($roww=mysql_fetch_object($ress)){
				  ?>
						  <th>&nbsp;</th>						
						  <th>&nbsp;</th>
					  <?php
					  }
			 }
			 elseif($row->id==5){
			  continue;
			 }
			 else{
			?>
				<th>&nbsp;</th>
			<?php
			}
			}
			
			
					
					$surchages = new Surchages();
					$fields="*";
					$join="";
					$having="";
					$groupby="";
					$orderby="";
					$where=" ";
					$surchages->retrieve($fields,$join,$where,$having,$groupby,$orderby);
					$res=$surchages->result;
					while($row=mysql_fetch_object($res)){
						?>
							<th><?php echo $row->name; ?></th>		
						<?php
						}
			}
			
			?>
			<th>&nbsp;</th>			
			<th>&nbsp;</th>
		</tr>
	</tfoot>
</table>
<hr>
<table align="center">
<tr>
	<td><label>Payment Date :</label>
	<input type="text" name="paidon" class="date_input input-medium" size="12" readonly="readonly" value="<?php echo $obj->paidon; ?>"/>
	</td>
	<td><label>Payment Mode :</label>
	<select name="paymentmodeid" class="input-medium">
	<option value="">Select...</option>
	<?php 
	$paymentmodes = new Paymentmodes();
	$fields="*";
	$where="";
	$having="";
	$groupby="";
	$orderby="";
	$join="";
	$paymentmodes->retrieve($fields, $join, $where, $having, $groupby, $orderby);
	while($row=mysql_fetch_object($paymentmodes->result)){
	?>
		<option value="<?php echo $row->id; ?>" <?php if($obj->paymentmodeid==$row->id){echo"selected";}?>><?php echo $row->name; ?></option>
	<?php 
	}
	?>
	</select>
	</td>
	<td><label>Bank :</label>
	<select name="bankid" class="input-medium">
	<option value="">Select...</option>
	<?php 
	$banks = new Banks();
	$fields="*";
	$where="";
	$having="";
	$groupby="";
	$orderby="";
	$join="";
	$banks->retrieve($fields, $join, $where, $having, $groupby, $orderby);
	while($row=mysql_fetch_object($banks->result)){
	?>
		<option value="<?php echo $row->id; ?>" <?php if($obj->bankid==$row->id){echo"selected";}?>><?php echo $row->name; ?></option>
	<?php 
	}
	?>
	</select>
	</td>
	<td><input type="submit" name="action" class="btn btn-success" value="Make Payment"/>
</tr>
</table>
<hr>
</form>
<?php
include"../../../foot.php";
?>
