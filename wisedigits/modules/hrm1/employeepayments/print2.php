<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once '../employees/Employees_class.php';
require_once("../employeepaidallowances/Employeepaidallowances_class.php");
require_once("../employeepaiddeductions/Employeepaiddeductions_class.php");
require_once '../employeepaidsurchages/Employeepaidsurchages_class.php';
require_once("../employeepayments/Employeepayments_class.php");
require_once '../assignments/Assignments_class.php';
require_once "../loans/Loans_class.php";
require_once "../employeeloans/Employeeloans_class.php";



$id=$_GET['id'];
$month=$_GET['month'];
$year=$_GET['year'];

$printedon=formatDate(Date("Y-m-d"))." ".Date("H :i :s");
	
	
	
		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PAYSLIP</title>

<script type="text/javascript">
  function print_doc()
  {
		
  		var printers = jsPrintSetup.getPrintersList().split(',');
		// Suppress print dialog
		jsPrintSetup.setSilentPrint(true);/** Set silent printing */

		var i;
		for(i=0; i<printers.length;i++)
		{alert(i+": "+printers[i]);
			if(printers[i].indexOf('<?php echo SPRINTER; ?>')>-1)
			{	
				jsPrintSetup.setPrinter(printers[i]);
			}
			
		}
		//set number of copies to 2
		jsPrintSetup.setOption('numCopies',<?php echo RECEIPTCOPIES; ?>);
		jsPrintSetup.setOption('headerStrCenter','');
		jsPrintSetup.setOption('headerStrRight','');
		jsPrintSetup.setOption('headerStrLeft','');
		jsPrintSetup.setOption('footerStrCenter','');
		jsPrintSetup.setOption('footerStrRight','');
		jsPrintSetup.setOption('footerStrLeft','');
		jsPrintSetup.setOption('marginLeft','1');
		jsPrintSetup.setOption('marginRight','1');
		
		// Do Print
		jsPrintSetup.printWindow(window);
		
		window.close();
		//window.top.hidePopWin(true);
		// Restore print dialog
		//jsPrintSetup.setSilentPrint(false); /** Set silent printing back to false */
 
  }
 </script>
<style media="print" type="text/css">
.noprint{ display:none;}
body {font-family:'Tahoma';font-size:10px;}
</style>

</head>

<body onload="print_doc();">
<div align="left" id="print_content" style="width:98%; margin:0px auto;">
   <table class="tgrid gridd" width="50%" cellspacing="0" align="left">
   <thead> 
   <tr>
   <td colspan="9" align="center"> <img src="../../../images/magana-logo.png" width="240" height="120"/></td>
   </tr>
   <tr><td colspan="9"><div>
   <div class="hfields" align="left">
 <div align="center" style="font-weight:bold;page-break-inside:avoid; page-break-after:avoid; page-break-before:avoid; display:none;">

 <span style="display:block; padding:0px 0px 2px;"><?php echo COMPANY_NAME;?> </span>
<span style="display:block; padding:0px 0px 1px;"><?php echo COMPANY_TYPE;?> </span>
<span style="display:block; padding:0px 0px 1px;"><?php echo COMPANY_ADDR;?> </span>
<span style="display:block; padding:0px 0px 1px;"><?php echo COMPANY_TOWN;?></span>
<span style="display:block; padding:0px 0px 1px;"><?php echo COMPANY_DESC;?></span>
<span style="display:block; padding:0px 0px 1px;"><?php echo COMPANY_TEL;?> </span>
<span style="display:block; padding:0px;"><?php echo COMPANY_DESC;?></span> </div>
<?php if(PRINTTITLE==1){
 ?>
 <span style="display:block; padding:3px 10px; font-size:16px; text-align:center; font-weight:bold; color:#fff; background-color:#999"><?php echo COMPANY_NAME; ?></span></div>
 <?php 
 }?>
 <span style="display:block; padding:3px 10px; font-size:16px; text-align:center; font-weight:bold; color:#fff; background-color:#999">Pay Slip</span></div>
 <div class="hfields" align="left" style="float:left; width:100%; padding-left:5px;font-size: 14;">
 
 Payment for: <?php echo date("M",mktime(0, 0, 0, $month, 1)).' '.$year; ?><br/></div>
 
   </div></td></tr>
   <tr>
<th colspan="3"><hr /></th>
</tr>
  <tr style="font-size:12px;">
  <?php 
  
  $employees = new Employees();
	$fields="*";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$where=" where id='$id'";
	$employees->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$emp=$employees->fetchObject;
	
	$employeepayments = new Employeepayments();
	$fields="*";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$where=" where employeeid='$emp->id'";
	$employeepayments->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$pay=$employeepayments->fetchObject;
	
	//$check=retrieveScheduleByEmployee($emp->id);
	
	$assignments = new Assignments();
	$fields="*";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$where=" where id='$emp->assignmentid'";
	$assignments->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$ass=$assignments->fetchObject;
  
  
  
  
  
  ?>
  
  
  
  
  
  
  
<th colspan="3">Name:&nbsp;<strong>

<?php echo initialCap($emp->firstname); ?>&nbsp;<?php echo initialCap($emp->middlename); ?>&nbsp;<?php echo initialCap($emp->lastname); ?></strong></th>
</tr>
<tr valign="top" style="font-size:12px; ">
	  <td width="60%">Id No:</td>
      <td colspan="2"><?php echo $emp->idno; ?></td>
	  </tr>
	  
	  <tr valign="top" style="font-size:12px; ">
	  <td width="60%">Payroll No:</td>
      <td colspan="2"><?php echo $emp->payrollno; ?></td>
	  </tr>
	  
	  <tr valign="top" style="font-size:12px; ">
	  <td >Assignment:</td>
      <td colspan="2"><?php echo $ass->name; ?></td>
	  </tr>
	  
	  <tr valign="top" style="font-size:12px; ">
	  <td >Grade:</td>
      <td colspan="2"><?php echo $emp->grade; ?></td>
	  </tr>
<tr>
<th colspan="3"><hr /></th>
</tr>
</thead>
		<tbody class="" style="width:80%;  " align="left">    
	  <tr>
	  <td colspan="3" align="left"><u>PAYMENTS</u></td>
	  </tr>	  
	  <tr valign="top" style="font-size:12px; ">
	  <td >Basic Salary:</td>
	  <td>&nbsp;</td>
      <td align="right"><?php echo formatNumber($emp->basic); ?></td>
	  </tr>	  
	  <tr valign="top" style="font-size:12px; ">
	  <?php 
	  $employeepaidallowances = new Employeepaidallowances();
	$fields="hrm_employeepaidallowances.*, hrm_allowances.name allowanceid";
	$join=" left join hrm_allowances on hrm_allowances.id=hrm_employeepaidallowances.allowanceid ";
	$having="";
	$groupby="";
	$orderby="";
	$where=" where hrm_employeepaidallowances.employeeid='$emp->id' and hrm_employeepaidallowances.month='$month' and hrm_employeepaidallowances.year='$year'";
	$employeepaidallowances->retrieve($fields,$join,$where,$having,$groupby,$orderby);echo mysql_error();
	$rs = $employeepaidallowances->result;
	$t=0;
	while($rw=mysql_fetch_object($rs))
	  {	  	
	  $t+=$rw->amount;
	  ?>
	  
	  
	  <td ><?php echo initialCap($rw->allowanceid); ?>:</td>
	  <td>&nbsp;</td>
	  <td align="right"><?php echo formatNumber($rw->amount); ?></td>
	  <?
	      }
	  
	  ?>
      
	  </tr>
	  <tr valign="top" style="font-size:12px; ">
	  <td>&nbsp;</td>
      <td align="right"><hr/></td>
	  </tr>
	  <tr valign="top" style="font-size:12px; ">
	  <td ></td>
	  <td>&nbsp;</td>
      <td align="right"><?php echo formatNumber($emp->basic+$t) ?></td>
	  </tr>
	  
	   <tr valign="top" style="font-size:12px; ">
	  <td colspan="3" align="left"><u>DEDUCTIONS</u></td>
	  </tr>	
	  	  
	  <?php 
	  $total=0;
	$employeepaiddeductions = new Employeepaiddeductions();
	$fields="hrm_employeepaiddeductions.*, hrm_deductions.name deductionid, hrm_deductions.id ded";
	$join=" left join hrm_deductions on hrm_deductions.id=hrm_employeepaiddeductions.deductionid ";
	$having="";
	$groupby="";
	$orderby="";
	$where=" where hrm_employeepaiddeductions.employeeid='$emp->id' and hrm_employeepaiddeductions.month='$month' and hrm_employeepaiddeductions.year='$year'";
	$employeepaiddeductions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$res = $employeepaiddeductions->result;
	  //$res=retrievePaidEmployeeDeductions($id,$month,$year,$pay->paydate);
	  while($row=mysql_fetch_object($res)){
	  	
	  	if($row->ded==5){
		  $loans = new Loans();
		  $fields="*";
		  $join="";
		  $having="";
		  $groupby="";
		  $orderby="";
		  $where=" where id='$row->loanid' ";
		  $loans->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		  $loans = $loans->fetchObject;
		  
		  $row->deductionid=$loans->name;
		  
		  if($row->ded==5)
		    $row->deductionid=$loans->name." Interest";
	  	}
		  
		  $total+=$row->amount;
		  if($row->amount>0){
		  ?>
		  <tr valign="top" style="font-size:12px; ">
			  <td><?php echo initialCap($row->deductionid); ?>:</td>
			  <td>&nbsp;</td>
			   <td align="right"><?php echo formatNumber($row->amount); ?></td>
			   <td>&nbsp;</td>
		  	</tr>
			   <?
		  }
	  }
	  ?>
	 
	  <?php 
	  $employeepaidsurchages = new Employeepaidsurchages();
	  $fields="hrm_employeepaidsurchages.*, hrm_surchages.name surchageid";
	  $join=" left join hrm_surchages on hrm_surchages.id=hrm_employeepaidsurchages.empsurchageid ";
	  $having="";
	  $groupby="";
	  $orderby="";
	  $where=" where hrm_employeepaidsurchages.employeeid='$emp->id' and hrm_employeepaidsurchages.month='$month' and hrm_employeepaidsurchages.year='$year'";
	  $employeepaidsurchages->retrieve($fields,$join,$where,$having,$groupby,$orderby);echo mysql_error();
	  $res = $employeepaidsurchages->result;
	 // $res=retrievePaidEmployeeSurchages($pay->employeeid,$pay->month,$pay->year,$pay->paydate);
		  while($row=mysql_fetch_object($res)){
		  	$total+=$row->amount;
		  	
		  	?>
		  	 <tr valign="top" style="font-size:12px; ">
		  	<td >Surchage - <?php echo  initialCap($row->surchageid); ?>:</td>
		  	<td align="right"><?php echo formatNumber($row->amount); ?></td>
		  	<td>&nbsp;</td>
		  	</tr>
		  	
		<?  }
	  ?>
      
	  
	  <tr valign="top" style="font-size:12px; ">
	  <td >&nbsp;</td>
      <td align="right"><hr/></td>
      <td>&nbsp;</td>
	  </tr>
	  
	  <tr valign="top" style="font-size:12px; ">
	  <td >Total Deductions:</td>
	  <td>&nbsp;</td>
      <td align="right"><?php echo formatNumber($total); ?></td>
	  </tr>
	  
	  <tr valign="top" style="font-size:12px; ">
	  <td >&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right"><hr/></td>
	  </tr>
	  
	  <tr valign="top" style="font-size:12px; ">
	  <td >Net Pay:</td>
	  <td>&nbsp;</td>
      <td align="right"><?php echo formatNumber($emp->basic+$t-$total); ?></td>
	  </tr>
	  
	  <tr valign="top" style="font-size:1principal2px; ">
	  <td >&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right"><hr/></td>
	  </tr>
	  
	  
	  <!--$employeeloans = new EmployeeLoans();
	 $fields="hrm_employeeloans.principal, hrm_loans.name loanid";
	  $join=" left join hrm_loans on hrm_loans.id=hrm_employeeloans.loanid ";
	  $having="";
	  $groupby="";
	  $orderby="";
	  $where=" where hrm_employeeloans.employeeid='$emp->id' ";
	  $employeeloans->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	  while($r = mysql_fetch_object($employeeloans->result)){
	    $pdf->Cell(30,4,$r->loanid.' TO DATE: ',0,0);
	    $pdf->Cell(30,4,' ',0,0);
	    $pdf->Cell(30,4,formatNumber($r->principal),0,1,'R');
	    
	    
	    }-->
	    
	 <!-- <tr>
	  <td colspan="3" align="left"><u>STATEMENTS</u></td>
	  </tr>-->
	  
	  <?php 
	  $rs=retrieveEmpStatements($id,$month,$year);
		  while($rw=mysql_fetch_object($rs)){
		  	$ded=retrieveDeduction($rw->deductionid);	  
		  	
		  	if($rw->deductionid==17){
		  	$rsl=retrieveEmpLoans($id," and principal>0 and loanid='$rw->loanid' ");
		  	while($lr=mysql_fetch_object($rsl)){
		  		if(!empty($lr->loanid)){
		  			$rw->amount=0;
			  		$loan=retrieveLoanByEmployee($id,$lr->loanid);
			  		$l=retrieveLoan($lr->loanid);
			  		$rw->amount=$loan->principal;
			  		if(!empty($rw->amount)){
				  		$pdf->Cell(30,4,initialCap($l->loan).' TO DATE: ',0,0);
					  	$pdf->Cell(30,4,' ',0,0);
					  	$pdf->Cell(30,4,formatNumber($rw->amount),0,1,'R');
			  		}
			  		
				  	$interest=retrievePaidLoanInterest($lr->loanid,$month,$year);
				  	if(!empty($interest->amount)){
					  	//$pdf->Cell(30,4,initialCap($l->loan).' Interest TO DATE: ',0,0);
					  	//$pdf->Cell(30,4,' ',0,0);
					  	//$pdf->Cell(30,4,formatNumber($interest->amount),0,1,'R');
				  	}
		  		}
		  	}
		  	}
		  	elseif($rw->deductionid==18){
		  		
		  	}
		  	else{
				if($rw->deductionid==1)
					$rw=retrieveEmpStatement($id,$month,$year,1);
					?>
		  
		  <tr>
	  <td colspan="3" align="left"><u>TAX DETAILS</u></td>
	  </tr>	
					<?
		  	}
		  }
		  if($pay->paye>0){
// 		  	$pdf->Cell(20,4,' ',0,1,'R');
// 		  	$pdf->SetFont('Arial','B',10);
// 		  	$pdf->Cell(80,4,'TAX DETAILS ',0,1);
// 		  	$pdf->SetFont('Arial','',9);
// 		  	$pdf->Cell(30,4,'Taxable Pay: ',0,0);
// 		  	$pdf->Cell(30,4,' ',0,0);
// 		  	$pdf->Cell(30,4,formatNumber($pay->payable-$nssf->amount),0,1,'R');

?>
		  	<tr valign="top" style="font-size:12px; ">
	  <td >Taxable Pay:</td>
	  <td>&nbsp;</td>
      <td align="right"><?php echo formatNumber($pay->payable-$nssf->amount); ?></td>
	  </tr>
		  	<!--$pdf->Cell(30,4,'PAYE: ',0,0);
		  	$pdf->Cell(30,4,' ',0,0);-->
		<tr valign="top" style="font-size:12px; ">
	  <td >PAYE:</td>
	  <td>&nbsp;</td>
      <td align="right"><?php echo formatNumber($pay->paye); ?></td>
	  </tr>
	    	
		  	<!--
		  	$pdf->Cell(30,4,formatNumber($pay->paye),0,1,'R');
		  	$pdf->Cell(30,4,'Tax Relief: ',0,0);-->
		  	<tr valign="top" style="font-size:12px; ">
	  <td >Tax Relief:</td>
	  <td>&nbsp;</td>
      <td align="right"><?php echo formatNumber(1162); ?></td>
	  </tr>
		  	<!--$pdf->Cell(30,4,' ',0,0);
		  	$pdf->Cell(30,4,formatNumber(1162),0,1,'R');-->
		  	<?
		  }
	  
	/*  
	  
	  
	  $rs=retrieveEmpStatements($id,$month,$year);echo mysql_error();
	  while($rw=mysql_fetch_object($rs)){
	  	$ded=retrieveDeduction($rw->deductionid);
	  	if($rw->deductionid==17){
	  		$loan=retrieveLoanByEmployee($id,$rw->loanid);
	  		$rw->amount=$loan->initialvalue-$rw->amount;
	  		$ln=retrieveLoan($rw->loanid);
	  		$ded->deduction=$ln->loan;
	  	}
	  	if($rw->deductionid==18){
	  		$ln=retrieveLoan($rw->loanid);
	  		$ded->deduction=$ln->loan." Interest";
	  	}
	  	
	  	if($rw->deductionid==1){
	  	}*/
	  	?>
	  	<tr valign="top" style="font-size:12px; ">
		  <td ><?php echo initialCap($ded->deduction); if($rw->deductionid!=17){?> TO DATE:<?php }?></td>
		  <td>&nbsp;</td>
	      <td align="right"><?php echo formatNumber($rw->amount); ?></td>
		  </tr>
	  	<?php 

	  ?>
	  
	 <!-- <?php if($pay->paye>0){?>
	  <tr>
	  <td colspan="3" align="left"><u>TAX DETAILS</u></td>
	  </tr>	 
	  
	  <tr valign="top" style="font-size:12px; ">
	  <td >Taxable Pay:</td>
	  <td>&nbsp;</td>
      <td align="right"><?php echo formatNumber($pay->payable-$nssf->amount); ?></td>
	  </tr>
	  
	  <tr valign="top" style="font-size:12px; ">
	  <td >PAYE:</td>
	  <td>&nbsp;</td>
      <td align="right"><?php echo formatNumber($pay->paye); ?></td>
	  </tr>
	  
	  <tr valign="top" style="font-size:12px; ">
	  <td >Tax Relief:</td>
	  <td>&nbsp;</td>
      <td align="right"><?php echo formatNumber(1162); ?></td>
	  </tr>
	  <?php }?>
	  <tr><td colspan="3">&nbsp;</td></tr>-->
	<tr valign="top" style="font-size:12px; ">
	  <td colspan='3' align="center"><?php echo COMPANY_SLOGAN; ?></td>
	  </tr>

      </tbody>
  </table>
  
</div>
</body>
</html>
<?php 

$i=$_SESSION['i'];
while($i<3){
	redirect("print2.php?id=".$i);
	$i++;
	$_SESSION['i']=$i;
}
?>
