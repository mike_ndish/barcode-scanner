<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Employeeleaveapplications_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Employeeleaveapplications";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="4230";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$employeeleaveapplications=new Employeeleaveapplications();
if(!empty($delid)){
	$employeeleaveapplications->id=$delid;
	$employeeleaveapplications->delete($employeeleaveapplications);
	redirect("employeeleaveapplications.php");
}
//Authorization.
$auth->roleid="4229";//View
$auth->levelid=$_SESSION['level'];

?>

<div class="container">
<hr>
<table style="clear:both;"  class="table table-striped table-bordered" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Employee </th>
			<th>Type Of Leave </th>
			<th>Start Date </th>
			<th>Duration (Working Days) </th>
			<th>Date Applied </th>
			<th>Status </th>
			<th>Remarks </th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="hrm_employeeleaveapplications.id, concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname)) as employeeid, hrm_leaves.name as leaveid, hrm_employeeleaveapplications.startdate, hrm_employeeleaveapplications.duration, hrm_employeeleaveapplications.appliedon, hrm_employeeleaveapplications.status, hrm_employeeleaveapplications.remarks, hrm_employeeleaveapplications.createdby, hrm_employeeleaveapplications.createdon, hrm_employeeleaveapplications.lasteditedby, hrm_employeeleaveapplications.lasteditedon";
		$join=" left join hrm_employees on hrm_employeeleaveapplications.employeeid=hrm_employees.id  left join hrm_leaves on hrm_employeeleaveapplications.leaveid=hrm_leaves.id ";
		$having="";
		$groupby="";
		$orderby="";
		$employeeleaveapplications->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$employeeleaveapplications->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->employeeid; ?></td>
			<td><?php echo $row->leaveid; ?></td>
			<td><?php echo formatDate($row->startdate); ?></td>
			<td><?php echo $row->duration; ?></td>
			<td><?php echo formatDate($row->appliedon); ?></td>
			<td><?php echo $row->status; ?></td>
			<td><?php echo $row->remarks; ?></td>
			<td><a href="javascript:;" onclick="showPopWin('addemployeeleaveapplications_proc.php?id=<?php echo $row->id; ?>',600,430);">Grant</a></td>
			<td><a href="javascript:;" onclick="showPopWin('addemployeeleaveapplications_proc.php?id=<?php echo $row->id; ?>',600,430);">Send Back</a></td>
			<td><a href="javascript:;" onclick="showPopWin('addemployeeleaveapplications_proc.php?id=<?php echo $row->id; ?>',600,430);">Decline</a></td>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>

<hr>
</div> <!-- contend -->
<?php
include"../../../foot.php";
?>
