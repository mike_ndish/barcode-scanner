<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Employeeleaves_class.php");
require_once("../../auth/rules/Rules_class.php");
// require_once("../../hrm/employeeleavedays/Employeeleavedays_class.php");
require_once("../../hrm/assignments/Assignments_class.php");
require_once("../../hrm/levels/Levels_class.php");
require_once("../../hrm/levelleavedays/Levelleavedays_class.php");

if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Employeeleaves";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="4238";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$employeeleaves=new Employeeleaves();
if(!empty($delid)){
	$employeeleaves->id=$delid;
	$employeeleaves->delete($employeeleaves);
	redirect("employeeleaves.php");
}
//Authorization.
$auth->roleid="4237";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>

<a class="btn" onclick="showPopWin('addemployeeleaves_proc.php',540,400);"><span>ADD EMPLOYEE LEAVES</span></a>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Employee</th>
			<th>Leave</th>
			<th>Days Allocated </th>
			<th>Start Date </th>
			<th>Duration(Days) </th>
			<th>Days Remianing</th>
			
<?php
//Authorization.
$auth->roleid="4239";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="4240";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="hrm_employeeleaves.id,hrm_employeeleaves.employeeid,hrm_employeeleaves.leaveid, concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname)) as employeename,hrm_employees.assignmentid, hrm_employeeleaves.startdate, hrm_employeeleaves.duration, hrm_employeeleaves.createdby,hrm_employeeleaves.remarks, hrm_leaves.name as leavename, hrm_employeeleaves.createdon, hrm_employeeleaves.lasteditedby, hrm_employeeleaves.lasteditedon";
		$join="left join hrm_employees on hrm_employeeleaves.employeeid=hrm_employees.id left join hrm_leaves on hrm_employeeleaves.leaveid=hrm_leaves.id ";
		$having="";
		$groupby="";
		$orderby="";
		$employeeleaves->retrieve($fields,$join,$where,$having,$groupby,$orderby);//echo $employeeleaves->sql;
		$res=$employeeleaves->result;
		while($row=mysql_fetch_object($res)){
		
		if (!empty($row->assignmentid)){
		  
		$assignments = New Assignments();
		$fields = "hrm_assignments.levelid";
		$join="";
		$where = " where id = $row->assignmentid ";
		$having="";
		$groupby="";
		$orderby="";
		$assignments->retrieve($fields,$join,$where,$having,$groupby,$orderby);//echo $assignments->sql;
		$rs=$assignments->result;
		$rww = mysql_fetch_object($rs);
		
		$levelleavedays = New Levelleavedays();
		$fields = "*";
		$join="";
		$where = " where levelid = $rww->levelid and leaveid = $row->leaveid";
		$having="";
		$groupby="";
		$orderby="";
		$levelleavedays->retrieve($fields,$join,$where,$having,$groupby,$orderby);//echo $levels->sql;
		$rs=$levelleavedays->result;
		$rw = mysql_fetch_object($rs);
		
		$masiku=$rw->leavedays;
		
		}
		
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->employeename; ?></td>
			<td><?php echo $row->leavename; ?><? echo $row->remarks;  ?></td>
			<td><?php echo $masiku; ?></td>
			<td><?php echo formatDate($row->startdate); ?></td>
			<td><?php echo $row->duration; ?></td>
			<td><?php echo ($masiku-$row->duration)?></td>

<?php
//Authorization.
$auth->roleid="4239";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addemployeeleaves_proc.php?id=<?php echo $row->id; ?>',540,450);"><img src="../view.png" alt="view" title="view" /></a></td>
<?php
}
//Authorization.
$auth->roleid="4240";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='employeeleaves.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')"><img src="../trash.png" alt="delete" title="delete" /></a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
