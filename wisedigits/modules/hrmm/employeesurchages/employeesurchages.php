<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Employeesurchages_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Employeesurchages";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="1170";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$employeesurchages=new Employeesurchages();
if(!empty($delid)){
	$employeesurchages->id=$delid;
	$employeesurchages->delete($employeesurchages);
	redirect("employeesurchages.php");
}
//Authorization.
$auth->roleid="1169";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <input onclick="showPopWin('addemployeesurchages_proc.php',600,430);" value="Add Employeesurchages " type="button"/></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Surcharge </th>
			<th>Employee </th>
			<th>Surchage Type </th>
			<th>Amount </th>
			<th>Charged On </th>
			<th>From </th>
			<th>From </th>
			<th>To </th>
			<th>To </th>
			<th>Remarks </th>
<?php
//Authorization.
$auth->roleid="1171";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="1172";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="hrm_employeesurchages.id, hrm_surchages.name as surchageid, hrm_employees.name as employeeid, hrm_surchagetypes.name as surchagetypeid, hrm_employeesurchages.amount, hrm_employeesurchages.chargedon, hrm_employeesurchages.frommonth, hrm_employeesurchages.fromyear, hrm_employeesurchages.tomonth, hrm_employeesurchages.toyear, hrm_employeesurchages.remarks, hrm_employeesurchages.createdby, hrm_employeesurchages.createdon, hrm_employeesurchages.lasteditedby, hrm_employeesurchages.lasteditedon";
		$join=" left join hrm_surchages on hrm_employeesurchages.surchageid=hrm_surchages.id  left join hrm_employees on hrm_employeesurchages.employeeid=hrm_employees.id  left join hrm_surchagetypes on hrm_employeesurchages.surchagetypeid=hrm_surchagetypes.id ";
		$having="";
		$groupby="";
		$orderby="";
		$employeesurchages->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$employeesurchages->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->surchageid; ?></td>
			<td><?php echo $row->employeeid; ?></td>
			<td><?php echo $row->surchagetypeid; ?></td>
			<td><?php echo formatNumber($row->amount); ?></td>
			<td><?php echo formatDate($row->chargedon); ?></td>
			<td><?php echo $row->frommonth; ?></td>
			<td><?php echo $row->fromyear; ?></td>
			<td><?php echo $row->tomonth; ?></td>
			<td><?php echo $row->toyear; ?></td>
			<td><?php echo $row->remarks; ?></td>
<?php
//Authorization.
$auth->roleid="1171";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addemployeesurchages_proc.php?id=<?php echo $row->id; ?>',600,430);">View</a></td>
<?php
}
//Authorization.
$auth->roleid="1172";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='employeesurchages.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
