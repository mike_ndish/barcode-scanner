<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Issuance_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
//Redirect to horizontal layout
redirect("addissuance_proc.php?retrieve=".$_GET['retrieve']);

$page_title="Issuance";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="4771";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$issuance=new Issuance();
if(!empty($delid)){
	$issuance->id=$delid;
	$issuance->delete($issuance);
	redirect("issuance.php");
}
//Authorization.
$auth->roleid="4770";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <a href='addissuance_proc.php'>New Issuance</a></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Item </th>
			<th>Department </th>
			<th>Member Of Staff </th>
			<th>Quantity </th>
			<th>Issued On </th>
			<th>Issue No </th>
			<th>Remarks </th>
			<th>Memo </th>
			<th>Received </th>
			<th>Received On </th>
<?php
//Authorization.
$auth->roleid="4772";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="4773";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="inv_issuance.id, inv_items.name as itemid, hrm_departments.name as departmentid, concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname)) as employeeid, inv_issuance.quantity, inv_issuance.issuedon, inv_issuance.documentno, inv_issuance.remarks, inv_issuance.memo, inv_issuance.received, inv_issuance.receivedon, inv_issuance.createdby, inv_issuance.createdon, inv_issuance.lasteditedby, inv_issuance.lasteditedon, inv_issuance.ipaddress";
		$join=" left join inv_items on inv_issuance.itemid=inv_items.id  left join hrm_departments on inv_issuance.departmentid=hrm_departments.id  left join hrm_employees on inv_issuance.employeeid=hrm_employees.id ";
		$having="";
		$groupby="";
		$orderby="";
		$issuance->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$issuance->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->itemid; ?></td>
			<td><?php echo $row->departmentid; ?></td>
			<td><?php echo $row->employeeid; ?></td>
			<td><?php echo formatNumber($row->quantity); ?></td>
			<td><?php echo formatDate($row->issuedon); ?></td>
			<td><?php echo formatNumber($row->documentno); ?></td>
			<td><?php echo $row->remarks; ?></td>
			<td><?php echo $row->memo; ?></td>
			<td><?php echo $row->received; ?></td>
			<td><?php echo formatDate($row->receivedon); ?></td>
<?php
//Authorization.
$auth->roleid="4772";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="addissuance_proc.php?id=<?php echo $row->id; ?>">View</a></td>
<?php
}
//Authorization.
$auth->roleid="4773";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='issuance.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
