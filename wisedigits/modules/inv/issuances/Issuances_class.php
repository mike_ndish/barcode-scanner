<?php 
require_once("IssuancesDBO.php");
require_once("../issuancedetails/IssuancedetailsDBO.php");
class Issuances
{				
	var $id;			
	var $departmentid;			
	var $employeeid;			
	var $issuedon;			
	var $documentno;			
	var $memo;			
	var $received;			
	var $receivedon;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $ipaddress;			
	var $issuancesDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		if(empty($obj->departmentid))
			$obj->departmentid='NULL';
		$this->departmentid=$obj->departmentid;
		if(empty($obj->employeeid))
			$obj->employeeid='NULL';
		$this->employeeid=$obj->employeeid;
		$this->issuedon=str_replace("'","\'",$obj->issuedon);
		$this->documentno=str_replace("'","\'",$obj->documentno);
		$this->memo=str_replace("'","\'",$obj->memo);
		$this->received=str_replace("'","\'",$obj->received);
		$this->receivedon=str_replace("'","\'",$obj->receivedon);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get departmentid
	function getDepartmentid(){
		return $this->departmentid;
	}
	//set departmentid
	function setDepartmentid($departmentid){
		$this->departmentid=$departmentid;
	}

	//get employeeid
	function getEmployeeid(){
		return $this->employeeid;
	}
	//set employeeid
	function setEmployeeid($employeeid){
		$this->employeeid=$employeeid;
	}

	//get issuedon
	function getIssuedon(){
		return $this->issuedon;
	}
	//set issuedon
	function setIssuedon($issuedon){
		$this->issuedon=$issuedon;
	}

	//get documentno
	function getDocumentno(){
		return $this->documentno;
	}
	//set documentno
	function setDocumentno($documentno){
		$this->documentno=$documentno;
	}

	//get memo
	function getMemo(){
		return $this->memo;
	}
	//set memo
	function setMemo($memo){
		$this->memo=$memo;
	}

	//get received
	function getReceived(){
		return $this->received;
	}
	//set received
	function setReceived($received){
		$this->received=$received;
	}

	//get receivedon
	function getReceivedon(){
		return $this->receivedon;
	}
	//set receivedon
	function setReceivedon($receivedon){
		$this->receivedon=$receivedon;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	function add($obj,$shop){
		$issuancesDBO = new IssuancesDBO();
			if($issuancesDBO->persist($obj)){		
				$issuancedetails = new Issuancedetails();
				$obj->issuanceid=$issuancesDBO->id;
				$issuancedetails->add($obj,$shop);

				$this->id=$issuancesDBO->id;
				$this->sql=$issuancesDBO->sql;
			}
		return true;	
	}			
	function edit($obj,$where="",$shop){
		$issuancesDBO = new IssuancesDBO();

		//first delete all records under old documentno
		$where=" where documentno='$obj->olddocumentno' and mode='$obj->oldmode'";
		$issuancesDBO->delete($obj,$where);

		$gn = new GeneralJournals();
		$where=" where documentno='$obj->olddocumentno' and transactionid='2' mode='$obj->oldmode' ";
		$gn->delete($obj,$where);

		$num=count($shop);
		$i=0;
		$total=0;
		while($i<$num){
			$obj->quantity=$shop['quantity'];
			$obj->remarks=$shop['remarks'];
			$obj->itemid=$shop['itemid'];
			if($issuancesDBO->update($obj,$where)){
				$this->sql=$issuancesDBO->sql;
			}
		}
		return true;	
	}			
	function delete($obj,$where=""){			
		$issuancesDBO = new IssuancesDBO();
		if($issuancesDBO->delete($obj,$where=""))		
			$this->sql=$issuancesDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$issuancesDBO = new IssuancesDBO();
		$this->table=$issuancesDBO->table;
		$issuancesDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$issuancesDBO->sql;
		$this->result=$issuancesDBO->result;
		$this->fetchObject=$issuancesDBO->fetchObject;
		$this->affectedRows=$issuancesDBO->affectedRows;
	}			
	function validate($obj){
	
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
