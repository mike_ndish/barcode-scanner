<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Issuances_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../hrm/departments/Departments_class.php");
require_once("../../hrm/employees/Employees_class.php");
require_once("../issuancedetails/Issuancedetails_class.php");
require_once("../../inv/items/Items_class.php");
//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="8381";//Edit
}
else{
	$auth->roleid="8379";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
if(empty($obj->action)){
	$obj->issuedon=date('Y-m-d');

	$defs=mysql_fetch_object(mysql_query("select (max(documentno)+1) documentno from inv_issuance"));
	if($defs->documentno == null){
		$defs->documentno=1;
	}
	$obj->documentno=$defs->documentno;

}
	
if($obj->action=="Save"){
	$issuances=new Issuances();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$shpissuances=$_SESSION['shpissuances'];
	$error=$issuances->validates($obj);
	if(!empty($error)){
		$error=$error;
	}
	elseif(empty($shpissuances)){
		$error="No items in the sale list!";
	}
	else{
		$issuances=$issuances->setObject($obj);
		if($issuances->add($issuances,$shpissuances)){
			$error=SUCCESS;
			$saved="Yes";
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$issuances=new Issuances();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$issuances->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$issuances=$issuances->setObject($obj);
		$shpissuances=$_SESSION['shpissuances'];
		if($issuances->edit($issuances,$shpissuances)){
			$error=UPDATESUCCESS;
			redirect("addissuances_proc.php?id=".$issuances->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if($obj->action2=="Add"){

	if(empty($obj->itemid)){
		$error=" must be provided";
	}
	else{
	$_SESSION['obj']=$obj;
	if(empty($obj->iterator))
		$it=0;
	else
		$it=$obj->iterator;
	$shpissuances=$_SESSION['shpissuances'];

	$items = new Items();
	$fields=" * ";
	$join="";
	$groupby="";
	$having="";
	$where=" where id='$obj->itemid'";
	$items->retrieve($fields, $join, $where, $having, $groupby, $orderby);
	$items=$items->fetchObject;

	;
	$shpissuances[$it]=array('quantity'=>"$obj->quantity", 'remarks'=>"$obj->remarks", 'itemid'=>"$obj->itemid", 'itemname'=>"$items->name", 'code'=>"$obj->code", 'total'=>"$obj->total");

 	$it++;
		$obj->iterator=$it;
 	$_SESSION['shpissuances']=$shpissuances;

	$obj->quantity="";
 	$obj->remarks="";
 	$obj->itemid="";
 }
}

if(empty($obj->action)){

	$departments= new Departments();
	$fields="hrm_departments.id, hrm_departments.name, hrm_departments.code, hrm_departments.leavemembers, hrm_departments.description, hrm_departments.createdby, hrm_departments.createdon, hrm_departments.lasteditedby, hrm_departments.lasteditedon, hrm_departments.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$departments->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$employees= new Employees();
	$fields="hrm_employees.id, hrm_employees.pfnum, hrm_employees.firstname, hrm_employees.middlename, hrm_employees.lastname, hrm_employees.gender, hrm_employees.bloodgroup, hrm_employees.rhd, hrm_employees.supervisorid, hrm_employees.startdate, hrm_employees.enddate, hrm_employees.dob, hrm_employees.idno, hrm_employees.passportno, hrm_employees.phoneno, hrm_employees.email, hrm_employees.officemail, hrm_employees.physicaladdress, hrm_employees.nationalityid, hrm_employees.countyid, hrm_employees.constituencyid, hrm_employees.location, hrm_employees.town, hrm_employees.marital, hrm_employees.spouse, hrm_employees.spouseidno, hrm_employees.spousetel, hrm_employees.spouseemail, hrm_employees.nssfno, hrm_employees.nhifno, hrm_employees.pinno, hrm_employees.helbno, hrm_employees.employeebankid, hrm_employees.bankbrancheid, hrm_employees.bankacc, hrm_employees.clearingcode, hrm_employees.ref, hrm_employees.basic, hrm_employees.assignmentid, hrm_employees.gradeid, hrm_employees.statusid, hrm_employees.image, hrm_employees.createdby, hrm_employees.createdon, hrm_employees.lasteditedby, hrm_employees.lasteditedon, hrm_employees.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$employees->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$issuances=new Issuances();
	$where=" where id=$id ";
	$fields="inv_issuances.id, inv_issuances.departmentid, inv_issuances.employeeid, inv_issuances.issuedon, inv_issuances.documentno, inv_issuances.memo, inv_issuances.received, inv_issuances.receivedon, inv_issuances.createdby, inv_issuances.createdon, inv_issuances.lasteditedby, inv_issuances.lasteditedon, inv_issuances.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$issuances->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$issuances->fetchObject;

	//for autocompletes
	$departments = new Departments();
	$fields=" * ";
	$where=" where id='$obj->departmentid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$departments->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$departments->fetchObject;

	$obj->departmentname=$auto->name;
	$employees = new Employees();
	$fields=" * ";
	$where=" where id='$obj->employeeid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$employees->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$employees->fetchObject;

	$obj->employeename=$auto->name;
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Issuances ";
include "addissuances.php";
?>