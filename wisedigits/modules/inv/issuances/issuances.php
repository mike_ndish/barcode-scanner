<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Issuances_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
//Redirect to horizontal layout
redirect("addissuances_proc.php?retrieve=".$_GET['retrieve']);

$page_title="Issuances";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="8380";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$issuances=new Issuances();
if(!empty($delid)){
	$issuances->id=$delid;
	$issuances->delete($issuances);
	redirect("issuances.php");
}
//Authorization.
$auth->roleid="8379";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <a href='addissuances_proc.php'>New Issuances</a></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Department </th>
			<th>Member Of Staff </th>
			<th>Issued On </th>
			<th>Issue No </th>
			<th>Memo </th>
			<th>Received </th>
			<th>Received On </th>
<?php
//Authorization.
$auth->roleid="8381";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="8382";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="inv_issuances.id, hrm_departments.name as departmentid, hrm_employees.name as employeeid, inv_issuances.issuedon, inv_issuances.documentno, inv_issuances.memo, inv_issuances.received, inv_issuances.receivedon, inv_issuances.createdby, inv_issuances.createdon, inv_issuances.lasteditedby, inv_issuances.lasteditedon, inv_issuances.ipaddress";
		$join=" left join hrm_departments on inv_issuances.departmentid=hrm_departments.id  left join hrm_employees on inv_issuances.employeeid=hrm_employees.id ";
		$having="";
		$groupby="";
		$orderby="";
		$issuances->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$issuances->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->departmentid; ?></td>
			<td><?php echo $row->employeeid; ?></td>
			<td><?php echo formatDate($row->issuedon); ?></td>
			<td><?php echo formatNumber($row->documentno); ?></td>
			<td><?php echo $row->memo; ?></td>
			<td><?php echo $row->received; ?></td>
			<td><?php echo formatDate($row->receivedon); ?></td>
<?php
//Authorization.
$auth->roleid="8381";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="addissuances_proc.php?id=<?php echo $row->id; ?>">View</a></td>
<?php
}
//Authorization.
$auth->roleid="8382";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='issuances.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
