<?php
session_start();

$page_title="Items";
include"../../../head.php";
?>
<ul id="cmd-buttons">
	<li><a class="button icon chat" href="../../inv/items/items.php">Items</a></li>
	<li><a class="button icon chat" href="../../inv/departmentcategorys/departmentcategorys.php">Dept Categories</a></li>
	<li><a class="button icon chat" href="../../inv/categorys/categorys.php">Categories</a></li>
	<li><a class="button icon chat" href="../../inv/purchases/purchases.php">Purchases</a></li>
	<li><a class="button icon chat" href="../../inv/purchases/purchases.php?retrieve=1">Retrieve Purchases</a></li>
	<li><a class="button icon chat" href="../../inv/departments/departments.php">Departments</a></li>
	<li><a class="button icon chat" href="../../inv/returnoutwards/returnoutwards.php">Credit Note</a></li>
	<li><a class="button icon chat" href="../../inv/returnoutwards/returnoutwards.phpretrieve?=1">Retrieve Credit Note</a></li>
	<li><a class="button icon chat" href="../../inv/returnnotes/returnnotes.php">Return Notes</a></li>
	<li><a class="button icon chat" href="../../inv/returnnotes/returnnotes.php?retrieve=1">Retrieve Return Notes</a></li>
	<li><a class="button icon chat" href="../../inv/subdivides/subdivides.php">Re-packaging</a></li>
	<li><a class="button icon chat" href="../../inv/issuance/issuance.php">Issuance</a></li>
	<li><a class="button icon chat" href="../../inv/unitofmeasures/unitofmeasures.php">Unit of Measures</a></li>
</ul>
<?php
include"../../../foot.php";
?>
