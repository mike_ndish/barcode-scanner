<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Items_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Items";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="704";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$items=new Items();
if(!empty($delid)){
	$items->id=$delid;
	$items->delete($items);
	redirect("items.php");
}
//Authorization.
$auth->roleid="703";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div class="container">
<a class="btn btn-info" onclick="showPopWin('additems_proc.php',600,430);">Add Items</a>
<?php }?>
<hr>

<script type="text/javascript" charset="utf-8">
 <?php $_SESSION['aColumns']=array('inv_items.id','inv_items.code code','inv_items.name','inv_departments.name departmentid','inv_departmentcategorys.name deptcategoryid','inv_categorys.name categoryid','inv_items.quantity','inv_items.status','1','1');?>
 <?php $_SESSION['sColumns']=array('id','code','name','departmentid','deptcategoryid','categoryid','quantity','status','1','1');?>
 <?php $_SESSION['join']=" left join inv_departments on inv_departments.id=inv_items.departmentid left join inv_departmentcategorys on inv_departmentcategorys.id=inv_items.departmentcategoryid left join inv_categorys on inv_categorys.id=inv_items.categoryid ";?>
 <?php $_SESSION['sTable']="inv_items";?>
 <?php $_SESSION['sOrder']=" ";?>
 <?php $_SESSION['sWhere']="";?>
 <?php $_SESSION['sGroup']="";?>
 $(document).ready(function() {
	TableTools.DEFAULTS.aButtons = [ "copy", "csv", "xls","pdf" ];
 	$('#tbl').dataTable( {
		"sDom": 'T<"H"lfr>t<"F"ip>',
		"oTableTools": {
			"sSwfPath": "../../../media/swf/copy_cvs_xls_pdf.swf"
		},
 		"bJQueryUI": true,
 		"bSort":true,
 		"sPaginationType": "full_numbers",
 		"sScrollY": 250,
		"bJQueryUI": true,
		"bRetrieve":true,
		"sAjaxSource": "../../server/server/processing.php?sTable=inv_items",
		"fnRowCallback": function( nRow, aaData, iDisplayIndex ) {
			
			$('td:eq(0)', nRow).html(iDisplayIndex+1);
			$('td:eq(1)', nRow).html("<a href='../generaljournal/account.php?id="+aaData[0]+"'>"+aaData[1]+"</a>");
			$('td:eq(2)', nRow).html(aaData[2]);
			$('td:eq(3)', nRow).html(aaData[3]);
			$('td:eq(4)', nRow).html(aaData[4]);
			$('td:eq(5)', nRow).html(aaData[5]);
			$('td:eq(6)', nRow).html(aaData[6]);
			$('td:eq(7)', nRow).html(aaData[7]);
			$('td:eq(8)', nRow).html("<a href='javascript:;' onclick='showPopWin(&quot;addtenants_proc.php?id="+aaData[0]+"&quot;, 600, 600);'><img src='../view.png' alt='view' title='view' /></a>");
			$('td:eq(9)', nRow).html("<a href='javascript:;' onclick='showPopWin(&quot;vacant.php?tenantid="+aaData[0]+"&quot;, 600, 600);'>Allocate</a>");
			return nRow;
		}
 	} );
 } );
 </script>
 
<table style="clear:both;"  class="tgrid display" id="tbl" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Code </th>
			<th>Name </th>
			<th>Department </th>
			<th>Dept Category </th>
			<th>Categoryid </th>
			<th>Quantity </th>
			<th>Status </th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
