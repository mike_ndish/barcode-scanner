<?php 
require_once("PurchasesDBO.php");
require_once("../purchasedetails/PurchasedetailsDBO.php");
class Purchases
{				
	var $id;			
	var $documentno;			
	var $lpono;			
	var $storeid;			
	var $supplierid;			
	var $batchno;			
	var $remarks;			
	var $purchasemodeid;			
	var $boughton;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $ipaddress;			
	var $projectid;			
	var $purchasesDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->documentno=str_replace("'","\'",$obj->documentno);
		$this->lpono=str_replace("'","\'",$obj->lpono);
		if(empty($obj->storeid))
			$obj->storeid='NULL';
		$this->storeid=$obj->storeid;
		if(empty($obj->supplierid))
			$obj->supplierid='NULL';
		$this->supplierid=$obj->supplierid;
		$this->batchno=str_replace("'","\'",$obj->batchno);
		$this->remarks=str_replace("'","\'",$obj->remarks);
		if(empty($obj->purchasemodeid))
			$obj->purchasemodeid='NULL';
		$this->purchasemodeid=$obj->purchasemodeid;
		$this->boughton=str_replace("'","\'",$obj->boughton);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		if(empty($obj->projectid))
			$obj->projectid='NULL';
		$this->projectid=$obj->projectid;
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get documentno
	function getDocumentno(){
		return $this->documentno;
	}
	//set documentno
	function setDocumentno($documentno){
		$this->documentno=$documentno;
	}

	//get lpono
	function getLpono(){
		return $this->lpono;
	}
	//set lpono
	function setLpono($lpono){
		$this->lpono=$lpono;
	}

	//get storeid
	function getStoreid(){
		return $this->storeid;
	}
	//set storeid
	function setStoreid($storeid){
		$this->storeid=$storeid;
	}

	//get supplierid
	function getSupplierid(){
		return $this->supplierid;
	}
	//set supplierid
	function setSupplierid($supplierid){
		$this->supplierid=$supplierid;
	}

	//get batchno
	function getBatchno(){
		return $this->batchno;
	}
	//set batchno
	function setBatchno($batchno){
		$this->batchno=$batchno;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	//get purchasemodeid
	function getPurchasemodeid(){
		return $this->purchasemodeid;
	}
	//set purchasemodeid
	function setPurchasemodeid($purchasemodeid){
		$this->purchasemodeid=$purchasemodeid;
	}

	//get boughton
	function getBoughton(){
		return $this->boughton;
	}
	//set boughton
	function setBoughton($boughton){
		$this->boughton=$boughton;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	//get projectid
	function getProjectid(){
		return $this->projectid;
	}
	//set projectid
	function setProjectid($projectid){
		$this->projectid=$projectid;
	}

	function add($obj,$shop){
		$purchasesDBO = new PurchasesDBO();
			if($purchasesDBO->persist($obj)){		
				$purchasedetails = new Purchasedetails();
				$obj->purchaseid=$purchasesDBO->id;
				$purchasedetails->add($obj,$shop);

				$this->id=$purchasesDBO->id;
				$this->sql=$purchasesDBO->sql;
			}
		return true;	
	}			
	function edit($obj,$where="",$shop){
		$purchasesDBO = new PurchasesDBO();

		//first delete all records under old documentno
		$where=" where documentno='$obj->olddocumentno' and mode='$obj->oldmode'";
		$purchasesDBO->delete($obj,$where);

		$gn = new GeneralJournals();
		$where=" where documentno='$obj->olddocumentno' and transactionid='2' mode='$obj->oldmode' ";
		$gn->delete($obj,$where);

		$num=count($shop);
		$i=0;
		$total=0;
		while($i<$num){
			$obj->quantity=$shop['quantity'];
			$obj->itemid=$shop['itemid'];
			$obj->remarks=$shop['remarks'];
			if($purchasesDBO->update($obj,$where)){
				$this->sql=$purchasesDBO->sql;
			}
		}

				//Make a journal entry

				//retrieve account to debit
		$generaljournalaccounts = new Generaljournalaccounts();
		$fields="*";
		$where=" where refid='$obj->supplierid' and acctypeid='30'";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$generaljournalaccounts->retrieve($fields, $join, $where, $having, $groupby, $orderby);
		$generaljournalaccounts=$generaljournalaccounts->fetchObject;

				//retrieve account to credit
		$generaljournalaccounts2 = new Generaljournalaccounts();
		$fields="*";
		$where=" where refid='1' and acctypeid='26'";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$generaljournalaccounts2->retrieve($fields, $join, $where, $having, $groupby, $orderby);
		$generaljournalaccounts2=$generaljournalaccounts2->fetchObject;

				//Get transaction Identity
		$transaction = new Transactions();
		$fields="*";
		$where=" where lower(replace(name,' ',''))='purchases'";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$transaction->retrieve($fields, $join, $where, $having, $groupby, $orderby);
		$transaction=$transaction->fetchObject;

		$ob->transactdate=$obj->boughton;

				//make debit entry
		$generaljournal = new Generaljournals();
		$ob->tid=$purchases->id;
		$ob->documentno="$obj->documentno";
		$ob->remarks="Purchase of Invoice $obj->documentno";
		$ob->memo=$purchases->remarks;
		$ob->accountid=$generaljournalaccounts->id;
		$ob->daccountid=$generaljournalaccounts2->id;
		$ob->transactionid=$transaction->id;
		$ob->mode="credit";
		$ob->debit=0;
		$ob->credit=$total;
		$generaljournal->setObject($ob);
		$generaljournal->add($generaljournal);

				//make credit entry
		$generaljournal2 = new Generaljournals();
		$ob->tid=$purchases->id;
		$ob->documentno=$obj->documentno;
		$ob->remarks="Invoice $obj->documentno from $obj->suppliername";
		$ob->memo=$purchases->remarks;
		$ob->daccountid=$generaljournalaccounts->id;
		$ob->accountid=$generaljournalaccounts2->id;
		$ob->transactionid=$transaction->id;
		$ob->mode="credit";
		$ob->debit=$total;
		$ob->credit=0;
		$ob->did=$generaljournal->id;
		$generaljournal2->setObject($ob);
		$generaljournal2->add($generaljournal2);

		$generaljournal->did=$generaljournal2->id;
		$generaljournal->edit($generaljournal);

		return true;	
	}			
	function delete($obj,$where=""){			
		$purchasesDBO = new PurchasesDBO();
		if($purchasesDBO->delete($obj,$where=""))		
			$this->sql=$purchasesDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$purchasesDBO = new PurchasesDBO();
		$this->table=$purchasesDBO->table;
		$purchasesDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$purchasesDBO->sql;
		$this->result=$purchasesDBO->result;
		$this->fetchObject=$purchasesDBO->fetchObject;
		$this->affectedRows=$purchasesDBO->affectedRows;
	}			
	function validate($obj){
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
		if(empty($obj->boughton)){
			$error="Purchase Date should be provided";
		}
		elseif(empty($obj->documentno)){
			$error="Invoice/Receipt No should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}
}				
?>
