<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Requisitions_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../inv/items/Items_class.php");
//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="7504";//Edit
}
else{
	$auth->roleid="7502";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
	
if($obj->action=="Save"){
	$requisitions=new Requisitions();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$shprequisitions=$_SESSION['shprequisitions'];
	$error=$requisitions->validates($obj);
	if(!empty($error)){
		$error=$error;
	}
	elseif(empty($shprequisitions)){
		$error="No items in the sale list!";
	}
	else{
		$requisitions=$requisitions->setObject($obj);
		if($requisitions->add($requisitions,$shprequisitions)){
			$error=SUCCESS;
			redirect("addrequisitions_proc.php?id=".$requisitions->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$requisitions=new Requisitions();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$requisitions->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$requisitions=$requisitions->setObject($obj);
		$shprequisitions=$_SESSION['shprequisitions'];
		if($requisitions->edit($requisitions,$shprequisitions)){
			$error=UPDATESUCCESS;
			redirect("addrequisitions_proc.php?id=".$requisitions->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if($obj->action2=="Add"){

	if(empty($obj->itemid)){
		$error="Item must be provided";
	}
	elseif(empty($obj->quantity)){
		$error="Quantity must be provided";
	}
	else{
	$_SESSION['obj']=$obj;
	if(empty($obj->iterator))
		$it=0;
	else
		$it=$obj->iterator;
	$shprequisitions=$_SESSION['shprequisitions'];

	$items = new Items();
	$fields=" * ";
	$join="";
	$groupby="";
	$having="";
	$where=" where id='$obj->itemid'";
	$items->retrieve($fields, $join, $where, $having, $groupby, $orderby);
	$items=$items->fetchObject;

	;
	$shprequisitions[$it]=array('itemid'=>"$obj->itemid", 'itemname'=>"$items->name", 'quantity'=>"$obj->quantity", 'memo'=>"$obj->memo", 'total'=>"$obj->total");

 	$it++;
		$obj->iterator=$it;
 	$_SESSION['shprequisitions']=$shprequisitions;

	$obj->itemname="";
 	$obj->itemid="";
 	$obj->quantity="";
 	$obj->memo="";
 }
}

if(empty($obj->action)){

	$items= new Items();
	$fields="inv_items.id, inv_items.code, inv_items.name, inv_items.departmentid, inv_items.departmentcategoryid, inv_items.categoryid, inv_items.manufacturer, inv_items.strength, inv_items.costprice, inv_items.tradeprice, inv_items.retailprice, inv_items.size, inv_items.unitofmeasureid, inv_items.vatclasseid, inv_items.generaljournalaccountid, inv_items.generaljournalaccountid2, inv_items.discount, inv_items.reorderlevel, inv_items.reorderquantity, inv_items.quantity, inv_items.reducing, inv_items.status, inv_items.createdby, inv_items.createdon, inv_items.lasteditedby, inv_items.lasteditedon, inv_items.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$items->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$requisitions=new Requisitions();
	$where=" where id=$id ";
	$fields="inv_requisitions.id, inv_requisitions.documentno, inv_requisitions.itemid, inv_requisitions.quantity, inv_requisitions.memo, inv_requisitions.requisitiondate, inv_requisitions.remarks, inv_requisitions.status, inv_requisitions.ipaddress, inv_requisitions.createdby, inv_requisitions.createdon, inv_requisitions.lasteditedby, inv_requisitions.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$requisitions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$requisitions->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Requisitions ";
include "addrequisitions.php";
?>