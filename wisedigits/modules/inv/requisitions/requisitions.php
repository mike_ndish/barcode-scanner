<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Requisitions_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
//Redirect to horizontal layout
redirect("addrequisitions_proc.php?retrieve=".$_GET['retrieve']);

$page_title="Requisitions";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="7503";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$requisitions=new Requisitions();
if(!empty($delid)){
	$requisitions->id=$delid;
	$requisitions->delete($requisitions);
	redirect("requisitions.php");
}
//Authorization.
$auth->roleid="7502";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <a href='addrequisitions_proc.php'>New Requisitions</a></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Requisition No </th>
			<th>Item </th>
			<th>Quantity </th>
			<th>Memo </th>
			<th>Requisition Date </th>
			<th>Remarks </th>
			<th>Status </th>
<?php
//Authorization.
$auth->roleid="7504";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="7505";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="inv_requisitions.id, inv_requisitions.documentno, inv_items.name as itemid, inv_requisitions.quantity, inv_requisitions.memo, inv_requisitions.requisitiondate, inv_requisitions.remarks, inv_requisitions.status, inv_requisitions.ipaddress, inv_requisitions.createdby, inv_requisitions.createdon, inv_requisitions.lasteditedby, inv_requisitions.lasteditedon";
		$join=" left join inv_items on inv_requisitions.itemid=inv_items.id ";
		$having="";
		$groupby="";
		$orderby="";
		$requisitions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$requisitions->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->documentno; ?></td>
			<td><?php echo $row->itemid; ?></td>
			<td><?php echo formatNumber($row->quantity); ?></td>
			<td><?php echo $row->memo; ?></td>
			<td><?php echo formatDate($row->requisitiondate); ?></td>
			<td><?php echo $row->remarks; ?></td>
			<td><?php echo $row->status; ?></td>
<?php
//Authorization.
$auth->roleid="7504";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="addrequisitions_proc.php?id=<?php echo $row->id; ?>">View</a></td>
<?php
}
//Authorization.
$auth->roleid="7505";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='requisitions.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
