<?php 
require_once("ReturnoutwarddetailsDBO.php");
class Returnoutwarddetails
{				
	var $id;			
	var $returnoutwardid;			
	var $itemid;			
	var $quantity;			
	var $costprice;			
	var $tax;			
	var $discount;			
	var $total;			
	var $ipaddress;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $returnoutwarddetailsDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		if(empty($obj->returnoutwardid))
			$obj->returnoutwardid='NULL';
		$this->returnoutwardid=$obj->returnoutwardid;
		if(empty($obj->itemid))
			$obj->itemid='NULL';
		$this->itemid=$obj->itemid;
		$this->quantity=str_replace("'","\'",$obj->quantity);
		$this->costprice=str_replace("'","\'",$obj->costprice);
		$this->tax=str_replace("'","\'",$obj->tax);
		$this->discount=str_replace("'","\'",$obj->discount);
		$this->total=str_replace("'","\'",$obj->total);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get returnoutwardid
	function getReturnoutwardid(){
		return $this->returnoutwardid;
	}
	//set returnoutwardid
	function setReturnoutwardid($returnoutwardid){
		$this->returnoutwardid=$returnoutwardid;
	}

	//get itemid
	function getItemid(){
		return $this->itemid;
	}
	//set itemid
	function setItemid($itemid){
		$this->itemid=$itemid;
	}

	//get quantity
	function getQuantity(){
		return $this->quantity;
	}
	//set quantity
	function setQuantity($quantity){
		$this->quantity=$quantity;
	}

	//get costprice
	function getCostprice(){
		return $this->costprice;
	}
	//set costprice
	function setCostprice($costprice){
		$this->costprice=$costprice;
	}

	//get tax
	function getTax(){
		return $this->tax;
	}
	//set tax
	function setTax($tax){
		$this->tax=$tax;
	}

	//get discount
	function getDiscount(){
		return $this->discount;
	}
	//set discount
	function setDiscount($discount){
		$this->discount=$discount;
	}

	//get total
	function getTotal(){
		return $this->total;
	}
	//set total
	function setTotal($total){
		$this->total=$total;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj,$shop){
		$returnoutwarddetailsDBO = new ReturnoutwarddetailsDBO();
		$num=count($shop);
		$i=0;
		$total=0;
		while($i<$num){
			$obj->itemid=$shop[$i]['itemid'];
			$obj->itemname=$shop[$i]['itemname'];
			$obj->quantity=$shop[$i]['quantity'];
			$obj->costprice=$shop[$i]['costprice'];
			$obj->tax=$shop[$i]['tax'];
			$obj->discount=$shop[$i]['discount'];
			$obj->total=$shop[$i]['total'];
			$obj->remarks=$shop[$i]['remarks'];
			if($returnoutwarddetailsDBO->persist($obj)){		
				$this->id=$returnoutwarddetailsDBO->id;
				$this->sql=$returnoutwarddetailsDBO->sql;
			}
			$i++;
		}
		return true;	
	}			
	function edit($obj,$where=""){
		$returnoutwarddetailsDBO = new ReturnoutwarddetailsDBO();
		if($returnoutwarddetailsDBO->update($obj,$where)){
			$this->sql=$returnoutwarddetailsDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$returnoutwarddetailsDBO = new ReturnoutwarddetailsDBO();
		if($returnoutwarddetailsDBO->delete($obj,$where=""))		
			$this->sql=$returnoutwarddetailsDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$returnoutwarddetailsDBO = new ReturnoutwarddetailsDBO();
		$this->table=$returnoutwarddetailsDBO->table;
		$returnoutwarddetailsDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$returnoutwarddetailsDBO->sql;
		$this->result=$returnoutwarddetailsDBO->result;
		$this->fetchObject=$returnoutwarddetailsDBO->fetchObject;
		$this->affectedRows=$returnoutwarddetailsDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->returnoutwardid)){
			$error="Return Outward should be provided";
		}
		else if(empty($obj->itemid)){
			$error="Item should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
