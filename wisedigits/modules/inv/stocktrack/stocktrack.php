<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Stocktrack_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Stocktrack";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="728";//View
$auth->levelid=$_SESSION['level'];

$ob = (object)$_GET;

auth($auth);
include"../../../rptheader.php";

$delid=$_GET['delid'];
$itemid=$_GET['itemid'];
$stocktrack=new Stocktrack();
if(!empty($delid)){
	$stocktrack->id=$delid;
	$stocktrack->delete($stocktrack);
	redirect("stocktrack.php");
}
//Authorization.
$auth->roleid="727";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<!-- <div style="float:left;" class="buttons"> <input onclick="showPopWin('addstocktrack_proc.php',600,430);" value="Add Stocktrack " type="button"/></div> -->
<?php }?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

	//TableToolsInit.sSwfPath = "../../../media/swf/ZeroClipboard.swf";
	$('#example').dataTable( {
		"sScrollY": 500,
		"bJQueryUI": true,
		"iDisplayLength":20,
		"sPaginationType": "full_numbers"
	} );
} );
</script> 
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Item Name </th>
			<th>Document No. </th>
			<th>Quantity </th>
			<th>Remain </th>
			<th>Transaction </th>
			<th>Created By</th>
			<th>Record Date</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="inv_stocktrack.id, inv_items.name itemid, auth_users.username username, inv_stocktrack.tid, inv_stocktrack.documentno, inv_stocktrack.batchno, inv_stocktrack.quantity, inv_stocktrack.costprice, inv_stocktrack.value, inv_stocktrack.discount, inv_stocktrack.tradeprice, inv_stocktrack.retailprice, inv_stocktrack.applicabletax, inv_stocktrack.expirydate, inv_stocktrack.recorddate, inv_stocktrack.status, inv_stocktrack.remain, inv_stocktrack.transaction, inv_stocktrack.createdby, inv_stocktrack.createdon, inv_stocktrack.lasteditedby, inv_stocktrack.lasteditedon, inv_stocktrack.ipaddress";
		$join=" left join inv_items on inv_items.id=inv_stocktrack.itemid left join auth_users on auth_users.id=inv_stocktrack.createdby ";
		$having="";
		$groupby="";
		$orderby="";
		if(!empty($ob->itemid))
		  $where=" where inv_items.id='$ob->itemid' ";
		$stocktrack->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$stocktrack->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->itemid; ?></td>
			<td><?php echo $row->documentno; ?></td>
			<td align="right"><?php echo $row->quantity; ?></td>
			<td align="right"><?php echo $row->remain; ?></td>
			<td><?php echo $row->transaction; ?></td>
			<td><?php echo $row->username; ?></td>
			<td><?php echo formatDate($row->recorddate); ?></td>

		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
