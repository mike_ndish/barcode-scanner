<?php 
require_once("TransfersDBO.php");
class Transfers
{				
	var $id;			
	var $documentno;			
	var $storeid;			
	var $tostoreid;			
	var $itemid;			
	var $quantity;			
	var $remarks;			
	var $transferedon;			
	var $memo;			
	var $ipaddress;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $transfersDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->documentno=str_replace("'","\'",$obj->documentno);
		if(empty($obj->storeid))
			$obj->storeid='NULL';
		$this->storeid=$obj->storeid;
		if(empty($obj->tostoreid))
			$obj->tostoreid='NULL';
		$this->tostoreid=$obj->tostoreid;
		if(empty($obj->itemid))
			$obj->itemid='NULL';
		$this->itemid=$obj->itemid;
		$this->quantity=str_replace("'","\'",$obj->quantity);
		$this->remarks=str_replace("'","\'",$obj->remarks);
		$this->transferedon=str_replace("'","\'",$obj->transferedon);
		$this->memo=str_replace("'","\'",$obj->memo);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get documentno
	function getDocumentno(){
		return $this->documentno;
	}
	//set documentno
	function setDocumentno($documentno){
		$this->documentno=$documentno;
	}

	//get storeid
	function getStoreid(){
		return $this->storeid;
	}
	//set storeid
	function setStoreid($storeid){
		$this->storeid=$storeid;
	}

	//get tostoreid
	function getTostoreid(){
		return $this->tostoreid;
	}
	//set tostoreid
	function setTostoreid($tostoreid){
		$this->tostoreid=$tostoreid;
	}

	//get itemid
	function getItemid(){
		return $this->itemid;
	}
	//set itemid
	function setItemid($itemid){
		$this->itemid=$itemid;
	}

	//get quantity
	function getQuantity(){
		return $this->quantity;
	}
	//set quantity
	function setQuantity($quantity){
		$this->quantity=$quantity;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	//get transferedon
	function getTransferedon(){
		return $this->transferedon;
	}
	//set transferedon
	function setTransferedon($transferedon){
		$this->transferedon=$transferedon;
	}

	//get memo
	function getMemo(){
		return $this->memo;
	}
	//set memo
	function setMemo($memo){
		$this->memo=$memo;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj){
		$transfersDBO = new TransfersDBO();
		if($transfersDBO->persist($obj)){
			$this->id=$transfersDBO->id;
			$this->sql=$transfersDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$transfersDBO = new TransfersDBO();
		if($transfersDBO->update($obj,$where)){
			$this->sql=$transfersDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$transfersDBO = new TransfersDBO();
		if($transfersDBO->delete($obj,$where=""))		
			$this->sql=$transfersDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$transfersDBO = new TransfersDBO();
		$this->table=$transfersDBO->table;
		$transfersDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$transfersDBO->sql;
		$this->result=$transfersDBO->result;
		$this->fetchObject=$transfersDBO->fetchObject;
		$this->affectedRows=$transfersDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->documentno)){
			$error="Transfer No should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
		if(empty($obj->documentno)){
			$error="Transfer No should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}
}				
?>
