<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Transfers_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../inv/stores/Stores_class.php");
require_once("../../inv/items/Items_class.php");
require_once("../../inv/stores2/Stores2_class.php");
//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="7496";//Edit
}
else{
	$auth->roleid="7494";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
	
if($obj->action=="Save"){
	$transfers=new Transfers();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$error=$transfers->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$transfers=$transfers->setObject($obj);
		if($transfers->add($transfers)){
			$error=SUCCESS;
			redirect("addtransfers_proc.php?id=".$transfers->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$transfers=new Transfers();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$transfers->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$transfers=$transfers->setObject($obj);
		if($transfers->edit($transfers)){
			$error=UPDATESUCCESS;
			redirect("addtransfers_proc.php?id=".$transfers->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if(empty($obj->action)){

	$stores= new Stores();
	$fields="inv_stores.id, inv_stores.name, inv_stores.remarks, inv_stores.ipaddress, inv_stores.createdby, inv_stores.createdon, inv_stores.lasteditedby, inv_stores.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$stores->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$items= new Items();
	$fields="inv_items.id, inv_items.code, inv_items.name, inv_items.departmentid, inv_items.departmentcategoryid, inv_items.categoryid, inv_items.manufacturer, inv_items.strength, inv_items.costprice, inv_items.tradeprice, inv_items.retailprice, inv_items.size, inv_items.unitofmeasureid, inv_items.vatclasseid, inv_items.generaljournalaccountid, inv_items.generaljournalaccountid2, inv_items.discount, inv_items.reorderlevel, inv_items.reorderquantity, inv_items.quantity, inv_items.reducing, inv_items.status, inv_items.createdby, inv_items.createdon, inv_items.lasteditedby, inv_items.lasteditedon, inv_items.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$items->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$stores2= new Stores2();
	$fields="";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$stores2->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$transfers=new Transfers();
	$where=" where id=$id ";
	$fields="inv_transfers.id, inv_transfers.documentno, inv_transfers.storeid, inv_transfers.tostoreid, inv_transfers.itemid, inv_transfers.quantity, inv_transfers.remarks, inv_transfers.transferedon, inv_transfers.memo, inv_transfers.ipaddress, inv_transfers.createdby, inv_transfers.createdon, inv_transfers.lasteditedby, inv_transfers.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$transfers->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$transfers->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Transfers ";
include "addtransfers.php";
?>