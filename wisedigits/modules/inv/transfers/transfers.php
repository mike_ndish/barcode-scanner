<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Transfers_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Transfers";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="7495";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$transfers=new Transfers();
if(!empty($delid)){
	$transfers->id=$delid;
	$transfers->delete($transfers);
	redirect("transfers.php");
}
//Authorization.
$auth->roleid="7494";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <input onclick="showPopWin('addtransfers_proc.php',600,430);" value="Add Transfers " type="button"/></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Transfer No </th>
			<th>From Strore </th>
			<th>To Store </th>
			<th>Item </th>
			<th> </th>
			<th>Remarks </th>
			<th>Transfered On </th>
			<th>Memo </th>
<?php
//Authorization.
$auth->roleid="7496";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="7497";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="inv_transfers.id, inv_transfers.documentno, inv_stores.name as storeid, inv_stores2.name as tostoreid, inv_items.name as itemid, inv_transfers.quantity, inv_transfers.remarks, inv_transfers.transferedon, inv_transfers.memo, inv_transfers.ipaddress, inv_transfers.createdby, inv_transfers.createdon, inv_transfers.lasteditedby, inv_transfers.lasteditedon";
		$join=" left join inv_stores on inv_transfers.storeid=inv_stores.id  left join inv_stores2 on inv_transfers.tostoreid=inv_stores2.id  left join inv_items on inv_transfers.itemid=inv_items.id ";
		$having="";
		$groupby="";
		$orderby="";
		$transfers->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$transfers->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->documentno; ?></td>
			<td><?php echo $row->storeid; ?></td>
			<td><?php echo $row->tostoreid; ?></td>
			<td><?php echo $row->itemid; ?></td>
			<td><?php echo formatNumber($row->quantity); ?></td>
			<td><?php echo $row->remarks; ?></td>
			<td><?php echo formatDate($row->transferedon); ?></td>
			<td><?php echo $row->memo; ?></td>
<?php
//Authorization.
$auth->roleid="7496";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addtransfers_proc.php?id=<?php echo $row->id; ?>',600,430);">View</a></td>
<?php
}
//Authorization.
$auth->roleid="7497";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='transfers.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
