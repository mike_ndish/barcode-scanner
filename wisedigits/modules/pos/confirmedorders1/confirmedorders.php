<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Confirmedorders_class.php");
require_once("../../auth/rules/Rules_class.php");
require_once("../../pos/packinglists/Packinglists_class.php");
require_once("../../pos/confirmedorderdetails/Confirmedorderdetails_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
//Redirect to horizontal layout
//redirect("addconfirmedorders_proc.php?retrieve=".$_GET['retrieve']);

$page_title="Confirmedorders";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="8704";//View
$auth->levelid=$_SESSION['level'];

$obj = (object)$_POST;

if(empty($obj->orderedon)){
  $obj->orderedon=date("Y-m-d");
}

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$confirmedorders=new Confirmedorders();
if(!empty($delid)){
	$confirmedorders->id=$delid;
	$confirmedorders->delete($confirmedorders);
	redirect("confirmedorders.php");
}
//Authorization.
$auth->roleid="8703";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <a href='addconfirmedorders_proc.php'>New Confirmedorders</a></div>
<?php }?>

<form action="" method="post">
<div>
  <input name="orderedon" type="text" size="12" value="<?php echo $obj->orderedon; ?>" class="date_input" readonly/>
  <input type="submit" name="action" value="Filter"/>
</div>
</form>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Order No </th>
			<th>Customer </th>
			<th>Remarks </th>
			<th>Packing No</th>
			<th>Total Boxes</th>
			<th>Packed Boxes</th>
			<!--<th>Pack Rate</th>-->
<?php
//Authorization.
$auth->roleid="8705";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="8671";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="pos_confirmedorders.id, pos_confirmedorders.orderno, crm_customers.id customer, crm_customers.name as customerid, pos_confirmedorders.orderedon, pos_confirmedorders.remarks, pos_confirmedorders.ipaddress, pos_confirmedorders.createdby, pos_confirmedorders.createdon, pos_confirmedorders.lasteditedby, pos_confirmedorders.lasteditedon";
		$join=" left join crm_customers on pos_confirmedorders.customerid=crm_customers.id ";
		$having="";
		$groupby=" group by orderno ";
		$orderby="";
		if(!empty($obj->orderedon)){
		  $where=" where pos_confirmedorders.orderedon='$obj->orderedon' ";
		}
		$confirmedorders->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$confirmedorders->result;
		$totalboxes=0;
		$totalpacked=0;
		while($row=mysql_fetch_object($res)){
		$i++;
		$packinglists =new Packinglists();
		$fields="distinct pos_packinglists.boxno, pos_packinglists.documentno , count(*) boxes ";
		$join=" ";
		$having="";
		$groupby="";
		$orderby="";
		$where=" where pos_packinglists.orderno='$row->orderno'";
		$packinglists->retrieve($fields,$join,$where,$having,$groupby,$orderby);//echo $packinglists->sql;
		$packinglists = $packinglists->fetchObject;
		
		$confirmedorderdetails = new Confirmedorderdetails();
		$fields="sum(pos_confirmedorderdetails.quantity) quantity, pos_confirmedorderdetails.packrate";
		$join=" left join pos_confirmedorders on pos_confirmedorders.id=pos_confirmedorderdetails.confirmedorderid ";
		$where=" where pos_confirmedorders.orderno='$row->orderno'";
		$having="";
		$groupby="";
		$orderby="";
		$confirmedorderdetails->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$confirmedorderdetails = $confirmedorderdetails->fetchObject;
		if($packinglists->boxes==0)
		  $packinglists->boxes="";
		  
		$totalboxes+=$confirmedorderdetails->quantity;
		$totalpacked+=$packinglists->boxes;
		
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->orderno; ?></td>
			<td><?php echo strtoupper($row->customerid); ?></td>
			<td><?php echo $row->remarks; ?></td>
			<td><?php echo $packinglists->documentno; ?></td>
			<td><?php echo $confirmedorderdetails->quantity; ?></td>
			<td><a href="../packinglists/packinglists.php?packingno=<?php echo $packinglists->documentno; ?>"><?php echo $packinglists->boxes; ?></td>
<!-- 			<td><?php //echo $confirmedorderdetails->packrate; ?></td> -->
<?php
//Authorization.
$auth->roleid="8705";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="addconfirmedorders_proc.php?orderno=<?php echo $row->orderno; ?>">View</a></td>
			<td><a href='../packinglists/addpackinglists_proc.php?packingno=<?php echo $packinglists->documentno; ?>&customerid=<?php echo $row->customer; ?>&boxno=<?php echo $packinglists->boxes+1; ?>'>Packing</a></td>
<?php
}
//Authorization.

$auth->roleid="8671";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
//   if($packinglists->boxes!=$confirmedorderdetails->quantity){
  ?>
			<td><a href='../packinglists/addpackinglists_proc.php?orderno=<?php echo $row->orderno; ?>&customerid=<?php echo $row->customer; ?>&boxno=<?php echo $packinglists->boxes+1; ?>'>Boxing</a></td>
			<?php if(!empty($packinglists->documentno)){?>
			<td><a href='../packinglists/addpackinglists_proc.php?returns=1&packingno=<?php echo $packinglists->documentno; ?>&customerid=<?php echo $row->customer; ?>&box=1'>Returns</a></td>
			<?php }else{?>
			<td>&nbsp;</td>
			<?php }?>
<?php //}
//else{?>
<!-- 			<td>Complete</td> -->
<?php //}
} ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
	<tfoot>
	<tr>
	  <th>&nbsp;</th>
	  <th>&nbsp;</th>
	  <th>&nbsp;</th>
	  <th>&nbsp;</th>
<!-- 	  <th>&nbsp;</th> -->
	  <th>Total: <?php echo $totalboxes; ?></th>
	  <th>Packed: <?php echo $totalpacked; ?></th>
	  <th>Remaining: <?php echo $totalboxes-$totalpacked; ?></th>
	  <?php
//Authorization.
$auth->roleid="8705";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="8706";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
	</tr>
	</tfoot>
</table>
<?php
include"../../../foot.php";
?>
