<?php 
require_once("InvoicesDBO.php");
require_once("../../../modules/pos/invoicedetails/InvoicedetailsDBO.php");
class Invoices
{				
	var $id;			
	var $documentno;			
	var $packingno;			
	var $customerid;			
	var $agentid;	
	var $currencyid;
	var $vatable;
	var $vat;
	var $exchangerate;
	var $exchangerate2;
	var $remarks;			
	var $soldon;			
	var $memo;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $ipaddress;			
	var $invoicesDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->documentno=str_replace("'","\'",$obj->documentno);
		$this->invoiceno=str_replace("'","\'",$obj->invoiceno);
		$this->packingno=str_replace("'","\'",$obj->packingno);
		if(empty($obj->customerid))
			$obj->customerid='NULL';
		$this->customerid=$obj->customerid;
		if(empty($obj->agentid))
			$obj->agentid='NULL';
		$this->agentid=$obj->agentid;
		$this->currencyid=$obj->currencyid;
		$this->vat=$obj->vat;
		$this->vatable=$obj->vatable;
		$this->exchangerate=$obj->exchangerate;
		$this->exchangerate2=$obj->exchangerate2;
		$this->consignee=$obj->consignee;
		$this->shippedon=str_replace("'","\'",$obj->shippedon);
		$this->actualweight=str_replace("'","\'",$obj->actualweight);
		$this->volumeweight=str_replace("'","\'",$obj->volumeweight);
		$this->awbno=str_replace("'","\'",$obj->awbno);
		$this->dropoffpoint=str_replace("'","\'",$obj->dropoffpoint);
		$this->remarks=str_replace("'","\'",$obj->remarks);
		$this->soldon=str_replace("'","\'",$obj->soldon);
		$this->memo=str_replace("'","\'",$obj->memo);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get documentno
	function getDocumentno(){
		return $this->documentno;
	}
	//set documentno
	function setDocumentno($documentno){
		$this->documentno=$documentno;
	}

	//get packingno
	function getPackingno(){
		return $this->packingno;
	}
	//set packingno
	function setPackingno($packingno){
		$this->packingno=$packingno;
	}

	//get customerid
	function getCustomerid(){
		return $this->customerid;
	}
	//set customerid
	function setCustomerid($customerid){
		$this->customerid=$customerid;
	}

	//get agentid
	function getAgentid(){
		return $this->agentid;
	}
	//set agentid
	function setAgentid($agentid){
		$this->agentid=$agentid;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	//get soldon
	function getSoldon(){
		return $this->soldon;
	}
	//set soldon
	function setSoldon($soldon){
		$this->soldon=$soldon;
	}

	//get memo
	function getMemo(){
		return $this->memo;
	}
	//set memo
	function setMemo($memo){
		$this->memo=$memo;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	function add($obj,$shop,$shp){
		$invoicesDBO = new InvoicesDBO();
			if($invoicesDBO->persist($obj)){
			
				//update packing list
				mysql_query("update pos_packinglists set status=1 where documentno='$obj->packingno'");
				
				$invoicedetails = new Invoicedetails();
				$obj->invoiceid=$invoicesDBO->id;
				$invoicedetails->add($obj,$shop);
				
				$invoiceconsumables = new Invoiceconsumables();
				$obj->invoiceid=$invoicesDBO->id;
				$invoiceconsumables->add($obj,$shp);

				$this->id=$invoicesDBO->id;
				$this->sql=$invoicesDBO->sql;
			}
		return true;	
	}			
	function edit($obj,$where="",$shop){
		$invoicesDBO = new InvoicesDBO();

		//first delete all records under old documentno
		$where=" where documentno='$obj->olddocumentno' and mode='$obj->oldmode'";
		$invoicesDBO->delete($obj,$where);

		$gn = new GeneralJournals();
		$where=" where documentno='$obj->olddocumentno' and transactionid='2' mode='$obj->oldmode' ";
		$gn->delete($obj,$where);

		$num=count($shop);
		$i=0;
		$total=0;
		while($i<$num){
			$obj->itemid=$shop['itemid'];
			$obj->quantity=$shop['quantity'];
			$obj->price=$shop['price'];
			$obj->discount=$shop['discount'];
			$obj->bonus=$shop['bonus'];
			if($invoicesDBO->update($obj,$where)){
				$this->sql=$invoicesDBO->sql;
			}
		}
		return true;	
	}			
	function delete($obj,$where=""){			
		$invoicesDBO = new InvoicesDBO();
		if($invoicesDBO->delete($obj,$where=""))		
			$this->sql=$invoicesDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$invoicesDBO = new InvoicesDBO();
		$this->table=$invoicesDBO->table;
		$invoicesDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$invoicesDBO->sql;
		$this->result=$invoicesDBO->result;
		$this->fetchObject=$invoicesDBO->fetchObject;
		$this->affectedRows=$invoicesDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->documentno)){
			$error="Document No should be provided";
		}
		else if(empty($obj->customerid)){
			$error="Customer should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
		if(empty($obj->documentno)){
			$error="Document No should be provided";
		}
		else if(empty($obj->customerid)){
			$error="Customer should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}
}				
?>
