<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");

require_once("../../crm/customers/Customers_class.php");
require_once("../../crm/agents/Agents_class.php");
require_once("../invoicedetails/Invoicedetails_class.php");
require_once("../../pos/invoices/Invoices_class.php");
require_once("../../pos/items/Items_class.php");

//$tenant = $_GET['tenant'];
$doc=$_GET['doc'];
$packingno=$_GET['packingno'];
$invoicedon=$_GET['invoicedon'];
$customerid = $_GET['customerid'];

$customers = new Customers();
$fields="concat(crm_customers.code,' ', crm_customers.name) name, crm_customers.address, crm_agents.name agentid, crm_agents.address agentaddress, sys_currencys.name currencyid";
$where=" where crm_customers.id='$customerid'";	
$join=" left join crm_agents on crm_agents.id=crm_customers.agentid left join sys_currencys on sys_currencys.id=crm_customers.currencyid ";
$having="";
$groupby="";
$orderby="";
$customers->retrieve($fields,$join,$where,$having,$groupby,$orderby);
$customers = $customers->fetchObject;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../../../fs-css/printable.css" media="all" type="text/css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?></title>

<script type="text/javascript">
  function print_doc()
  {
  		var printers = jsPrintSetup.getPrintersList().split(',');
		// Suppress print dialog
		jsPrintSetup.setSilentPrint(false);/** Set silent printing */

		var i;
		for(i=0; i<printers.length;i++)
		{//alert(i+": "+printers[i]);
		//alert(printers[i]+"="+'<?php echo $_SESSION["smallprinter"];?>');
			if(printers[i].indexOf('<?php echo $_SESSION["smallprinter"];?>')>-1)
			{	//alert(i+": "+printers[i]);
				jsPrintSetup.setPrinter(printers[i]);
			}
			
		}
		//set number of copies to 2
		jsPrintSetup.setOption('numCopies',1);
		jsPrintSetup.setOption('headerStrCenter','');
		jsPrintSetup.setOption('headerStrRight','');
		jsPrintSetup.setOption('headerStrLeft','');
		jsPrintSetup.setOption('footerStrCenter','');
		jsPrintSetup.setOption('footerStrRight','');
		jsPrintSetup.setOption('footerStrLeft','');
		jsPrintSetup.setOption('marginTop','4.8');
		jsPrintSetup.setOption('marginBottom','0');
		jsPrintSetup.setOption('marginLeft','4');
		jsPrintSetup.setOption('marginRight','');
		
		// Do Print
		jsPrintSetup.printWindow(window);
		
		//window.close();
		//window.top.hidePopWin(true);
		// Restore print dialog
		//jsPrintSetup.setSilentPrint(false); /** Set silent printing back to false */
 
  }
 </script>
  <link href="../../../css/bootstrap.css" rel="stylesheet">
<link href="../../../css/bootstrap.min.css" rel="stylesheet">
<style media="all" type="text/css">
body{margin:0px;padding:0px;}
table{width:100%;overflow-y:visible; overflow-x:hidden;border:1px solid #ccc;margin:0px;padding:0px;}
td{border:1px dotted #ccc;}pos
table{overflow-y:visible; overflow-x:hidden;}
tbody{overflow-y:visible; overflow-x:visible; height:auto;}
div{overflow-y:visible; overflow-x:visible; height:auto;}
hideTr{ display:table-row;}
table tr.hideTr[style] {
   display: table-row !important;
}
.fnt{font-family:tahoma;font-size:9px;}
.fntt{font-family: tahoma ;font-size:10px;}
div#tablePagination, div.print{display:none;}
div#tablePagination[style]{display:none !important; }
tr.brk{
page-break-after: always;
}
.noprint{ display:none;}
</style>
<style media="screen">
#testTable2 {width:100%; height:1160px !important;}
</style>
</head>

<body onload="print_doc();">
<div class="print"><a href="javascript:print();">Print</a>&nbsp;<a class="review" href="javascript:viewAll();">View All</a></div>
<!-- headder -->
<table class="table table-stripped">
<tr>
<td colspan="7">
<div style="text-align:center"> 
            <div style="text-align:center;text-transform:uppercase;"><strong><?php echo $_SESSION['companyname']; ?></strong>
            <div class="fntt"><span class="desc"><?php echo $_SESSION['companydesc']; ?></span><br/>
            <span class="addr"><?php echo $_SESSION['companActual/V.weight Ratioyaddr']; ?>,<?php echo $_SESSION['companytown']; ?></span><br />
            <span class="tel"><strong>Tel:</strong> <?php echo $_SESSION['companytel']; ?> </span>  <br/>
            <strong>Website:</strong><span style="text-transform:lowercase;"><?php echo $_SESSION['companyweb']; ?></span><br />
            <strong>Email:</strong><span style="text-transform:lowercase;"><?php echo $_SESSION['companyemail']; ?></span><br />
<p><span class="tel"><strong>PIN:</strong> <?php echo $_SESSION['companypin']; ?></span> <span class="tel"><strong>VAT:</strong> <?php echo $_SESSION['companyvat']; ?></span></p>
</td>
</tr>
<tr>
<td colspan="7" align="center">
<strong>
          EXPORT INVOICE
            <?php
            if($_GET['retrieved']==1){
				?>
				- Copy
				<?php 
			}
			?>
            </h2>
            </strong>   
            </span>
            </div>
</td>
</tr>

<tr>

<td width="50%" colspan="3">
<h6>Shipped To:</h6>
		      <div style="font-size:11px !important;font-family:'arial narrow';"><strong>Agent Name: </strong><?php echo $customers->agentname; ?><br/><?php echo $customers->agentaddress; ?></div>
		      <div style="font-size:10px !important;"><strong>Invoice No: </strong> <?php echo $doc; ?></div>
</td>
<td width="50%" colspan="4">
<h6>Sold To:</h6>
<div style="font-size:10px !important;"><strong>Client Name: </strong><?php echo $customers->name; ?><br><?php echo $customers->address; ?></div>
             <div style="font-size:10px !important;"><strong>Invoice Date</strong> <?php echo": ".$invoicedon;?></div>
	  <div style="font-size:10px !important;"><strong>Invoice No:</strong> <?php echo $doc; ?></div>
            
</td>
</tr>
<tr>

<td width="50%" colspan="7">
	<div style="font-size:10px !important;"><strong>Invoice No: </strong> <?php echo $doc; ?></div>
	<div style="font-size:10px !important;"><strong>Delivery No: </strong><?php echo $packingno; ?></div>
</td>

</tr>
<tr>

<td width="50%" colspan="4">
	<div style="font-size:10px !important;">Currency: <?php echo $customers->currencyid; ?></div>
	<div style="font-size:10px !important;">No Of Boxes:<?php //echo $_SESSION['username']; ?></div>
</td>
<td width="50%" colspan="3">
	  <div style="font-size:10px !important;">Date Shipped:  <?php echo $doc; ?></div>
	<div style="font-size:10px !important;">No Of Stems:<?php //echo $_SESSION['username']; ?></div>
            
</td>
</tr>

<tr>

<td width="30%" colspan="2">
      <div style="font-size:10px !important;">Actual Weight:<?php echo $doc; ?></div>
</td>
<td width="30%" colspan="2">
    <div style="font-size:10px !important;">Volume weight:<?php //echo $_SESSION['username']; ?></div>
</td>
<td width="40%" colspan="3">	
	<div style="font-size:10px !important;">Actual/V.weight Ratio: <?php echo $doc; ?></div>
	<div style="font-size:10px !important;">AWB No:  <?php echo $doc; ?></div>
	<div style="font-size:10px !important;">Drop Off Point:  <?php echo $doc; ?></div>
</td>

</tr>

</table>
<!-- headerend -->

<!-- bodyyy -->
<table width="100%" class="table table-stripped table-bordered">
   	<tr>
			      <th>#</th>
			      <th>VARIETY</span></th>
			      <th>SIZE</span></th>
			      <th>HEAD TYPE</span></th>
			      <th>NO OF BOXES</span></th>
			      <th>TOTAL STEMS</span></th>
			      <th>PRICE</span></th>
			      <th>FOB VALUE</span></th>
	</tr>
        <?php
        $i=0;
        $invoicedetails = new Invoicedetails();
        $fields=" pos_items.name itemid, pos_sizes.name sizeid, sum(pos_invoicedetails.quantity) quantity";
        $where=" where pos_invoices.documentno='$doc'";
        $join="left join pos_invoices on pos_invoices.id=pos_invoicedetails.invoiceid left join pos_items on pos_items.id=pos_invoicedetails.itemid ";
        $having="";
	$groupby=" group by pos_items.id ";
	$orderby="";
	$invoicedetails->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	while($row=mysql_fetch_object($invoicedetails->result)){$i++;
	?>
	  <tr>
	    <td><?php echo $i; ?></td>
	    <td><?php echo $row->itemid; ?></td>
	    <td><?php echo $row->sizeid; ?></td>
	    <td></td>
	    <td></td>
	    <td><?php echo $row->quantity; ?></td>
	    <td><?php echo formatNumber($row->price); ?></td>
	    <td></td>
	  </tr>
	<?php
	}
        ?>
  </table>
<!-- bodyend -->

<!-- foooter -->
<table class="table table-codensed">
<tr>
<div style="font-size:11px !important;"><strong>Payment Details:</strong> All Payments are to be borne by: <?php echo "clientname"; ?></div>
<div style="font-size:11px !important;"><strong>Payment Terms:</strong>All payments to be made within the agrred period from the invoice date to<br/> Magana Flowerds Kenya Limited Account Number <?php echo "account eg usd,euro"; ?></div>
</tr>
<tr>

<div style="font-size:11px !important;text-align:center;"><strong>The exporter of the products,covered by this document HCDA NO.3373 declares that,<br/>except where otherwise clearly indicated,this products are of Kenyan preferential origin,and are FLOS,MPS,Global Gap and KFC silver certified.</strong></div>
<div style="font-size:11px !important;">Signed:....................................</div>
<div style="font-size:11px !important;">Dated:.....................................</div>

</tr>
<tr>
<div style="font-size:11px !important;text-align:center;">
           <ul>
           <h6>TERMS AND CONDITIONS</h6>
           <li><i>Until payment is paid in full to the seller for the Goods,the goods shall remain the property of the seller.</i></li>
           <li><i>Not withstanding the foregoing,the risk in the goods and all liabilities to third parties in respect thereof shall pass to the buyer on delivery.</i></li>
           </ul>
           
</div>
         </tr>


</table>

<!-- footerend -->
<script type="text/javascript" language="javascript" src="../../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" language="javascript" src="../../js/jquery.tablePagination.0.2.js"></script>
<script type="text/javascript" language="javascript" src="../../js/jquery_003.js"></script>
<script type="text/javascript" language="javascript" src="../../js/jquery.easing.min.js"></script>
<script type="text/javascript" language="javascript">
$('tbody tr', $('#menuTable2')).addClass('hideTr'); //hiding rows for test
            var options = {
              currPage : 1, 
              ignoreRows : $('', $('#menuTable2')),
              optionsForRows : [2,3,4,5],
              firstArrow : (new Image()).src="../../media/inv-images/firstBlue.gif",
              prevArrow : (new Image()).src="../../media/inv-images/prevBlue.gif",
              lastArrow : (new Image()).src="../../media/inv-images/lastBlue.gif",
              nextArrow : (new Image()).src="../../media/inv-images/nextBlue.gif"
            }
            $('#menuTable2').tablePagination(options);
			$('a.review').toggle(function(){
				$('tbody tr', $('#menuTable2')).show();
				$('div#tablePagination').hide();
				subTotal();
				},function(){
				$('tbody tr', $('#menuTable2')).addClass('hideTr');
				$('div#tablePagination').show();
				 $('#menuTable2').tablePagination(options);
				 subTotal();
				}
			
			);

</script>
<script type="text/javascript" language="javascript">
function subTotal()
{	
	var subTot = 0;
			var subrow =$('#menuTable2 tr[style*=table-row]');
			subrow.children('td.t2').each(function() {
					subTot += parseFloat($(this).html().replace("$","")); 
				});
			$('#subTot').html(subTot);
}
$(document).ready(function(){
	subTotal(); 
	var fTot = 0;
			var nrow =$('#menuTable2 tr');
			nrow.children('td.t2').each(function() {
					fTot += parseFloat($(this).html().replace("$","")); 
				});
			$('#fTot').html(fTot);
	$('span#tablePagination_paginater img').click(function(){
		subTotal();
	});
	$('#tablePagination_currPage').change(function(){
		subTotal();
	});
	
});		
</script>
</body>
</html>
