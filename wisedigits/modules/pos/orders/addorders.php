<title>WiseDigits ERP: Orders </title>
<?php 
include "../../../head.php";

?>
<script type="text/javascript">
$().ready(function() {
 $("#customername").autocomplete("../../../modules/server/server/search.php?main=crm&module=customers&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#customername").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("customername").value=data[0];
     document.getElementById("customerid").value=data[1];
     document.getElementById("tel").value=data[13];
     document.getElementById("address").value=data[12];
     document.getElementById("remarks").value=data[27];
   }
 });
 $("#itemname").autocomplete("../../../modules/server/server/search.php?main=pos&module=items&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#itemname").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("itemname").value=data[0];
     document.getElementById("itemid").value=data[1];
   }
 });
 $("#itemname").autocomplete("../../../modules/server/server/search.php?main=pos&module=items&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#itemname").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("itemname").value=data[0];
     document.getElementById("itemid").value=data[1];
   }
 });
 $("#employeename").autocomplete("../../../modules/server/server/search.php?main=hrm&module=employees&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#employeename").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("employeename").value=data[0];
     document.getElementById("employeeid").value=data[1];
   }
 });
});
</script>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 </script>

<div class='main'>
<form class="forms" id="theform" action="addorders_proc.php" name="orders" method="POST" enctype="multipart/form-data">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>
		Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
			<tr>
				<td><input type="hidden" name="confirm" value="<?php echo $obj->confirm; ?>"/><label>Invoiced To:</label></td>
				<td><input type='text' size='20' name='customername' id='customername' value='<?php echo $obj->customername; ?>'>
					<input type="hidden" name='customerid' id='customerid' value='<?php echo $obj->customerid; ?>'></td>
				
				</tr>
				<tr>
				<td>
				Shipped To:
				</td>
				<td>
				<select name="customerconsigneeid" class="selectbox">
<option value="">Select...</option>
<?php
	$customerconsignees=new Customerconsignees();
	$where="  ";
	$fields="*";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$customerconsignees->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($customerconsignees->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->customerconsigneeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select></td>
				</td>
				</tr>
				<tr>
				<td><label>TelNo.:</label></td>
				<td><input type='text' name='tel' id='tel' size='16' readonly value='<?php echo $obj->tel; ?>'/></td>			<tr>
				<td><label>Address:</label></td>
				<td><textarea name='address' id='address' readonly><?php echo $obj->address; ?></textarea></td>
				<td><label>Remarks:</label></td>
				<td><textarea name='remarks' id='remarks' readonly><?php echo $obj->remarks; ?></textarea></td>
				
			</td>
			</tr>
		</table>
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
	<tr>
		<th align="right">Product  </th>
		<th align="right">Length  </th>
				
		<th align="right">Pack Rate  </th>
		<th align="right">No Of Boxes  </th>
		<th align="right">Memo  </th>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<td><select name="itemid"  class="selectbox">
<option value="">Select...</option>
<?php
	$items=new Items();
	$where="  ";
	$fields="*";
	$join="";
	$having="";
	$groupby="";
	$orderby=" order by name";
	$items->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($items->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->itemid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
<font color='red'>*</font>
		</td>	
		<td><select name="sizeid"  class="selectbox">
<option value="">Select...</option>
<?php
	$sizes=new Sizes();
	$where="  ";
	$fields="*";
	$join="";
	$having="";
	$groupby="";
	$orderby=" order by name";
	$sizes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($sizes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->sizeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
<font color='red'>*</font>
		</td>	
		<td><input type="text" name="packrate" id="packrate" size="16" value="<?php echo $obj->packrate; ?>"><font color='red'>*</font></td>
		<td><input type="text" name="quantity" id="quantity" size="16" value="<?php echo $obj->quantity; ?>"><font color='red'>*</font></td>
		
		<td><textarea name="memo" id="memo"><?php echo $obj->memo; ?></textarea></td>
	<td><input type="submit" name="action2" value="Add"/></td>
	</tr>
	</table>
		<table align='center'>
			<tr>
			<td>
		Order No:<input type="text" name="orderno" id="orderno" readonly size="16"  value="<?php echo $obj->orderno; ?>">
		Date Ordered:<input type="date" name="orderedon" id="orderedon"  class="date_input" size="16" readonly  value="<?php echo $obj->orderedon; ?>">
		Remarks:<textarea name="remarks" ><?php echo $obj->remarks; ?></textarea>
			</td>
			</tr>
		</table>
<table style="clear:both" class="tgrid display" id="tbl" cellpadding="0" align="center" width="98%" cellspacing="0">
	<thead>
	<tr style="font-size:18px; vertical-align:text-top; ">
		<th align="left" >#</th>
		<?php if(!empty($obj->retrieve)){?>
		<th>&nbsp;</th>
		<?php }?>
		<th align="left">Product  </th>
		<th align="left">Length  </th>
		<th align="left">Pack Rate  </th>
		<th align="left">Qty</th>
		<th align="left">No Of Boxes  </th>
		<th align="left">Memo <input type="hidden" name="iterator" value="<?php echo $obj->iterator; ?>"/> </th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php
	
	if($_SESSION['shporders']){
		$shporders=$_SESSION['shporders'];
		$i=0;
		$j=$obj->iterator;
		$total=0;
		while($j>0){
		?>
		<tr style="font-size:12px; vertical-align:text-top; ">
			<td><?php echo ($i+1); ?></td>
			<?php if(!empty($obj->retrieve)){?>
			<td><input type="checkbox" name="<?php echo $shporders[$i]['id']; ?>"/></td>
			<?php }?>
			<td><?php echo $shporders[$i]['itemname']; ?> </td>
			<td><?php echo $shporders[$i]['sizename']; ?> </td>
			<td><?php echo $shporders[$i]['packrate']; ?> </td>
			<td><?php echo $shporders[$i]['quantity']; ?> </td>
			<td><?php echo $shporders[$i]['memo']; ?> </td>
			<td><?php echo $shporders[$i]['total']; ?> </td>
			<td><a href="edit.php?i=<?php echo $i; ?>&action=edit&edit=1">Edit</a></td>
			<td><a href="edit.php?i=<?php echo $i; ?>&action=del&edit=1" onclick="return confirm('Are you sure you want to delete?')">Del</a></td>
		</tr>
		<?php
		$i++;
		$j--;
		}
	}
	?>
	</tbody>
</table>
<table align="center" width="98%">
	<?php //if(empty($obj->retrieve)){?>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
		
	</tr>
	<?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="2" align="center"><input type="button" value="Print" onclick="Clickheretoprint();"/>
		<input type="submit" name="action" id="action" value="Confirm Order" />
		<input type="submit" name="action" id="action" value="Copy Order" />
		
		</td>
	</tr>
	<?php }?>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
include "../../../foot.php";
if(!empty($error)){
	showError($error);
}
if($saved=="Yes"){
	redirect("addorders_proc.php?retrieve=");
}

?>