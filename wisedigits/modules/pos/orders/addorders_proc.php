<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Orders_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../crm/customers/Customers_class.php");
require_once("../orderdetails/Orderdetails_class.php");
require_once("../../pos/orders/Orders_class.php");
require_once("../../pos/items/Items_class.php");
require_once("../../pos/sizes/Sizes_class.php");
require_once("../../crm/customerconsignees/Customerconsignees_class.php");

//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="8665";//Edit
}
else{
	$auth->roleid="8663";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

if(!empty($ob->confirm)){
  $obj->confirm=$ob->confirm;
}

if(!empty($ob->orderno)){
  $obj->invoiceno=$ob->orderno;
  $obj->action="Filter";
  $obj->retrieve=1;
}
if(!empty($ob->copy)){
  $obj = $_SESSION['ob'];
  $obj->copy=1;
  //$obj->action="Save";
}
$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
if(empty($obj->action)){
	

}
	
if($obj->action=="Save"){
	$orders=new Orders();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$shporders=$_SESSION['shporders'];
	$error=$orders->validates($obj);
	if(!empty($error)){
		$error=$error;
	}
	elseif(empty($shporders)){
		$error="No items in the sale list!";
	}
	else{
		$orders=$orders->setObject($obj);
		if($orders->add($orders,$shporders)){
			$error=SUCCESS;
			$saved="Yes";
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$orders=new Orders();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$orders->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$orders=$orders->setObject($obj);
		$shporders=$_SESSION['shporders'];
		if($orders->edit($orders,"",$shporders)){
			$error=UPDATESUCCESS;
			$saved="Yes";
			//redirect("addorders_proc.php?id=".$orders->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if($obj->action2=="Add"){

	if(empty($obj->itemid)){
		$error=" must be provided";
	}
	elseif(empty($obj->quantity)){
		$error=" must be provided";
	}
	else{
	$_SESSION['obj']=$obj;
	if(empty($obj->iterator))
		$it=0;
	else
		$it=$obj->iterator;
	$shporders=$_SESSION['shporders'];

	$items = new Items();
	$fields=" * ";
	$join="";
	$groupby="";
	$having="";
	$where=" where id='$obj->itemid'";
	$items->retrieve($fields, $join, $where, $having, $groupby, $orderby);
	$items=$items->fetchObject;
	
	$sizes = new Sizes();
	$fields=" * ";
	$join="";
	$groupby="";
	$having="";
	$where=" where id='$obj->sizeid'";
	$sizes->retrieve($fields, $join, $where, $having, $groupby, $orderby);
	$sizes=$sizes->fetchObject;
	
	$shporders[$it]=array('id'=>"$ob->id",'itemid'=>"$obj->itemid",'sizeid'=>"$obj->sizeid",'sizename'=>"$sizes->name", 'itemname'=>"$items->name", 'quantity'=>"$obj->quantity",'packrate'=>"$obj->packrate", 'memo'=>"$obj->memo");

 	$it++;
		$obj->iterator=$it;
 	$_SESSION['shporders']=$shporders;

	$obj->itemid="";
	$obj->sizeid="";
 	$obj->total=0;
	$obj->quantity="";
 	$obj->memo="";
 }
}

if($obj->action=="Filter"){
	if(!empty($obj->invoiceno)){
		$orders = new Orders();
		$fields="pos_orderdetails.id, pos_orders.orderno, crm_customers.id as customerid, pos_orderdetails.quantity, pos_orderdetails.packrate, pos_orderdetails.memo, pos_items.id itemid, pos_items.name itemname, pos_orderdetails.sizeid sizeid, pos_sizes.name sizename, pos_orders.orderedon, pos_orders.remarks, pos_orders.ipaddress, pos_orders.createdby, pos_orders.createdon, pos_orders.lasteditedby, pos_orders.lasteditedon";
		$join=" left join crm_customers on pos_orders.customerid=crm_customers.id left join pos_orderdetails on pos_orderdetails.orderid=pos_orders.id left join pos_items on pos_items.id=pos_orderdetails.itemid left join pos_sizes on pos_sizes.id=pos_orderdetails.sizeid ";
		$having="";
		$groupby="";
		$orderby="";
		$where=" where pos_orders.orderno='$obj->invoiceno'";
		$orders->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$orders->result;
		$it=0;
		while($row=mysql_fetch_object($res)){
				
			$ob=$row;
			$shporders[$it]=array('id'=>"$ob->id",'itemid'=>"$ob->itemid", 'itemname'=>"$ob->itemname",'sizeid'=>"$ob->sizeid", 'sizename'=>"$ob->sizename", 'quantity'=>"$ob->quantity" ,'packrate'=>"$ob->packrate", 'memo'=>"$ob->memo");

			$it++;
		}

		//for autocompletes
		$customers = new Customers();
		$fields=" * ";
		$where=" where id='$ob->customerid'";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$customers->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$auto=$customers->fetchObject;
		$auto->customername=$auto->name;		

		$obj = (object) array_merge((array) $obj, (array) $ob);
		$obj = (object) array_merge((array) $obj, (array) $auto);
		$obj->remarks = $ob->remarks;
		
		$obj->itemid="";
		$obj->sizeid="";
		$obj->total=0;
		$obj->quantity="";
		$obj->memo="";

		$obj->iterator=$it;
		
		$obj->action="Update";
		//$obj->retrieve='';
		
		$_SESSION['obj']=$obj;
		$_SESSION['shporders']=$shporders;
	}
}

if($obj->action=="Confirm Order"){
  $shporders=$_SESSION['shporders'];
  $_SESSION['shpconfirmedorders']="";
  $num = count($shporders);
  $i=0;
  $j=0;
  
  $ob->orderno = $obj->orderno;
  $ob->orderedon=$obj->orderedon;
  $ob->remarks=$obj->remarks;
  $ob->customerid = $obj->customerid;
  $ob->customername = $obj->customername;
  $ob->requisitionno = $obj->documentno;
  
  
  while($i<$num){        
      
    if(isset($_POST[$shporders[$i]['id']])){
          $shpconfirmedorders[$j]=$shporders[$i];
          $j++;
    }
    $i++;
  }
  $ob->iterator=$j;
  $_SESSION['ob']=$ob;
  $_SESSION['shpconfirmedorders']=$shpconfirmedorders;
  $_SESSION['shporders']="";
  
  redirect("../confirmedorders/addconfirmedorders_proc.php?confirm=1");
}

if($obj->action=="Copy Order"){
  $shporders=$_SESSION['shporders'];
  $num = count($shporders);
  $i=0;
  $j=0;
 
   $def=mysql_fetch_object(mysql_query("select (max(orderno)+1) orderno from pos_orders"));
   if($def->orderno == null){
		$def->orderno=1;
	}
  
  $ob->orderno = $def->orderno;
  $ob->orderedon=$obj->orderedon;
//   $ob->remarks=$obj->remarks;
  $ob->customerid = $obj->customerid;
  $ob->customername = $obj->customername;
//   $ob->requisitionno = $obj->documentno;
  
  
  while($i<$num){        
      
    if(isset($_POST[$shporders[$i]['id']])){
          $shporder[$j]=$shporders[$i];
          $j++;
    }
    $i++;
  }
  $ob->iterator=$j;
  $_SESSION['ob']=$ob;
  $_SESSION['shporders']=$shporder;
  
  redirect("addorders_proc.php?copy=1");
}

if(empty($obj->action)){

	$customers= new Customers();
	$fields="crm_customers.id, crm_customers.name, crm_customers.agentid, crm_customers.departmentid, crm_customers.categorydepartmentid, crm_customers.categoryid, crm_customers.employeeid, crm_customers.idno, crm_customers.pinno, crm_customers.address, crm_customers.tel, crm_customers.fax, crm_customers.email, crm_customers.contactname, crm_customers.contactphone, crm_customers.nextofkin, crm_customers.nextofkinrelation, crm_customers.nextofkinaddress, crm_customers.nextofkinidno, crm_customers.nextofkinpinno, crm_customers.nextofkintel, crm_customers.creditlimit, crm_customers.creditdays, crm_customers.discount, crm_customers.showlogo, crm_customers.statusid, crm_customers.remarks, crm_customers.createdby, crm_customers.createdon, crm_customers.lasteditedby, crm_customers.lasteditedon, crm_customers.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$customers->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$orders=new Orders();
	$where=" where id=$id ";
	$fields="pos_orders.id, pos_orders.orderno, pos_orders.customerid, pos_orders.orderedon, pos_orders.remarks, pos_orders.ipaddress, pos_orders.createdby, pos_orders.createdon, pos_orders.lasteditedby, pos_orders.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$orders->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$orders->fetchObject;

	//for autocompletes
	$customers = new Customers();
	$fields=" id, name ";
	$where=" where id='$obj->customerid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$customers->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$customers->fetchObject;

	$obj->customername=$auto->name;
	$items = new Items();
	$fields=" * ";
	$where=" where id='$obj->itemid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$items->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$items->fetchObject;

	$obj->itemname=$auto->name;
	$items = new Items();
	$fields=" * ";
	$where=" where id='$obj->itemid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$items->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$items->fetchObject;

	$obj->itemname=$auto->name;
	$employees = new Employees();
	$fields=" * ";
	$where=" where id='$obj->employeeid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$employees->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$employees->fetchObject;

	$obj->employeename=$auto->name;
}

if(empty($obj->retrieve)){
  if(empty($_GET['edit'])){
      if(empty($obj->action) and empty($obj->action2) and empty( $obj->copy)){
	$_SESSION['shporders']="";
			  
	$defs=mysql_fetch_object(mysql_query("select (max(orderno)+1) orderno from pos_orders"));
	if($defs->orderno == null){
		$defs->orderno=1;
	}
	$obj->orderno=$defs->orderno;

	$obj->orderedon=date("Y-m-d");
	
	$obj->action="Save";
      }
      else{
	$obj->action="Update";
      }
  } 
  else{
    $obj=$_SESSION['obj'];
  }
  
}
else{
  $obj->action="Update";
}
if (!empty( $obj->copy)){
$obj->action="Save";
}
$page_title="Orders ";
include "addorders.php";
?>