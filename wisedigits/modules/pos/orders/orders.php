<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Orders_class.php");
require_once("../../auth/rules/Rules_class.php");
require_once("../confirmedorders/Confirmedorders_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
//Redirect to horizontal layout
//redirect("addorders_proc.php?retrieve=".$_GET['retrieve']);

$page_title="Orders";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="8664";//View
$auth->levelid=$_SESSION['level'];

$obj = (object)$_POST;

if(empty($obj->orderedon)){
  $obj->orderedon=date("Y-m-d");
}

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$orders=new Orders();
if(!empty($delid)){
	$orders->id=$delid;
	$orders->delete($orders);
	redirect("orders.php");
}
//Authorization.
$auth->roleid="8663";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <a href='addorders_proc.php'>New Orders</a></div>
<?php }?>

<style type="text/css">
.green{
  background-image: -webkit-gradient(linear, left 0%, left 100%, from(#ffffff), to(#33CC33));
  background-image: -webkit-linear-gradient(top, #ffffff, 0%, #33CC33, 100%);
  background-image: -moz-linear-gradient(top, #ffffff 0%, #33CC33 100%);
  background-image: linear-gradient(to bottom, #ffffff 0%, #33CC33 100%);
}
</style>

<form action="" method="post">
<div>
  <input name="orderedon" type="text" size="12" value="<?php echo $obj->orderedon; ?>" class="date_input" readonly/>
  <input type="submit" name="action" value="Filter"/>
</div>
</form>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Order No </th>
			<th>Customer </th>
			<th>Date Ordered </th>
			<th>Remarks </th>
<?php
//Authorization.
$auth->roleid="8665";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="8666";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="pos_orders.id, pos_orders.orderno, crm_customers.name as customerid, pos_orders.orderedon, pos_orders.remarks, pos_orders.ipaddress, pos_orders.createdby, pos_orders.createdon, pos_orders.lasteditedby, pos_orders.lasteditedon";
		$join=" left join crm_customers on pos_orders.customerid=crm_customers.id ";
		$having="";
		$groupby="";
		$orderby="";
		if(!empty($obj->orderedon)){
		  $where=" where pos_orders.orderedon='$obj->orderedon'";
		}
		$orders->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$orders->result;
		while($row=mysql_fetch_object($res)){
		$i++;
		
		$class="";
		$confirmedorders = new Confirmedorders();
		$fields="*";
		$join="";
		$where=" where orderno='$row->orderno' ";
		$having="";
		$groupby="";
		$orderby="";
		$confirmedorders->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		if($confirmedorders->affectedRows>0)
		  $class="green";
		
	?>
		<tr class="<?php echo $class; ?>">
			<td><?php echo $i; ?></td>
			<td><?php echo $row->orderno; ?></td>
			<td><?php echo $row->customerid; ?></td>
			<td><?php echo formatDate($row->orderedon); ?></td>
			<td><?php echo $row->remarks; ?></td>
<?php
//Authorization.
$auth->roleid="8665";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="addorders_proc.php?orderno=<?php echo $row->orderno; ?>">View</a></td>
<?php
}
//Authorization.
$auth->roleid="8666";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td>&nbsp;</td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
