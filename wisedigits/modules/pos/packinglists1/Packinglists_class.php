<?php 
require_once("PackinglistsDBO.php");
require_once("../../../modules/pos/packinglistdetails/PackinglistdetailsDBO.php");
class Packinglists
{				
	var $id;			
	var $documentno;			
	var $orderno;			
	var $boxno;			
	var $customerid;			
	var $packedon;			
	var $fleetid;			
	var $employeeid;			
	var $remarks;			
	var $ipaddress;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $packinglistsDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->documentno=str_replace("'","\'",$obj->documentno);
		$this->orderno=str_replace("'","\'",$obj->orderno);
		$this->boxno=str_replace("'","\'",$obj->boxno);
		if(empty($obj->customerid))
			$obj->customerid='NULL';
		$this->customerid=$obj->customerid;
		$this->packedon=str_replace("'","\'",$obj->packedon);
		if(empty($obj->fleetid))
			$obj->fleetid='NULL';
		$this->fleetid=$obj->fleetid;
		if(empty($obj->employeeid))
			$obj->employeeid='NULL';
		$this->employeeid=$obj->employeeid;
		$this->remarks=str_replace("'","\'",$obj->remarks);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get documentno
	function getDocumentno(){
		return $this->documentno;
	}
	//set documentno
	function setDocumentno($documentno){
		$this->documentno=$documentno;
	}

	//get orderno
	function getOrderno(){
		return $this->orderno;
	}
	//set orderno
	function setOrderno($orderno){
		$this->orderno=$orderno;
	}

	//get boxno
	function getBoxno(){
		return $this->boxno;
	}
	//set boxno
	function setBoxno($boxno){
		$this->boxno=$boxno;
	}

	//get customerid
	function getCustomerid(){
		return $this->customerid;
	}
	//set customerid
	function setCustomerid($customerid){
		$this->customerid=$customerid;
	}

	//get packedon
	function getPackedon(){
		return $this->packedon;
	}
	//set packedon
	function setPackedon($packedon){
		$this->packedon=$packedon;
	}

	//get fleetid
	function getFleetid(){
		return $this->fleetid;
	}
	//set fleetid
	function setFleetid($fleetid){
		$this->fleetid=$fleetid;
	}

	//get employeeid
	function getEmployeeid(){
		return $this->employeeid;
	}
	//set employeeid
	function setEmployeeid($employeeid){
		$this->employeeid=$employeeid;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj,$shop){
		$packinglistsDBO = new PackinglistsDBO();
			if($packinglistsDBO->persist($obj)){		
				$packinglistdetails = new Packinglistdetails();
				$obj->packinglistid=$packinglistsDBO->id;
				$packinglistdetails->add($obj,$shop);

				$this->id=$packinglistsDBO->id;
				$this->sql=$packinglistsDBO->sql;
			}
		return true;	
	}			
	function edit($obj,$where="",$shop){
		$packinglistsDBO = new PackinglistsDBO();

		$packinglists = new Packinglists();
		//first delete all records under old documentno
		$where=" where documentno='$obj->documentno' and boxno='$obj->boxno'";
		$packinglistsDBO->delete($obj,$where);

		$packinglists=$packinglists->setObject($obj);
		if($packinglists->add($packinglists,$shop)){
		  return true;	
		}
		else
		  return false;
	}			
	function delete($obj,$where=""){			
		$packinglistsDBO = new PackinglistsDBO();
		if($packinglistsDBO->delete($obj,$where=""))		
			$this->sql=$packinglistsDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$packinglistsDBO = new PackinglistsDBO();
		$this->table=$packinglistsDBO->table;
		$packinglistsDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$packinglistsDBO->sql;
		$this->result=$packinglistsDBO->result;
		$this->fetchObject=$packinglistsDBO->fetchObject;
		$this->affectedRows=$packinglistsDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->documentno)){
			$error="Packing No should be provided";
		}
		else if(empty($obj->customerid)){
			$error="Customer should be provided";
		}
		else if(empty($obj->packedon)){
			$error="Date of Packing should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
		if(empty($obj->documentno)){
			$error="Packing No should be provided";
		}
		else if(empty($obj->customerid)){
			$error="Customer should be provided";
		}
		else if(empty($obj->packedon)){
			$error="Date of Packing should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}
}				
?>
