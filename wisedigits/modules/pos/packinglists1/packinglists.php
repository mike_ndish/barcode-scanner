<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Packinglists_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
//Redirect to horizontal layout
redirect("addpackinglists_proc.php?retrieve=".$_GET['retrieve']);

$page_title="Packinglists";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="8672";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$packinglists=new Packinglists();
if(!empty($delid)){
	$packinglists->id=$delid;
	$packinglists->delete($packinglists);
	redirect("packinglists.php");
}
//Authorization.
$auth->roleid="8671";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <a href='addpackinglists_proc.php'>New Packinglists</a></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Packing No </th>
			<th>Order No </th>
			<th>Box No </th>
			<th>Customer </th>
			<th>Date Of Packing </th>
			<th>Vehicle </th>
			<th>Driver </th>
			<th>Remarks </th>
<?php
//Authorization.
$auth->roleid="8673";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="8674";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="pos_packinglists.id, pos_packinglists.documentno, pos_packinglists.orderno, pos_packinglists.boxno, crm_customers.name as customerid, pos_packinglists.packedon, assets_fleets.name as fleetid, hrm_employees.name as employeeid, pos_packinglists.remarks, pos_packinglists.ipaddress, pos_packinglists.createdby, pos_packinglists.createdon, pos_packinglists.lasteditedby, pos_packinglists.lasteditedon";
		$join=" left join crm_customers on pos_packinglists.customerid=crm_customers.id  left join assets_fleets on pos_packinglists.fleetid=assets_fleets.id  left join hrm_employees on pos_packinglists.employeeid=hrm_employees.id ";
		$having="";
		$groupby="";
		$orderby="";
		$packinglists->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$packinglists->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->documentno; ?></td>
			<td><?php echo $row->orderno; ?></td>
			<td><?php echo $row->boxno; ?></td>
			<td><?php echo $row->customerid; ?></td>
			<td><?php echo formatDate($row->packedon); ?></td>
			<td><?php echo $row->fleetid; ?></td>
			<td><?php echo $row->employeeid; ?></td>
			<td><?php echo $row->remarks; ?></td>
<?php
//Authorization.
$auth->roleid="8673";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="addpackinglists_proc.php?id=<?php echo $row->id; ?>">View</a></td>
<?php
}
//Authorization.
$auth->roleid="8674";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='packinglists.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
