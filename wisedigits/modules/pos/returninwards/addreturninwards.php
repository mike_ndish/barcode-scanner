<title>WiseDigits ERP: Returninwards </title>
<?php 
include "../../../head.php";

?>
<script type="text/javascript">
$().ready(function() {
 $("#itemname").autocomplete("../../../modules/server/server/search.php?main=pos&module=items&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#itemname").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("itemname").value=data[0];
     document.getElementById("itemid").value=data[1];
     document.getElementById("code").value=data[2];
     document.getElementById("tax").value=data[10];
     document.getElementById("discount").value=data[9];
     document.getElementById("retailprice").value=data[8];
     document.getElementById("tradeprice").value=data[7];
   }
 });
 $("#customername").autocomplete("../../../modules/server/server/search.php?main=motor&module=customers&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#customername").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("customername").value=data[0];
     document.getElementById("customerid").value=data[1];
     document.getElementById("address").value=data[3];
     document.getElementById("mobile").value=data[4];
     document.getElementById("remarks").value=data[6];
   }
 });
});
<?php include'js.php'; ?>
</script>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 </script>

<div class='main'>
<form class="forms" id="theform" action="addreturninwards_proc.php" name="returninwards" method="POST" enctype="multipart/form-data">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
			<tr>
				<td><label>Customer:</label></td>
				<td><input type='text' size='20' name='customername' id='customername' value='<?php echo $obj->customername; ?>'>
					<input type="hidden" name='customerid' id='customerid' value='<?php echo $obj->customerid; ?>'></td>
				<td><label>Customer Address:</label></td>
				<td><textarea name='address' id='address' readonly><?php echo $obj->address; ?></textarea></td>
			<tr>
				<td><label>Mobile No:</label></td>
				<td><input type='text' name='mobile' id='mobile' size='20' readonly value='<?php echo $obj->mobile; ?>'/></td>				<td><label>Remarks:</label></td>
				<td><textarea name='remarks' id='remarks' readonly><?php echo $obj->remarks; ?></textarea></td>
			</td>
			</tr>
		</table>
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
	<tr>
		<th align="right">Item  </th>
		<th align="right">Item Code  </th>
		<th align="right">VAT  </th>
		<th align="right">Discount %  </th>
		<th align="right">Retail Price  </th>
		<th align="right">Trade Price  </th>
		<th align="right">Quantity  </th>
		<th>Total</th>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<td><input type='text' size='20' name='itemname'  onchange="calculateTotal();" onblur="calculateTotal();"  id='itemname' value='<?php echo $obj->itemname; ?>'>
			<input type="hidden" name='itemid' id='itemid' value='<?php echo $obj->itemid; ?>'>		<td>
		<input type='text' name='code' id='code'  size='8' readonly value='<?php echo $obj->code; ?>'/>
		</td>
		<td>
		<input type='text' name='tax' id='tax'  onchange="calculateTotal();" onblur="calculateTotal();"  size='4'  value='<?php echo $obj->tax; ?>'/>
		</td>
		<td>
		<input type='text' name='discount' id='discount'  onchange="calculateTotal();" onblur="calculateTotal();"  size='4'  value='<?php echo $obj->discount; ?>'/>
		</td>
		<td>
		<input type='text' name='retailprice' id='retailprice'  onchange="calculateTotal();" onblur="calculateTotal();"  size='8'  value='<?php echo $obj->retailprice; ?>'/>
		</td>
		<td>
		<input type='text' name='tradeprice' id='tradeprice'  onchange="calculateTotal();" onblur="calculateTotal();"  size='8'  value='<?php echo $obj->tradeprice; ?>'/>
		</td>

		</td>
<font color='red'>*</font>		<td><input type="text" name="quantity" id="quantity" onchange="calculateTotal();" onblur="calculateTotal();"  size="2" value="<?php echo $obj->quantity; ?>"></td>
	<td><input type="text" name="total" id="total" size='8' readonly value="<?php echo $obj->total; ?>"/></td>
	<td><input type="submit" name="action2" value="Add"/></td>
	</tr>
	</table>
		<table align='center'>
			<tr>
			<td>
		Credit Note No.:<input type="text" name="creditnoteno" id="creditnoteno" readonly size="6"  value="<?php echo $obj->creditnoteno; ?>">
		Returned On:<input type="date" name="returnedon" id="returnedon"  class="date_input" size="8" readonly  value="<?php echo $obj->returnedon; ?>">
		Memo:<textarea name="memo" ><?php echo $obj->memo; ?></textarea>

<input type="hidden" name="olddocumentno" id="olddocumentno"  size="0"  value="<?php echo $obj->olddocumentno; ?>">

<input type="hidden" name="edit" id="edit"  size="0"  value="<?php echo $obj->edit; ?>">
		Receipt/Invoice No:<input type="text" name="documentno" id="documentno"  size="8"  value="<?php echo $obj->documentno; ?>">
		:<select name='mode'  class="selectbox">
			<option value='cashretail' <?php if($obj->mode=='cashretail'){echo"selected";}?>>cashretail</option>
			<option value='creditretail' <?php if($obj->mode=='creditretail'){echo"selected";}?>>creditretail</option>
			<option value='cashwholesale' <?php if($obj->mode=='cashwholesale'){echo"selected";}?>>cashwholesale</option>
			<option value='creditwholesale' <?php if($obj->mode=='creditwholesale'){echo"selected";}?>>creditwholesale</option>
		</select>

<input type="hi" name="oldmode" id="oldmode" hidden size="0"  value="<?php echo $obj->oldmode; ?>">
			</td>
			</tr>
		</table>
<table style="clear:both" class="tgrid display" id="tbl" cellpadding="0" align="center" width="98%" cellspacing="0">
	<thead>
	<tr style="font-size:18px; vertical-align:text-top; ">
		<th align="left" >#</th>
		<th align="left">Item  </th>
		<th align="right">Item Code  </th>
		<th align="right">VAT  </th>
		<th align="right">Discount %  </th>
		<th align="right">Retail Price  </th>
		<th align="right">Trade Price  </th>
		<th align="left">Quantity  </th>
		<th align='left'>Total</th>
		<th><input type="hidden" name="iterator" value="<?php echo $obj->iterator; ?>"/></th>
		<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php
	if($_SESSION['shpreturninwards']){
		$shpreturninwards=$_SESSION['shpreturninwards'];
		$i=0;
		$j=$obj->iterator;
		$total=0;
		while($j>0){

		$total+=$shpreturninwards[$i]['total'];
		?>
		<tr style="font-size:12px; vertical-align:text-top; ">
			<td><?php echo ($i+1); ?></td>
			<td><?php echo $shpreturninwards[$i]['itemname']; ?> </td>
			<td><?php echo $shpreturninwards[$i]['code']; ?> </td>
			<td><?php echo $shpreturninwards[$i]['tax']; ?> </td>
			<td><?php echo $shpreturninwards[$i]['discount']; ?> </td>
			<td><?php echo $shpreturninwards[$i]['retailprice']; ?> </td>
			<td><?php echo $shpreturninwards[$i]['tradeprice']; ?> </td>
			<td><?php echo $shpreturninwards[$i]['quantity']; ?> </td>
			<td><?php echo $shpreturninwards[$i]['total']; ?> </td>
			<td><a href="edit.php?i=<?php echo $i; ?>&action=edit&edit=<?php echo $obj->edit; ?>">Edit</a></td>
			<td><a href="edit.php?i=<?php echo $i; ?>&action=del&edit=<?php echo $obj->edit; ?>">Del</a></td>
		</tr>
		<?php
		$i++;
		$j--;
		}
	}
	?>
	</tbody>
</table>
<table align="center" width="98%">
	<tr>
		<td colspan="2" align="center">Total:<input type="text" size='12' readonly value="<?php echo $total; ?>"/></td>
	</tr>
	<?php if(empty($obj->retrieve)){?>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
	<?php }else{?>
	<tr>
		<td colspan="2" align="center"><input type="button" name="action" id="action" value="Print" onclick="Clickheretoprint();"/></td>
	</tr>
	<?php }?>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
include "../../../foot.php";
if(!empty($error)){
	showError($error);
}
if($saved=="Yes"){
	redirect("addreturninwards_proc.php?retrieve=");
}

?>