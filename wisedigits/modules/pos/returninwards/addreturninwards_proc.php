<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Returninwards_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../pos/items/Items_class.php");
require_once("../../fn/generaljournalaccounts/Generaljournalaccounts_class.php");
require_once("../../fn/generaljournals/Generaljournals_class.php");
require_once("../../sys/transactions/Transactions_class.php");
//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="2194";//Edit
}
else{
	$auth->roleid="2192";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
if(empty($obj->action)){
	$defs=mysql_fetch_object(mysql_query("select max(creditnoteno)+1 creditnoteno from pos_returninwards"));
	if($defs->creditnoteno == null){
		$defs->creditnoteno=1;
	}
	$obj->creditnoteno=$defs->creditnoteno;

	$obj->returnedon=date('Y-m-d');

}
	
if($obj->action=="Save"){
	$returninwards=new Returninwards();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$shpreturninwards=$_SESSION['shpreturninwards'];
	$error=$returninwards->validates($obj);
	if(!empty($error)){
		$error=$error;
	}
	elseif(empty($shpreturninwards)){
		$error="No items in the sale list!";
	}
	else{
		$returninwards=$returninwards->setObject($obj);
		if($returninwards->add($returninwards,$shpreturninwards)){
			$error=SUCCESS;
			$saved="Yes";
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$returninwards=new Returninwards();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$returninwards->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$returninwards=$returninwards->setObject($obj);
		$shpreturninwards=$_SESSION['shpreturninwards'];
		if($returninwards->edit($returninwards,$shpreturninwards)){

			//Make a journal entry

			//retrieve account to debit
			$generaljournalaccounts = new Generaljournalaccounts();
			$fields="*";
			$where=" where refid='$obj->customerid' and acctypeid='29'";
			$join="";
			$having="";
			$groupby="";
			$orderby="";
			$generaljournalaccounts->retrieve($fields, $join, $where, $having, $groupby, $orderby);
			$generaljournalaccounts=$generaljournalaccounts->fetchObject;

			//retrieve account to credit
			$generaljournalaccounts2 = new Generaljournalaccounts();
			$fields="*";
			$where=" where refid='1' and acctypeid='27'";
			$join="";
			$having="";
			$groupby="";
			$orderby="";
			$generaljournalaccounts2->retrieve($fields, $join, $where, $having, $groupby, $orderby);
			$generaljournalaccounts2=$generaljournalaccounts2->fetchObject;
			$error=UPDATESUCCESS;
			redirect("addreturninwards_proc.php?id=".$returninwards->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if($obj->action2=="Add"){

	if(empty($obj->itemid)){
		$error="Item must be provided";
	}
	elseif(empty($obj->quantity)){
		$error="Quantity must be provided";
	}
	else{
	$_SESSION['obj']=$obj;
	if(empty($obj->iterator))
		$it=0;
	else
		$it=$obj->iterator;
	$shpreturninwards=$_SESSION['shpreturninwards'];

	$items = new Items();
	$fields=" * ";
	$join="";
	$groupby="";
	$having="";
	$where=" where id='$obj->itemid'";
	$items->retrieve($fields, $join, $where, $having, $groupby, $orderby);
	$items=$items->fetchObject;

	;
	$shpreturninwards[$it]=array('itemid'=>"$obj->itemid", 'itemname'=>"$items->name", 'code'=>"$obj->code", 'tax'=>"$obj->tax", 'discount'=>"$obj->discount", 'retailprice'=>"$obj->retailprice", 'tradeprice'=>"$obj->tradeprice", 'quantity'=>"$obj->quantity", 'total'=>"$obj->total");

 	$it++;
		$obj->iterator=$it;
 	$_SESSION['shpreturninwards']=$shpreturninwards;

	$obj->itemname="";
 	$obj->itemid="";
 	$obj->code="";
 	$obj->tax="";
 	$obj->discount="";
 	$obj->retailprice="";
 	$obj->tradeprice="";
 	$obj->total=0;
	$obj->quantity="";
 }
}

if(empty($obj->action)){

	$items= new Items();
	$fields="pos_items.id, pos_items.code, pos_items.name, pos_items.departmentid, pos_items.categoryid, pos_items.costprice, pos_items.tradeprice, pos_items.retailprice, pos_items.discount, pos_items.tax, pos_items.stock, pos_items.reorderlevel, pos_items.itemstatusid, pos_items.remarks, pos_items.createdby, pos_items.createdon, pos_items.lasteditedby, pos_items.lasteditedon, pos_items.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$items->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$returninwards=new Returninwards();
	$where=" where id=$id ";
	$fields="pos_returninwards.id, pos_returninwards.customerid, pos_returninwards.documentno, pos_returninwards.creditnoteno, pos_returninwards.mode, pos_returninwards.itemid, pos_returninwards.quantity, pos_returninwards.costprice, pos_returninwards.tradeprice, pos_returninwards.retailprice, pos_returninwards.tax, pos_returninwards.discount, pos_returninwards.total, pos_returninwards.returnedon, pos_returninwards.memo, pos_returninwards.createdby, pos_returninwards.createdon, pos_returninwards.lasteditedby, pos_returninwards.lasteditedon, pos_returninwards.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$returninwards->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$returninwards->fetchObject;

	//for autocompletes
	$items = new Items();
	$fields=" * ";
	$where=" where id='$obj->itemid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$items->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$items->fetchObject;

	$obj->itemname=$auto->name;
	$customers = new Customers();
	$fields=" * ";
	$where=" where id='$obj->customerid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$customers->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$customers->fetchObject;

	$obj->customername=$auto->name;
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Returninwards ";
include "addreturninwards.php";
?>