<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Returninwards_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
//Redirect to horizontal layout
redirect("addreturninwards_proc.php?retrieve=".$_GET['retrieve']);

$page_title="Returninwards";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="2193";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$returninwards=new Returninwards();
if(!empty($delid)){
	$returninwards->id=$delid;
	$returninwards->delete($returninwards);
	redirect("returninwards.php");
}
//Authorization.
$auth->roleid="2192";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <a href='addreturninwards_proc.php'>New Returninwards</a></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Customer </th>
			<th>Receipt/Invoice No </th>
			<th>Credit Note No. </th>
			<th> </th>
			<th>Item </th>
			<th>Quantity </th>
			<th>Cost Price </th>
			<th>Trade Price </th>
			<th>Retail Price </th>
			<th>VAT </th>
			<th>Discount </th>
			<th>Total </th>
			<th>Returned On </th>
			<th>Memo </th>
<?php
//Authorization.
$auth->roleid="2194";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="2195";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="pos_returninwards.id, pos_returninwards.customerid, pos_returninwards.documentno, pos_returninwards.creditnoteno, pos_returninwards.mode, pos_items.name as itemid, pos_returninwards.quantity, pos_returninwards.costprice, pos_returninwards.tradeprice, pos_returninwards.retailprice, pos_returninwards.tax, pos_returninwards.discount, pos_returninwards.total, pos_returninwards.returnedon, pos_returninwards.memo, pos_returninwards.createdby, pos_returninwards.createdon, pos_returninwards.lasteditedby, pos_returninwards.lasteditedon, pos_returninwards.ipaddress";
		$join=" left join pos_items on pos_returninwards.itemid=pos_items.id ";
		$having="";
		$groupby="";
		$orderby="";
		$returninwards->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$returninwards->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->customerid; ?></td>
			<td><?php echo $row->documentno; ?></td>
			<td><?php echo $row->creditnoteno; ?></td>
			<td><?php echo $row->mode; ?></td>
			<td><?php echo $row->itemid; ?></td>
			<td><?php echo formatNumber($row->quantity); ?></td>
			<td><?php echo formatNumber($row->costprice); ?></td>
			<td><?php echo formatNumber($row->tradeprice); ?></td>
			<td><?php echo formatNumber($row->retailprice); ?></td>
			<td><?php echo formatNumber($row->tax); ?></td>
			<td><?php echo formatNumber($row->discount); ?></td>
			<td><?php echo formatNumber($row->total); ?></td>
			<td><?php echo formatDate($row->returnedon); ?></td>
			<td><?php echo $row->memo; ?></td>
<?php
//Authorization.
$auth->roleid="2194";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="addreturninwards_proc.php?id=<?php echo $row->id; ?>">View</a></td>
<?php
}
//Authorization.
$auth->roleid="2195";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='returninwards.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
