<title>WiseDigits ERP: Graded </title>
<?php 
include "../../../head.php";

?>
<script type="text/javascript">
$().ready(function() {

 $("#employeename").autocomplete("../../../modules/server/server/search.php?main=hrm&module=employees&field=concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname))", {
 	width: 260,
 	selectFirst: false
 });
 $("#employeename").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("employeename").value=data[0];
     document.getElementById("employeeid").value=data[1];
   }
 });
});
<?php include'js.php'; ?>
</script>
 <script type="text/javascript" charset="utf-8">
 var tbl;
 var iterator=0;
 $(document).ready(function() {
 	tbl = $('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} ); 	
 } );
 
 
 $(document).ready(function () {
 $('#barcode').on('change',function(){ 
            //document.getElementById("barcode").value=str;
            
            var str=$(this).val();
	    var st = str.split("-");
	    
	    if(st.length<2){
	      $('#chatAudio')[0].play();
	      alert("Scan the client bar code first");
	      $("#barcode").val("");
	      $("#barcode").focus();
	    }else{	    
	      document.getElementById("itemid").value=parseInt(st[0]);
	      document.getElementById("sizeid").value=st[1];
	      document.getElementById("quantity").value=st[2];
	      if(st.length==5){
		document.getElementById("downsize").value=st[4];
	      }
	      else{
		document.getElementById("downsize").value='';
	      }
	      
	      document.getElementById("barcode2").focus();
	    }
    });
  });
  
$(document).ready(function(){
  $("#barcode2").on("change",function(){
    var str=$(this).val();
    
    var st = str.split("-");
    if(st.length>1){
      $('#chatAudio')[0].play();
      alert("Scan the employee bar code correctly");
      $("#barcode2").val("");
      $("#barcode2").focus();
    }
    else{    
      $.get("get.php",{id:parseInt(str)},function(data){
	$("#employeename").val(data);      
      });
      document.getElementById("employeeid").value=parseInt(str);
    }
  })
})
 function readBarcode(str){try{
  
  //str = str.substring(0,(str.length-1));
  document.getElementById("barcode").value=str;
  var st = str.split("-");
  
  if(st[2]>100){
    alert("Please Check your client barcode");
  }
  else{
    document.getElementById("itemid").value=parseInt(st[0]);
    document.getElementById("sizeid").value=st[1];
    document.getElementById("quantity").value=st[2];
    if(st[3]!=""){
      document.getElementById("downsize").value=st[3];
    }
    
    document.getElementById("barcode2").focus();
  }  
  
 }catch(e){alert(e);}}
 
 function readBarcode2(str){try{
  
  //str = str.substring(0,(str.length-1));
  document.getElementById("barcode2").value=str;
  
  var st = str.split("-");
  
  getEmployee(parseInt(st[0]));
  
  document.getElementById("employeeid").value=parseInt(st[0]);
  
  
 }catch(e){alert(e);}}
 
 function checkForm(form,event){
    
    var target = event.explicitOriginalTarget || event.relatedTarget ||
        document.activeElement || {};

    if(target.type=="text"){
    
      if(document.getElementById("barcode").value!="" && document.getElementById("barcode2").value!="" ){
	//return true;
	$.post( "addgraded_proc.php", { action2: "Add", sizeid:$("#sizeid").val(), itemid:$("#itemid").val(), quantity:$("#quantity").val(), employeeid:$("#employeeid").val(), iterator:$("#iterator").val(), status:$("#status").val(), downsize:$("#downsize").val() } );

	
	tbl.fnAddData( [
		iterator+1,
		$("#sizeid option:selected").text(),
		$("#itemid option:selected").text(),
		$("#quantity").val(),
		$("#employeename").val(),
		$("#downsize").val(),
		"",
		"" ] );
	
	iterator++;
	$("#barcode").val("");
	$("#barcode2").val("");
	$("#barcode").focus();
	$("#iterator").val(iterator);
	return false;
      }
      else{
	return false;
      }
      return false;
     }
 }
 
 function placeCursorOnPageLoad()
{
	document.getElementById("barcode").focus();/*
	document.getElementById("barcode").select();
	document.getElementById("barcode2").value="";*/
		
}

function GetXmlHttpObject()
{
  if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
  
  if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
  return null;
}
function getEmployee(id)
{
	try{
	var xmlhttp;
	var url="get.php?id="+id;
	xmlhttp=GetXmlHttpObject();
	
	if (xmlhttp==null)
	  {
	  alert ("Browser does not support HTTP Request");
	  return;
	  }  
	/*** changed ***/
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{alert(xmlhttp.responseText);
			document.getElementById("employeename").value=xmlhttp.responseText;
		}
	};
	
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
	}catch(e){alert(e);}
}
womAdd('placeCursorOnPageLoad()');
womOn();
 </script>

<div class='main'>
<form class="forms" id="theform" action="addgraded_proc.php" name="graded" method="POST" enctype="multipart/form-data" onsubmit="return checkForm(this, event);">

	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
			<tr>
				<td><label>Date Graded:</label></td>
<td><input type="text" name="gradedon" id="gradedon" class="date_input" size="12" readonly  value="<?php echo $obj->gradedon; ?>">
<input type="hidden" name='status' id='status' value='<?php echo $obj->status; ?>'></td>
			</tr>
			<tr>
			<td>
		<label>Remarks:</label>			</td>
			<td>
<textarea name="remarks" id="remarks"><?php echo $obj->remarks; ?></textarea>			</td>
			</tr>
			
			<tr>
			<td>Barcode:
			</td>
			<td>
			  <input type="text" name="barcode" id="barcode" value="<?php echo $obj->barcode; ?>" />
			  &nbsp;
			  <input type="text" name="barcode2" id="barcode2" value="<?php echo $obj->barcode2; ?>"/>
			</td>
			</tr>
		</table>
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
	<tr>
		<th align="right">Size  </th>
		<th align="right">Product  </th>
		<th align="right">Quantity  </th>
		<th align="right">Employee  </th>
		<th align="right">Downsize  </th>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<td><select name="sizeid" id="sizeid" class="selectbox">
<option value="">Select...</option>
<?php
	$sizes=new Sizes();
	$where="  ";
	$fields="*";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$sizes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($sizes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->sizeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
<font color='red'>*</font>		<td><select name="itemid" id="itemid" class="selectbox">
<option value="">Select...</option>
<?php
	$items=new Items();
	$where="  ";
	$fields="*";
	$join="";
	$having="";
	$groupby="";
	$orderby=" order by name ";
	$items->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($items->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->itemid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
<font color='red'>*</font>
		</td>		
		<td><input type="text" name="quantity" id="quantity" size="20" value="<?php echo $obj->quantity; ?>"><font color='red'>*</font></td>
		
		<td><input type='text' size='20' name='employeename'  id='employeename' value='<?php echo $obj->employeename; ?>'>
			<input type="hidden" name='employeeid' id='employeeid' value='<?php echo $obj->employeeid; ?>'>
		</td>
		<td><input type="text" readonly name="downsize" id="downsize" size="2" value="<?php echo $obj->downsize; ?>"></td>
<font color='red'>*</font>	<td><input type="submit" name="action2" value="Add"/></td>
	</tr>
	</table>
<table style="clear:both" class="tgrid display" id="tbl" cellpadding="0" align="center" width="98%" cellspacing="0">
	<thead>
	<tr style="font-size:18px; vertical-align:text-top; ">
		<th align="left" >#</th>
		<th align="left">Size  </th>
		<th align="left">Product  </th>
		<th align="left">Quantity  </th>
		<th align="left">Employee  </th>
		<th align="left">Downsize</th>
		<th><input type="hidden" name="iterator" id="iterator" value="<?php echo $obj->iterator; ?>"/></th>
		<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php
	if($_SESSION['shpgraded']){
		$shpgraded=$_SESSION['shpgraded'];
		$i=0;
		$j=$obj->iterator;
		$total=0;
		while($j>0){
		?>
		<tr style="font-size:12px; vertical-align:text-top; ">
			<td><?php echo ($i+1); ?></td>
			<td><?php echo $shpgraded[$i]['sizename']; ?> </td>
			<td><?php echo $shpgraded[$i]['itemname']; ?> </td>
			<td><?php echo $shpgraded[$i]['quantity']; ?> </td>
			<td><?php echo $shpgraded[$i]['employeename']; ?> </td>
			<td><?php echo $shpgraded[$i]['downsize']; ?> </td>
			<td><?php echo $shpgraded[$i]['total']; ?> </td>
			<td><a href="edit.php?i=<?php echo $i; ?>&action=edit&edit=<?php echo $obj->edit; ?>">Edit</a></td>
			<td><a href="edit.php?i=<?php echo $i; ?>&action=del&edit=<?php echo $obj->edit; ?>">Del</a></td>
		</tr>
		<?php
		$i++;
		$j--;
		}
	}
	?>
	</tbody>
</table>
<table align="center" width="98%">
	<?php if(empty($obj->retrieve)){?>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="button"  value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
	<?php }else{?>
	<tr>
		<td colspan="2" align="center"><input type="button" name="action" id="action" value="Print" onclick="Clickheretoprint();"/></td>
	</tr>
	<?php }?>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
include "../../../foot.php";
if(!empty($error)){
	showError($error);
}
if($saved=="Yes"){
	redirect("addgraded_proc.php?retrieve=&status=".$obj->status);
}

?>