<?php
session_start();

$page_title="Graded";
include"../../../head.php";
?>
<ul id="cmd-buttons">
	<li><a class="button icon chat" href="../../post/teams/teams.php">Teams</a></li>
	<li><a class="button icon chat" href="../../post/teamroles/teamroles.php">Team Roles</a></li>
	<li><a class="button icon chat" href="../../post/teammembers/teammembers.php">Team Members</a></li>
	<li><a class="button icon chat" href="../../post/harvestrejects/harvestrejects.php">Packing Hall Rejects</a></li>
	<li><a class="button icon chat" href="../../post/harvestrejects/harvestrejects.php?reduce=reduce">Cold Store Rejects</a></li>
	<li><a class="button icon chat" href="../../prod/rejects/rejects.php?reduce=reduce">Pre-cool Store Rejects</a></li>
	<li><a class="button icon chat" href="../../prod/harvests/harvests.php?status=checkedin">Harvest</a></li>
	<li><a class="button icon chat" href="../../prod/varietys/varietys.php?precool=1">Varieties(Pre-cool)</a></li>
	<li><a class="button icon chat" href="../../prod/harvests/harvests.php?status=checkedout">Pre-cool Check Out</a></li>
	<li><a class="button icon chat" href="../../prod/harvests/harvests.php?status=stocktake">Pre-cool Stock Take</a></li>
	<li><a class="button icon chat" href="../../prod/harvests/harvests.php?status=return">Pre-cool Returns</a></li>
	<li><a class="button icon chat" href="../../post/graded/graded.php?status=checkedin">Graded</a></li>
	<li><a class="button icon chat" href="../../post/graded/graded.php?status=stocktake">Cold Store StockTake</a></li>
	<li><a class="button icon chat" href="../../post/graded/graded.php?status=checkedout">Cold Store Returns</a></li>
	<li><a class="button icon chat" href="../../pos/confirmedorders/confirmedorders.php">Confirmed Orders</a></li>
	<li><a class="button icon chat" href="../../post/graded/barcodegen.php">Generate Employee Barcodes</a></li>
</ul>
<?php
include"../../../foot.php";
?>
