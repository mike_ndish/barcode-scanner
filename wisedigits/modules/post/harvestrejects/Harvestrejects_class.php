<?php 
require_once("HarvestrejectsDBO.php");
class Harvestrejects
{				
	var $id;			
	var $rejecttypeid;			
	var $sizeid;			
	var $itemid;			
	var $quantity;			
	var $gradedon;			
	var $reportedon;			
	var $employeeid;			
	var $barcode;			
	var $remarks;			
	var $status;
	var $reduce;
	var $ipaddress;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $harvestrejectsDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		if(empty($obj->rejecttypeid))
			$obj->rejecttypeid='NULL';
		$this->rejecttypeid=$obj->rejecttypeid;
		if(empty($obj->sizeid))
			$obj->sizeid='NULL';
		$this->sizeid=$obj->sizeid;
		if(empty($obj->itemid))
			$obj->itemid='NULL';
		$this->itemid=$obj->itemid;
		$this->quantity=str_replace("'","\'",$obj->quantity);
		$this->gradedon=str_replace("'","\'",$obj->gradedon);
		$this->reportedon=str_replace("'","\'",$obj->reportedon);
		if(empty($obj->employeeid))
			$obj->employeeid='NULL';
		$this->employeeid=$obj->employeeid;
		$this->barcode=str_replace("'","\'",$obj->barcode);
		$this->remarks=str_replace("'","\'",$obj->remarks);
		$this->status=str_replace("'","\'",$obj->status);
		$this->reduce=str_replace("'","\'",$obj->reduce);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get rejecttypeid
	function getRejecttypeid(){
		return $this->rejecttypeid;
	}
	//set rejecttypeid
	function setRejecttypeid($rejecttypeid){
		$this->rejecttypeid=$rejecttypeid;
	}

	//get sizeid
	function getSizeid(){
		return $this->sizeid;
	}
	//set sizeid
	function setSizeid($sizeid){
		$this->sizeid=$sizeid;
	}

	//get itemid
	function getItemid(){
		return $this->itemid;
	}
	//set itemid
	function setItemid($itemid){
		$this->itemid=$itemid;
	}

	//get quantity
	function getQuantity(){
		return $this->quantity;
	}
	//set quantity
	function setQuantity($quantity){
		$this->quantity=$quantity;
	}

	//get gradedon
	function getGradedon(){
		return $this->gradedon;
	}
	//set gradedon
	function setGradedon($gradedon){
		$this->gradedon=$gradedon;
	}

	//get reportedon
	function getReportedon(){
		return $this->reportedon;
	}
	//set reportedon
	function setReportedon($reportedon){
		$this->reportedon=$reportedon;
	}

	//get employeeid
	function getEmployeeid(){
		return $this->employeeid;
	}
	//set employeeid
	function setEmployeeid($employeeid){
		$this->employeeid=$employeeid;
	}

	//get barcode
	function getBarcode(){
		return $this->barcode;
	}
	//set barcode
	function setBarcode($barcode){
		$this->barcode=$barcode;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	//get status
	function getStatus(){
		return $this->status;
	}
	//set status
	function setStatus($status){
		$this->status=$status;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj){
		$harvestrejectsDBO = new HarvestrejectsDBO();
		if($harvestrejectsDBO->persist($obj)){
		
			//record item stocks		
			if($obj->reduce=="reduce")
			{
			  $itemstocks = new Itemstocks();
			  $itemstocks->reduceStock($obj);
			}
				
			$this->id=$harvestrejectsDBO->id;
			$this->sql=$harvestrejectsDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$harvestrejectsDBO = new HarvestrejectsDBO();
		if($harvestrejectsDBO->update($obj,$where)){
			$this->sql=$harvestrejectsDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$harvestrejectsDBO = new HarvestrejectsDBO();
		if($harvestrejectsDBO->delete($obj,$where=""))		
			$this->sql=$harvestrejectsDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$harvestrejectsDBO = new HarvestrejectsDBO();
		$this->table=$harvestrejectsDBO->table;
		$harvestrejectsDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$harvestrejectsDBO->sql;
		$this->result=$harvestrejectsDBO->result;
		$this->fetchObject=$harvestrejectsDBO->fetchObject;
		$this->affectedRows=$harvestrejectsDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->rejecttypeid)){
			$error="Reject Type should be provided";
		}
// 		else if(empty($obj->sizeid)){
// 			$error="Size should be provided";
// 		}
		else if(empty($obj->itemid)){
			$error="Product should be provided";
		}
		else if(empty($obj->quantity)){
			$error="Quantity should be provided";
		}
		else if(empty($obj->gradedon)){
			$error="Date Graded should be provided";
		}
// 		else if(empty($obj->employeeid)){
// 			$error="Employee should be provided";
// 		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
		if(empty($obj->gradedon)){
			$error="Date Graded should be provided";
		}
		else if(empty($obj->employeeid)){
			$error="Employee should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}
}				
?>
