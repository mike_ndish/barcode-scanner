<title>WiseDigits ERP: Harvestrejects </title>
<?php 
include "../../../head.php";

?>
<script type="text/javascript">
$().ready(function() {
 $("#employeename").autocomplete("../../../modules/server/server/search.php?main=hrm&module=employees&field=concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname))", {
 	width: 260,
 	selectFirst: false
 });
 $("#employeename").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("employeename").value=data[0];
     document.getElementById("employeeid").value=data[1];
   }
 });
});

$(document).ready(function () {
 $('#barcode').on('change',function(){ 
            //document.getElementById("barcode").value=str;
            
            var str=$(this).val();
	    var st = str.split("-");
	    
	    if(st.length<2){
	      $('#chatAudio')[0].play();
	      alert("Scan the client bar code first");
	      $("#barcode").val("");
	      $("#barcode").focus();
	    }else{	    
	      document.getElementById("itemid").value=parseInt(st[0]);
	      document.getElementById("sizeid").value=st[1];
	      document.getElementById("quantity").value=st[2];
	      
	      $("#barcode").val("");
	      $("#barcode2").focus();
	    }
    });
    
    $("#barcode2").on("change",function(){
    var str=$(this).val();
    
    var st = str.split("-");
    if(st.length>1){
      $('#chatAudio')[0].play();
      alert("Scan the employee bar code correctly");
      $("#barcode2").val("");
      $("#barcode2").focus();
    }
    else{    
      $.get("../graded/get.php",{id:parseInt(str)},function(data){
	$("#employeename").val(data);      
      });
      document.getElementById("employeeid").value=parseInt(str);
      $("#barcode2").val("");
      $("#rejecttypeid").focus();
    }
  });
  });

 function checkForm(form,event){
   
    var target = event.explicitOriginalTarget || event.relatedTarget ||
        document.activeElement || {};

    if(target.type=="text"){
    
      
	return false;
      }
      else{
	return true;
      }
     
 }
 
function placeCursorOnPageLoad()
{
	document.getElementById("barcode").focus();
		
}

womAdd('placeCursorOnPageLoad()');
womOn();
</script>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 </script>

<div class='main'>
<form class="forms" id="theform" action="addharvestrejects_proc.php" name="harvestrejects" method="POST" enctype="multipart/form-data" onSubmit="return checkForm(this,event);">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
	<tr>
		<td colspan="2"><input type="hidden" name="id" id="id" value="<?php echo $obj->id; ?>">
		<input type="hidden" name="reduce" id="reduce" value="<?php echo $obj->reduce; ?>"></td>
	</tr>
	
	
	<tr>
		<td align="right">Client BarCode : </td>
		<td><input type="text" name="barcode" id="barcode" value="<?php echo $obj->barcode; ?>"></td>
	</tr>
	<tr>
		<td align="right">Employee BarCode : </td>
		<td><input type="text" name="barcode2" id="barcode2" value="<?php echo $obj->barcode2; ?>"></td>
	</tr>
	<tr>
		<td align="right">Reject Type : </td>
			<td><select name="rejecttypeid" id="rejecttypeid" class="selectbox">
<option value="">Select...</option>
<?php
	$rejecttypes=new Rejecttypes();
	$where="  ";
	$fields="prod_rejecttypes.id, prod_rejecttypes.name, prod_rejecttypes.remarks, prod_rejecttypes.ipaddress, prod_rejecttypes.createdby, prod_rejecttypes.createdon, prod_rejecttypes.lasteditedby, prod_rejecttypes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby=" order by name ";
	$rejecttypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($rejecttypes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->rejecttypeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select><font color='red'>*</font>
		</td>
	</tr>
	<tr>
		<td align="right">Size : </td>
			<td><select name="sizeid" id="sizeid" class="selectbox">
<option value="">Select...</option>
<?php
	$sizes=new Sizes();
	$where="  ";
	$fields="prod_sizes.id, prod_sizes.name, prod_sizes.remarks, prod_sizes.ipaddress, prod_sizes.createdby, prod_sizes.createdon, prod_sizes.lasteditedby, prod_sizes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$sizes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($sizes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->sizeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">Product : </td>
			<td><select name="itemid" id="itemid" class="selectbox">
<option value="">Select...</option>
<?php
	$items=new Items();
	$where="  ";
	$fields="*";
	$join="";
	$having="";
	$groupby="";
	$orderby=" order by name ";
	$items->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($items->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->itemid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select><font color='red'>*</font>
		</td>
	</tr>
	<tr>
		<td align="right">Quantity : </td>
		<td><input type="text" name="quantity" id="quantity" size="8"  value="<?php echo $obj->quantity; ?>"><font color='red'>*</font></td>
	</tr>
	<tr>
		<td align="right">Date Graded : </td>
		<td><input type="text" name="gradedon" id="gradedon" class="date_input" size="12" readonly  value="<?php echo $obj->gradedon; ?>"><font color='red'>*</font></td>
	</tr>
	<tr>
		<td align="right">Date Reported : </td>
		<td><input type="text" name="reportedon" id="reportedon" class="date_input" size="12" readonly  value="<?php echo $obj->reportedon; ?>"></td>
	</tr>
	<tr>
		<td align="right">Employee : </td>
			<td><input type='text' size='40' name='employeename' id='employeename' value='<?php echo $obj->employeename; ?>'>
			<input type="hidden" name='employeeid' id='employeeid' value='<?php echo $obj->employeeid; ?>'>
		</td>
	</tr>
	<tr>
		<td align="right">Remarks : </td>
		<td><textarea name="remarks"><?php echo $obj->remarks; ?></textarea></td>
	</tr>
	<tr>
		<td align="right">Status : </td>
		<td><select name='status' class="selectbox">
			<option value='Local Market' <?php if($obj->status=='Local Market'){echo"selected";}?>>Local Market</option>
			<option value='Discarded' <?php if($obj->status=='Discarded'){echo"selected";}?>>Discarded</option>
		</select></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
include "../../../foot.php";
if(!empty($error)){
	showError($error);
}
?>