<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Inwards_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
//Redirect to horizontal layout
redirect("addinwards_proc.php?retrieve=".$_GET['retrieve']);

$page_title="Inwards";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="8064";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$inwards=new Inwards();
if(!empty($delid)){
	$inwards->id=$delid;
	$inwards->delete($inwards);
	redirect("inwards.php");
}
//Authorization.
$auth->roleid="8063";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <a href='addinwards_proc.php'>New Inwards</a></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Inward Note No </th>
			<th>Delivery Note No </th>
			<th>Project </th>
			<th>Supplier </th>
			<th>Inward Date </th>
			<th>Remarks </th>
			<th>Browse File </th>
<?php
//Authorization.
$auth->roleid="8065";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="8066";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="proc_inwards.id, proc_inwards.documentno, proc_inwards.deliverynoteno, con_projects.name as projectid, proc_suppliers.name as supplierid, proc_inwards.inwarddate, proc_inwards.remarks, proc_inwards.file, proc_inwards.ipaddress, proc_inwards.createdby, proc_inwards.createdon, proc_inwards.lasteditedby, proc_inwards.lasteditedon";
		$join=" left join con_projects on proc_inwards.projectid=con_projects.id  left join proc_suppliers on proc_inwards.supplierid=proc_suppliers.id ";
		$having="";
		$groupby="";
		$orderby="";
		$inwards->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$inwards->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->documentno; ?></td>
			<td><?php echo $row->deliverynoteno; ?></td>
			<td><?php echo $row->projectid; ?></td>
			<td><?php echo $row->supplierid; ?></td>
			<td><?php echo formatDate($row->inwarddate); ?></td>
			<td><?php echo $row->remarks; ?></td>
			<td><?php echo $row->file; ?></td>
<?php
//Authorization.
$auth->roleid="8065";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="addinwards_proc.php?id=<?php echo $row->id; ?>">View</a></td>
<?php
}
//Authorization.
$auth->roleid="8066";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='inwards.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
