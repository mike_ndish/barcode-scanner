<title>WiseDigits ERP: Purchaseorders </title>
<?php 
include "../../../head.php";

?>
<script type="text/javascript">
$().ready(function() {
 $("#itemname").autocomplete("../../../modules/server/server/search.php?main=inv&module=items&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#itemname").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("itemname").value=data[0];
     document.getElementById("itemid").value=data[1];
     document.getElementById("code").value=data[2];
     document.getElementById("tax").value=data[2];
     document.getElementById("costprice").value=data[9];
     document.getElementById("tradeprice").value=data[10];
   }
 });
 $("#suppliername").autocomplete("../../../modules/server/server/search.php?main=proc&module=suppliers&field=proc_suppliers.name&extra=sys_currencys.rate, sys_currencys.eurorate&join=left join sys_currencys on sys_currencys.id=proc_suppliers.currencyid&extratitle=rate,eurorate", {
 	width: 260,
 	selectFirst: false
 });
 $("#suppliername").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("suppliername").value=data[0];
     document.getElementById("supplierid").value=data[1];
     document.getElementById("currencyid").value=data[5];
     document.getElementById("contact").value=data[8];
     document.getElementById("physicaladdress").value=data[9];
     document.getElementById("tel").value=data[10];
     document.getElementById("cellphone").value=data[13];
     document.getElementById("email").value=data[12];
     document.getElementById("rate").value=data[19];
     document.getElementById("eurorate").value=data[20];
   }
 });
 $("#projectname").autocomplete("../../../modules/server/server/search.php?main=con&module=projects&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#projectname").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("projectname").value=data[0];
     document.getElementById("projectid").value=data[1];
   }
 });
});
<?php include'js.php'; ?>
</script>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 </script>
  <script type="text/javascript">
function Clickheretoprint()
{ 
	var msg;
	msg="Do you want to print LPO?";
	var ans=confirm(msg);
	if(ans)
	{
 		<?php $_SESSION['obj']=$obj; ?>
		poptastic('print.php?obj=<?php  echo str_replace('&','',serialize($obj)); ?>&supplierid=<?php echo $obj->supplierid; ?>&projectid=<?php echo $obj->projectid; ?>&documentno=<?php echo $obj->documentno; ?>',700,1020);
	}
}
 </script>
<div class='container'>
<div class="content">
<form class="forms" id="theform" action="addpurchaseorders_proc.php" name="purchaseorders" method="POST" enctype="multipart/form-data">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input class="btn" type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
			<tr>
				<td><label>Supplier:</label></td>
				<td><input type='text' size='20' name='suppliername' id='suppliername' value='<?php echo $obj->suppliername; ?>'>
					<input type="hidden" name='supplierid' id='supplierid' value='<?php echo $obj->supplierid; ?>'></td>
				<td><label>Contact:</label></td>
				<td><input type='text' name='contact' id='contact' size='0' readonly value='<?php echo $obj->contact; ?>'/></td>			<tr>
				<td><label>Physical Address:</label></td>
				<td><textarea name='physicaladdress' id='physicaladdress' readonly><?php echo $obj->physicaladdress; ?></textarea></td>
				<td><label>Phone No.:</label></td>
				<td><input type='text' name='tel' id='tel' size='8' readonly value='<?php echo $obj->tel; ?>'/></td>			<tr>
				<td><label>Cell-Phone:</label></td>
				<td><input type='text' name='cellphone' id='cellphone' size='8' readonly value='<?php echo $obj->cellphone; ?>'/></td>				<td><label>E-mail:</label></td>
				<td><input type='text' name='email' id='email' size='0' readonly value='<?php echo $obj->email; ?>'/></td>			</td>
			</tr>
			<tr>
			  <td>Exchange Rate</td>
			  <td><select name="currencyid" id="currencyid">
				<option value="">Select...</option>
				<?php
				$currencys = new Currencys();
				$fields="* ";
				$join=" ";
				$having="";
				$groupby="";
				$orderby="";
				$where=" ";
				$currencys->retrieve($fields,$join,$where,$having,$groupby,$orderby);
				while($row=mysql_fetch_object($currencys->result)){
				  ?>
				  <option value="<?php echo $row->id; ?>" <?php if($row->id==$obj->currencyid){echo"selected";}?>><?php echo $row->name; ?></option>
				  <?php
				}
				?>
			      </select>
			      <input type="text" size='6' readonly name="rate" id="rate" value="<?php echo $obj->rate; ?>"/>
			      <input type="text" size='6' readonly name="eurorate" id="eurorate" value="<?php echo $obj->eurorate; ?>"/>
			</tr>
			<tr>
				<td><label>Project:</label></td>
				<td><textarea name='projectname' style="width:400;" id='projectname'><?php echo $obj->projectname; ?></textarea>
					<input type="hidden" name='projectid' id='projectid' value='<?php echo $obj->projectid; ?>'></td>
			</td>
			</tr>
		</table>
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
	<tr>
		<th align="right">Item Description  </th>
		<th align="right">Code  </th>
		<th align="right">VAT  </th>
		<th align="right">Cost Price  </th>
		<th align="right">Trade Price  </th>
		<th align="right">Quantity  </th>
		<th>Total</th>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<td><input type='text' size='20' name='itemname'  onchange="calculateTotal();" onblur="calculateTotal();"  id='itemname' value='<?php echo $obj->itemname; ?>'>
			<input type="hidden" name='itemid' id='itemid' value='<?php echo $obj->itemid; ?>'>		<td>
		<input type='text' name='code' id='code'  size='4' readonly value='<?php echo $obj->code; ?>'/>
		</td>
		<td>
		<input type='text' name='tax' id='tax'  onchange="calculateTotal();" onblur="calculateTotal();"  size='4'  value='<?php echo $obj->tax; ?>'/>
		</td>
		<td>
		<input type='text' name='costprice' id='costprice'  onchange="calculateTotal();" onblur="calculateTotal();"  size='8'  value='<?php echo $obj->costprice; ?>'/>
		</td>
		<td>
		<input type='text' name='tradeprice' id='tradeprice'  onchange="calculateTotal();" onblur="calculateTotal();"  size='8'  value='<?php echo $obj->tradeprice; ?>'/>
		</td>

		</td>
<font color='red'>*</font>		<td><input type="text" name="quantity" id="quantity" onchange="calculateTotal();" onblur="calculateTotal();"  size="4" value="<?php echo $obj->quantity; ?>"></td>
	<td><input type="text" name="total" id="total" size='8' readonly value="<?php echo $obj->total; ?>"/></td>
	<td><input type="submit" class="btn btn-primary" name="action2" value="Add"/></td>
	</tr>
	</table>
		<table align='center'>
			<tr>
			<td>
		Memo:<textarea name="memo" ><?php echo $obj->memo; ?></textarea>

<input type="hidden" name="olddocumentno" id="olddocumentno" hidden size="0"  value="<?php echo $obj->olddocumentno; ?>">

<input type="hidden" name="edit" id="edit" hidden size="0"  value="<?php echo $obj->edit; ?>">
		Document No.:<input type="text" name="documentno" id="documentno" readonly size="5"  value="<?php echo $obj->documentno; ?>">
		Order On:<input type="date" name="orderedon" id="orderedon"  class="date_input" size="8" readonly  value="<?php echo $obj->orderedon; ?>">
		Requisition No:<textarea name="requisitionno" ><?php echo $obj->requisitionno; ?></textarea>
			</td>
			</tr>
		</table>
<table style="clear:both" class="tgrid display" id="tbl" cellpadding="0" align="center" width="98%" cellspacing="0">
	<thead>
	<tr style="vertical-align:text-top; ">
		<th align="left" >#</th>
		<th align="left">Item  </th>
		<th align="right">Code  </th>
		<th align="right">VAT  </th>
		<th align="right">Cost Price  </th>
		<th align="right">Trade Price  </th>
		<th align="left">Quantity  </th>
		<th align='left'>Total</th>
		<th><input type="hidden" name="iterator" value="<?php echo $obj->iterator; ?>"/></th>
		<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php
	if($_SESSION['shppurchaseorders']){
		$shppurchaseorders=$_SESSION['shppurchaseorders'];
		$i=0;
		$j=$obj->iterator;
		$total=0;
		while($j>0){

		$total+=$shppurchaseorders[$i]['total'];
		?>
		<tr style="font-size:12px; vertical-align:text-top; ">
			<td><?php echo ($i+1); ?></td>
			<td><?php echo $shppurchaseorders[$i]['itemname']; ?> </td>
			<td><?php echo $shppurchaseorders[$i]['code']; ?> </td>
			<td><?php echo $shppurchaseorders[$i]['tax']; ?> </td>
			<td><?php echo $shppurchaseorders[$i]['costprice']; ?> </td>
			<td><?php echo $shppurchaseorders[$i]['tradeprice']; ?> </td>
			<td><?php echo $shppurchaseorders[$i]['quantity']; ?> </td>
			<td align="right"><?php echo formatNumber($shppurchaseorders[$i]['total']); ?> </td>
			<td><a href="edit.php?i=<?php echo $i; ?>&action=edit&edit=<?php echo $obj->edit; ?>">Edit</a></td>
			<td><a href="edit.php?i=<?php echo $i; ?>&action=del&edit=<?php echo $obj->edit; ?>">Del</a></td>
		</tr>
		<?php
		$i++;
		$j--;
		}
	}
	?>
	</tbody>
</table>
<table align="center" width="98%">
	<tr>
		<td colspan="2" align="center">Total:<input type="text" size='12' readonly value="<?php echo $total; ?>"/></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input class="btn btn-primary" type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input class="btn btn-danger" type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
	<?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="2" align="center"><input class="btn btn-primary" type="button" name="action" id="action" value="Print" onclick="Clickheretoprint();"/></td>
	</tr>
	<?php }?>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
</div>
</div>
<?php 
include "../../../foot.php";
if(!empty($error)){
	showError($error);
}
if($saved=="Yes"){
	redirect("addpurchaseorders_proc.php?retrieve=");
}

?>