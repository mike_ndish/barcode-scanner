<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Purchaseorders_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../con/projects/Projects_class.php");
require_once("../../proc/suppliers/Suppliers_class.php");
require_once("../purchaseorderdetails/Purchaseorderdetails_class.php");
require_once("../../inv/items/Items_class.php");
require_once("../../proc/purchaseorders/Purchaseorders_class.php");
require_once("../../pm/tasks/Tasks_class.php");
require_once("../../pm/notifications/Notifications_class.php");
require_once("../../pm/notificationrecipients/Notificationrecipients_class.php");
require_once("../../hrm/employees/Employees_class.php");
require_once("../../wf/routedetails/Routedetails_class.php");
require_once("../../inv/departments/Departments_class.php");
require_once("../../proc/config/Config_class.php");
require_once("../../sys/currencys/Currencys_class.php");

//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="8069";//Edit
}
else{
	$auth->roleid="8067";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
	
if($obj->action=="Save"){
	$purchaseorders=new Purchaseorders();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$shppurchaseorders=$_SESSION['shppurchaseorders'];
	$error=$purchaseorders->validates($obj);
	if(!empty($error)){
		$error=$error;
	}
	elseif(empty($shppurchaseorders)){
		$error="No items in the sale list!";
	}
	else{
		$purchaseorders=$purchaseorders->setObject($obj);
		if($purchaseorders->add($purchaseorders,$shppurchaseorders)){
			$error=SUCCESS;
			$saved="Yes";
			$_SESSION['shppurchaseorders']="";
			
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$purchaseorders=new Purchaseorders();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$purchaseorders->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$purchaseorders=$purchaseorders->setObject($obj);
		$shppurchaseorders=$_SESSION['shppurchaseorders'];
		if($purchaseorders->edit($purchaseorders,"",$shppurchaseorders)){
			$error=UPDATESUCCESS;
			$saved="Yes";
			$_SESSION['shppurchaseorders']="";
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if($obj->action2=="Add"){

	if(empty($obj->quantity)){
		$error=" must be provided";
	}
	elseif(empty($obj->itemid)){
		$error=" must be provided";
	}
	else{
	$_SESSION['obj']=$obj;
	if(empty($obj->iterator))
		$it=0;
	else
		$it=$obj->iterator;
	$shppurchaseorders=$_SESSION['shppurchaseorders'];

	$items = new Items();
	$fields=" * ";
	$join="";
	$groupby="";
	$having="";
	$where=" where id='$obj->itemid'";
	$items->retrieve($fields, $join, $where, $having, $groupby, $orderby);
	$items=$items->fetchObject;

	;
	$shppurchaseorders[$it]=array('quantity'=>"$obj->quantity", 'itemid'=>"$obj->itemid", 'itemname'=>"$items->name", 'code'=>"$obj->code", 'tax'=>"$obj->tax", 'costprice'=>"$obj->costprice", 'tradeprice'=>"$obj->tradeprice", 'remarks'=>"$obj->remarks", 'total'=>"$obj->total");

 	$it++;
		$obj->iterator=$it;
 	$_SESSION['shppurchaseorders']=$shppurchaseorders;

	$obj->quantity="";
	$obj->costprice="";
	$obj->tradeprice="";
	$obj->total="";
	$obj->itemname="";
	$obj->code="";
 	$obj->itemid="";
 	$obj->remarks="";
 }
}

if($obj->action=="Filter"){
	if(!empty($obj->invoiceno)){
		$purchaseorders = new Purchaseorders();
		$fields="proc_purchaseorders.id, con_projects.id as projectid, proc_purchaseorders.documentno, inv_items.id as itemid,inv_items.name itemname,  proc_purchaseorderdetails.quantity, proc_purchaseorderdetails.costprice, proc_purchaseorderdetails.total, proc_purchaseorderdetails.memo, proc_purchaseorders.requisitionno, proc_suppliers.id as supplierid, proc_purchaseorders.remarks, proc_purchaseorders.orderedon, proc_purchaseorders.file, proc_purchaseorders.createdby, proc_purchaseorders.createdon, proc_purchaseorders.lasteditedby, proc_purchaseorders.lasteditedon, proc_purchaseorders.ipaddress";
		$join=" left join proc_purchaseorderdetails on proc_purchaseorderdetails.purchaseorderid=proc_purchaseorders.id left join inv_items on inv_items.id=proc_purchaseorderdetails.itemid left join con_projects on proc_purchaseorders.projectid=con_projects.id  left join proc_suppliers on proc_purchaseorders.supplierid=proc_suppliers.id ";
		$having="";
		$groupby="";
		$orderby="";
		$where=" where proc_purchaseorders.documentno='$obj->invoiceno'";
		$purchaseorders->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$purchaseorders->result;
		$it=0;
		while($row=mysql_fetch_object($res)){
				
			$items = new Items();
			$fields=" * ";
			$join="";
			$groupby="";
			$having="";
			$where=" where id='$row->itemid'";
			$items->retrieve($fields, $join, $where, $having, $groupby, $orderby);
			$items=$items->fetchObject;
	  
			$ob=$row;
			$row->total=$row->quantity*$row->costprice;
			$shppurchaseorders[$it]=array('quantity'=>"$row->quantity", 'itemid'=>"$row->itemid", 'itemname'=>"$items->name", 'code'=>"$row->code", 'tax'=>"$row->tax", 'costprice'=>"$row->costprice", 'tradeprice'=>"$row->tradeprice", 'remarks'=>"$row->remarks", 'total'=>"$row->total",'createdby'=>"$obj->createdby",'createdon'=>"$obj->createdon");

			$it++;
		}

		//for autocompletes
		$suppliers = new Suppliers();
		$fields=" * ";
		$where=" where id='$ob->supplierid'";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$suppliers->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$auto=$suppliers->fetchObject;
		$auto->suppliername=$auto->name;
		
		$projects = new Projects();
		$fields=" * ";
		$where=" where id='$ob->projectid'";
		$join="";
		$having="";
		$groupby="";
		$orderby="";
		$projects->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$auto2=$projects->fetchObject;
		$auto2->projectname=$auto2->name;

		$obj = (object) array_merge((array) $obj, (array) $ob);
		$obj = (object) array_merge((array) $obj, (array) $auto);	
		$obj = (object) array_merge((array) $obj, (array) $auto2);

		$obj->iterator=$it;
		
		$_SESSION['shppurchaseorders']=$shppurchaseorders;
	}
}

if(empty($obj->action)){

	$projects= new Projects();
	$fields="con_projects.id, con_projects.tenderid, con_projects.name, con_projects.projecttypeid, con_projects.customerid, con_projects.employeeid, con_projects.regionid, con_projects.subregionid, con_projects.contractno, con_projects.physicaladdress, con_projects.scope, con_projects.value, con_projects.dateawarded, con_projects.acceptanceletterdate, con_projects.contractsignedon, con_projects.orderdatetocommence, con_projects.startdate, con_projects.expectedenddate, con_projects.actualenddate, con_projects.liabilityperiodtype, con_projects.liabilityperiod, con_projects.remarks, con_projects.statusid, con_projects.ipaddress, con_projects.createdby, con_projects.createdon, con_projects.lasteditedby, con_projects.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$projects->retrieve($fields,$join,$where,$having,$groupby,$orderby);


	$suppliers= new Suppliers();
	$fields="proc_suppliers.id, proc_suppliers.code, proc_suppliers.name, proc_suppliers.suppliercategoryid, proc_suppliers.regionid, proc_suppliers.subregionid, proc_suppliers.contact, proc_suppliers.physicaladdress, proc_suppliers.tel, proc_suppliers.fax, proc_suppliers.email, proc_suppliers.cellphone, proc_suppliers.status, proc_suppliers.createdby, proc_suppliers.createdon, proc_suppliers.lasteditedby, proc_suppliers.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$suppliers->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$purchaseorders=new Purchaseorders();
	$where=" where id=$id ";
	$fields="proc_purchaseorders.id, proc_purchaseorders.projectid, proc_purchaseorders.documentno, proc_purchaseorders.requisitionno, proc_purchaseorders.supplierid, proc_purchaseorders.remarks, proc_purchaseorders.orderedon, proc_purchaseorders.file, proc_purchaseorders.createdby, proc_purchaseorders.createdon, proc_purchaseorders.lasteditedby, proc_purchaseorders.lasteditedon, proc_purchaseorders.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$purchaseorders->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$purchaseorders->fetchObject;

	//for autocompletes
	$suppliers = new Suppliers();
	$fields=" * ";
	$where=" where id='$obj->supplierid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$suppliers->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$suppliers->fetchObject;

	$obj->suppliername=$auto->name;
	$projects = new Projects();
	$fields=" * ";
	$where=" where id='$obj->projectid'";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$projects->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$auto=$projects->fetchObject;

	$obj->projectname=$auto->name;
}

if(empty($obj->retrieve)){
  if(empty($_GET['edit'])){
      if(empty($obj->action) and empty($obj->action2)){
      
	if(empty($_GET['raise']))
	  $_SESSION['shppurchaseorders']="";
	else{
	  
	  $obj=$_SESSION['ob'];
	  $obj->iterator=count($_SESSION['shppurchaseorders']);
	}
			  
	$defs=mysql_fetch_object(mysql_query("select (max(documentno)+1) documentno from proc_purchaseorders"));
	if($defs->documentno == null){
		$defs->documentno=1;
	}
	$obj->documentno=$defs->documentno;

	$obj->orderedon=date("Y-m-d");	
	
      }
      $obj->action="Save";
  } 
  else{
    $obj=$_SESSION['obj'];
  }
  
}
else{  
    $obj->action="Update";
}
	
$page_title="Purchaseorders ";
include "addpurchaseorders.php";
?>