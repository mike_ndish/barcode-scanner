<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Purchaseorders_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}
//Redirect to horizontal layout
redirect("addpurchaseorders_proc.php?retrieve=".$_GET['retrieve']);

$page_title="Purchaseorders";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="8068";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$purchaseorders=new Purchaseorders();
if(!empty($delid)){
	$purchaseorders->id=$delid;
	$purchaseorders->delete($purchaseorders);
	redirect("purchaseorders.php");
}
//Authorization.
$auth->roleid="8067";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<a class="btn btn-info" href='addpurchaseorders_proc.php'>New Purchaseorders</a>
<?php }?>
<div style="clear:both;"></div>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Project </th>
			<th>Document No. </th>
			<th>Requisition No </th>
			<th>Supplier </th>
			<th>Remarks </th>
			<th>Order On </th>
			<th>Browse File </th>
<?php
//Authorization.
$auth->roleid="8069";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="8070";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="proc_purchaseorders.id, con_projects.name as projectid, proc_purchaseorders.documentno, proc_purchaseorders.requisitionno, proc_suppliers.name as supplierid, proc_purchaseorders.remarks, proc_purchaseorders.orderedon, proc_purchaseorders.file, proc_purchaseorders.createdby, proc_purchaseorders.createdon, proc_purchaseorders.lasteditedby, proc_purchaseorders.lasteditedon, proc_purchaseorders.ipaddress";
		$join=" left join con_projects on proc_purchaseorders.projectid=con_projects.id  left join proc_suppliers on proc_purchaseorders.supplierid=proc_suppliers.id ";
		$having="";
		$groupby="";
		$orderby="";
		$purchaseorders->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$purchaseorders->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->projectid; ?></td>
			<td><?php echo $row->documentno; ?></td>
			<td><?php echo $row->requisitionno; ?></td>
			<td><?php echo $row->supplierid; ?></td>
			<td><?php echo $row->remarks; ?></td>
			<td><?php echo formatDate($row->orderedon); ?></td>
			<td><?php echo $row->file; ?></td>
<?php
//Authorization.
$auth->roleid="8069";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="addpurchaseorders_proc.php?id=<?php echo $row->id; ?>">View</a></td>
<?php
}
//Authorization.
$auth->roleid="8070";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='purchaseorders.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
