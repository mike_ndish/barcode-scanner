<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("../../inv/departments/Departments_class.php");

$db=new DB();

$page_title="Suppliers";
include"../../../head.php";
?>
<ul id="cmd-buttons">
<?php
$departments= new Departments();
$fields="inv_departments.id, inv_departments.name, inv_departments.code, inv_departments.remarks, inv_departments.createdby, inv_departments.createdon, inv_departments.lasteditedby, inv_departments.lasteditedon, inv_departments.ipaddress";
$join="";
$having="";
$groupby="";
$orderby="";
$departments->retrieve($fields,$join,$where,$having,$groupby,$orderby);
while($row=mysql_fetch_object($departments->result)){
?>
	<li><a class="button icon chat" href="../../proc/requisitions/requisitions.php?departmentid=<?php echo $row->id; ?>">Requisitions (<?php echo $row->code; ?>)</a></li>
<?php
}
?>
	<li><a class="button icon chat" href="../../proc/requisitions/requisitions.php?retrieve=1">Retrieve Requisitions</a></li>
	<li><a class="button icon chat" href="../../proc/purchaseorders/purchaseorders.php">Local Purchase Orders</a></li>
	<li><a class="button icon chat" href="../../proc/purchaseorders/purchaseorders.php?retrieve=1">Retrieve LPO</a></li>
	<li><a class="button icon chat" href="../../proc/deliverynotes/deliverynotes.php">Delivery Notes</a></li>
	<li><a class="button icon chat" href="../../proc/deliverynotes/deliverynotes.php?retrieve=1">Retrieve Delivery Notes</a></li>
<!-- 	<li><a class="button icon chat" href="../../proc/inwards/inwards.php">Inwards</a></li> -->
	<li><a class="button icon chat" href="../../proc/requisitions/reconciliation.php">Reconciliation</a></li>
	<li><a class="button icon chat" href="../../proc/suppliers/suppliers.php">Suppliers</a></li>
	<li><a class="button icon chat" href="../../proc/supplieritems/supplieritems.php">Suppliers Price Lists</a></li>
	<li><a class="button icon chat" href="../../proc/suppliercategorys/suppliercategorys.php">Supplier Categorys</a></li>
	<li><a class="button icon chat" href="../../proc/config/config.php">Configuration</a></li>
</ul>
<?php
include"../../../foot.php";
?>
