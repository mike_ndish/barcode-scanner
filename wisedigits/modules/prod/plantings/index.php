<?php
session_start();

$page_title="Plantings";
include"../../../head.php";
?>
<ul id="cmd-buttons">
	<li><a class="button icon chat" href="../../prod/sizes/sizes.php">Sizes</a></li>
	<li><a class="button icon chat" href="../../prod/colours/colours.php">Colours</a></li>
	<li><a class="button icon chat" href="../../prod/blocks/blocks.php">Blocks</a></li>
	<li><a class="button icon chat" href="../../prod/types/types.php">Genetics</a></li>
	<li><a class="button icon chat" href="../../prod/sections/sections.php">Sections</a></li>
	<li><a class="button icon chat" href="../../prod/varietys/varietys.php">Varieties</a></li>
	<li><a class="button icon chat" href="../../prod/greenhouses/greenhouses.php">Green Houses</a></li>
	<li><a class="button icon chat" href="../../prod/greenhousevarietys/greenhousevarietys.php">Areas</a></li>
	<li><a class="button icon chat" href="../../prod/breeders/breeders.php">Breeders</a></li>
	<li><a class="button icon chat" href="../../prod/chemicals/chemicals.php">Chemicals</a></li>
	<li><a class="button icon chat" href="../../prod/nozzles/nozzles.php">Nozzles</a></li>
	<li><a class="button icon chat" href="../../prod/spraymethods/spraymethods.php">Spray Methods</a></li>
	<li><a class="button icon chat" href="../../prod/qualitychecks/qualitychecks.php">Quality Checks</a></li>
	<li><a class="button icon chat" href="../../prod/rejecttypes/rejecttypes.php">Reject Types</a></li>	
	<li><a class="button icon chat" href="../../prod/rejects/rejects.php">Production Rejects</a></li>
	<li><a class="button icon chat" href="../../prod/checkitems/checkitems.php">Quality Check Items</a></li>
	<li><a class="button icon chat" href="../../prod/plantings/plantings.php">Plantings</a></li>
	<li><a class="button icon chat" href="../../prod/breederdeliverys/breederdeliverys.php">Breeder Deliveries</a></li>
	<li><a class="button icon chat" href="../../prod/uproots/uproots.php">Uproots</a></li>
	<li><a class="button icon chat" href="../../prod/forecastings/forecastings.php">Forecastings</a></li>
	<li><a class="button icon chat" href="../../prod/sprayprogrammes/sprayprogrammes.php">Spray Programmes</a></li>
	<li><a class="button icon chat" href="../../prod/harvests/barcodegen.php">Generate Barcodes</a></li>
	<li><a class="button icon chat" href="../../prod/fetilizers/fetilizers.php">Fetilizers</a></li>
	<li><a class="button icon chat" href="../../prod/valves/valves.php">Irrigation Valves</a></li>
	<li><a class="button icon chat" href="../../prod/irrigationtanks/irrigationtanks.php">Irrigation Tanks</a></li>	
	<li><a class="button icon chat" href="../../prod/irrigationsystems/irrigationsystems.php">Irrigation Systems</a></li>
	<li><a class="button icon chat" href="../../prod/irrigations/irrigations.php">Irrigations</a></li>
	
</ul>
<?php
include"../../../foot.php";
?>
