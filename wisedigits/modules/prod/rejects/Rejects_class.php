<?php 
require_once("RejectsDBO.php");
class Rejects
{				
	var $id;			
	var $rejecttypeid;			
	var $varietyid;			
	var $sizeid;			
	var $plantingdetailid;			
	var $greenhouseid;			
	var $quantity;			
	var $employeeid;			
	var $barcode;			
	var $harvestedon;			
	var $reportedon;			
	var $remarks;			
	var $status;	
	var $reduce;
	var $ipaddress;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $rejectsDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		if(empty($obj->rejecttypeid))
			$obj->rejecttypeid='NULL';
		$this->rejecttypeid=$obj->rejecttypeid;
		if(empty($obj->varietyid))
			$obj->varietyid='NULL';
		$this->varietyid=$obj->varietyid;
		if(empty($obj->sizeid))
			$obj->sizeid='NULL';
		$this->sizeid=$obj->sizeid;
		if(empty($obj->plantingdetailid))
			$obj->plantingdetailid='NULL';
		$this->plantingdetailid=$obj->plantingdetailid;
		if(empty($obj->greenhouseid))
			$obj->greenhouseid='NULL';
		$this->greenhouseid=$obj->greenhouseid;
		$this->quantity=str_replace("'","\'",$obj->quantity);
		if(empty($obj->employeeid))
			$obj->employeeid='NULL';
		$this->employeeid=$obj->employeeid;
		$this->barcode=str_replace("'","\'",$obj->barcode);
		$this->harvestedon=str_replace("'","\'",$obj->harvestedon);
		$this->reportedon=str_replace("'","\'",$obj->reportedon);
		$this->remarks=str_replace("'","\'",$obj->remarks);
		$this->status=str_replace("'","\'",$obj->status);
		$this->reduce=str_replace("'","\'",$obj->reduce);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get rejecttypeid
	function getRejecttypeid(){
		return $this->rejecttypeid;
	}
	//set rejecttypeid
	function setRejecttypeid($rejecttypeid){
		$this->rejecttypeid=$rejecttypeid;
	}

	//get varietyid
	function getVarietyid(){
		return $this->varietyid;
	}
	//set varietyid
	function setVarietyid($varietyid){
		$this->varietyid=$varietyid;
	}

	//get sizeid
	function getSizeid(){
		return $this->sizeid;
	}
	//set sizeid
	function setSizeid($sizeid){
		$this->sizeid=$sizeid;
	}

	//get plantingdetailid
	function getPlantingdetailid(){
		return $this->plantingdetailid;
	}
	//set plantingdetailid
	function setPlantingdetailid($plantingdetailid){
		$this->plantingdetailid=$plantingdetailid;
	}

	//get greenhouseid
	function getGreenhouseid(){
		return $this->greenhouseid;
	}
	//set greenhouseid
	function setGreenhouseid($greenhouseid){
		$this->greenhouseid=$greenhouseid;
	}

	//get quantity
	function getQuantity(){
		return $this->quantity;
	}
	//set quantity
	function setQuantity($quantity){
		$this->quantity=$quantity;
	}

	//get employeeid
	function getEmployeeid(){
		return $this->employeeid;
	}
	//set employeeid
	function setEmployeeid($employeeid){
		$this->employeeid=$employeeid;
	}

	//get barcode
	function getBarcode(){
		return $this->barcode;
	}
	//set barcode
	function setBarcode($barcode){
		$this->barcode=$barcode;
	}

	//get harvestedon
	function getHarvestedon(){
		return $this->harvestedon;
	}
	//set harvestedon
	function setHarvestedon($harvestedon){
		$this->harvestedon=$harvestedon;
	}

	//get reportedon
	function getReportedon(){
		return $this->reportedon;
	}
	//set reportedon
	function setReportedon($reportedon){
		$this->reportedon=$reportedon;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	//get status
	function getStatus(){
		return $this->status;
	}
	//set status
	function setStatus($status){
		$this->status=$status;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj){
		$rejectsDBO = new RejectsDBO();
		if($rejectsDBO->persist($obj)){
		
			//record item stocks		
			if($obj->reduce=="reduce")
			{
			  $itemstocks = new Varietystocks();
			  $itemstocks->reduceStock($obj);
			}
			
			$this->id=$rejectsDBO->id;
			$this->sql=$rejectsDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$rejectsDBO = new RejectsDBO();
		if($rejectsDBO->update($obj,$where)){
			$this->sql=$rejectsDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$rejectsDBO = new RejectsDBO();
		if($rejectsDBO->delete($obj,$where=""))		
			$this->sql=$rejectsDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$rejectsDBO = new RejectsDBO();
		$this->table=$rejectsDBO->table;
		$rejectsDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$rejectsDBO->sql;
		$this->result=$rejectsDBO->result;
		$this->fetchObject=$rejectsDBO->fetchObject;
		$this->affectedRows=$rejectsDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->rejecttypeid)){
			$error="Reject Type should be provided";
		}
		else if(empty($obj->varietyid)){
			$error="Variety should be provided";
		}/*
		else if(empty($obj->sizeid)){
			$error="Length should be provided";
		}*//*
		else if(empty($obj->plantingdetailid)){
			$error="Planting Detail should be provided";
		}*/
		else if(empty($obj->quantity)){
			$error="Quantity should be provided";
		}
		else if(empty($obj->reportedon)){
			$error="Date Reported should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
