<title>WiseDigits ERP: Rejects </title>
<?php 
include "../../../head.php";

?>
<script type="text/javascript">

$(document).ready(function() {
 $("#employeename").autocomplete("../../../modules/server/server/search.php?main=hrm&module=employees&field=concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname))", {
 	width: 260,
 	selectFirst: false
 });
 $("#employeename").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("employeename").value=data[0];
     document.getElementById("employeeid").value=data[1];
   }
 });
 try{
 $('#barcode').on('change',function(){ 
            
            var str=$(this).val();
	    var st = str.split("-");
	    
	    if(st.length<2){
	      alert("Scan the bar code first");
	      
	    }else{	    
	      
	        document.getElementById("plantingdetailid").value=1;
		document.getElementById("employeeid").value=st[0];
		document.getElementById("employeename").value=st[0];
		document.getElementById("varietyid").value=st[2];
		document.getElementById("greenhouseid").value=st[1];
		document.getElementById("plantingdetailid").value=1;
		document.getElementById("sizeid").value=st[3];
		document.getElementById("quantity").value=st[4];
		document.getElementById("harvestedon").value=st[5]+"-"+st[6]+"-"+st[7];
		
		$.get("../../post/graded/get.php",{id:parseInt(st[0])},function(data){
		  $("#employeename").val(data);      
		});
	      
	      //document.getElementById("barcode2").focus();
	    }
	    $("#barcode").val("");
	      $("#rejecttypeid").focus();
    });}catch(e){alert(e);}
 });
 
 function checkForm(form,event){
   
    var target = event.explicitOriginalTarget || event.relatedTarget ||
        document.activeElement || {};

    if(target.type=="text"){
    
      
	return false;
      }
      else{
	return true;
      }
     
 }
 
function placeCursorOnPageLoad()
{
	document.getElementById("barcode").focus();
		
}

womAdd('placeCursorOnPageLoad()');
womOn();
 </script>

<div class='main'>
<form class="forms" id="theform" action="addrejects_proc.php" name="rejects" method="POST" enctype="multipart/form-data" onsubmit="return checkForm(this, event);">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
	<tr>
		<td colspan="2"><input type="hidden" name="id" id="id" value="<?php echo $obj->id; ?>">
		<input type="hidden" name="reduce" id="reduce" value="<?php echo $obj->reduce; ?>"></td>
	</tr>
	
	<tr>
		<td align="right">Barcode : </td>
		<td><input type="text" name="barcode" id="barcode" value="<?php echo $obj->barcode; ?>"></td>
	</tr>
	
	
	<tr>
		<td align="right">Reject Type : </td>
			<td><select name="rejecttypeid" id="rejecttypeid" class="selectbox">
<option value="">Select...</option>
<?php
	$rejecttypes=new Rejecttypes();
	$where="  ";
	$fields="prod_rejecttypes.id, prod_rejecttypes.name, prod_rejecttypes.remarks, prod_rejecttypes.ipaddress, prod_rejecttypes.createdby, prod_rejecttypes.createdon, prod_rejecttypes.lasteditedby, prod_rejecttypes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$rejecttypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($rejecttypes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->rejecttypeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select><font color='red'>*</font>
		</td>
	</tr>
	
	<tr>
		<td align="right">Variety : </td>
			<td><select name="varietyid" id="varietyid" class="selectbox">
<option value="">Select...</option>
<?php
	$varietys=new Varietys();
	$where="  ";
	$fields="prod_varietys.id, prod_varietys.name, prod_varietys.typeid, prod_varietys.colourid, prod_varietys.duration, prod_varietys.quantity, prod_varietys.stems, prod_varietys.remarks, prod_varietys.ipaddress, prod_varietys.createdby, prod_varietys.createdon, prod_varietys.lasteditedby, prod_varietys.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$varietys->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($varietys->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->varietyid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select><font color='red'>*</font>
		</td>
	</tr>
	<tr>
		<td align="right">Length : </td>
			<td><select name="sizeid" id="sizeid" class="selectbox">
<option value="">Select...</option>
<?php
	$sizes=new Sizes();
	$where="  ";
	$fields="prod_sizes.id, prod_sizes.name, prod_sizes.remarks, prod_sizes.ipaddress, prod_sizes.createdby, prod_sizes.createdon, prod_sizes.lasteditedby, prod_sizes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$sizes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($sizes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->sizeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select><font color='red'>*</font>
		</td>
	</tr>
	<tr>
		<td align="right">Planting Detail : </td>
			<td><select name="plantingdetailid" id="plantingdetailid" class="selectbox">
<option value="">Select...</option>
<?php
	$plantingdetails=new Plantingdetails();
	$where="  ";
	$fields="prod_plantingdetails.id, prod_plantingdetails.plantingid, prod_plantingdetails.varietyid, prod_plantingdetails.areaid, prod_plantingdetails.quantity, prod_plantingdetails.memo, prod_plantingdetails.ipaddress, prod_plantingdetails.createdby, prod_plantingdetails.createdon, prod_plantingdetails.lasteditedby, prod_plantingdetails.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$plantingdetails->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($plantingdetails->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->plantingdetailid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select><font color='red'>*</font>
		</td>
	</tr>
	<tr>
		<td align="right">Green House : </td>
			<td><select name="greenhouseid" id="greenhouseid" class="selectbox">
<option value="">Select...</option>
<?php
	$greenhouses=new Greenhouses();
	$where="  ";
	$fields="prod_greenhouses.id, prod_greenhouses.name, prod_greenhouses.sectionid, prod_greenhouses.remarks, prod_greenhouses.ipaddress, prod_greenhouses.createdby, prod_greenhouses.createdon, prod_greenhouses.lasteditedby, prod_greenhouses.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$greenhouses->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($greenhouses->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->greenhouseid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">Quantity : </td>
		<td><input type="text" name="quantity" id="quantity" size="8"  value="<?php echo $obj->quantity; ?>"><font color='red'>*</font></td>
	</tr>
	<tr>
		<td align="right">Harvester : </td>
			<td><input type='text' size='40' name='employeename' id='employeename' value='<?php echo $obj->employeename; ?>'>
			<input type="hidden" name='employeeid' id='employeeid' value='<?php echo $obj->employeeid; ?>'>
		</td>
	</tr>
	<tr>
		<td align="right">Date Harvested : </td>
		<td><input type="text" name="harvestedon" id="harvestedon" class="date_input" size="12" readonly  value="<?php echo $obj->harvestedon; ?>"></td>
	</tr>
	<tr>
		<td align="right">Date Reported : </td>
		<td><input type="text" name="reportedon" id="reportedon" class="date_input" size="12" readonly  value="<?php echo $obj->reportedon; ?>"><font color='red'>*</font></td>
	</tr>
	<tr>
		<td align="right">Remarks : </td>
		<td><textarea name="remarks"><?php echo $obj->remarks; ?></textarea></td>
	</tr>
	<tr>
		<td align="right">Status : </td>
		<td><select name='status' class="selectbox">
			<option value='Local Market' <?php if($obj->status=='Local Market'){echo"selected";}?>>Local Market</option>
			<option value='Discarded' <?php if($obj->status=='Discarded'){echo"selected";}?>>Discarded</option>
		</select></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
include "../../../foot.php";
if(!empty($error)){
	showError($error);
}
?>