<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Varietys_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Varietys";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="8616";//View
$auth->levelid=$_SESSION['level'];

$ob = (object)$_GET;

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$varietys=new Varietys();
if(!empty($delid)){
	$varietys->id=$delid;
	$varietys->delete($varietys);
	redirect("varietys.php");
}
//Authorization.
$auth->roleid="8615";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth) and empty($ob->precool)){
?>
<div style="float:left;" class="buttons"> <input onclick="showPopWin('addvarietys_proc.php',600,430);" value="Add Varietys " type="button"/></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Variety </th>
			<th>Type </th>
			<th>Colour </th>
			<th>Expected Duration (Wks) </th>
			<th>Stems per Buckect</th>
			<?php if($ob->precool==1){ ?>
			<th>Quantity </th>
			<?php }?>
			<th>Remarks </th>
			<th>&nbsp;</th>
<?php
//Authorization.
$auth->roleid="8617";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth) and empty($ob->precool)){
?>
			<th>&nbsp;</th>
			<?php if($ob->precool==1){ ?>
			<th>&nbsp;</th>
			<?php } ?>
<?php
}
//Authorization.
$auth->roleid="8618";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth) and empty($ob->precool)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="prod_varietys.id, prod_varietys.name, prod_types.name as typeid, prod_colours.name as colourid, prod_varietys.duration, prod_varietys.quantity, prod_varietys.stems, prod_varietys.remarks, prod_varietys.ipaddress, prod_varietys.createdby, prod_varietys.createdon, prod_varietys.lasteditedby, prod_varietys.lasteditedon";
		$join=" left join prod_types on prod_varietys.typeid=prod_types.id  left join prod_colours on prod_varietys.colourid=prod_colours.id ";
		$having="";
		$groupby="";
		$orderby="";
		$varietys->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$varietys->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->name; ?></td>
			<td><?php echo $row->typeid; ?></td>
			<td><?php echo $row->colourid; ?></td>
			<td align="center"><?php echo $row->duration; ?></td>
			<td align="center"><?php echo $row->stems; ?></td>
			<?php if($ob->precool==1){ ?>
			<td align="center"><?php echo $row->quantity; ?></td>
			<?php }?>
			<td><?php echo $row->remarks; ?></td>
<?php
//Authorization.
$auth->roleid="8617";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth) and empty($ob->precool)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addvarietys_proc.php?id=<?php echo $row->id; ?>',600,430);">View</a></td>
			<?php if($ob->precool==1){ ?>
			<td><a href="javascript:;" onclick="showPopWin('../varietystocks/varietystocks.php?id=<?php echo $row->id; ?>',1020,600);">Stock Card</a></td>
			<?php }?>
<?php
}
//Authorization.
$auth->roleid="8618";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth) and empty($ob->precool)){
?>
			<td><a href='varietys.php?delid=<?php echo $row->id; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
