<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Varietystocks_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Varietystocks";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="8688";//Add
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../rptheader.php";

$delid=$_GET['delid'];
$id=$_GET['id'];
$varietystocks=new Varietystocks();
if(!empty($delid)){
	$varietystocks->id=$delid;
	$varietystocks->delete($varietystocks);
	redirect("varietystocks.php");
}
//Authorization.
$auth->roleid="8687";//View
$auth->levelid=$_SESSION['level'];

?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

	//TableToolsInit.sSwfPath = "../../../media/swf/ZeroClipboard.swf";
TableTools.DEFAULTS.aButtons = [ "copy", "csv", "xls","pdf" ];
 	$('#example').dataTable( {
		"sDom": 'T<"H"lfr>t<"F"ip>',
		"oTableTools": {
			"sSwfPath": "../../../media/swf/copy_cvs_xls_pdf.swf"
		},
		"bJQueryUI": true,
		"iDisplayLength":20,
		"sPaginationType": "full_numbers"
	} );
} );
</script> 
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Document No </th>
			<th>Variety </th>
			<th>Length </th>
			<th>Area </th>
			<th>Action </th>
			<th>Quantity </th>
			<th>Remain </th>
			<th>Date Recorded </th>
			<th>Date Of Action </th>
			<th>User</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields="prod_varietystocks.id, prod_varietystocks.documentno, prod_varietys.name as varietyid, prod_sizes.name sizeid, prod_areas.name as areaid, prod_varietystocks.transaction, prod_varietystocks.quantity, prod_varietystocks.remain, prod_varietystocks.recordedon, prod_varietystocks.actedon, prod_varietystocks.ipaddress, auth_users.username createdby, prod_varietystocks.createdon, prod_varietystocks.lasteditedby, prod_varietystocks.lasteditedon";
		$join=" left join prod_varietys on prod_varietystocks.varietyid=prod_varietys.id  left join prod_areas on prod_varietystocks.areaid=prod_areas.id left join prod_sizes on prod_sizes.id=prod_varietystocks.sizeid left join auth_users on auth_users.id=prod_varietystocks.createdby ";
		$having="";
		$groupby="";
		$orderby="";
		if(!empty($id)){
		  $where=" where prod_varietys.id='$id' ";
		}
		$varietystocks->retrieve($fields,$join,$where,$having,$groupby,$orderby);echo mysql_error();
		$res=$varietystocks->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->documentno; ?></td>
			<td><?php echo $row->varietyid; ?></td>
			<td><?php echo $row->sizeid; ?></td>
			<td><?php echo $row->areaid; ?></td>
			<td><?php echo $row->transaction; ?></td>
			<td><?php echo formatNumber($row->quantity); ?></td>
			<td><?php echo formatNumber($row->remain); ?></td>
			<td><?php echo formatDate($row->recordedon); ?></td>
			<td><?php echo formatDate($row->createdon); ?></td>
			<td><?php echo $row->createdby; ?></td>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
