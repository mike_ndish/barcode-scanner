<?php 
require_once("AcctypesDBO.php");
class Acctypes
{				
	var $id;			
	var $name;			
	var $balance;			
	var $acctypesDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		$this->name=str_replace("'","\'",$obj->name);
		$this->balance=str_replace("'","\'",$obj->balance);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get name
	function getName(){
		return $this->name;
	}
	//set name
	function setName($name){
		$this->name=$name;
	}

	//get balance
	function getBalance(){
		return $this->balance;
	}
	//set balance
	function setBalance($balance){
		$this->balance=$balance;
	}

	function add($obj){
		$acctypesDBO = new AcctypesDBO();
		if($acctypesDBO->persist($obj)){
			$this->id=$acctypesDBO->id;
			$this->sql=$acctypesDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$acctypesDBO = new AcctypesDBO();
		if($acctypesDBO->update($obj,$where)){
			$this->sql=$acctypesDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$acctypesDBO = new AcctypesDBO();
		if($acctypesDBO->delete($obj,$where=""))		
			$this->sql=$acctypesDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$acctypesDBO = new AcctypesDBO();
		$this->table=$acctypesDBO->table;
		$acctypesDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$acctypesDBO->sql;
		$this->result=$acctypesDBO->result;
		$this->fetchObject=$acctypesDBO->fetchObject;
		$this->affectedRows=$acctypesDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->balance)){
			$error="Balance Type? should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
