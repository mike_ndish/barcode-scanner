<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Acctypes_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="110";//<img src="../edit.png" alt="edit" title="edit" />
}
else{
	$auth->roleid="108";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
	
	
if($obj->action=="Save"){
	$acctypes=new Acctypes();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$error=$acctypes->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$acctypes=$acctypes->setObject($obj);
		if($acctypes->add($acctypes)){
			$error=SUCCESS;
			redirect("addacctypes_proc.php?id=".$acctypes->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$acctypes=new Acctypes();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$acctypes->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$acctypes=$acctypes->setObject($obj);
		if($acctypes->edit($acctypes)){
			$error=UPDATESUCCESS;
			redirect("addacctypes_proc.php?id=".$acctypes->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if(empty($obj->action)){
}

if(!empty($id)){
	$acctypes=new Acctypes();
	$where=" where id=$id ";
	$fields="sys_acctypes.id, sys_acctypes.name, sys_acctypes.balance";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$acctypes->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$acctypes->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Acctypes ";
include "addacctypes.php";
?>