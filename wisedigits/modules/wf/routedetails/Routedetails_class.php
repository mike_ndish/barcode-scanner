<?php 
require_once("RoutedetailsDBO.php");
class Routedetails
{				
	var $id;			
	var $routeid;			
	var $assignmentid;			
	var $systemtaskid;			
	var $follows;			
	var $expectedduration;			
	var $durationtype;			
	var $remarks;			
	var $ipaddress;			
	var $createdby;			
	var $createdon;			
	var $lasteditedby;			
	var $lasteditedon;			
	var $routedetailsDBO;
	var $fetchObject;
	var $sql;
	var $result;
	var $table;
	var $affectedRows;

	function setObject($obj){
		$this->id=str_replace("'","\'",$obj->id);
		if(empty($obj->routeid))
			$obj->routeid='NULL';
		$this->routeid=$obj->routeid;
		if(empty($obj->assignmentid))
			$obj->assignmentid='NULL';
		$this->assignmentid=$obj->assignmentid;
		if(empty($obj->systemtaskid))
			$obj->systemtaskid='NULL';
		$this->systemtaskid=$obj->systemtaskid;
		$this->follows=str_replace("'","\'",$obj->follows);
		$this->expectedduration=str_replace("'","\'",$obj->expectedduration);
		$this->durationtype=str_replace("'","\'",$obj->durationtype);
		$this->remarks=str_replace("'","\'",$obj->remarks);
		$this->ipaddress=str_replace("'","\'",$obj->ipaddress);
		$this->createdby=str_replace("'","\'",$obj->createdby);
		$this->createdon=str_replace("'","\'",$obj->createdon);
		$this->lasteditedby=str_replace("'","\'",$obj->lasteditedby);
		$this->lasteditedon=str_replace("'","\'",$obj->lasteditedon);
		return $this;
	
	}
	//get id
	function getId(){
		return $this->id;
	}
	//set id
	function setId($id){
		$this->id=$id;
	}

	//get routeid
	function getRouteid(){
		return $this->routeid;
	}
	//set routeid
	function setRouteid($routeid){
		$this->routeid=$routeid;
	}

	//get assignmentid
	function getAssignmentid(){
		return $this->assignmentid;
	}
	//set assignmentid
	function setAssignmentid($assignmentid){
		$this->assignmentid=$assignmentid;
	}

	//get systemtaskid
	function getSystemtaskid(){
		return $this->systemtaskid;
	}
	//set systemtaskid
	function setSystemtaskid($systemtaskid){
		$this->systemtaskid=$systemtaskid;
	}

	//get follows
	function getFollows(){
		return $this->follows;
	}
	//set follows
	function setFollows($follows){
		$this->follows=$follows;
	}

	//get expectedduration
	function getExpectedduration(){
		return $this->expectedduration;
	}
	//set expectedduration
	function setExpectedduration($expectedduration){
		$this->expectedduration=$expectedduration;
	}

	//get durationtype
	function getDurationtype(){
		return $this->durationtype;
	}
	//set durationtype
	function setDurationtype($durationtype){
		$this->durationtype=$durationtype;
	}

	//get remarks
	function getRemarks(){
		return $this->remarks;
	}
	//set remarks
	function setRemarks($remarks){
		$this->remarks=$remarks;
	}

	//get ipaddress
	function getIpaddress(){
		return $this->ipaddress;
	}
	//set ipaddress
	function setIpaddress($ipaddress){
		$this->ipaddress=$ipaddress;
	}

	//get createdby
	function getCreatedby(){
		return $this->createdby;
	}
	//set createdby
	function setCreatedby($createdby){
		$this->createdby=$createdby;
	}

	//get createdon
	function getCreatedon(){
		return $this->createdon;
	}
	//set createdon
	function setCreatedon($createdon){
		$this->createdon=$createdon;
	}

	//get lasteditedby
	function getLasteditedby(){
		return $this->lasteditedby;
	}
	//set lasteditedby
	function setLasteditedby($lasteditedby){
		$this->lasteditedby=$lasteditedby;
	}

	//get lasteditedon
	function getLasteditedon(){
		return $this->lasteditedon;
	}
	//set lasteditedon
	function setLasteditedon($lasteditedon){
		$this->lasteditedon=$lasteditedon;
	}

	function add($obj){
				
		$routedetail = new Routedetails();
		$fields="*";
		$where=" where follows='$obj->follows' ";
		$having="";
		$orderby=" order by follows ";
		$groupby="";
		$join="";
		$routedetail->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$routedetail = $routedetail->fetchObject;		
		  
		$routedetailsDBO = new RoutedetailsDBO();
		if($routedetailsDBO->persist($obj)){
			$routedetail->follows=$routedetailsDBO->id;
			$rt = new Routedetails();
			$rt = $rt->setObject($routedetail);
			
			$rt->edit($routedetail);
			
			$this->id=$routedetailsDBO->id;
			$this->sql=$routedetailsDBO->sql;
			return true;	
		}
	}			
	function edit($obj,$where=""){
		$routedetail = new Routedetails();
		$fields="*";
		$where=" where follows='$obj->follows' ";
		$having="";
		$orderby=" order by follows ";
		$groupby="";
		$join="";
		$routedetail->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$routedetail = $routedetail->fetchObject;
		
		$routedetailsDBO = new RoutedetailsDBO();
		if($routedetailsDBO->update($obj,$where)){
		
			$routedetail->follows=$routedetailsDBO->id;
			$rt = new Routedetails();
			$rt = $rt->setObject($routedetail);
			
			$rt->edit($routedetail);
			
			$this->sql=$routedetailsDBO->sql;
		}
			return true;	
	}			
	function delete($obj,$where=""){			
		$routedetailsDBO = new RoutedetailsDBO();
		if($routedetailsDBO->delete($obj,$where=""))		
			$this->sql=$routedetailsDBO->sql;
			return true;	
	}			
	function retrieve($fields,$join,$where,$having,$groupby,$orderby){			
		$routedetailsDBO = new RoutedetailsDBO();
		$this->table=$routedetailsDBO->table;
		$routedetailsDBO->retrieve($fields,$join,$where,$having,$groupby,$orderby);		
		$this->sql=$routedetailsDBO->sql;
		$this->result=$routedetailsDBO->result;
		$this->fetchObject=$routedetailsDBO->fetchObject;
		$this->affectedRows=$routedetailsDBO->affectedRows;
	}			
	function validate($obj){
		if(empty($obj->routeid)){
			$error="Route should be provided";
		}
	
		if(!empty($error))
			return $error;
		else
			return null;
	
	}

	function validates($obj){
	
			return null;
	
	}
}				
?>
