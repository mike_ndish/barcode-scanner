<title>WiseDigits: Routedetails </title>
<?php 
include "../../../headerpop.php";

?>
 <script type="text/javascript" charset="utf-8">
 $(document).ready(function() {
 	$('#tbl').dataTable( {
 		"sScrollY": 180,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers"
 	} );
 } );
 </script>

<div class='main'>
<form class="forms" id="theform" action="addroutedetails_proc.php" name="routedetails" method="POST" enctype="multipart/form-data">
	<table width="100%" class="titems gridd" border="0" align="center" cellpadding="2" cellspacing="0" id="tblSample">
 <?php if(!empty($obj->retrieve)){?>
	<tr>
		<td colspan="4" align="center"><input type="hidden" name="retrieve" value="<?php echo $obj->retrieve; ?>"/>Document No:<input type="text" size="4" name="invoiceno"/>&nbsp;<input type="submit" name="action" value="Filter"/></td>
	</tr>
	<?php }?>
	<tr>
		<td colspan="2"><input type="hidden" name="id" id="id" value="<?php echo $obj->id; ?>"></td>
	</tr>
	<tr>
		<td align="right">Route : </td>
			<td><select name="routeid" class="selectbox">
<option value="">Select...</option>
<?php
	$routes=new Routes();
	$where="  ";
	$fields="wf_routes.id, wf_routes.name, wf_routes.moduleid, wf_routes.remarks, wf_routes.ipaddress, wf_routes.createdby, wf_routes.createdon, wf_routes.lasteditedby, wf_routes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$routes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($routes->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->routeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select><font color='red'>*</font>
		</td>
	</tr>
	<tr>
		<td align="right">Assignment : </td>
			<td><select name="assignmentid" class="selectbox">
<option value="">Select...</option>
<?php
	$assignments=new Assignments();
	$where="  ";
	$fields="hrm_assignments.id, hrm_assignments.code, hrm_assignments.name, hrm_assignments.departmentid, hrm_assignments.levelid, hrm_assignments.remarks, hrm_assignments.createdby, hrm_assignments.createdon, hrm_assignments.lasteditedby, hrm_assignments.lasteditedon, hrm_assignments.ipaddress";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$assignments->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($assignments->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->assignmentid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">System Task : </td>
			<td><select name="systemtaskid" class="selectbox">
<option value="">Select...</option>
<?php
	$systemtasks=new Systemtasks();
	$where="  ";
	$fields="wf_systemtasks.id, wf_systemtasks.name, wf_systemtasks.action, wf_systemtasks.remarks, wf_systemtasks.ipaddress, wf_systemtasks.createdby, wf_systemtasks.createdon, wf_systemtasks.lasteditedby, wf_systemtasks.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$systemtasks->retrieve($fields,$join,$where,$having,$groupby,$orderby);

	while($rw=mysql_fetch_object($systemtasks->result)){
	?>
		<option value="<?php echo $rw->id; ?>" <?php if($obj->systemtaskid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
	<?php
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td align="right">Comes After : </td>
		<td>
		<select name="follows" class="selectbox">
		<option value="">Select...</option>
		<?php
		
		$routedetails = new Routedetails();
		$fields="wf_routedetails.id, hrm_assignments.name assignmentid";
		$join="left join hrm_assignments on hrm_assignments.id=wf_routedetails.assignmentid ";
		$orderby = " order by wf_routedetails.follows ";
		$having="";
		$groupby="";
		$where=" where wf_routedetails.routeid='$obj->routeid' ";
		$routedetails->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		
		$i=0;
		while($rw=mysql_fetch_object($routedetails->result)){$i++;
		?>
			<option value="<?php echo $rw->id; ?>" <?php if($obj->follows==$rw->id){echo "selected";}?>><?php echo $i; ?>: &nbsp; <?php echo initialCap($rw->assignmentid);?></option>
		<?php
		}
		?>
		</select>
		
		</td>
	</tr>
	<tr>
		<td align="right">Expected Stay : </td>
		<td><input type="text" name="expectedduration" id="expectedduration" size="8"  value="<?php echo $obj->expectedduration; ?>"></td>
	</tr>
	<tr>
		<td align="right">Duration Type : </td>
		<td><select name='durationtype' class="selectbox">
			<option value='Hrs' <?php if($obj->durationtype=='Hrs'){echo"selected";}?>>Hrs</option>
			<option value='Days' <?php if($obj->durationtype=='Days'){echo"selected";}?>>Days</option>
			<option value='Weeks' <?php if($obj->durationtype=='Weeks'){echo"selected";}?>>Weeks</option>
			<option value='Months' <?php if($obj->durationtype=='Months'){echo"selected";}?>>Months</option>
		</select></td>
	</tr>
	<tr>
		<td align="right">Remarks : </td>
		<td><textarea name="remarks"><?php echo $obj->remarks; ?></textarea></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="action" id="action" value="<?php echo $obj->action; ?>">&nbsp;<input type="submit" name="action" id="action" value="Cancel" onclick="window.top.hidePopWin(true);"/></td>
	</tr>
<?php if(!empty($obj->id)){?>
<?php }?>
	<?php if(!empty($obj->id)){?> 
<?php }?>
</table>
</form>
<?php 
if(!empty($error)){
	showError($error);
}
?>