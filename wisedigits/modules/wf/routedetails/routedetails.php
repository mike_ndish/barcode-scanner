<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Routedetails_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

$page_title="Routedetails";
//connect to db
$db=new DB();
//Authorization.
$auth->roleid="7451";//View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../head.php";

$delid=$_GET['delid'];
$id=$_GET['id'];
$routedetails=new Routedetails();
if(!empty($delid)){
	$routedetails->id=$delid;
	$routedetails->delete($routedetails);
	redirect("routedetails.php?id=".$id);
}
//Authorization.
$auth->roleid="7450";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
<div style="float:left;" class="buttons"> <input onclick="showPopWin('addroutedetails_proc.php?routeid=<?php echo $id; ?>',600,430);" value="Add Routedetails " type="button"/></div>
<?php }?>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<th>Route </th>
			<th>Assignment </th>
			<th>System Task </th>
			<th>Comes After </th>
			<th>Expected Stay </th>
			<th>Remarks </th>
<?php
//Authorization.
$auth->roleid="7452";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php
}
//Authorization.
$auth->roleid="7453";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<th>&nbsp;</th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$fields=" wf_routedetails.routeid, hrm_assignments.name ";
		$groupby="";
		$join=" left join hrm_assignments on hrm_assignments.id=wf_routedetails.assignmentid ";
		$where=" where wf_routedetails.routeid='$id' ";
		$having="";
		$orderedby="";
		$routedetails->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$num = $routedetails->affectedRows;


		$i=0;
		while($i<$num){
		  $routedetails = new Routedetails();
		  $fields=" wf_routedetails.id, wf_routes.name routeid, hrm_assignments.name assignmentid ";
		  $groupby="";
		  $join=" left join hrm_assignments on hrm_assignments.id=wf_routedetails.assignmentid left join wf_routes on wf_routes.id=wf_routedetails.routeid ";
		  $where=" where wf_routedetails.routeid='$id' and wf_routedetails.follows='$follows' ";
		  $having="";
		  $orderedby="";
		  $routedetails->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		  $row=$routedetails->fetchObject;
		  
		  $i++;
		  $follows=$row->id;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row->routeid; ?></td>
			<td><?php echo $row->assignmentid; ?></td>
			<td><?php echo $row->systemtaskid; ?></td>
			<td><?php echo $row->follows; ?></td>
			<td><?php echo $row->expectedduration; ?>&nbsp;<?php echo $row->durationtype; ?></td>
			<td><?php echo $row->remarks; ?></td>
<?php
//Authorization.
$auth->roleid="7452";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href="javascript:;" onclick="showPopWin('addroutedetails_proc.php?id=<?php echo $row->id; ?>',600,430);">View</a></td>
<?php
}
//Authorization.
$auth->roleid="7453";//View
$auth->levelid=$_SESSION['level'];

if(existsRule($auth)){
?>
			<td><a href='routedetails.php?delid=<?php echo $row->id; ?>&id=<?php echo $row->route; ?>' onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
<?php
include"../../../foot.php";
?>
