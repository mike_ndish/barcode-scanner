<?php 
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("Routes_class.php");
require_once("../../auth/rules/Rules_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../auth/users/login.php");
}

require_once("../../sys/modules/Modules_class.php");
//Authorization.
if(!empty($_GET['id'])){
	$auth->roleid="7456";//Edit
}
else{
	$auth->roleid="7454";//Add
}
$auth->levelid=$_SESSION['level'];
auth($auth);


//connect to db
$db=new DB();
$obj=(object)$_POST;
$ob=(object)$_GET;

$mode=$_GET['mode'];
if(!empty($mode)){
	$obj->mode=$mode;
}
$id=$_GET['id'];
$error=$_GET['error'];
if(!empty($_GET['retrieve'])){
	$obj->retrieve=$_GET['retrieve'];
}
	
	
if($obj->action=="Save"){
	$routes=new Routes();
	$obj->createdby=$_SESSION['userid'];
	$obj->createdon=date("Y-m-d H:i:s");
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");
	$obj->ipaddress=$_SERVER['REMOTE_ADDR'];
	$error=$routes->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$routes=$routes->setObject($obj);
		if($routes->add($routes)){
			$error=SUCCESS;
			redirect("addroutes_proc.php?id=".$routes->id."&error=".$error);
		}
		else{
			$error=FAILURE;
		}
	}
}
	
if($obj->action=="Update"){
	$routes=new Routes();
	$obj->lasteditedby=$_SESSION['userid'];
	$obj->lasteditedon=date("Y-m-d H:i:s");

	$error=$routes->validate($obj);
	if(!empty($error)){
		$error=$error;
	}
	else{
		$routes=$routes->setObject($obj);
		if($routes->edit($routes)){
			$error=UPDATESUCCESS;
			redirect("addroutes_proc.php?id=".$routes->id."&error=".$error);
		}
		else{
			$error=UPDATEFAILURE;
		}
	}
}
if(empty($obj->action)){

	$modules= new Modules();
	$fields="sys_modules.id, sys_modules.name";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$modules->retrieve($fields,$join,$where,$having,$groupby,$orderby);

}

if(!empty($id)){
	$routes=new Routes();
	$where=" where id=$id ";
	$fields="wf_routes.id, wf_routes.name, wf_routes.moduleid, wf_routes.remarks, wf_routes.ipaddress, wf_routes.createdby, wf_routes.createdon, wf_routes.lasteditedby, wf_routes.lasteditedon";
	$join="";
	$having="";
	$groupby="";
	$orderby="";
	$routes->retrieve($fields,$join,$where,$having,$groupby,$orderby);
	$obj=$routes->fetchObject;

	//for autocompletes
}
if(empty($id) and empty($obj->action)){
	if(empty($_GET['edit'])){
		$obj->action="Save";
	}
	else{
		$obj=$_SESSION['obj'];
	}
}	
elseif(!empty($id) and empty($obj->action)){
	$obj->action="Update";
}
	
	
$page_title="Routes ";
include "addroutes.php";
?>