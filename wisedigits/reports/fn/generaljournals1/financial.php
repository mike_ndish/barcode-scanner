<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("../../../modules/fn/generaljournals/Generaljournals_class.php");

$page_title = 'Finnacial Statement';

include "../../../head.php";

$generaljournals = new Generaljournals();
$obj=(object)$_POST;
if(empty($obj->action))
{
	$obj->fromdate=date('Y-m-d',mktime(0,0,0,date("m")-1,date("d"),date("Y")));
	$obj->todate=date('Y-m-d',mktime(0,0,0,date("m"),date("d"),date("Y")));
}
?>


   <table width="98%" border="0" align="center" class="tgrid display" id="example">
            <thead> <tr>
                <td colspan="5"><div align="center">Balance Sheet as at <?php echo formatDate($obj->todate);?></div></td>
                </tr>
                  <tr class="special-row1"><th align="left"></th>
                   	<th align="left">&nbsp;Ksh</th>
                	<th align="left">&nbsp;Ksh</th>
                    <th align="left">&nbsp;Ksh</th>
                    <th align="left">&nbsp;Ksh</th>	
               </tr>
                </thead>
              <tbody>
              
              <tr>
                <td ><strong>ASSETS</strong></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
                  
              <tr>
                <td ><strong>NON CURRENT ASSETS</strong></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              
              <?php 
              $where="(7)";
              $row=getBalanceForBS($where,1);
              $fixedassets=$row->value;
              ?>
               <tr>
                <td >Property, Plant & Equipment</td>
                <td >&nbsp;</td>
                <td align="right" ><?php echo formatNumber($row->value);?></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              
              <tr>
              	<td colspan="5">&nbsp;</td>
              </tr>
            
                  <tr>
                    <td ><strong>CURRENT ASSETS</strong></td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                  </tr>
                  
                   <?php 
              $where="(11,12,13,29)";
              $row=getBalanceForBS($where,1);
              $currentassets=$row->value;
              ?>
                  <tr>
                <td >Trade & Other Receivables</td>
                <td >&nbsp;</td>
                 <td align="right"><?php echo formatNumber($row->value);?></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              
              <?php 
              $where="(34)";
              $row=getBalanceForBS($where,1);
              $currentassets+=$row->value;
              ?>
              <tr>
                <td >Inventories</td>
                <td >&nbsp;</td>
                <td align="right"><?php echo formatNumber($row->value);?></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              
              <?php 
              $where="(8,24)";
              $row=getBalanceForBS($where,1);
              $currentassets+=$row->value;
              ?>
              <tr>
                <td >Cash and Bank Balances</td>
                <td >&nbsp;</td>
                <td align="right"><?php echo formatNumber($row->value);?></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
                                    
                  <tr>
                <td ><strong></strong></td>
                <td >&nbsp;</td>
                <td align="right" ><?php echo formatNumber($currentassets); ?></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              <?php 
              $totalassets=$currentassets+$fixedassets;
              ?>
             <tr>
                <td ><strong>TOTAL ASSETS</strong></td>
                <td >&nbsp;</td>
                <td align="right" ><?php echo formatNumber($totalassets); ?></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              
              <tr>
              	<td colspan="5">&nbsp;</td>
              </tr>
              
              <tr>
                <td ><strong>EQUITY</strong></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              
              <?php 
              $where="(35)";
              $row=getBalanceForBS($where,2);
              ?>
                <tr>
                <td >Issued Share Capital</td>
                <td >&nbsp;</td>
                <td align="right"><?php echo formatNumber($row->value);?></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              
              <?php 
              $where="(36)";
              $row=getBalanceForBS($where,2);
              ?>
                <tr>
                <td >Capital Fund</td>
                <td >&nbsp;</td>
                <td align="right"><?php echo formatNumber($row->value);?></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              
               <?php 
              $where="(37)";
              $row=getBalanceForBS($where,1);
              ?>
                <tr>
                <td >Revenue Reserves</td>
                <td >&nbsp;</td>
               <td align="right"><?php echo formatNumber($row->value);?></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              
              <tr>
              	<td colspan="5">&nbsp;</td>
              </tr>
              <tr>
              	<td colspan="5">&nbsp;</td>
              </tr>
              
              <tr>
                    <td ><strong>CURRENT LIABILITIES</strong></td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                  </tr>
                  
                  <?php 
              $where="(9,14,15,16,30)";
              $row=getBalanceForBS($where,2);
              ?>
                   <tr>
                <td >Trade and Other Payables</td>
                <td >&nbsp;</td>
                <td align="right"><?php echo formatNumber($row->value);?></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
             
              </tbody>        
            </table>
  
