<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("../../../modules/fn/inctransactions/Inctransactions_class.php");
require_once("../../../modules/auth/users/Users_class.php");
require_once("../../../modules/fn/incomes/Incomes_class.php");
require_once("../../../modules/sys/paymentmodes/Paymentmodes_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../../modules/auth/users/login.php");
}

$page_title="Inctransactions";
//connect to db
$db=new DB();

$obj=(object)$_POST;

include"../../../rptheader.php";

//processing filters
$rptwhere='';
$track=0;
$fds='';
$fd='';
if(!empty($obj->incomeid)){
	if($track>0)
		$rptwhere.="and";
	else
		$rptwhere.="where";

		$rptwhere.=" fn_inctransactions.incomeid='$obj->incomeid'";
	$track++;
}

if(!empty($obj->paymentmodeid )){
	if($track>0)
		$rptwhere.="and";
	else
		$rptwhere.="where";

		$rptwhere.=" fn_inctransactions.paymentmodeid ='$obj->paymentmodeid '";
	$track++;
}

if(!empty($obj->amount)){
	if($track>0)
		$rptwhere.="and";
	else
		$rptwhere.="where";

		$rptwhere.=" fn_inctransactions.amount='$obj->amount'";
	$track++;
}

if(!empty($obj->chequeno)){
	if($track>0)
		$rptwhere.="and";
	else
		$rptwhere.="where";

		$rptwhere.=" fn_inctransactions.chequeno='$obj->chequeno'";
	$track++;
}

if(!empty($obj->fromincomedate)){
	if($track>0)
		$rptwhere.="and";
	else
		$rptwhere.="where";

		$rptwhere.=" fn_inctransactions.incomedate>='$obj->fromincomedate'";
	$track++;
}

if(!empty($obj->toincomedate)){
	if($track>0)
		$rptwhere.="and";
	else
		$rptwhere.="where";

		$rptwhere.=" fn_inctransactions.incomedate<='$obj->toincomedate'";
	$track++;
}

if(!empty($obj->drawer)){
	if($track>0)
		$rptwhere.="and";
	else
		$rptwhere.="where";

		$rptwhere.=" fn_inctransactions.drawer='$obj->drawer'";
	$track++;
}

if(!empty($obj->documentno)){
	if($track>0)
		$rptwhere.="and";
	else
		$rptwhere.="where";

		$rptwhere.=" fn_inctransactions.documentno='$obj->documentno'";
	$track++;
}

//Processing Groupings
;$rptgroup='';
$track=0;
if(!empty($obj->grincomeid)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" incomeid ";
	$obj->shincomeid=1;
	$track++;
}

if(!empty($obj->grpaymentmodeid)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" paymentmodeid ";
	$obj->shpaymentmodeid=1;
	$track++;
}

if(!empty($obj->grchequeno)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" chequeno ";
	$obj->shchequeno=1;
	$track++;
}

if(!empty($obj->grjvno)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" jvno ";
	$obj->shjvno=1;
	$track++;
}

//Default shows
?>
<title><?php echo $page_title; ?></title>
<div id="main">
<div id="main-inner">
<div id="content">
<div id="content-inner">
<div id="content-header">
	<div class="page-title"><?php echo $page_title; ?></div>
	<div class="clearb"></div>
</div>
<div id="content-flex">
<div class="buttons"><a class="positive" href="javascript: expandCollapse('boxB','over');" style="vertical-align:text-top;">Open Popup To Filter</a></div>
<div id="boxB" class="sh" style="left: 10px; top: 63px; display: none; z-index: 500;">
<div id="box2"><div class="bar2" onmousedown="dragStart(event, 'boxB')"><span><strong>Choose Criteria</strong></span>
<a href="#" onclick="expandCollapse('boxB','over')">Close</a></div>
<form  action="inctransactions.php" method="post" name="inctransactions">
<table width="100%" border="0" align="center">
	<tr>
		<td width="50%" rowspan="2">
		<table class="tgrid gridd" border="0" align="right">
			<tr>
				<td>Income</td>
				<td>
				<select name='incomeid'>
				<option value="">Select...</option>
				<?php
				$incomes=new Incomes();
				$where="  ";
				$fields="fn_incomes.id, fn_incomes.name, fn_incomes.code, fn_incomes.remarks, fn_incomes.createdby, fn_incomes.createdon, fn_incomes.lasteditedby, fn_incomes.lasteditedon";
				$join="";
				$having="";
				$groupby="";
				$orderby="";
				$incomes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

				while($rw=mysql_fetch_object($incomes->result)){
				?>
					<option value="<?php echo $rw->id; ?>" <?php if($obj->incomeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
				<?php
				}
				?>
				</select>
</td>
			</tr>
			<tr>
				<td>Mode Of Payment</td>
				<td>
				<select name='paymentmodeid '>
				<option value="">Select...</option>
				<?php
				$paymentmodes=new Paymentmodes();
				$where="  ";
				$fields="sys_paymentmodes.id, sys_paymentmodes.name, sys_paymentmodes.remarks";
				$join="";
				$having="";
				$groupby="";
				$orderby="";
				$paymentmodes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

				while($rw=mysql_fetch_object($paymentmodes->result)){
				?>
					<option value="<?php echo $rw->id; ?>" <?php if($obj->paymentmodeid ==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
				<?php
				}
				?>
				</select>
</td>
			</tr>
			<tr>
				<td>Amount</td>
				<td><strong>From:</strong><input type='text' id='fromamount' size='from6' name='fromamount' value='<?php echo $obj->fromamount;?>'/>
								<br/><strong>To:</strong><input type='text' id='toamount' size='to6' name='toamount' value='<?php echo $obj->toamount;?>'></td>
			</tr>
			<tr>
				<td>Cheque No.</td>
				<td><input type='text' id='chequeno' size='6' name='chequeno' value='<?php echo $obj->chequeno;?>'></td>
			</tr>
			<tr>
				<td>Income Date</td>
				<td><strong>From:</strong><input type='text' id='fromincomedate' size='6' name='fromincomedate' readonly class="date_input" value='<?php echo $obj->fromincomedate;?>'/>
							<br/><strong>To:</strong><input type='text' id='toincomedate' size='6' name='toincomedate' readonly class="date_input" value='<?php echo $obj->toincomedate;?>'/></td>
			</tr>
			<tr>
				<td>Drawer</td>
				<td><input type='text' id='drawer' size='6' name='drawer' value='<?php echo $obj->drawer;?>'></td>
			</tr>
			<tr>
				<td>Document No.</td>
				<td><input type='text' id='documentno' size='6' name='documentno' value='<?php echo $obj->documentno;?>'></td>
			</tr>
		</table>
		</td>
		<td>
		<table class="tgrid gridd" width="100%" border="0" align="left">
			<tr>
			<th colspan="2"><div align="left"><strong>Group By (For Summarised Reports)</strong>: </div></th>
			</tr>
			<tr>
				<td><input type='checkbox' name='grincomeid' value='1' <?php if(isset($_POST['grincomeid']) ){echo"checked";}?>>&nbsp;Income</td>
				<td><input type='checkbox' name='grpaymentmodeid' value='1' <?php if(isset($_POST['grpaymentmodeid']) ){echo"checked";}?>>&nbsp;Mode Of Payment</td>
			<tr>
				<td><input type='checkbox' name='grchequeno' value='1' <?php if(isset($_POST['grchequeno']) ){echo"checked";}?>>&nbsp;Cheque No.</td>
				<td><input type='checkbox' name='grjvno' value='1' <?php if(isset($_POST['grjvno']) ){echo"checked";}?>>&nbsp;JV No.</td>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<table class="tgrid gridd" width="100%" border="0" align="left">
			<tr>
				<th colspan="3"><div align="left"><strong>Fields to Show (For Detailed Reports)</strong>: </div></th>
			</tr>
			<tr>
				<td><input type='checkbox' name='shincomeid' value='1' <?php if(isset($_POST['shincomeid'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Income</td>
				<td><input type='checkbox' name='shpaymentmodeid' value='1' <?php if(isset($_POST['shpaymentmodeid'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Mode Of Payment</td>
			<tr>
				<td><input type='checkbox' name='shamount' value='1' <?php if(isset($_POST['shamount'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Amount</td>
				<td><input type='checkbox' name='shbank' value='1' <?php if(isset($_POST['shbank'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Bank</td>
			<tr>
				<td><input type='checkbox' name='shchequeno' value='1' <?php if(isset($_POST['shchequeno']) ){echo"checked";}?>>&nbsp;Cheque No.</td>
				<td><input type='checkbox' name='shincomedate' value='1' <?php if(isset($_POST['shincomedate']) ){echo"checked";}?>>&nbsp;Income Date</td>
			<tr>
				<td><input type='checkbox' name='shremarks' value='1' <?php if(isset($_POST['shremarks'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Remarks</td>
				<td><input type='checkbox' name='shmemo' value='1' <?php if(isset($_POST['shmemo']) ){echo"checked";}?>>&nbsp;Memo</td>
			<tr>
				<td><input type='checkbox' name='shdrawer' value='1' <?php if(isset($_POST['shdrawer']) ){echo"checked";}?>>&nbsp;Drawer</td>
				<td><input type='checkbox' name='shjvno' value='1' <?php if(isset($_POST['shjvno']) ){echo"checked";}?>>&nbsp;JV No.</td>
			<tr>
				<td><input type='checkbox' name='shdocumentno' value='1' <?php if(isset($_POST['shdocumentno']) ){echo"checked";}?>>&nbsp;Document No.</td>
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align='center'><input type="submit" name="action" id="action" value="Filter" /></td>
	</tr>
</table>
</form>
</div>
</div>
<table style="clear:both;"  class="tgrid display" id="example" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<?php if($obj->shincomeid==1  or empty($obj->action)){ ?>
				<th>Income </th>
			<?php } ?>
			<?php if($obj->shpaymentmodeid==1  or empty($obj->action)){ ?>
				<th>Mode Of Payment </th>
			<?php } ?>
			<?php if($obj->shamount==1  or empty($obj->action)){ ?>
				<th>Amount </th>
			<?php } ?>
			<?php if($obj->shbank==1  or empty($obj->action)){ ?>
				<th>Bank </th>
			<?php } ?>
			<?php if($obj->shchequeno==1 ){ ?>
				<th>Cheque No. </th>
			<?php } ?>
			<?php if($obj->shincomedate==1 ){ ?>
				<th>Income Date </th>
			<?php } ?>
			<?php if($obj->shremarks==1  or empty($obj->action)){ ?>
				<th>Remarks </th>
			<?php } ?>
			<?php if($obj->shmemo==1 ){ ?>
				<th>Memo </th>
			<?php } ?>
			<?php if($obj->shdrawer==1 ){ ?>
				<th>Drawer </th>
			<?php } ?>
			<?php if($obj->shjvno==1 ){ ?>
				<th>JV No. </th>
			<?php } ?>
			<?php if($obj->shdocumentno==1 ){ ?>
				<th>Document No. </th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php
		$i=0;
		$inctransactions=new Inctransactions();
		$fields="fn_inctransactions.id, fn_incomes.name as incomeid, fn_inctransactions.amount, sys_paymentmodes.name as paymentmodeid, fn_inctransactions.bank, fn_inctransactions.chequeno, fn_inctransactions.incomedate, fn_inctransactions.remarks, fn_inctransactions.memo, fn_inctransactions.drawer, fn_inctransactions.jvno, fn_inctransactions.documentno, fn_inctransactions.createdby, fn_inctransactions.createdon, fn_inctransactions.lasteditedby, fn_inctransactions.lasteditedon".$fds.$fd;
		$join=" left join fn_incomes on fn_inctransactions.incomeid=fn_incomes.id  left join sys_paymentmodes on fn_inctransactions.paymentmodeid=sys_paymentmodes.id ";
		$having="";
		$where= " $rptwhere";
		$groupby= " $rptgroup";
		$orderby="";
		$inctransactions->retrieve($fields,$join,$where,$having,$groupby,$orderby);
		$res=$inctransactions->result;
		while($row=mysql_fetch_object($res)){
		$i++;
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<?php if($obj->shincomeid==1  or empty($obj->action)){ ?>
				<td><?php echo $row->incomeid; ?></td>
			<?php } ?>
			<?php if($obj->shpaymentmodeid==1  or empty($obj->action)){ ?>
				<td><?php echo $row->paymentmodeid; ?></td>
			<?php } ?>
			<?php if($obj->shamount==1  or empty($obj->action)){ ?>
				<td><?php echo $row->amount; ?></td>
			<?php } ?>
			<?php if($obj->shbank==1  or empty($obj->action)){ ?>
				<td><?php echo $row->bank; ?></td>
			<?php } ?>
			<?php if($obj->shchequeno==1 ){ ?>
				<td><?php echo $row->chequeno; ?></td>
			<?php } ?>
			<?php if($obj->shincomedate==1 ){ ?>
				<td><?php echo $row->incomedate; ?></td>
			<?php } ?>
			<?php if($obj->shremarks==1  or empty($obj->action)){ ?>
				<td><?php echo $row->remarks; ?></td>
			<?php } ?>
			<?php if($obj->shmemo==1 ){ ?>
				<td><?php echo $row->memo; ?></td>
			<?php } ?>
			<?php if($obj->shdrawer==1 ){ ?>
				<td><?php echo $row->drawer; ?></td>
			<?php } ?>
			<?php if($obj->shjvno==1 ){ ?>
				<td><?php echo $row->jvno; ?></td>
			<?php } ?>
			<?php if($obj->shdocumentno==1 ){ ?>
				<td><?php echo $row->documentno; ?></td>
			<?php } ?>
		</tr>
	<?php 
	}
	?>
	</tbody>
</div>
</div>
</div>
</div>
</div>
