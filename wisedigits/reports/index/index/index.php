<?php
session_start();
require_once "../../../lib.php";
require_once '../../../DB.php';

$db = new DB();

if(empty($_SESSION['userid'])){;
redirect("../../../modules/auth/users/login.php");
}

$page_title="Reports";

include"../../../head.php";
?>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1">PRODUCTION</a></li>
		<li><a href="#tabs-2">POST HARVEST</a></li>
		<li><a href="#tabs-3">SALES</a></li>
		<li><a href="#tabs-4">HRM</a></li>
		<li><a href="#tabs-5">FLEET</a></li>
		<li><a href="#tabs-6">PROCUREMENT</a></li>
		<li><a href="#tabs-7">INVENTORY</a></li>
		<li><a href="#tabs-8">FINANCE</a></li>
       	
	</ul>
	<div id="tabs-1" style="min-height:420px;">
			<ul>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/plantings/plantings.php',700,1020);">Plantings</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/breederdeliverys/breederdeliverys.php',700,1020);">Breeder Deliveries</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/qualitychecks/qualitychecks.php',700,1020);">Quality Checks</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/uproots/uproots.php',700,1020);">Uproots</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/varietys/varietys.php',700,1020);">Varietys</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/varietystocks/varietystockss.php',700,1020);">Pre-cool Stocks</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/forecastings/forecastings.php',700,1020);">Forecastings</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/greenhousevarietys/greenhousevarietys.php',700,1020);">Green House Varietys</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/sprayprogrammes/sprayprogrammes.php',700,1020);">Spray Programmes</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/rejects/rejectss.php',700,1020);">Production Rejects</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/irrigationmixtures/irrigationmixtures.php',700,1020);">Fertigation Report</a></li>
				
			</ul>
            		<div style="clear:both;"></div>
    </div><!-- TEnd -->
    <div id="tabs-2" style="min-height:420px;">
    <ul>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/harvests/harvests.php',700,1020);">Harvests</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/harvests/harvestss.php',700,1020);">Harvests Report</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../post/graded/graded.php',700,1020);">Graded</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../post/graded/gradeds.php',700,1020);">Graded Report</a></li>
				<!--<li><a class="button icon chat" href="javascript:poptastic('../../prod/rejects/rejects.php',700,1020);">Pre-cool Rejects</a></li>-->
				<li><a class="button icon chat" href="javascript:poptastic('../../prod/rejects/rejectss.php?reduce=reduce',700,1020);">Pre-cool Rejects Report</a></li>
				
				<!--<li><a class="button icon chat" href="javascript:poptastic('../../post/harvestrejects/harvestrejects.php',700,1020);">Cold Store Rejects</a></li>-->
				<li><a class="button icon chat" href="javascript:poptastic('../../post/harvestrejects/harvestrejectss.php',700,1020);">Cold Store Rejects Report</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../pos/itemstocks/itemstocks.php',700,1020);">Cold Store Stocks</a></li>
			</ul>
    		<div style="clear:both;"></div>
    </div><!-- TEnd -->
    <div id="tabs-3" style="min-height:420px;">
    <ul>
				<li><a class="button icon chat" href="javascript:poptastic('../../pos/orders/orders.php',700,1020);">Orders</a></li>
			
				<li><a class="button icon chat" href="javascript:poptastic('../../pos/returninwards/returninwards.php',700,1020);">Credit Notes</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../pos/confirmedorders/confirmedorders.php',700,1020);">Confirmed Orders</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../pos/confirmedorders/confirmedorderss.php',700,1020);">Confirmed Order Formatted</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../pos/packinglists/packinglists.php',700,1020);">Packing Lists</a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../pos/itemstocks/itemstocks.php',700,1020);">Cold store  Report</a></li>
				<!--<li><a class="button icon chat" href="javascript:poptastic('../../pos/invoices/invoices.php',700,1020);">Invoices</a></li>-->
				<li><a class="button icon chat" href="javascript:poptastic('../../pos/invoices/invoicess.php',700,1020);">Invoice Details</a></li>
			</ul>
				
			
    		<div style="clear:both;"></div>
    </div><!-- TEnd -->
     <div id="tabs-4" style="min-height:420px;">
    <ul>
				<li><a class="button icon chat" href="javascript:poptastic('../../hrm/employees/employees.php',700,1020);">Employees</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../hrm/employeedocuments/employeedocuments.php',700,1020);">Documents</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../hrm/employeequalifications/employeequalifications.php',700,1020);">Qualifications</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../hrm/employeecontracts/employeecontracts.php',700,1020);">Contracts</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../hrm/employeeinsurances/employeeinsurances.php',700,1020);">Insurances</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../hrm/employeedisplinarys/employeedisplinarys.php',700,1020);">Discplinaries</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../hrm/employeeclockings/employeeclockings.php',700,1020);">Clockings</a></li>
			</ul>
    		<div style="clear:both;"></div>
    </div><!-- TEnd -->
    
    <div id="tabs-6" style="min-height:420px;">
    			<ul>
    			<li><a class="button icon chat" href="javascript:poptastic('../../proc/requisitions/requisitions.php',700,1020);">Requisitions</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../proc/purchaseorders/purchaseorders.php',700,1020);">Purchase Orders</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../proc/deliverynotes/deliverynotes.php',700,1020);">Delivery notes</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../proc/inwards/inwards.php',700,1020);">Inwards</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../proc/supplieritems/supplieritems.php',700,1020);">Supplier Items</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../proc/suppliers/suppliers.php',700,1020);">Supplier Accounts</a></li>
    			

			</ul>
    		<div style="clear:both;"></div>
    		
    		</div><!-- TEnd -->
    		
    		<div id="tabs-7" style="min-height:420px;">
    			<ul>
    			<li><a class="button icon chat" href="javascript:poptastic('../../inv/items/items.php',700,1020);">Items Report </a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../inv/items/items.php',700,1020);">Reorder Level Report </a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../inv/items/items.php',700,1020);">Out of Stock Report </a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../inv/issuance/issuance.php',700,1020);">Issuance</a></li>    		
    			<li><a class="button icon chat" href="javascript:poptastic('../../inv/purchaseorders/purchaseorders.php',700,1020);">Purchase Orders Report </a></li>	
				<li><a class="button icon chat" href="javascript:poptastic('../../inv/purchases/purchases.php',700,1020);">Purchases Report </a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../fn/suppliers/suppliers.php',700,1020);">Suppliers Report </a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../inv/returnoutwards/returnoutwards.php',700,1020);">Returns Report </a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../inv/returnnotes/returnnotes.php',700,1020);">Return Notes Report </a></li>
				<li><a class="button icon chat" href="javascript:poptastic('../../fn/generaljournals/generaljournals.php?acctype=30&filter=true&balance=true',700,1020);">Supplier Accounts</a></li>
			</ul>
    		<div style="clear:both;"></div>
    </div><!-- TEnd -->
    
    <div id="tabs-5" style="min-height:420px;">
    <ul>
				<li><a class="button icon chat" href="javascript:poptastic('../../assets/fleets/fleets.php',700,1020);">Fleets</a></li>
			</ul>
    		<div style="clear:both;"></div>
    </div><!-- TEnd -->
    
     <div id="tabs-8" style="min-height:420px;">
    			<ul>
    			<li><a class="button icon chat" href="javascript:poptastic('../../fn/inctransactions/inctransactions.php',700,1020);">Incomes</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../fn/exptransactions/exptransactions.php',700,1020);">Expenses</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../fn/supplierpayments/supplierpayments.php',700,1020);">Supplier Payments</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../fn/customerpayments/customerpayments.php',700,1020);">Customer Payments</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../fn/imprests/imprests.php',700,1020);">Imprests</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../fn/impresttransactions/impresttransactions.php',700,1020);">Imprest Transactions</a></li>
    			<li><a class="button icon chat" href="javascript:poptastic('../../fn/generaljournalaccounts/generaljournalaccounts.php',700,1020);">Journal Accounts</a></li>
    			<?php if($_SESSION['SEPARATE_FINANCES']=="true"){?>
			<li><a class="button icon chat" href="javascript:poptastic('../../fn/generaljournals/generaljournals.php?grp=1&class=A',700,1020);">Chart of Accounts</a></li>
			<li><a class="button icon chat" href="javascript:poptastic('../../fn/generaljournals/generaljournals.php?grp=1&class=B',700,1020);">Property Chart of Accounts</a></li>
			<?php }else{?>
			<li><a class="button icon chat" href="javascript:poptastic('../../fn/generaljournals/generaljournals.php?grp=1',700,1020);">Chart of Accounts</a></li>
			<?php }?>
			<li><a class="button icon chat" href="javascript:poptastic('../../fn/generaljournals/generaljournals.php?tb=true',700,1020);">Trial Balance</a></li>
			<li><a class="button icon chat" href="javascript:poptastic('../../fn/generaljournals/generaljournals.php?acctypeid=8&balance=true',700,1020);">Banks</a></li>
			<li><a class="button icon chat" href="../../fn/generaljournals/income.php">Income Statement</a></li>
			<li><a class="button icon chat" href="../../fn/generaljournals/dtincome.php">Detailed Income Statement</a></li>
			<li><a class="button icon chat" href="../../fn/generaljournals/financial.php">Financial Statement</a></li>
			</ul>
    		<div style="clear:both;"></div>
    </div><!-- TEnd -->
    
 
     		<div style="clear:both;"></div>
<?php
include"../../../foot.php";
?>
