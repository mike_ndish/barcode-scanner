<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("../../../modules/pos/invoices/Invoices_class.php");
require_once("../../../modules/auth/users/Users_class.php");
require_once("../../../modules/auth/rules/Rules_class.php");
require_once("../../../modules/crm/customers/Customers_class.php");
require_once("../../../modules/crm/agents/Agents_class.php");
require_once("../../../modules/pos/items/Items_class.php");
require_once("../../../modules/pos/sizes/Sizes_class.php");
require_once("../../../modules/auth/users/Users_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../../modules/auth/users/login.php");
}

$page_title="Invoices";
//connect to db
$db=new DB();

$obj=(object)$_POST;

//Authorization.
$auth->roleid="8728";//Report View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../rptheader.php";

$rptwhere='';
$rptjoin='';
$track=0;
$k=0;
$fds='';
$fd='';
$aColumns=array('1');
$sColumns=array('1');
//Processing Groupings
$rptgroup='';
$track=0;

$join=" left join pos_invoices on pos_invoices.id=pos_invoicedetails.invoiceid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}
		
if(!empty($obj->grdocumentno) or !empty($obj->grpackingno) or !empty($obj->grcustomerid) or !empty($obj->gragentid) or !empty($obj->grsoldon) or !empty($obj->grcreatedby) or !empty($obj->grcreatedon) ){
// 	$obj->shdocumentno='';
// 	$obj->shpackingno='';
// 	$obj->shcustomerid='';
// 	$obj->shagentid='';
// 	$obj->shremarks='';
// 	$obj->shsoldon='';
// 	$obj->shmemo='';
// 	$obj->shcreatedby='';
// 	$obj->shcreatedon='';
// 	$obj->shipaddress='';
// 	$obj->shcontinentid='';
// 	$obj->shcountryid='';
// 	$obj->shexchangerate='';
// 	$obj->shexchangerate2='';
// 	$obj->shcurrencyid='';
}

if(!empty($obj->grdocumentno)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" documentno ";
	$obj->shdocumentno=1;
	$track++;
}

if(!empty($obj->grpackingno)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" packingno ";
	$obj->shpackingno=1;
	$track++;
}

if(!empty($obj->grcustomerid)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" customerid ";
	$obj->shcustomerid=1;
	$track++;
}

if(!empty($obj->gragentid)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" agentid ";
	$obj->shagentid=1;
	$track++;
}

if(!empty($obj->grsoldon)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" soldon ";
	$obj->shsoldon=1;
	$track++;
}

if(!empty($obj->grcreatedby)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" createdby ";
	$obj->shcreatedby=1;
	$track++;
}

if(!empty($obj->grcreatedon)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" createdon ";
	$obj->shcreatedon=1;
	$track++;
}

//processing columns to show
	if(!empty($obj->shdocumentno)  or empty($obj->action)){
		array_push($sColumns, 'documentno');
		array_push($aColumns, "pos_invoices.documentno");
		$k++;
		}

	if(!empty($obj->shpackingno)  or empty($obj->action)){
		array_push($sColumns, 'packingno');
		array_push($aColumns, "pos_invoices.packingno");
		$k++;
		}

	if(!empty($obj->shcustomerid)  or empty($obj->action)){
		array_push($sColumns, 'customerid');
		array_push($aColumns, "crm_customers.name as customerid");
		$rptjoin.=" left join crm_customers on crm_customers.id=pos_invoices.customerid ";
		$k++;
		}

	if(!empty($obj->shcontinentid) ){
		array_push($sColumns, 'continentid');
		array_push($aColumns, "crm_continents.name continentid");
		$k++;
		$join=" left join crm_continents on crm_continents.id=crm_customers.continentid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}

		$join=" left join crm_customers on crm_customers.id=pos_invoices.customerid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}
		 
		}

	if(!empty($obj->shcountryid)){
		array_push($sColumns, 'countryid');
		array_push($aColumns, "crm_countrys.name countryid");
		$k++;
		
		$join=" left join crm_countrys on crm_countrys.id=crm_customers.countryid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}
		
		$join=" left join crm_customers on crm_customers.id=pos_invoices.customerid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}
		}

	if(!empty($obj->shagentid)  or empty($obj->action)){
		array_push($sColumns, 'agentid');
		array_push($aColumns, "crm_agents.name as agentid");
		$rptjoin.=" left join crm_agents on crm_agents.id=pos_invoices.agentid ";
		$k++;
		}

	if(!empty($obj->shremarks) ){
		array_push($sColumns, 'remarks');
		array_push($aColumns, "pos_invoices.remarks");
		$k++;
		}

	if(!empty($obj->shsoldon)  or empty($obj->action)){
		array_push($sColumns, 'soldon');
		array_push($aColumns, "pos_invoices.soldon");
		$k++;
		}

	if(!empty($obj->shmemo) ){
		array_push($sColumns, 'memo');
		array_push($aColumns, "pos_invoices.memo");
		$k++;
		}

	if(!empty($obj->shcreatedby) ){
		array_push($sColumns, 'createdby');
		array_push($aColumns, "pos_invoices.createdby");
		$k++;
		}

	if(!empty($obj->shcreatedon)  or empty($obj->action)){
		array_push($sColumns, 'createdon');
		array_push($aColumns, "pos_invoices.createdon");
		$k++;
		}

	if(!empty($obj->shipaddress) ){
		array_push($sColumns, 'ipaddress');
		array_push($aColumns, "pos_invoices.ipaddress");
		$k++;
		}	

	if(!empty($obj->shexchangerate) ){
		array_push($sColumns, 'exchangerate');
		array_push($aColumns, "pos_invoices.exchangerate");
		$k++;
		}

	if(!empty($obj->shexchangerate2)){
		array_push($sColumns, 'exchangerate2');
		array_push($aColumns, "pos_invoices.exchangerate2");
		$k++;
		}

	if(!empty($obj->shcurrencyid)){
		array_push($sColumns, 'currencyid');
		array_push($aColumns, "pos_invoices.currencyid");
		$k++;
		}



$track=0;

//processing filters
if(!empty($obj->documentno)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" pos_invoices.documentno='$obj->documentno'";
	$track++;
}

if(!empty($obj->packingno)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" pos_invoices.packingno='$obj->packingno'";
	$track++;
}

if(!empty($obj->customerid)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" pos_invoices.customerid='$obj->customerid'";
		$join=" left join crm_customers on crm_customers.id=pos_invoices.customerid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}
	$track++;
}

if(!empty($obj->agentid)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" pos_invoices.agentid='$obj->agentid'";
		$join=" left join crm_agents on pos_invoices.id=crm_agents.invoiceid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}
	$track++;
}

if(!empty($obj->fromsoldon)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" pos_invoices.soldon>='$obj->fromsoldon'";
	$track++;
}

if(!empty($obj->tosoldon)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" pos_invoices.soldon<='$obj->tosoldon'";
	$track++;
}

if(!empty($obj->createdby)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" pos_invoices.createdby='$obj->createdby'";
	$track++;
}

if(!empty($obj->fromcreatedon)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" pos_invoices.createdon>='$obj->fromcreatedon'";
	$track++;
}

if(!empty($obj->tocreatedon)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" pos_invoices.createdon<='$obj->tocreatedon'";
	$track++;
}

if(!empty($obj->itemid)){
	if($track>0)
		$rptwhere.="and";
	$rptwhere.=" pos_items.id='$obj->itemid' ";
	$join=" left join pos_invoicedetails on pos_invoices.id=pos_invoicedetails.invoiceid ";
	if(!strpos($rptjoin,trim($join))){
		$rptjoin.=$join;
	}
	$join=" left join pos_items on pos_items.id=pos_invoicedetails.itemid ";
	if(!strpos($rptjoin,trim($join))){
		$rptjoin.=$join;
	}
	$track++;
}

//Processing Joins
;$track=0;
//Default shows
?>
<title><?php echo $page_title; ?></title>
<script type="text/javascript">
$().ready(function() {
 $("#customername").autocomplete("../../../modules/server/server/search.php?main=crm&module=customers&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#customername").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("customername").value=data[0];
     document.getElementById("customerid").value=data[1];
   }
 });
 $("#agentname").autocomplete("../../../modules/server/server/search.php?main=crm&module=agents&field=name", {
 	width: 260,
 	selectFirst: false
 });
 $("#agentname").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("agentname").value=data[0];
     document.getElementById("agentid").value=data[1];
   }
 });
});
</script>
<script type="text/javascript" charset="utf-8">
<?php

$mnt = array();

 $sizes=new Sizes();
  $where="  ";
  $fields="*";
  $join="";
  $having="";
  $groupby="";
  $orderby=" order by name ";
  $sizes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

  
  $cols="";
  $mnt=$k;
  while($rw=mysql_fetch_object($sizes->result)){
  
    if($obj->shquantity==1 and $obj->grsizeid==1){
      $cols=" sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.quantity end) '$rw->name'";
      array_push($aColumns, $cols);
      array_push($sColumns, $rw->name);
      
      $k++;
    }
    if($obj->shprice==1 and $obj->grsizeid==1){
      $cols=" sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.price end) 'p$rw->name'";
      array_push($aColumns, $cols);
      array_push($sColumns, "p$rw->name");
      $mnt[]=$k;
      $k++;
    }
    
    if($obj->shtotal==1 and $obj->grsizeid==1){
      $cols=" sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.total end) 't$rw->name'";
      array_push($aColumns, $cols);
      array_push($sColumns, "t$rw->name");
      $mnt[]=$k;
      $k++;
    }
    
    if($obj->shtotaleuros==1 and $obj->grsizeid==1){
      $cols=" sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.total*pos_invoices.exchangerate end) 'e$rw->name'";
      array_push($aColumns, $cols);
      array_push($sColumns, "e$rw->name");
      $mnt[]=$k;
      $k++;
    }
    
    if($obj->shtotalkshs==1 and $obj->grsizeid==1){
      $cols=" sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.total*pos_invoices.exchangerate2 end) 'k$rw->name'";
      array_push($aColumns, $cols);
      array_push($sColumns, "k$rw->name");
      $k++;
    }
    
    if($obj->shavgeuros==1 and $obj->grsizeid==1){
      $cols=" sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.total*pos_invoices.exchangerate end)/sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.quantity end) 'ek$rw->name'";
      array_push($aColumns, $cols);
      array_push($sColumns, "ek$rw->name");
      $mnt[]=$k;
      $k++;
    }
    
    if($obj->shavgkshs==1 and $obj->grsizeid==1){
      $cols=" sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.total*pos_invoices.exchangerate2 end)/sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.quantity end) 'ak$rw->name'";
      array_push($aColumns, $cols);
      array_push($sColumns, "ak$rw->name");
      $mnt[]=$k;
      $k++;
    }
    
    if($obj->shpercstems==1 and $obj->grsizeid==1){
      $cols=" sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.quantity end)/sum(pos_invoicedetails.quantity) 'pk$rw->name'";
      array_push($aColumns, $cols);
      array_push($sColumns, "ak$rw->name");
      $k++;
    }
    
    if($obj->shpercrev==1 and $obj->grsizeid==1){
      $cols=" sum(case when pos_invoicedetails.sizeid=$rw->id then pos_invoicedetails.total*pos_invoices.exchangerate end)/sum(pos_invoicedetails.total*pos_invoices.exchangerate) 'rk$rw->name'";
      array_push($aColumns, $cols);
      array_push($sColumns, "rk$rw->name");
      $k++;
    }
  }

if(!empty($obj->shquantity)){
		array_push($sColumns, 'quantity');
		if(!empty($rptgroup)){
			array_push($aColumns, "sum(pos_invoicedetails.quantity) quantity");
		}else{
		array_push($aColumns, "pos_invoicedetails.quantity");
		}

		$k++;
		
		}

	
		
if(!empty($obj->shtotal)){
		array_push($sColumns, 'total');
		if(!empty($rptgroup)){
			array_push($aColumns, "sum(pos_invoicedetails.total) total");
		}else{
		array_push($aColumns, "pos_invoicedetails.total");
		}

		$k++;
		
		}
if(!empty($obj->shtotaleuros)){
		array_push($sColumns, 'total');
		if(!empty($rptgroup)){
			array_push($aColumns, "sum(pos_invoicedetails.total*pos_invoices.exchangerate) total");
		}else{
		array_push($aColumns, "pos_invoicedetails.total*pos_invoices.exchangerate total");
		}
		
		$k++;
		
		}
if(!empty($obj->shtotalkshs)){
		array_push($sColumns, 'total');
		if(!empty($rptgroup)){
			array_push($aColumns, "sum(pos_invoicedetails.total*pos_invoices.exchangerate2) total");
		}else{
		array_push($aColumns, "pos_invoicedetails.total*pos_invoices.exchangerate2 total");
		}
		
		$k++;
		
		}
		
if(!empty($obj->shavgeuros)){
		array_push($sColumns, 'total');
		if(!empty($rptgroup)){
			array_push($aColumns, "sum(pos_invoicedetails.total*pos_invoices.exchangerate)/sum(pos_invoicedetails.quantity) total");
		}else{
		array_push($aColumns, "(pos_invoicedetails.total*pos_invoices.exchangerate)/pos_invoicedetails.quantity total");
		}
		
		$k++;
		
		}
if(!empty($obj->shavgkshs)){
		array_push($sColumns, 'total');
		if(!empty($rptgroup)){
			array_push($aColumns, "sum(pos_invoicedetails.total*pos_invoices.exchangerate2)/sum(pos_invoicedetails.quantity) total");
		}else{
		array_push($aColumns, "(pos_invoicedetails.total*pos_invoices.exchangerate2)/pos_invoicedetails.quantity total");
		}

		$k++;
		
		}
		
?>
 <?php $_SESSION['aColumns']=$aColumns;?>
 <?php $_SESSION['sColumns']=$sColumns;?>
 <?php $_SESSION['join']="$rptjoin";?>
 <?php $_SESSION['sTable']="pos_invoicedetails";?>
 <?php $_SESSION['sOrder']="";?>
 <?php $_SESSION['sWhere']="$rptwhere";?>
 <?php $_SESSION['sGroup']="$rptgroup";?>
 
 $(document).ready(function() {
	TableTools.DEFAULTS.aButtons = [ "copy", "csv", "xls","pdf" ];
 	$('#tbl').dataTable( {
		"sDom": 'T<"H"lfr>t<"F"ip>',
		"oTableTools": {
			"sSwfPath": "../../../media/swf/copy_cvs_xls_pdf.swf"
		},
 		"bJQueryUI": true,
 		"bSort":true,
 		"sPaginationType": "full_numbers",
 		"sScrollY": 400,
 		"iDisplayLength":50,
		"bJQueryUI": true,
		"bRetrieve":true,
		"sAjaxSource": "../../../modules/server/server/processing.php?sTable=pos_invoices",
		"fnRowCallback": function( nRow, aaData, iDisplayIndex ) {
			
			$('td:eq(0)', nRow).html(iDisplayIndex+1);
			var num = aaData.length;
			for(var i=1; i<num; i++){
				if(i>=<?php echo $mnt; ?>)
				  $('td:eq('+i+')', nRow).html(aaData[i]).formatCurrency().attr('align','right');
				else
				  $('td:eq('+i+')', nRow).html(aaData[i]);
			}
			return nRow;
		},
 	} );
 } );
 </script>

<div id="main">
<div id="main-inner">
<div id="content">
<div id="content-inner">
<div id="content-header">
	<div class="page-title"><?php echo $page_title; ?></div>
	<div class="clearb"></div>
</div>
<div id="content-flex">
<button id="create-user">Filter</button>
<div id="toPopup" >
<div class="close"></div>
<span class="ecs_tooltip">Press Esc to close <span class="arrow"></span>
<div id="dialog-modal" title="Filter" style="font:tahoma;font-size:10px;">
<form  action="invoicess.php" method="post" name="invoices" >
<table width="100%" border="0" align="center">
	<tr>
		<td width="50%" rowspan="2">
		<table class="tgrid gridd" border="0" align="right">
			<tr>
				<td>Document No</td>
				<td><input type='text' id='documentno' size='20' name='documentno' value='<?php echo $obj->documentno;?>'></td>
			</tr>
			<tr>
				<td>Packing No</td>
				<td><input type='text' id='packingno' size='20' name='packingno' value='<?php echo $obj->packingno;?>'></td>
			</tr>
			<tr>
				<td>Customer</td>
				<td><input type='text' size='20' name='customername' id='customername' value='<?php echo $obj->customername; ?>'>
					<input type="hidden" name='customerid' id='customerid' value='<?php echo $obj->field; ?>'></td>
			</tr>
			<tr>
				<td>Agent</td>
				<td><input type='text' size='20' name='agentname' id='agentname' value='<?php echo $obj->agentname; ?>'>
					<input type="hidden" name='agentid' id='agentid' value='<?php echo $obj->field; ?>'></td>
			</tr>
			<tr>
				<td>Sold On</td>
				<td><strong>From:</strong><input type='text' id='fromsoldon' size='12' name='fromsoldon' readonly class="date_input" value='<?php echo $obj->fromsoldon;?>'/>
							<br/><strong>To:</strong><input type='text' id='tosoldon' size='12' name='tosoldon' readonly class="date_input" value='<?php echo $obj->tosoldon;?>'/></td>
			</tr>
			<tr>
				<td>Created By</td>
			<td>
			<select name='createdby' class='selectbox'>
				<option value=''>Select...</option>
				<?php
				$users = new Users();
				$fields="*";
				$where="";
				$join="   ";
				$having="";
				$groupby="";
				$orderby="";
				$users->retrieve($fields,$join,$where,$having,$groupby,$orderby);

				while($rw=mysql_fetch_object($users->result)){
				?>
					<option value="<?php echo $rw->id; ?>" <?php if($obj->createdby==$rw->id){echo "selected";}?>><?php echo $rw->username;?></option>
				<?php
				}
				?>
			</td>
			</tr>
			<tr>
				<td>Created On</td>
				<td><strong>From:</strong><input type='text' id='fromcreatedon' size='12' name='fromcreatedon' readonly class="date_input" value='<?php echo $obj->fromcreatedon;?>'/>
							<br/><strong>To:</strong><input type='text' id='tocreatedon' size='12' name='tocreatedon' readonly class="date_input" value='<?php echo $obj->tocreatedon;?>'/></td>
			</tr>
			<tr>
				<td>Product</td>
				<td>
				<select name='itemid' class='selectbox'>
				<option value="">Select...</option>
				<?php
				$items=new Items();
				$where="  ";
				$fields="pos_items.id, pos_items.code, pos_items.name, pos_items.departmentid, pos_items.categoryid, pos_items.sizeid, pos_items.price, pos_items.tax, pos_items.stock, pos_items.itemstatusid, pos_items.remarks, pos_items.createdby, pos_items.createdon, pos_items.lasteditedby, pos_items.lasteditedon, pos_items.ipaddress";
				$join="";
				$having="";
				$groupby="";
				$orderby="";
				$items->retrieve($fields,$join,$where,$having,$groupby,$orderby);

				while($rw=mysql_fetch_object($items->result)){
				?>
					<option value="<?php echo $rw->id; ?>" <?php if($obj->itemid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
				<?php
				}
				?>
				</select>
</td>
			</tr>
		</table>
		</td>
		<td>
		<table class="tgrid gridd" width="100%" border="0" align="left">
			<tr>
			<th colspan="2"><div align="left"><strong>Group By (For Summarised Reports)</strong>: </div></th>
			</tr>
			<tr>
				<td><input type='checkbox' name='grdocumentno' value='1' <?php if(isset($_POST['grdocumentno']) ){echo"checked";}?>>&nbsp;Document No</td>
				<td><input type='checkbox' name='grpackingno' value='1' <?php if(isset($_POST['grpackingno']) ){echo"checked";}?>>&nbsp;Packing No</td>
			<tr>
				<td><input type='checkbox' name='grcustomerid' value='1' <?php if(isset($_POST['grcustomerid']) ){echo"checked";}?>>&nbsp;Customer</td>
				<td><input type='checkbox' name='gragentid' value='1' <?php if(isset($_POST['gragentid']) ){echo"checked";}?>>&nbsp;Agent</td>
			<tr>
				<td><input type='checkbox' name='grsoldon' value='1' <?php if(isset($_POST['grsoldon']) ){echo"checked";}?>>&nbsp;Sold On</td>
				<td><input type='checkbox' name='grcreatedby' value='1' <?php if(isset($_POST['grcreatedby']) ){echo"checked";}?>>&nbsp;Created By</td>
			<tr>
				<td><input type='checkbox' name='grcreatedon' value='1' <?php if(isset($_POST['grcreatedon']) ){echo"checked";}?>>&nbsp;Created On</td>
				<td><input type='checkbox' name='grsizeid' value='1' <?php if(isset($_POST['grsizeid']) ){echo"checked";}?>>&nbsp;Group By Size</td>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<table class="tgrid gridd" width="100%" border="0" align="left">
			<tr>
				<th colspan="3"><div align="left"><strong>Fields to Show (For Detailed Reports)</strong>: </div></th>
			</tr>
			<tr>
				<td><input type='checkbox' name='shdocumentno' value='1' <?php if(isset($_POST['shdocumentno'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Document No</td>
				<td><input type='checkbox' name='shpackingno' value='1' <?php if(isset($_POST['shpackingno'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Packing No</td>
			<tr>
				<td><input type='checkbox' name='shcustomerid' value='1' <?php if(isset($_POST['shcustomerid'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Customer</td>
				<td><input type='checkbox' name='shagentid' value='1' <?php if(isset($_POST['shagentid'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Agent</td>
			<tr>
				<td><input type='checkbox' name='shremarks' value='1' <?php if(isset($_POST['shremarks']) ){echo"checked";}?>>&nbsp;Remarks</td>
				<td><input type='checkbox' name='shsoldon' value='1' <?php if(isset($_POST['shsoldon'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Sold On</td>
			<tr>
				<td><input type='checkbox' name='shmemo' value='1' <?php if(isset($_POST['shmemo']) ){echo"checked";}?>>&nbsp;Memo</td>
				<td><input type='checkbox' name='shcreatedby' value='1' <?php if(isset($_POST['shcreatedby']) ){echo"checked";}?>>&nbsp;Created By</td>
			<tr>
				<td><input type='checkbox' name='shcreatedon' value='1' <?php if(isset($_POST['shcreatedon'])  or empty($obj->action)){echo"checked";}?>>&nbsp;Created On</td>
				<td><input type='checkbox' name='shipaddress' value='1' <?php if(isset($_POST['shipaddress']) ){echo"checked";}?>>&nbsp;Ipaddress</td>
			<tr>
				<td><input type='checkbox' name='shcontinentid' value='1' <?php if(isset($_POST['shcontinentid'])  ){echo"checked";}?>>&nbsp;Continent</td>
				<td><input type='checkbox' name='shcountryid' value='1' <?php if(isset($_POST['shcountryid'])  ){echo"checked";}?>>&nbsp;Country</td>
			<tr>
				<td><input type='checkbox' name='shexchangerate' value='1' <?php if(isset($_POST['shexchangerate'])  ){echo"checked";}?>>&nbsp;Ksh</td>
				<td><input type='checkbox' name='shexchangerate2' value='1' <?php if(isset($_POST['shexchangerate2'])  ){echo"checked";}?>>&nbsp;Euro</td>
			<tr>
				<td><input type='checkbox' name='shcurrencyid' value='1' <?php if(isset($_POST['shcurrencyid'])  ){echo"checked";}?>>&nbsp;Currency</td>
				<td><input type='checkbox' name='shprice' value='1' <?php if(isset($_POST['shprice'])  ){echo"checked";}?>>&nbsp;Price</td>
			<tr>
				<td><input type='checkbox' name='shtotal' value='1' <?php if(isset($_POST['shtotal'])  ){echo"checked";}?>>&nbsp;Amount (Original Currency)</td>
				<td><input type='checkbox' name='shquantity' value='1' <?php if(isset($_POST['shquantity'])  ){echo"checked";}?>>&nbsp;Quantity</td>
			 <tr>
				<td><input type='checkbox' name='shtotaleuros' value='1' <?php if(isset($_POST['shtotaleuros'])  ){echo"checked";}?>>&nbsp;Amount (Euros)</td>
				<td><input type='checkbox' name='shtotalkshs' value='1' <?php if(isset($_POST['shtotalkshs'])  ){echo"checked";}?>>&nbsp;Amount (Kshs)</td>
			<tr>
				<td><input type='checkbox' name='shavgeuros' value='1' <?php if(isset($_POST['shavgeuros'])  ){echo"checked";}?>>&nbsp;Average (Euros)</td>
				<td><input type='checkbox' name='shavgkshs' value='1' <?php if(isset($_POST['shavgkshs'])  ){echo"checked";}?>>&nbsp;Average (Kshs)</td>
			<tr>
				<td><input type='checkbox' name='shpercstems' value='1' <?php if(isset($_POST['shpercstems'])  ){echo"checked";}?>>&nbsp;% Stems Sold</td>
				<td><input type='checkbox' name='shpercrev' value='1' <?php if(isset($_POST['shpercrev']) ){echo"checked";}?>>&nbsp;% Revenue</td>
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align='center'><input type="submit" class="btn" name="action" id="action" value="Filter" /></td>
	</tr>
</table>
</form>
</div>
</div>
<table style="clear:both;"  class="tgrid display" id="tbl" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<?php if($obj->shdocumentno==1  or empty($obj->action)){ ?>
				<th>Document No </th>
			<?php } ?>
			<?php if($obj->shpackingno==1  or empty($obj->action)){ ?>
				<th>Packing No </th>
			<?php } ?>
			<?php if($obj->shcustomerid==1  or empty($obj->action)){ ?>
				<th>Customer </th>
			<?php } ?>			
			<?php if($obj->shcontinentid==1){ ?>
				<th>Continent </th>
			<?php } ?>
			<?php if($obj->shcountryid==1){ ?>
				<th>Country </th>
			<?php } ?>
			<?php if($obj->shagentid==1  or empty($obj->action)){ ?>
				<th>Agent </th>
			<?php } ?>
			<?php if($obj->shremarks==1 ){ ?>
				<th>Remarks </th>
			<?php } ?>
			<?php if($obj->shsoldon==1  or empty($obj->action)){ ?>
				<th>Sold On </th>
			<?php } ?>
			<?php if($obj->shmemo==1 ){ ?>
				<th>Memo </th>
			<?php } ?>
			<?php if($obj->shcreatedby==1 ){ ?>
				<th>CreatedBy </th>
			<?php } ?>
			<?php if($obj->shcreatedon==1  or empty($obj->action)){ ?>
				<th>CreatedOn </th>
			<?php } ?>
			<?php if($obj->shipaddress==1 ){ ?>
				<th>IP Address</th>
			<?php } ?>
			<?php if($obj->shexchangerate==1 ){ ?>
				<th>Exchange Rate (Euro)</th>
			<?php } ?>
			<?php if($obj->shexchangerate2==1){ ?>
				<th>Exchange Rate (Kshs)</th>
			<?php } ?>
			<?php if($obj->shcurrencyid==1){ ?>
				<th>Customer Currency</th>
			<?php } ?>
			
			<?php  
			      $sizes=new Sizes();
			      $where="  ";
			      $fields="*";
			      $join="";
			      $having="";
			      $groupby="";
			      $orderby="";
			      $sizes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

			      while($rw=mysql_fetch_object($sizes->result)){
				if($obj->shquantity==1 and $obj->grsizeid==1){
			?>
				<th><?php echo $rw->name; ?></th>
			<?php }
			if($obj->shprice==1 and $obj->grsizeid==1){
			?>
			  <th><?php echo $rw->name; ?></th>
			<?php
			}
			
			if($obj->shtotal==1 and $obj->grsizeid==1){
			?>
			  <th><?php echo $rw->name; ?></th>
			<?php
			}
			
			if($obj->shtotaleuros==1 and $obj->grsizeid==1){
			?>
			  <th><?php echo $rw->name; ?></th>
			<?php
			}
			
			if($obj->shtotalkshs==1 and $obj->grsizeid==1){
			?>
			  <th><?php echo $rw->name; ?></th>
			<?php
			}
			
			if($obj->shavgeuros==1 and $obj->grsizeid==1){
			?>
			  <th><?php echo $rw->name; ?></th>
			<?php
			}
			
			if($obj->shavgkshs==1 and $obj->grsizeid==1){
			?>
			  <th><?php echo $rw->name; ?></th>
			<?php
			}
			
			if($obj->shpercstems==1 and $obj->grsizeid==1){
			?>
			  <th><?php echo $rw->name; ?></th>
			<?php
			}
			
			if($obj->shpercrev==1 and $obj->grsizeid==1){
			?>
			  <th><?php echo $rw->name; ?></th>
			<?php
			}
			
			} ?>
			<?php if($obj->shquantity==1){ ?>
				<th>Quantity </th>
			<?php } ?>
			
			<?php if($obj->shtotal==1){ ?>
				<th>Amount (Original Currency) </th>
			<?php } ?>
			<?php if($obj->shtotaleuros==1){ ?>
				<th>Amount (Euros) </th>
			<?php } ?>
			<?php if($obj->shtotalkshs==1){ ?>
				<th>Amount (Kshs) </th>
			<?php } ?>
			<?php if($obj->shavgeuros==1){ ?>
				<th>Average (Euros) </th>
			<?php } ?>
			<?php if($obj->shavgkshs==1){ ?>
				<th>Average (Kshs) </th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
	</tbody>
</div>
</div>
</div>
</div>
</div>
