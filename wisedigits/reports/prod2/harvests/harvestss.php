<?php
session_start();
require_once("../../../DB.php");
require_once("../../../lib.php");
require_once("../../../modules/prod/harvests/Harvests_class.php");
require_once("../../../modules/auth/users/Users_class.php");
require_once("../../../modules/auth/rules/Rules_class.php");
require_once("../../../modules/prod/varietys/Varietys_class.php");
require_once("../../../modules/prod/sizes/Sizes_class.php");
require_once("../../../modules/prod/plantingdetails/Plantingdetails_class.php");
require_once("../../../modules/prod/blocks/Blocks_class.php");
require_once("../../../modules/prod/greenhouses/Greenhouses_class.php");
require_once("../../../modules/prod/types/Types_class.php");
require_once("../../../modules/prod/types/Types_class.php");
require_once("../../../modules/prod/colours/Colours_class.php");
require_once("../../../modules/prod/colours/Colours_class.php");
require_once("../../../modules/hrm/employees/Employees_class.php");
require_once("../../../modules/auth/users/Users_class.php");


if(empty($_SESSION['userid'])){;
	redirect("../../../modules/auth/users/login.php");
}

$page_title="Harvests";
//connect to db
$db=new DB();

$obj=(object)$_POST;

//Authorization.
$auth->roleid="8800";//Report View
$auth->levelid=$_SESSION['level'];

auth($auth);
include"../../../rptheader.php";

if(empty($obj->action)){
	$obj->fromharvestedon=date('Y-m-d');
	$obj->toharvestedon=date('Y-m-d');
	
	$obj->grvarietyid=1;
}


$rptwhere=" ";
$rptjoin='';
$track=0;
$k=0;
$fds='';
$fd='';
$aColumns=array('1');
$sColumns=array('1');
//Processing Groupings
$rptgroup='';
$blocks="";

if(!empty($obj->grtypeid) or !empty($obj->grvarietyid) or !empty($obj->grgreenhouseid) or !empty($obj->grharvestedon) or !empty($obj->grcolourid) or !empty($obj->gremployeeid) ){
	$obj->shvarietyid='';
	$obj->shsizeid='';
	$obj->shplantingdetailid='';
	$obj->shgreenhouseid='';
	$obj->shquantity='';
	$obj->shharvestedon='';
	$obj->shbarcode='';
	$obj->shremarks='';
	$obj->shtypeid='';
	$obj->shcolourid='';
	$obj->shemployeeid='';
}


	$obj->shquantity=1;
	$obj->shsizeid=1;
	
	if(empty($obj->action)){
	  $obj->status="checkedin";
	}

if(!empty($obj->grbudgets)){
	
	if(empty($obj->month)){
	  $obj->month=date("m");
	  $obj->year=date("Y");
	}
	
	$obj->fromharvestedon = date("Y-m-d",mktime(0,0,0,$obj->month,01,$obj->year));
	$obj->toharvestedon = date("Y-m-d",mktime(0,0,0,$obj->month,31,$obj->year));
	
	$obj->shbudgets=1;
// 	$track++;
}


if(!empty($obj->grtypeid)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" typeid ";
	$obj->shtypeid=1;
	$track++;
}

if(!empty($obj->grgreenhouseid)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" block,section,greenhouse,variety ";
	$obj->shgreenhouseid=1;
	$obj->shvarietyid=1;
	$track++;
}


if(!empty($obj->grvarietyid)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" variety ";
	$obj->shvarietyid=1;
	$track++;
}

if(!empty($obj->grharvestedon)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" harvestedon ";
	$obj->shharvestedon=1;
	$track++;
}

if(!empty($obj->grcolourid)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" colourid ";
	$obj->shcolourid=1;
	$track++;
}

if(!empty($obj->gremployeeid)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" employeeid ";
	$obj->shemployeeid=1;
	$track++;
}
if(!empty($obj->grcreatedon)){
	if($track>0)
		$rptgroup.=", ";
	else
		$rptgroup.=" group by ";

	$rptgroup.=" createdon ";
	$obj->shcreatedon=1;
	$track++;
}

//processing columns to show
	

// 	if(!empty($obj->shsizeid)){
// 		array_push($sColumns, 'sizeid');
// 		array_push($aColumns, "prod_sizes.name as sizeid");
// 		$rptjoin.=" left join prod_sizes on prod_sizes.id=prod_harvests.sizeid ";
// 		$k++;
// 		}

	if(!empty($obj->shplantingdetailid)){
		array_push($sColumns, 'plantingdetailid');
		array_push($aColumns, "prod_harvests.remarks as plantingdetailid");
		$rptjoin.=" left join prod_plantingdetails on prod_plantingdetails.id=prod_harvests.plantingdetailid ";
		$k++;
		}

	if(!empty($obj->shgreenhouseid)){
						
		
		array_push($sColumns, 'block');
		array_push($aColumns, "prod_blocks.name as block");
		
		array_push($sColumns, 'section');
		array_push($aColumns, "prod_sections.name as section");
		
		array_push($sColumns, 'greenhouse');
		array_push($aColumns, "prod_greenhouses.name as greenhouse");
		$rptjoin.=" left join prod_greenhouses on prod_greenhouses.id=prod_harvests.greenhouseid ";
		$k++;
		
		
		$rptjoin.=" left join prod_sections on prod_sections.id=prod_greenhouses.sectionid ";
		$k++;
		
		
		$rptjoin.=" left join prod_blocks on prod_blocks.id=prod_sections.blockid ";
		$k++;
		
		
		//$obj->shvarietyid=1;
		
		$blocks="Yes";
		}

	if(!empty($obj->shvarietyid)){
		array_push($sColumns, 'variety');
		array_push($aColumns, "prod_varietys.name as variety");
		$rptjoin.=" left join prod_varietys on prod_varietys.id=prod_harvests.varietyid ";
		$k++;
		}

	if(!empty($obj->shharvestedon) ){
		array_push($sColumns, 'harvestedon');
		array_push($aColumns, "prod_harvests.harvestedon");
		$k++;
		}

	if(!empty($obj->shbarcode) ){
		array_push($sColumns, 'barcode');
		array_push($aColumns, "prod_harvests.barcode");
		$k++;
		}

	if(!empty($obj->shremarks)){
		array_push($sColumns, 'remarks');
		array_push($aColumns, "prod_harvests.remarks");
		$k++;
		}

	if(!empty($obj->shtypeid)){
		array_push($sColumns, 'typeid');
		array_push($aColumns, "prod_types.name as typeid");
		$k++;
		$join=" left join prod_varietys on prod_varietys.id=prod_harvests.varietyid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}
		$join=" left join prod_types on prod_types.id=prod_varietys.typeid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}
		}

	if(!empty($obj->shcolourid) ){
		array_push($sColumns, 'colourid');
		array_push($aColumns, "prod_colours.name as colourid");
		$k++;
		$join=" left join prod_varietys on prod_varietys.id=prod_harvests.varietyid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}
		$join=" left join prod_colours on prod_colours.id=prod_varietys.colourid ";
		if(!strpos($rptjoin,trim($join))){
			$rptjoin.=$join;
		}
		}

	if(!empty($obj->shemployeeid) ){
		array_push($sColumns, 'employeeid');
		array_push($aColumns, "concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname)) as employeeid");
		$rptjoin.=" left join hrm_employees on hrm_employees.id=prod_harvests.employeeid ";
		$k++;
		}
		
	if(!empty($obj->shcreatedon) ){
		array_push($sColumns, 'createdon');
		array_push($aColumns, "prod_harvests.createdon");
		$k++;
		}
		
	


$mnt = ($k+1);

$track=0;

//processing filters
if(!empty($obj->varietyid)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.varietyid='$obj->varietyid'";
	$track++;
}

if(!empty($obj->sizeid)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.sizeid='$obj->sizeid'";
	$track++;
}

if(!empty($obj->plantingdetailid)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.plantingdetailid='$obj->plantingdetailid'";
	$track++;
}

// if(!empty($obj->greenhouseid)){
// 	if($track>0)
// 		$rptwhere.="and";
// 		$rptwhere.=" prod_areas.greenhouseid='$obj->greenhouseid'";
// 		$join=" left join prod_greenhouses on prod_greenhouses.id=prod_harvests.greenhouseid ";
// 		
// 	$track++;
// }

if(!empty($obj->fromquantity)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.quantity>='$obj->fromquantity'";
	$track++;
}

if(!empty($obj->toquantity)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.quantity<='$obj->toquantity'";
	$track++;
}

if(!empty($obj->quantity)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.quantity='$obj->quantity'";
	$track++;
}

if(!empty($obj->fromharvestedon)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.harvestedon>='$obj->fromharvestedon'";
	$track++;
}

if(!empty($obj->toharvestedon)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.harvestedon<='$obj->toharvestedon'";
	$track++;
}

if(!empty($obj->barcode)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.barcode='$obj->barcode'";
	$track++;
}

if(!empty($obj->typeid)){
	if($track>0)
		$rptwhere.="and";
	$rptwhere.=" prod_types.id='$obj->typeid' ";
	$join=" left join prod_varietys on prod_varietys.id=prod_harvests.varietyid ";
	if(!strpos($rptjoin,trim($join))){
		$rptjoin.=$join;
	}
	$join=" left join prod_types on prod_types.id=prod_varietys.typeid ";
	if(!strpos($rptjoin,trim($join))){
		$rptjoin.=$join;
	}
	$track++;
}

if(!empty($obj->colourid)){
	if($track>0)
		$rptwhere.="and";
	$rptwhere.=" prod_colours.id='$obj->colourid' ";
	$join=" left join prod_varietys on prod_varietys.id=prod_harvests.varietyid ";
	if(!strpos($rptjoin,trim($join))){
		$rptjoin.=$join;
	}
	$join=" left join prod_colours on prod_colours.id=prod_varietys.colourid ";
	if(!strpos($rptjoin,trim($join))){
		$rptjoin.=$join;
	}
	$track++;
}

if(!empty($obj->employeeid)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.employeeid='$obj->employeeid'";
	$track++;
}

if(!empty($obj->status)){
	if($track>0)
		$rptwhere.="and";
		$rptwhere.=" prod_harvests.status='$obj->status'";
		
	$track++;
}

//Processing Joins
;$track=0;
//Default shows
if(!empty($obj->shplantingdetailid)){
	$fd.=" ,prod_harvests.remarks ";
}
if(!empty($obj->shtypeid)){
	$fd.=" ,prod_types.name ";
}
if(!empty($obj->shcolourid)){
	$fd.=" ,prod_colours.name ";
}
if(!empty($obj->shemployeeid)){
	$fd.=" ,concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname)) ";
}
?>
<style media="all" type="text/css">
.hide{
  visibility:hidden;
  display:none;
}
</style>
<title><?php echo $page_title; ?></title>
<script type="text/javascript">
$().ready(function() {
 $("#employeename").autocomplete("../../../modules/server/server/search.php?main=hrm&module=employees&field=concat(hrm_employees.firstname,' ',concat(hrm_employees.middlename,' ',hrm_employees.lastname))", {
 	width: 260,
 	selectFirst: false
 });
 $("#employeename").result(function(event, data, formatted) {
   if (data)
   {
     document.getElementById("employeename").value=data[0];
     document.getElementById("employeeid").value=data[1];
   }
 });
});
</script>
<script type="text/javascript" charset="utf-8">
<?php
 $sizes=new Sizes();
  $where="  ";
  $fields="*";
  $join="";
  $having="";
  $groupby="";
  $orderby="";
  $sizes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

  
  $cols="";
  while($rw=mysql_fetch_object($sizes->result)){
    $cols=" sum(case when sizeid=$rw->id then prod_harvests.quantity end) '$rw->name'";
    array_push($aColumns, $cols);
    array_push($sColumns, $rw->name);
    
    $k++;
    
    if($obj->shperc){
      if(!empty($rptgroup)){
	      $cols=" round(sum(case when sizeid=$rw->id then prod_harvests.quantity end)/sum(prod_harvests.quantity)*100,2) 'perc$rw->name'";
      }else{
	$cols=" round(sum(case when sizeid=$rw->id then prod_harvests.quantity end)/prod_harvests.quantity*100,2) 'perc$rw->name'";
      }
      
      array_push($aColumns, $cols);
      array_push($sColumns, 'perc'.$rw->name);
      
      $k++;
    }
  }

if(!empty($obj->shquantity)){
		array_push($sColumns, 'quantity');
		if(!empty($rptgroup)){
			array_push($aColumns, "sum(prod_harvests.quantity) quantity");
		}else{
		array_push($aColumns, "prod_harvests.quantity");
		}

		$k++;
		
		}
if(!empty($obj->shbudgets) ){
		array_push($sColumns, 'budgets');
		array_push($aColumns, "(select prod_productionbudgets.quantity from prod_productionbudgets left join prod_greenhousevarietys on prod_greenhousevarietys.id=prod_productionbudgets.greenhousevarietyid where month='$obj->month' and year='$obj->year' and varietyid=prod_harvests.varietyid) budgets"); 
		$k++;
		
		array_push($sColumns, 'percbudgets');
		array_push($aColumns, "concat(round(((select prod_productionbudgets.quantity from prod_productionbudgets left join prod_greenhousevarietys on prod_greenhousevarietys.id=prod_productionbudgets.greenhousevarietyid where month='$obj->month' and year='$obj->year' and varietyid=prod_harvests.varietyid)/sum(prod_harvests.quantity))*100,2),'','%') percbudgets");
		$k++;
		}
if(!empty($obj->sharea) ){
		array_push($sColumns, 'area');
		array_push($aColumns, "(select sum(prod_greenhousevarietys.area) from prod_greenhousevarietys  where prod_greenhousevarietys.varietyid=prod_harvests.varietyid and prod_greenhousevarietys.greenhouseid=prod_harvests.greenhouseid) area");
		$k++;
		
		array_push($sColumns, 'percarea');
		array_push($aColumns, "round((sum(prod_harvests.quantity)/(select sum(prod_greenhousevarietys.area) from prod_greenhousevarietys where prod_greenhousevarietys.varietyid=prod_harvests.varietyid and prod_greenhousevarietys.greenhouseid=prod_harvests.greenhouseid)),2) percarea");
		$k++;
		}
?>
 <?php $_SESSION['aColumns']=$aColumns;?>
 <?php $_SESSION['sColumns']=$sColumns;?>
 <?php $_SESSION['join']="$rptjoin";?>
 <?php $_SESSION['sTable']="prod_harvests";?>
 <?php $_SESSION['sOrder']="";?>
 <?php $_SESSION['sWhere']="$rptwhere";?>
 <?php 
 
 if(!empty($obj->grgreenhouseid)){
  $rptgroup.=" with rollup ";
 }
 
 $_SESSION['sGroup']="$rptgroup ";?>
 
 $(document).ready(function() {
	TableTools.DEFAULTS.aButtons = [ "copy", "csv", "xls","pdf" ];
 	var tbl = $('#tbl').dataTable( {
		"sDom": 'T<"H"lfr>t<"F"ip>',
		"oTableTools": {
			"sSwfPath": "../../../media/swf/copy_cvs_xls_pdf.swf"
		},
		"serverSide": true,
		"processing": true,
 		"bJQueryUI": true,
 		"bSort":false,
 		"sPaginationType": "full_numbers",
 		"sScrollY": 400,
 		"iDisplayLength":50,
		"bJQueryUI": true,
		"bRetrieve":true,
		"sAjaxSource": "../../../modules/server/server/processing.php?sTable=prod_harvests",
		"fnRowCallback": function( nRow, aaData, iDisplayIndex ) {
			
			
			var num = aaData.length;
			var bool=false;
			var block=aaData[1];
			var section=aaData[2];
			for(var i=1; i<num; i++){
				if((aaData[1]=='' || aaData[2]=='' || aaData[3]=='') ){
				  $('td:eq(0)', nRow).html(iDisplayIndex+1);
				  <?php if(!empty($obj->grgreenhouseid)){?>  
				  
				  aaData[4]=section;
				  aaData[3]=block;
				  aaData[2]="";
				  aaData[1]="";				  
				  
				  $('td:eq('+i+')', nRow).html('<strong>'+aaData[i]+'</strong>').addClass("group");
				  bool=true;
				  <?php } ?>
				}
				else if(aaData[4]==''){
				  $('td:eq(0)', nRow).html('').addClass("hide");
				  $('td:eq('+i+')', nRow).html('<strong>'+aaData[i]+'</strong>').addClass("hide");
				}
				else{
				$('td:eq(0)', nRow).html(iDisplayIndex+1);
				  block=aaData[1];
				  section=aaData[2];
				  if(aaData[4]!='')
				    $('td:eq('+i+')', nRow).html(aaData[i]);
				}
			}
			if(bool){
			  //tbl.row.add([aaData]);
			 
			}
			return nRow;
		},
		"fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
			$('th:eq(0)', nRow).html("");
			$('th:eq(1)', nRow).html("TOTAL Harvests");
			var total=[];
			//var k=0;
			for(var i=0; i<aaData.length; i++){
			  //var k = aaData[i].length;
			  
			  for(var j=<?php echo $mnt; ?>; j<aaData[i].length; j++){
			  
			    if(aaData[i][j]=='')
			      aaData[i][j]=0;			      
			      
			      if(i==0)
				total[j]=0;
				<?php if(!empty($obj->grgreenhouseid)){?> 
				if((aaData[i][1]!='' && aaData[i][2]!='' && aaData[i][3]!='' && aaData[i][4]!='') )
				  total[j] = parseFloat(total[j])+parseFloat(aaData[i][j]);	//alert(parseFloat(aaData[i][j]));	
				<?php }else{?>
				  total[j] = parseFloat(total[j])+parseFloat(aaData[i][j]);
				<?php }?>
			  }
			  
			}
			
			for(var i=<?php echo $mnt; ?>; i<total.length;i++){
			  $('th:eq('+i+')', nRow).html(total[i]);
			}
		}
 	} );
 	<?php if(!empty($obj->grgreenhouseid)){?>
//  	tbl.rowGrouping({
// 	  iGroupingColumnIndex: 1,
// 	  iGroupingColumnIndex2: 2
//  	}); 
 	<?php }?>
 } );
 </script>

<div id="main">
<div id="main-inner">
<div id="content">
<div id="content-inner">
<div id="content-header">
	<div class="page-title"><?php echo $page_title; ?></div>
	<div class="clearb"></div>
</div>
<div id="content-flex">
<button id="create-user">Filter</button>
<div id="toPopup" >
<div class="close"></div>
<span class="ecs_tooltip">Press Esc to close <span class="arrow"></span>
<div id="dialog-modal" title="Filter" style="font:tahoma;font-size:10px;">
<form  action="harvestss.php" method="post" name="harvests" >
<table width="100%" border="0" align="center">
	<tr>
		<td width="50%" rowspan="2">
		<table class="tgrid gridd" border="0" align="right">
			<tr>
				<td>Variety</td>
				<td>
				<select name='varietyid' class='selectbox'>
				<option value="">Select...</option>
				<?php
				$varietys=new Varietys();
				$where="  ";
				$fields="prod_varietys.id, prod_varietys.name, prod_varietys.typeid, prod_varietys.colourid, prod_varietys.duration, prod_varietys.quantity, prod_varietys.remarks, prod_varietys.ipaddress, prod_varietys.createdby, prod_varietys.createdon, prod_varietys.lasteditedby, prod_varietys.lasteditedon";
				$join="";
				$having="";
				$groupby="";
				$orderby="";
				$varietys->retrieve($fields,$join,$where,$having,$groupby,$orderby);

				while($rw=mysql_fetch_object($varietys->result)){
				?>
					<option value="<?php echo $rw->id; ?>" <?php if($obj->varietyid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
				<?php
				}
				?>
				</select>
</td>
			</tr>
			<tr>
				<td>Length</td>
				<td>
				<select name='sizeid' class='selectbox'>
				<option value="">Select...</option>
				<?php
				$sizes=new Sizes();
				$where="  ";
				$fields="prod_sizes.id, prod_sizes.name, prod_sizes.remarks, prod_sizes.ipaddress, prod_sizes.createdby, prod_sizes.createdon, prod_sizes.lasteditedby, prod_sizes.lasteditedon";
				$join="";
				$having="";
				$groupby="";
				$orderby="";
				$sizes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

				while($rw=mysql_fetch_object($sizes->result)){
				?>
					<option value="<?php echo $rw->id; ?>" <?php if($obj->sizeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
				<?php
				}
				?>
				</select>
</td>
			</tr>
			<tr>
				<td>Planting Details</td>
				<td>
				<select name='plantingdetailid' class='selectbox'>
				<option value="">Select...</option>
				<?php
				$plantingdetails=new Plantingdetails();
				$where="  ";
				$fields="prod_plantingdetails.id, prod_plantingdetails.plantingid, prod_plantingdetails.varietyid, prod_plantingdetails.greenhouseid, prod_plantingdetails.quantity, prod_plantingdetails.memo, prod_plantingdetails.ipaddress, prod_plantingdetails.createdby, prod_plantingdetails.createdon, prod_plantingdetails.lasteditedby, prod_plantingdetails.lasteditedon";
				$join="";
				$having="";
				$groupby="";
				$orderby="";
				$plantingdetails->retrieve($fields,$join,$where,$having,$groupby,$orderby);

				while($rw=mysql_fetch_object($plantingdetails->result)){
				?>
					<option value="<?php echo $rw->id; ?>" <?php if($obj->plantingdetailid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
				<?php
				}
				?>
				</select>
</td>
			</tr>
			<tr>
				<td>Block</td>
				<td>
				<select name='greenhouseid' class='selectbox'>
				<option value="">Select...</option>
				<?php
				$greenhouses=new Greenhouses();
				$where="  ";
				$fields="*";
				$join="";
				$having="";
				$groupby="";
				$orderby="";
				$greenhouses->retrieve($fields,$join,$where,$having,$groupby,$orderby);

				while($rw=mysql_fetch_object($greenhouses->result)){
				?>
					<option value="<?php echo $rw->id; ?>" <?php if($obj->greenhouseid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
				<?php
				}
				?>
				</select>
</td>
			</tr>
			<tr>
				<td>Quantity</td>
				<td><strong>From:</strong><input type='text' id='fromquantity' size='from16' name='fromquantity' value='<?php echo $obj->fromquantity;?>'/>
								<br/><strong>To:</strong><input type='text' id='toquantity' size='to16' name='toquantity' value='<?php echo $obj->toquantity;?>'></td>
			</tr>
			<tr>
				<td>Harvest Date</td>
				<td><strong>From:</strong><input type='text' id='fromharvestedon' size='12' name='fromharvestedon' readonly class="date_input" value='<?php echo $obj->fromharvestedon;?>'/>
							<br/><strong>To:</strong><input type='text' id='toharvestedon' size='12' name='toharvestedon' readonly class="date_input" value='<?php echo $obj->toharvestedon;?>'/></td>
			</tr>
			<tr>
				<td>Barcode</td>
				<td><input type='text' id='barcode' size='20' name='barcode' value='<?php echo $obj->barcode;?>'></td>
			</tr>
			<tr>
				<td>Variety Type</td>
				<td>
				<select name='typeid' class='selectbox'>
				<option value="">Select...</option>
				<?php
				$types=new Types();
				$where="  ";
				$fields="prod_types.id, prod_types.name, prod_types.remarks, prod_types.ipaddress, prod_types.createdby, prod_types.createdon, prod_types.lasteditedby, prod_types.lasteditedon";
				$join="";
				$having="";
				$groupby="";
				$orderby="";
				$types->retrieve($fields,$join,$where,$having,$groupby,$orderby);

				while($rw=mysql_fetch_object($types->result)){
				?>
					<option value="<?php echo $rw->id; ?>" <?php if($obj->typeid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
				<?php
				}
				?>
				</select>
</td>
			</tr>
			<tr>
				<td>Variety Colour</td>
				<td>
				<select name='colourid' class='selectbox'>
				<option value="">Select...</option>
				<?php
				$colours=new Colours();
				$where="  ";
				$fields="prod_colours.id, prod_colours.name, prod_colours.remarks, prod_colours.ipaddress, prod_colours.createdby, prod_colours.createdon, prod_colours.lasteditedby, prod_colours.lasteditedon";
				$join="";
				$having="";
				$groupby="";
				$orderby="";
				$colours->retrieve($fields,$join,$where,$having,$groupby,$orderby);

				while($rw=mysql_fetch_object($colours->result)){
				?>
					<option value="<?php echo $rw->id; ?>" <?php if($obj->colourid==$rw->id){echo "selected";}?>><?php echo initialCap($rw->name);?></option>
				<?php
				}
				?>
				</select>
</td>
			</tr>
			<tr>
				<td>Employee</td>
				<td><input type='text' size='20' name='employeename' id='employeename' value='<?php echo $obj->employeename; ?>'>
					<input type="hidden" name='employeeid' id='employeeid' value='<?php echo $obj->field; ?>'></td>
			</tr>
			<tr>
			  <td>Status</td>
			  <td>
			  <select name="status" class="selectbox">
			    <option value="checkedin">checkedin</option>
			    <option value="checkedout">checkedout</option>
			    <option value="stocktake">stocktake</option>
			  </select>
			  </td>
			</tr>
			
			<tr>
			  <td>Budget Month</td>
			  <td><select name="month" id="month" class="selectbox">
				<option value="">Select...</option>
				<option value="1" <?php if($obj->month==1){echo"selected";}?>>January</option>
				<option value="2" <?php if($obj->month==2){echo"selected";}?>>February</option>
				<option value="3" <?php if($obj->month==3){echo"selected";}?>>March</option>
				<option value="4" <?php if($obj->month==4){echo"selected";}?>>April</option>
				<option value="5" <?php if($obj->month==5){echo"selected";}?>>May</option>
				<option value="6" <?php if($obj->month==6){echo"selected";}?>>June</option>
				<option value="7" <?php if($obj->month==7){echo"selected";}?>>July</option>
				<option value="8" <?php if($obj->month==8){echo"selected";}?>>August</option>
				<option value="9" <?php if($obj->month==9){echo"selected";}?>>September</option>
				<option value="10" <?php if($obj->month==10){echo"selected";}?>>October</option>
				<option value="11" <?php if($obj->month==11){echo"selected";}?>>November</option>
				<option value="12" <?php if($obj->month==12){echo"selected";}?>>December</option>
			      </select>&nbsp;
			      
			      <select name="year" id="year" class="selectbox">
				<option value="">Select...</option>
				<?php
			$i=date("Y")-10;
			while($i<date("Y")+10)
			{
			      ?>
				<option value="<?php echo $i; ?>" <?php if($obj->year==$i){echo"selected";}?>><?php echo $i; ?></option>
				<?
			  $i++;
			}
			?>
			      </select>
			      </td>
			</tr>
			
		</table>
		</td>
		<td>
		<table class="tgrid gridd" width="100%" border="0" align="left">
			<tr>
			<th colspan="2"><div align="left"><strong>Group By (For Summarised Reports)</strong>: </div></th>
			</tr>
			<tr>
				<td><input type='checkbox' name='grtypeid' value='1' <?php if(isset($_POST['grtypeid']) ){echo"checked";}?>>&nbsp;Variety Type</td>
				<td><input type='checkbox' name='grvarietyid' value='1' <?php if(isset($_POST['grvarietyid']) ){echo"checked";}?>>&nbsp;Variety</td>
			<tr>
				<td><input type='checkbox' name='grgreenhouseid' value='1' <?php if(isset($_POST['grgreenhouseid'])or empty($obj->action) ){echo"checked";}?>>&nbsp;Green House</td>
				<td><input type='checkbox' name='grharvestedon' value='1' <?php if(isset($_POST['grharvestedon']) ){echo"checked";}?>>&nbsp;Harvest Date</td>
			<tr>
				<td><input type='checkbox' name='grcolourid' value='1' <?php if(isset($_POST['grcolourid']) ){echo"checked";}?>>&nbsp;Variety Colour</td>
				<td><input type='checkbox' name='gremployeeid' value='1' <?php if(isset($_POST['gremployeeid']) ){echo"checked";}?>>&nbsp;Employee</td>
			<tr>
				<td><input type='checkbox' name='grbudgets' value='1' <?php if(isset($_POST['grbudgets']) ){echo"checked";}?>>&nbsp;Compare to Budgets</td>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<table class="tgrid gridd" width="100%" border="0" align="left">
			<tr>
				<th colspan="3"><div align="left"><strong>Fields to Show (For Detailed Reports)</strong>: </div></th>
			</tr>
			<tr>
				<td><input type='checkbox' name='shperc' value='1' <?php if(isset($_POST['shperc'])){echo"checked";}?>>&nbsp;% By Size</td>
				<td><input type='checkbox' name='sharea' value='1' <?php if(isset($_POST['sharea'])){echo"checked";}?>>&nbsp;Area</td>
			
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align='center'><input type="submit" class="btn" name="action" id="action" value="Filter" /></td>
	</tr>
</table>
</form>
</div>
</div>
<table style="clear:both;"  class="tgrid display" id="tbl" width="98%" border="0" cellspacing="0" cellpadding="2" align="center" >
	<thead>
		<tr>
			<th>#</th>
			<?php if($obj->shplantingdetailid==1){ ?>
				<th>Planting Detail </th>
			<?php } ?>
			<?php if($obj->shgreenhouseid==1){ ?>
				<th>Block </th>
				<th>Section </th>				
				<th>Green House </th>
			<?php } ?>
			<?php if($obj->shharvestedon==1 ){ ?>
				<th>Date Harvested </th>
			<?php } ?>
			<?php if($obj->shbarcode==1 ){ ?>
				<th>BarCode </th>
			<?php } ?>
			<?php if($obj->shremarks==1){ ?>
				<th>Remarks </th>
			<?php } ?>
			<?php if($obj->shtypeid==1){ ?>
				<th>Type </th>
			<?php } ?>
			<?php if($obj->shcolourid==1 ){ ?>
				<th>Colour</th>
			<?php } ?>
			<?php if($obj->shemployeeid==1 ){ ?>
				<th>Employee </th>
			<?php } ?>
			
			<?php if($obj->shcreatedon==1 ){ ?>
				<th>Date </th>
			<?php } ?>
			
			<?php if($obj->shvarietyid==1){ ?>
				<th>Variety </th>
			<?php } ?>
			<?php if($obj->shsizeid==1){ 
			      $sizes=new Sizes();
			      $where="  ";
			      $fields="*";
			      $join="";
			      $having="";
			      $groupby="";
			      $orderby="";
			      $sizes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

			      while($rw=mysql_fetch_object($sizes->result)){
			      
			?>
				<th><?php echo $rw->name; ?></th>
			<?php 
			if($obj->shperc==1){
			  ?>
			    <th>%</th>
			  <?php
			  }
			}} ?>
			<?php if($obj->shquantity==1){ ?>
				<th>Sub Total </th>
			<?php } ?>
			
			<?php if($obj->shbudgets==1){ ?>
				<th>Budget</th>
				<th>% Budget</th>
			<?php } ?>
			<?php if($obj->sharea==1){ ?>
				<th>Area</th>
				<th>Stems/m<sup>2</sup></th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
	</tbody>
	<tfoot>
	<tr>
		<th>#</th>
			<?php if($obj->shplantingdetailid==1){ ?>
				<th>&nbsp; </th>
			<?php } ?>
			<?php if($obj->shgreenhouseid==1){ ?>
				<th>&nbsp; </th>
				<th>&nbsp; </th>
				<th>&nbsp; </th>
			<?php } ?>
			<?php if($obj->shharvestedon==1 ){ ?>
				<th>&nbsp; </th>
			<?php } ?>
			<?php if($obj->shbarcode==1 ){ ?>
				<th>&nbsp; </th>
			<?php } ?>
			<?php if($obj->shremarks==1){ ?>
				<th>&nbsp; </th>
			<?php } ?>
			<?php if($obj->shtypeid==1){ ?>
				<th>&nbsp; </th>
			<?php } ?>
			<?php if($obj->shcolourid==1 ){ ?>
				<th>&nbsp; </th>
			<?php } ?>
			<?php if($obj->shemployeeid==1 ){ ?>
				<th>&nbsp; </th>
			<?php } ?>
			
			<?php if($obj->shcreatedon==1 ){ ?>
				<th>&nbsp; </th>
			<?php } ?>
			
			<?php if($obj->shvarietyid==1){ ?>
				<th>&nbsp; </th>
			<?php } ?>
			<?php if($obj->shsizeid==1){ 
			      
			      $sizes=new Sizes();
			      $where="  ";
			      $fields="*";
			      $join="";
			      $having="";
			      $groupby="";
			      $orderby="";
			      $sizes->retrieve($fields,$join,$where,$having,$groupby,$orderby);

			      while($rw=mysql_fetch_object($sizes->result)){
			    ?>
				<th>&nbsp; </th>
			<?php 
			if($obj->shperc==1){
			?>
			  <th>&nbsp;</th>
			<?php
			}
			}} ?>
			<?php if($obj->shquantity==1){ ?>
				<th>&nbsp; </th>
			<?php } ?>
			<?php if($obj->shbudgets==1){ ?>
				<th>&nbsp; </th>
				<th>&nbsp; </th>
			<?php } ?>
			<?php if($obj->sharea==1){ ?>
				<th>&nbsp; </th>
				<th>&nbsp; </th>
			<?php } ?>
	</tr>
	</tfoot>
</div>
</div>
</div>
</div>
</div>
